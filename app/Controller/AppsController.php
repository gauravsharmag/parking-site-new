<?php

App::uses('SimplePasswordHasher', 'Controller/Component/Auth');

class AppsController extends AppController {

    var $uses = array();
	var $allowedReferers=array('noc.digital6tech.com','onlineparkingpass.com','internetparkingpass.com','contracttow.com');
    /*     * ***********************************************************
     * Property-Wise Vehicle Search
     * 
     * Requirement: api_key(Compulsory),key(SEARCH KEY Compulsory),property_id(Optional)
     * 
     */
	
 public function vehicle_search() { 
        $this->autoRender = false;  
        $response = array();
        $this->response->header('Access-Control-Allow-Origin', '*');
        $this->response->type('json');
        if ($this->request->is('POST')) {
            $data = $this->request->input('json_decode', true);
            if ($this->token_check($data['api_key'])) {
                if (!isset($data['property_id'])) {
                    $data['property_id'] = "";
                }
                $searchArray = array();
                $this->loadModel('CustomerPass');
                $this->loadModel('Vehicle');
                $this->Vehicle->unbindModel(array('hasMany' => array('CustomerPass')));
                $this->Vehicle->recursive = -1;
                $condition2 = array("license_plate_number LIKE '%" . $data['key'] . "%'");
                if (!empty($data['property_id'])) {
                    $condition2['Vehicle.property_id'] = $data['property_id'];
                }
                $customerVehicles = $this->Vehicle->find('all', array(
                   'fields' => array(
									 'Vehicle.id',
									 'Vehicle.owner',
									 'Vehicle.make',
									 'Vehicle.model',
									 'Vehicle.color',
									 'Vehicle.license_plate_number',
									 'Vehicle.license_plate_state',
									 'Vehicle.last_4_digital_of_vin',
									// 'Vehicle.vehicle_archived',
									// 'CONCAT(Vehicle.owner,"-", Vehicle.make,"-",Vehicle.model,"-",Vehicle.license_plate_number,"-",Vehicle.last_4_digital_of_vin) AS vehicle_info',
									 'CustomerPass.id',
									 'CustomerPass.property_id',
									 'CustomerPass.membership_vaild_upto',
									 'CustomerPass.pass_valid_upto',
									 'CustomerPass.RFID_tag_number',
									 'CustomerPass.assigned_location',
									 'CustomerPass.is_guest_pass',
									// 'CustomerPass.pass_archived',
									 'Pass.name AS PassName',
									 'Package.name AS PackageName',
									 'User.first_name',
									 'User.last_name',
									 'User.phone'
								),
                    'joins' => array(
                        array(
                            'table' => 'customer_passes',
                            'alias' => 'CustomerPass',
                            'type' => 'LEFT',
                            'conditions' => array(
                                'CustomerPass.vehicle_id=Vehicle.id'
                            )
                        ),
						array(
							'table' => 'passes',
							'alias' => 'Pass',
							'type' => 'INNER',
							'conditions' => array(
								'Pass.id=CustomerPass.pass_id'
							)
						),
						array(
							'table' => 'packages',
							'alias' => 'Package',
							'type' => 'LEFT',
							'conditions' => array(
								'Package.id=CustomerPass.package_id'
							)
						),
						array(
							'table' => 'users',
							'alias' => 'User',
							'type' => 'INNER',
							'conditions' => array(
								'User.id=CustomerPass.user_id'
							)
						)
                    ),
                    'conditions' => $condition2
                ));
                $response['success'] = true;
                $response['data'] = $customerVehicles;
                $this->response->body(json_encode($response));	
            } else {
                $response['success'] = false;
                $response['data'] = "User Not Found";
                $this->response->body(json_encode($response));
            }
        }
    }

    /*     * ***********************************************************
     * single Vehicle Search
     * 
     * Requirement: api_key(Required),vehicle_id(Required)
     * 
     */

    public function single_vehicle_search() {
        $this->autoRender = false;
        $response = array();
        $this->response->header('Access-Control-Allow-Origin', '*');
        $this->response->type('json');
        $this->response->header('Access-Control-Allow-Origin', '*');
        $this->response->type('json');
        if ($this->request->is('POST')) {
            $data = $this->request->input('json_decode', true);
            if ($this->token_check($data['api_key'])) {
                $this->loadModel('Vehicle');
                $customerPass = $this->Vehicle->field('customer_pass_id', array('Vehicle.id' => $data['vehicle_id']));
                $fields = "Vehicle.*,User.first_name,User.last_name,User.username,User.phone,Property.name";
                $joins = array(
                    array(
                        'table' => 'users',
                        'alias' => 'User',
                        'type' => 'LEFT',
                        'conditions' => array(
                            'User.id=Vehicle.user_id'
                        )
                    ),
                    array(
                        'table' => 'properties',
                        'alias' => 'Property',
                        'type' => 'LEFT',
                        'conditions' => array(
                            'Property.id=Vehicle.property_id'
                        )
                    )
                );
                if (!is_null($customerPass)) {
                    $fields = $fields . ",CustomerPass.*";
                    $joins[] = array(
                        'table' => 'customer_passes',
                        'alias' => 'CustomerPass',
                        'type' => 'INNER',
                        'conditions' => array(
                            'CustomerPass.vehicle_id=Vehicle.id'
                        )
                    );
                }
                $this->Vehicle->unbindModel(array('hasMany' => array('CustomerPass'), 'belongsTo' => array('Property', 'User'), 'hasOne' => array('ChangeVehicleDetail')));
                $result = $this->Vehicle->find('first', array(
                    'fields' => $fields,
                    'joins' => $joins,
                    'conditions' => array('Vehicle.id' => $data['vehicle_id'])
                ));
                if (!empty($result)) {
                    if (is_null($customerPass)) {
                        $result['CustomerPass'] = null;
                    }
                    $response['success'] = true;
                    $response['message'] = "Vehicle found";
                    $response['data'] = $result;
                } else {
                    $response['success'] = false;
                    $response['message'] = "Vehicle Not found";
                    $response['data'] = null;
                }
                $this->response->body(json_encode($response));
            } else {
                $response['success'] = false;
                $response['message'] = "User Not Found";
                $this->response->body(json_encode($response));
            }
        } else {
            $response['success'] = false;
            $response['message'] = "Request is not valid";
            $this->response->body(json_encode($response));
        }
    }

    /*     * *********************************************************************
     * User Search Function
     * 
     * Requirements: api_key(Required),username('key required value can be empty'),property_id(key required value optional)
     * 
     */

    public function get_user() {
        $this->autoRender = false;
        $response = array();
        $data = $this->request->input('json_decode', true);
        if ($this->request->is('POST')) {
            if ($this->token_check($data['api_key'])) {
                $conditions = " ";
                if (!empty($data['username'])) {
                    $conditions = $conditions . "User.username LIKE '%" . $data['username'] . "%' AND User.role_id=4";
                    if (!empty($data['email'])) {
                        $conditions = $conditions . " AND User.email LIKE'%" . $data['email'] . "%'";
                        if (!empty($data['property_id'])) {
                            $conditions = $conditions . " AND PropertyUser.property_id=" . $data['property_id'] . "";
                        }
                    } else {
                        if (!empty($data['property_id'])) {
                            $conditions = $conditions . " AND PropertyUser.property_id=" . $data['property_id'] . "";
                        }
                    }
                } else {
                    if (!empty($data['email'])) {
                        $conditions = $conditions . "User.email LIKE '%" . $data['email'] . "%' AND User.role_id = 4";
                        if (!empty($data['property_id'])) {
                            $conditions = $conditions . " AND PropertyUser.property_id=" . $data['property_id'] . "";
                        }
                    } else {
                        if (!empty($data['property_id'])) {
                            $conditions = $conditions . " PropertyUser.property_id=" . $data['property_id'] . " AND PropertyUser.role_id=4";
                        }
                    }
                }
                $this->loadModel('PropertyUser');
                $this->PropertyUser->unbindModel(array('belongsTo' => array('Property', 'Role')));
                $this->PropertyUser->recursive = -1;
                $userData = $this->PropertyUser->find('all', array(
                    'fields' => 'User.username,User.email,User.first_name,User.last_name,User.address_line_1,User.address_line_2,User.city,User.state,User.zip,User.phone,User.archived',
                    'joins' => array(
                        array(
                            'table' => 'users',
                            'alias' => 'User',
                            'type' => 'LEFT',
                            'conditions' => array(
                                'User.id=PropertyUser.user_id'
                            )
                        )
                    ),
                    'conditions' => $conditions
                ));
                $array = array();
                for ($i = 0; $i < count($userData); $i++) {
                    $array[] = $userData[$i]['User'];
                }
                $response['data'] = $array;
                //$response['request']=$data;
                //$response['conditions']=$conditions;
                $response['success'] = true;
                $this->response->header('Access-Control-Allow-Origin', '*');
                $this->response->type('json');
                $this->response->body(json_encode($response));
            } else {
                $response['success'] = false;
                $this->response->header('Access-Control-Allow-Origin', '*');
                $this->response->type('json');
                $this->response->body(json_encode($response));
            }
        } else {
            $response['success'] = false;
            $this->response->header('Access-Control-Allow-Origin', '*');
            $this->response->type('json');
            $this->response->body(json_encode($response));
        }
    }

    /*     * *****************************************************************
     * Return All Properties
     */

    public function get_properties() {
        $this->autoRender = false;
        $response = array();
        $data = $this->request->input('json_decode', true);
        if ($this->request->is('POST')) {
            if ($this->token_check($data['api_key'])) {
                $this->loadModel('Property');
                $conditions=[];
                $properties = [];
                if($this->PropertyIds!='ALL'){
					$conditions=array('Property.id'=>$this->PropertyIds);
					if($this->PropertyIds){
						$properties =  $this->Property->find('list', array('fields' => array('name'),'conditions'=>$conditions));
					}
				}else{
					$properties =  $this->Property->find('list', array('fields' => array('name')));
				}
                if ($properties) {
                    $response['success'] = true;
                    $response['message'] = "All Properties list";
                    $response['data'] = $properties;
                    $this->response->header('Access-Control-Allow-Origin', '*');
                    $this->response->type('json');
                    $this->response->body(json_encode($response));
                } else {
                    $response['success'] = false;
                    $response['message'] = "Properties list not fetched";
                    $response['data'] = null;
                    $this->response->header('Access-Control-Allow-Origin', '*');
                    $this->response->type('json');
                    $this->response->body(json_encode($response));
                }
            } else {
                $response['success'] = false;
                $response['message'] = "Unauthorized User";
                $response['data'] = null;
                $this->response->header('Access-Control-Allow-Origin', '*');
                $this->response->type('json');
                $this->response->body(json_encode($response));
            }
        }
    }

    /*     * *****************************************************
     * User Token Check
     */

    public function token_check($api_key = null) {
		$data = $this->request->input('json_decode', true);
		CakeLog::write('INDATA',json_encode($data));
        $this->loadModel('User');
        $this->User->recursive = -1;
        $this->CurrentUser=$data = $this->User->find('first', array('conditions' => array('api_key' => $api_key)));
        if (!empty($data)) {
            $this->getPropertyIds();
            return true;
        } else {
            return false;
        }
    }
	private function getPropertyIds(){
		$this->PropertyIds='ALL';
		if($this->CurrentUser['User']['role_id']==2 || $this->CurrentUser['User']['role_id']==3){
			$this->loadModel('PropertyUser');
			$PropertyList=$this->PropertyUser->find('list',array('conditions'=>array('PropertyUser.user_id'=>$this->CurrentUser['User']['id']),
													'fields' => array('PropertyUser.property_id', 'PropertyUser.property_id')
			));
			if($PropertyList){
				$PropertyList=array_values($PropertyList);
			}
			$this->PropertyIds=$PropertyList;
		}
	}
	
    /*     * *****************************************************
     * User Login And Token Generate
     */

    public function api_login() {
        $this->autoRender = false;
        $response = array();
        $data = $this->request->input('json_decode', true);
        //debug($this->request->is('POST'));
        //debug($this->request);die;
        if ($this->request->is('POST')) {
            $passwordHasher = new SimplePasswordHasher();
            $password = $passwordHasher->hash($data['password']);
            $username = $data['username'];
            $this->loadModel('User');
            $this->User->recursive = -1;
            $result = $this->User->find('first', array('conditions' => array('username' => $username, 'password' => $password,'role_id'=>array(1,2,3))));
            $api_key = '';
            if (!empty($result)) {
                if (is_null($result['User']['api_key_time'])) {
                    $length = 20;
                    $api_key = substr(str_shuffle(md5(time())), 0, $length);
                    $dt = new DateTime();
                    $currentDateTime = $dt->format('Y-m-d H:i:s');
                    $array['User'] = array('id' => $result['User']['id'],
                        'api_key' => $api_key,
                        'api_key_time' => $currentDateTime
                    );
                    if ($this->User->save($array, false)) {
                        $response['success'] = true;
                        $response['api_key'] = $api_key;
                    } else {
                        $response['success'] = false;
                        $response['message'] = "Api key not generated";
                    }
                } else {
                    /* $dt = new DateTime();
                      $currentDateTime = $dt->format('Y-m-d H:i:s');
                      $diff = strtotime($currentDateTime) - strtotime($result['User']['api_key_time']);
                      $diff_in_hrs = $diff / 3600;
                      if($diff_in_hrs>72){
                      $length = 20;
                      $api_key = substr(str_shuffle(md5(time())),0,$length);
                      $array['User']=array('id'=>$result['User']['id'],
                      'api_key'=>$api_key,
                      'api_key_time'=>$currentDateTime
                      );
                      if($this->User->save($array,false)){
                      $response['success']=true;
                      $response['api_key']=$api_key;
                      }else{
                      $response['success']=false;
                      $response['api_key']="Api key not generated";
                      }
                      }else{
                      $response['success']=true;
                      $response['api_key']=$result['User']['api_key'];
                      } */
                    $response['success'] = true;
                    $response['api_key'] = $result['User']['api_key'];
                }
            } else {
                $response['success'] = false;
                $response['message'] = "Unauthorized User";
            }
            $this->response->header('Access-Control-Allow-Origin', '*');
            $this->response->type('json');
            $this->response->body(json_encode($response));
        } else {
            $response['success'] = false;
            $response['data'] = null;
            $response['message'] = "Invalid request";
            $this->response->header('Access-Control-Allow-Origin', '*');
            $this->response->type('json');
            $this->response->body(json_encode($response));
        }
    }

    /*     * *******************************************
     * Property-Wise Vehicle Details
     * Requirements: api_key(Required),property_id(required)
     */

    public function property_vehicle_details() {
        $this->autoRender = false;
        $response = array();
        $this->response->header('Access-Control-Allow-Origin', '*');
        $this->response->type('json');
        if ($this->request->is('POST')) {
            $data = $this->request->input('json_decode', true);
            $dt = new DateTime();
			$currentDateTime = $dt->format('Y-m-d H:i:s');
            if ($this->token_check($data['api_key'])) {
                $customerVehicles=$searchArray = array();
                if($data['property_id']!=7){
					$this->loadModel('Vehicle');
					$this->Vehicle->unbindModel(array('hasMany' => array('CustomerPass')));
					$this->Vehicle->recursive = -1;
					$customerVehicles = $this->Vehicle->find('all', array(
						'fields' => 'Vehicle.*,CustomerPass.*,Pass.name AS PassName,Package.name AS PackageName,Package.is_guest,User.first_name,User.last_name,User.phone',
						'joins' => array(
							array(
								'table' => 'customer_passes',
								'alias' => 'CustomerPass',
								'type' => 'LEFT',
								'conditions' => array(
									'CustomerPass.vehicle_id=Vehicle.id'
								)
							),
							array(
								'table' => 'passes',
								'alias' => 'Pass',
								'type' => 'LEFT',
								'conditions' => array(
									'Pass.id=CustomerPass.pass_id'
								)
							),
							array(
								'table' => 'packages',
								'alias' => 'Package',
								'type' => 'LEFT',
								'conditions' => array(
									'Package.id=CustomerPass.package_id'
								)
							),
							array(
								'table' => 'users',
								'alias' => 'User',
								'type' => 'LEFT',
								'conditions' => array(
									'User.id=Vehicle.user_id'
								)
							)
						),
						'conditions' => array('Vehicle.property_id' => $data['property_id'])
							)
					);
					
					// debug($customerVehicles);die;
					
				}else{
					$this->loadModel('CustomerPass');
					$this->CustomerPass->recursive = -1;
					$customerVehicles = $this->CustomerPass->find('all', array(
						'fields' => 'Vehicle.*,CustomerPass.*,Pass.name AS PassName,Package.name AS PackageName,Package.is_guest,User.first_name,User.last_name,User.phone',
						'joins' => array(
							array(
								'table' => 'vehicles',
								'alias' => 'Vehicle',
								'type' => 'LEFT',
								'conditions' => array(
									'CustomerPass.vehicle_id=Vehicle.id'
								)
							),
							array(
								'table' => 'passes',
								'alias' => 'Pass',
								'type' => 'LEFT',
								'conditions' => array(
									'Pass.id=CustomerPass.pass_id'
								)
							),
							array(
								'table' => 'packages',
								'alias' => 'Package',
								'type' => 'LEFT',
								'conditions' => array(
									'Package.id=CustomerPass.package_id'
								)
							),
							array(
								'table' => 'users',
								'alias' => 'User',
								'type' => 'LEFT',
								'conditions' => array(
									'User.id=CustomerPass.user_id'
								)
							)
						),
						'conditions' => array('CustomerPass.property_id' => $data['property_id'])
							)
					);
				}
				foreach ($customerVehicles as $vehicle) {
						$status = "unknown";
						$status_msg = "unknown status";
						$is_guest = "No";
						if($data['property_id']!=7){
							if (is_null($vehicle['Vehicle']['customer_pass_id'])) {
								$status = "NoPass";
								$status_msg = "Pass Not Assigned";
							} else {
								if ($vehicle['CustomerPass']['pass_valid_upto'] < $currentDateTime) {
									$status = "PassExpired";
									$status_msg = "Pass Expired";
									if (is_null($vehicle['CustomerPass']['package_id'])) {
										$status_msg = $status_msg . ", Package Was Not Bought";
									}
									if ($vehicle['Package']['is_guest'] == 1) {
										$is_guest = "Yes";
									}
								} else {
									if ($vehicle['Package']['is_guest'] == 1) {
										$is_guest = "Yes";
									}
									if (is_null($vehicle['CustomerPass']['package_id'])) {
										$status = "PassValid";
										$status_msg = "Pass Is Valid But Package Not Bought";
									} else {
										if ($vehicle['CustomerPass']['membership_vaild_upto'] < $currentDateTime) {
											$status = "PassExpired";
											$status_msg = "Pass Is Valid But Package Expired";
										} else {
											$status = "PassActive";
											$status_msg = "Pass Active";
										}
									}
								}
							}
						}else{
							if ($vehicle['CustomerPass']['pass_valid_upto'] < $currentDateTime) {
									$status = "PassExpired";
									$status_msg = "Pass Expired";
									if (is_null($vehicle['CustomerPass']['package_id'])) {
										$status_msg = $status_msg . ", Package Was Not Bought";
									}
									if ($vehicle['Package']['is_guest'] == 1) {
										$is_guest = "Yes";
									}
								} else {
									if ($vehicle['Package']['is_guest'] == 1) {
										$is_guest = "Yes";
									}
									if (is_null($vehicle['CustomerPass']['package_id'])) {
										$status = "PassValid";
										$status_msg = "Pass Is Valid But Package Not Bought";
									} else {
										if ($vehicle['CustomerPass']['membership_vaild_upto'] < $currentDateTime) {
											$status = "PassExpired";
											$status_msg = "Pass Is Valid But Package Expired";
										} else {
											$status = "PassActive";
											$status_msg = "Pass Active";
										}
									}
								}
						}
						$searchArray[] = array('Vehicle' => array('id' => $vehicle['Vehicle']['id'],
								'make' => $vehicle['Vehicle']['make'],
								'model' => $vehicle['Vehicle']['model'],
								'color' => $vehicle['Vehicle']['color'],
								'license_plate_number' => $vehicle['Vehicle']['license_plate_number'],
								'license_plate_state' => $vehicle['Vehicle']['license_plate_state'],
								'last_4_digital_of_vin' => $vehicle['Vehicle']['last_4_digital_of_vin'],
								'RFID_tag_number' => $vehicle['CustomerPass']['RFID_tag_number'],
								'PassName' => $vehicle['Pass']['PassName'],
								'pass_valid_upto' => $vehicle['CustomerPass']['pass_valid_upto'],
								'PackageName' => $vehicle['Package']['PackageName'],
								'membership_vaild_upto' => $vehicle['CustomerPass']['membership_vaild_upto'],
								'GuestVehicle' => $is_guest,
								'user_name' => $vehicle['User']['first_name'] . ' ' . $vehicle['User']['last_name'],
								'contact_number' => $vehicle['User']['phone'],
								'status' => $status,
								'status_msg' => $status_msg,
								'assigned_location'=>$vehicle['CustomerPass']['assigned_location']
							)
						);
					}
                //	debug($searchArray);die;
                $response['success'] = true;
                $response['data'] = $searchArray;
                $this->response->body(json_encode($response));
            } else {
                $response['success'] = false;
                $response['data'] = "User Not Found";
                $this->response->body(json_encode($response));
            }
        } else {
            $response['success'] = false;
            $response['data'] = "Wrong Request";
            $this->response->body(json_encode($response));
        }
    }

      /*     * *****************************************************
     * Vehicle Last Seen  On 22nd Sep
     */

    public function vehicle_last_seen() {
		$arr=array('api_key'=>'72bc5e571454f0fe81f6',
					'CustomerPass'=>array(array( 'rfid'=>43649,
												  'customer_pass_id'=>9522,
												  'property_id'=>14,
												  'vehicle_details'=>'Gabriel-Acura -4s-1AE0807-2780',
												  'last_seen'=>'2016-02-18 13:27:12',
												  'assigned_location'=>26
											),
											array( 'rfid'=>43628,
												  'customer_pass_id'=>9418,
												  'property_id'=>14,
												  'vehicle_details'=>'',
												  'last_seen'=>'2016-02-18 13:27:12',
												  'assigned_location'=>''
											)
										)
				);
		//debug(json_encode($arr));die;
        $this->autoRender = false;
        $response = array();
        $this->response->header('Access-Control-Allow-Origin', '*');
        $this->response->type('json');
        if ($this->request->is('POST')) {
            $data = $this->request->input('json_decode', true);
            if ($this->token_check($data['api_key'])) {
				CakeLog::write('inputLatSeen',json_encode($data));
				if(!empty($data['CustomerPass'])){
					$this->loadModel('LastSeenVehicle');
					if($this->LastSeenVehicle->saveMany($data['CustomerPass'],array('validate'=>false))){
						$response['success'] = true;
						$response['message'] = "Data saved successfully";
					}else{
						$response['success'] = false;
						$response['message'] = "Data could not be saved";
					}
				}else{
					$response['success'] = false;
					$response['message'] = "No Data Found";
				}
            } else {
                $response['success'] = false;
                $response['message'] = "Incorrect API key";
            }
          
        } else {
            $response['success'] = false;
            $response['message'] = "Wrong Request";
        }
        $this->response->body(json_encode($response));
    }
	public function properties() {
		  $this->autoRender = false;
		if(isset($_SERVER['HTTP_ORIGIN'])){
				$origin=parse_url($_SERVER['HTTP_ORIGIN']); 
				if(in_array($origin['host'],$this->allowedReferers)){	
						$this->autoRender = false;
						$response = array();
						$data = $this->request->input('json_decode', true);
						if ($this->request->is('POST')) {
								$this->loadModel('Property');
								$properties = $this->Property->find('list', array('fields' => array('name')));
								if($origin['host']==Configure::read('MY_HOST')){
									$properties = $this->Property->find('list', array('fields' => array('sub_domain','name')));
									$newproperties=array();
									foreach($properties as $key=>$value){
										$newproperties['//'.$key.'.'.$_SERVER['SERVER_NAME']]=$value;
									}
									$properties=$newproperties;
								}
								if ($properties) {
									$response['success'] = true;
									$response['message'] = "All Properties list";
									$response['data'] = $properties;
									$this->response->header('Access-Control-Allow-Origin', '*');
									$this->response->type('json');
									$this->response->body(json_encode($response));
								} else {
									$response['success'] = false;
									$response['message'] = "Properties list not fetched";
									$response['data'] = null;
									$this->response->header('Access-Control-Allow-Origin', '*');
									$this->response->type('json');
									$this->response->body(json_encode($response));
								}
						} else {
							$response['success'] = false;
							$response['data'] = null;
							$response['message'] = "Invalid request";
							$this->response->header('Access-Control-Allow-Origin', '*');
								$this->response->type('json');
								$this->response->body(json_encode($response));
						} 
			}else{
					$response='Invalid Request';
					$this->response->header('Access-Control-Allow-Origin', '*');
					$this->response->type('json');
					$this->response->body(json_encode($response));
				}
			}else{
				$response='Request not valid';
				$this->response->header('Access-Control-Allow-Origin', '*');
				$this->response->type('json');
				$this->response->body(json_encode($response));
			}
    }
     public function cost_listing($fromDate1,$toDate1){
			if(isset($_SERVER['HTTP_ORIGIN'])){
				$origin=parse_url($_SERVER['HTTP_ORIGIN']); 
				if(in_array($origin['host'],$this->allowedReferers)){
					$this->autoRender=false;
					$fromDate=date("Y-m-d", strtotime($fromDate1));
					$toDate=date("Y-m-d", strtotime($toDate1));
					
					$conditions='';
					$conditionsCount='';
					if($fromDate==$toDate  ){
						$toDate=$fromDate.' 23:59:59';
					}else{
						$toDate=$toDate.' 23:59:59';
					}
					
					$conditions= " Transaction.date_time BETWEEN '".$fromDate." 00:00:00' AND '".$toDate."' ";
					//debug($conditions);die;
					$this->loadModel('Property');
					$this->loadModel('CustomerPass');
					$this->loadModel('Transaction');
					$this->Property->recursive=-1;
					$allProperty=$this->Property->find('all',array('fields'=>array('id','name'),'order'=>array('id'=>'ASC')));
					$this->Transaction->recursive=-1;
					$cost=$this->Transaction->find('all',array(
																'fields'=>array('pu.property_id','p.name','SUM(Transaction.amount) as cost'),
																'joins'=>array(
																				array(
																						'table'=>'property_users',
																						'alias'=>'pu',
																						'type'=>'LEFT',
																						'conditions'=>array(
																							'pu.user_id=Transaction.user_id'
																						)
																				),
																				array(
																					'table'=>'properties',
																					'alias'=>'p',
																					'type'=>'LEFT',
																					'conditions'=>array(
																							'p.id=pu.property_id'
																					)
																				)
																		
																),
																'group'=>array('pu.property_id'),
																'conditions'=>$conditions,
																'order'=>array('pu.property_id'=>'ASC')
					
					
					));
					$this->CustomerPass->recursive=-1;
					$passCount=$this->CustomerPass->find('all',array('fields'=>array('property_id','count(*) as passcount'),
																		'conditions'=>array('RFID_tag_number IS NOT NULL and created BETWEEN "'.$fromDate.' 00:00:00" AND "'.$toDate.'"'),
																		'group'=>array('CustomerPass.property_id'),
																		'order'=>array('CustomerPass.property_id'=>'ASC')
																		)
														);
					
					$totalPasses=$totalAmount=0;
								for($i=0;$i<count($allProperty);$i++){
									$allProperty[$i]['Property']['cost']=0;
									$allProperty[$i]['Property']['passCount']=0;
									for($j=0;$j<count($cost);$j++){
										if($cost[$j]['pu']['property_id']==$allProperty[$i]['Property']['id']){
											$allProperty[$i]['Property']['cost']=$cost[$j][0]['cost'];
											$totalAmount=$totalAmount+$cost[$j][0]['cost'];
										}
									}
									for($k=0;$k<count($passCount);$k++){
										if($passCount[$k]['CustomerPass']['property_id']==$allProperty[$i]['Property']['id']){
											$allProperty[$i]['Property']['passCount']=$passCount[$k][0]['passcount'];
											$totalPasses=$totalPasses+$passCount[$k][0]['passcount'];
										}
									}
								} 					
					$allProperty[]['TotalAmount']=$totalAmount; 
					$allProperty[]['TotalPasses']=$totalPasses;
					$response['data']=$allProperty;
					$response['site']=strtoupper($_SERVER['SERVER_NAME']);
					$this->response->header('Access-Control-Allow-Origin', '*');
					$this->response->type('json');
					$this->response->body(json_encode($response));
				}else{
					$response='Invalid Request';
					$this->response->header('Access-Control-Allow-Origin', '*');
					$this->response->type('json');
					$this->response->body(json_encode($response));
				}
			}else{
				$response='Request not valid';
				$this->response->header('Access-Control-Allow-Origin', '*');
				$this->response->type('json');
				$this->response->body(json_encode($response));
			}
			//echo json_encode($allProperty);
  }
   public function passes_list($property_id)
    {
	       if(isset($_SERVER['HTTP_ORIGIN'])){
				$origin=parse_url($_SERVER['HTTP_ORIGIN']); 
				if(in_array($origin['host'],$this->allowedReferers)){	
					$this->autoRender=false;
					$this->response->header('Access-Control-Allow-Origin', '*');
					$response=array();
					$this->response->type('json');
					$this->loadModel('Pass');
					$this->Pass->recursive=-1;
					$response=$this->Pass->find('list',array('fields'=>'name','conditions'=>array('property_id'=>$property_id)));		
					$this->response->body(json_encode($response));
				}else{
					$response='Invalid Request';
					$this->response->header('Access-Control-Allow-Origin', '*');
					$this->response->type('json');
					$this->response->body(json_encode($response));
				}
			}else{
				$response='Request not valid';
				$this->response->header('Access-Control-Allow-Origin', '*');
				$this->response->type('json');
				$this->response->body(json_encode($response));
			}
    }
    public function passes_detail_interval($fromDate1,$toDate1,$passId,$propertyId){
		if(isset($_SERVER['HTTP_ORIGIN'])){
				$origin=parse_url($_SERVER['HTTP_ORIGIN']); 
				if(in_array($origin['host'],$this->allowedReferers)){		
						$this->autoRender=false;
						$fromDate=date("Y-m-d", strtotime($fromDate1));
						$toDate=date("Y-m-d", strtotime($toDate1));
						
						$conditions='';
						$conditionsCount='';
						if($fromDate==$toDate  ){
							$toDate=$fromDate.' 23:59:59';
						}else{
							$toDate=$toDate.' 23:59:59';
						}
						$pass_id=$passId;
						$property_id=$propertyId;
						
						
						$conditions="cp.created BETWEEN '".$fromDate."' AND '".$toDate."' AND cp.property_id=".$property_id." ";
						if($pass_id){
							$conditions.= "AND cp.pass_id= ".$pass_id;
						}
						$conditionsCount=array('created BETWEEN ? AND ?' => array($fromDate,$toDate),'property_id'=>$property_id);
						if($pass_id){
							$conditionsCount['pass_id']=$pass_id;
						}
						//debug($conditionsCount);die;  
						$this->loadModel('CustomerPass');
						$this->loadModel('Pass');
						$response=array();
						$this->CustomerPass->recursive=-1;
						$totalPass=$this->CustomerPass->find('count',array('conditions'=>$conditionsCount));
						$response['totalPass']=$totalPass;
						$response['condition']=$conditions;
						$this->loadModel('Pass');
						$passName=$this->Pass->givePassName($pass_id);
						$response['passName']=$passName;
						$this->response->header('Access-Control-Allow-Origin', '*');
						$this->response->type('json');
						$this->response->body(json_encode($response));
			}else{
					$response='Invalid Request';
					$this->response->header('Access-Control-Allow-Origin', '*');
					$this->response->type('json');
					$this->response->body(json_encode($response));
				}
			}else{
				$response='Request not valid';
				$this->response->header('Access-Control-Allow-Origin', '*');
				$this->response->type('json');
				$this->response->body(json_encode($response));
			}
			//$this->response->send();
		}
		public function passes_interval()
		{
			if(isset($_SERVER['HTTP_ORIGIN'])){
				$origin=parse_url($_SERVER['HTTP_ORIGIN']); 
				if(in_array($origin['host'],$this->allowedReferers)){				
							$this->autoRender=$this->layout=false;
							$this->loadModel('CustomerPass');
							$part1=array_keys($this->request->params['named']);
							$part2=array_values($this->request->params['named']);
							$condition=$part1[0].":".$part2[0];
							$output=array();
							$output = $this->CustomerPass->get_data_passwise_interval(json_decode($condition));
							$this->response->type('json');
							$this->response->body(json_encode($output));
				}else{
					$response='Invalid Request';
					$this->response->header('Access-Control-Allow-Origin', '*');
					$this->response->type('json');
					$this->response->body(json_encode($response));
				}
			}else{
				$response='Request not valid';
				$this->response->header('Access-Control-Allow-Origin', '*');
				$this->response->type('json');
				$this->response->body(json_encode($response));
			}
			//$this->response->send();
		}
	public function all_rfids(){
		$this->autoRender = false;
        $response = array();
        $this->response->header('Access-Control-Allow-Origin', '*');
        $this->response->type('json');
        if ($this->request->is('POST')) {
            $data = $this->request->input('json_decode', true);
            if ($this->token_check($data['api_key'])) {
				$searchArray = array();
                $this->loadModel('CustomerPass');
                $this->CustomerPass->unbindModel(array('belongsTo'=>array('Property','Transaction')));
                $allCustomerPass=$this->CustomerPass->find('all',array('conditions'=>array('CustomerPass.property_id'=>$data['property_id'],'CustomerPass.RFID_tag_number IS NOT NULL'),
																		));
			//	debug($allCustomerPass);die;
				 $dt = new DateTime();
				 $currentDateTime = $dt->format('Y-m-d H:i:s');
				 foreach ($allCustomerPass as $vehicle) {
					 //debug($vehicle);die;
                    $status = "unknown";
                    $status_msg = "unknown status";
                    $is_guest = "No";
                  
                        if ($vehicle['CustomerPass']['pass_valid_upto'] < $currentDateTime) {
                            $status = "PassExpired";
                            $status_msg = "Pass Expired";
                            if (is_null($vehicle['CustomerPass']['package_id'])) {
                                $status_msg = $status_msg . ", Package Was Not Bought";
                            }
                            if ($vehicle['Package']['is_guest'] == 1) {
                                $is_guest = "Yes";
                            }
                        } else {
                            if ($vehicle['Package']['is_guest'] == 1) {
                                $is_guest = "Yes";
                            }
                            if (is_null($vehicle['CustomerPass']['package_id'])) {
                                $status = "PassValid";
                                $status_msg = "Pass Is Valid But Package Not Bought";
                            } else {
                                if ($vehicle['CustomerPass']['membership_vaild_upto'] < $currentDateTime) {
                                    $status = "PassExpired";
                                    $status_msg = "Pass Is Valid But Package Expired";
                                } else {
                                    $status = "PassActive";
                                    $status_msg = "Pass Active";
                                }
                            }
                        }
                    
                    $searchArray[] = array('RFID' => array('id' => $vehicle['Vehicle']['id'],
                            'make' => $vehicle['Vehicle']['make'],
                            'model' => $vehicle['Vehicle']['model'],
                            'color' => $vehicle['Vehicle']['color'],
                            'license_plate_number' => $vehicle['Vehicle']['license_plate_number'],
                            'license_plate_state' => $vehicle['Vehicle']['license_plate_state'],
                            'last_4_digital_of_vin' => $vehicle['Vehicle']['last_4_digital_of_vin'],
                            'RFID_tag_number' => $vehicle['CustomerPass']['RFID_tag_number'],
                            'PassName' => $vehicle['Pass']['name'],
                            'pass_valid_upto' => $vehicle['CustomerPass']['pass_valid_upto'],
                            'PackageName' => $vehicle['Package']['name'],
                            'membership_vaild_upto' => $vehicle['CustomerPass']['membership_vaild_upto'],
                            'GuestVehicle' => $is_guest,
                            'user_name' => $vehicle['User']['first_name'] . ' ' . $vehicle['User']['last_name'],
                            'contact_number' => $vehicle['User']['phone'],
                            'status' => $status,
                            'status_msg' => $status_msg,
                            'assigned_location'=>$vehicle['CustomerPass']['assigned_location']
                        )
                    );
                }
                $response['success'] = true;
                $response['message'] = "Data Found";
                $response['data'] = $searchArray;
                $this->response->body(json_encode($response));
               
			} else {
                $response['success'] = false;
                $response['message'] = "User Not Found";
                $response['data'] = NULL;
                $this->response->body(json_encode($response));
            }
        } else {
            $response['success'] = false;
            $response['message'] = "Wrong Request";
            $response['data'] = NULL;
            $this->response->body(json_encode($response));
        }
	}
		public function get_property_vehicles(){
		$this->autoRender = false;
        $response = array();
        $this->response->header('Access-Control-Allow-Origin', '*');
        $this->response->type('json');
        if ($this->request->is('POST')) {
            $data = $this->request->input('json_decode', true);   
           // debug($data);die;
            $dt = new DateTime();
			$currentDateTime = $dt->format('Y-m-d H:i:s');
            if ($this->token_check($data['api_key'])) {
                $customerVehicles=$searchArray = array();
              
					$this->loadModel('CustomerPass');
					$this->CustomerPass->recursive = -1;
					$customerVehicles = $this->CustomerPass->find('all', array(
						'fields' => array(
									 'Vehicle.id',
									 'Vehicle.owner',
									 'Vehicle.make',
									 'Vehicle.model',
									 'Vehicle.color',
									 'Vehicle.license_plate_number',
									 'Vehicle.license_plate_state',
									 'Vehicle.last_4_digital_of_vin',
									// 'Vehicle.vehicle_archived',
									// 'CONCAT(Vehicle.owner,"-", Vehicle.make,"-",Vehicle.model,"-",Vehicle.license_plate_number,"-",Vehicle.last_4_digital_of_vin) AS vehicle_info',
									 'CustomerPass.id',
									 'CustomerPass.property_id',
									 'CustomerPass.membership_vaild_upto',
									 'CustomerPass.pass_valid_upto',
									 'CustomerPass.RFID_tag_number',
									 'CustomerPass.assigned_location',
									 'CustomerPass.is_guest_pass',
									 //'CustomerPass.pass_archived',
									 'Pass.name AS PassName',
									 'Package.name AS PackageName',
									 'User.first_name',
									 'User.last_name',
									 'User.phone'
								),
						'joins' => array(
							array(
								'table' => 'vehicles',
								'alias' => 'Vehicle',
								'type' => 'LEFT',
								'conditions' => array(
									'CustomerPass.vehicle_id=Vehicle.id'
								)
							),
							array(
								'table' => 'passes',
								'alias' => 'Pass',
								'type' => 'INNER',
								'conditions' => array(
									'Pass.id=CustomerPass.pass_id'
								)
							),
							array(
								'table' => 'packages',
								'alias' => 'Package',
								'type' => 'LEFT',
								'conditions' => array(
									'Package.id=CustomerPass.package_id'
								)
							),
							array(
								'table' => 'users',
								'alias' => 'User',
								'type' => 'INNER',
								'conditions' => array(
									'User.id=CustomerPass.user_id'
								)
							)
						),
						'conditions' => array('CustomerPass.property_id' => $data['property_id'])
							)
					);
					
                $response['success'] = true;
                $response['data'] = $customerVehicles;
                //CakeLog::write('outData',json_encode($response));
                $this->response->body(json_encode($response));
            } else {
                $response['success'] = false;
                $response['data'] = "User Not Found";
                $this->response->body(json_encode($response));
            }
        } else {
            $response['success'] = false;
            $response['data'] = "Wrong Request";
            $this->response->body(json_encode($response));
        }
	}
	public function properties_api() {
		  $this->autoRender = false;
		if(isset($_SERVER['HTTP_ORIGIN'])){
				$origin=parse_url($_SERVER['HTTP_ORIGIN']); 
				if(in_array($origin['host'],$this->allowedReferers)){	 
						$this->autoRender = false;
						$response = array();
						$data = $this->request->input('json_decode', true);
						if ($this->request->is('POST')) {
								$this->loadModel('Property');
								$properties = $this->Property->find('list', array('fields' => array('name')));
								if($origin['host']=='contracttow.com'){
									$properties = $this->Property->find('list', array('fields' => array('sub_domain','name')));
									$newproperties=array();
									foreach($properties as $key=>$value){
										$newproperties['//'.$key.'.'.$_SERVER['SERVER_NAME']]=$value;
									}
									$properties=$newproperties;
								}
								if ($properties) {
									$response['success'] = true;
									$response['message'] = "All Properties list";
									$response['data'] = $properties;
									$this->response->header('Access-Control-Allow-Origin', '*');
									$this->response->type('json');
									$this->response->body(json_encode($response));
								} else {
									$response['success'] = false;
									$response['message'] = "Properties list not fetched";
									$response['data'] = null;
									$this->response->header('Access-Control-Allow-Origin', '*');
									$this->response->type('json');
									$this->response->body(json_encode($response));
								}
						} else {
							$response['success'] = false;
							$response['data'] = null;
							$response['message'] = "Invalid request";
							$this->response->header('Access-Control-Allow-Origin', '*');
								$this->response->type('json');
								$this->response->body(json_encode($response));
						} 
			}else{
					$response='Invalid Request';
					$this->response->header('Access-Control-Allow-Origin', '*');
					$this->response->type('json');
					$this->response->body(json_encode($response));
				}
			}else{
				$response='Request not valid';
				$this->response->header('Access-Control-Allow-Origin', '*');
				$this->response->type('json');
				$this->response->body(json_encode($response));
			}
    }
}

?>

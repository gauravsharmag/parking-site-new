<?php
App::uses('AppController', 'Controller');
/**
 * CouponCodes Controller
 *
 * @property CouponCode $CouponCode
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class CouponCodesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->CouponCode->recursive = 0;
		$this->set('couponCodes', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->CouponCode->exists($id)) {
			throw new NotFoundException(__('Invalid coupon code'));
		}
		$options = array('conditions' => array('CouponCode.' . $this->CouponCode->primaryKey => $id));
		$this->set('couponCode', $this->CouponCode->find('first', $options));
	}

/**
 * admin_add method

 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->request->data['CouponCode']['code']=trim($this->request->data['CouponCode']['code']);
			if(empty($this->request->data['CouponCode']['code'])){
				$this->Session->setFlash(__('Coupon Code Is Either Empty Or Contain Spaces'),'error');
				return $this->redirect($this->referer());
			}else{
				$coupons=explode(',',$this->request->data['CouponCode']['code']);
				for($j=0;$j<count($coupons);$j++){
					$coupons[$j]=trim($coupons[$j]);
					if($coupons[$j]){
						$arr['CouponCode']=array(
												 'code'=>$coupons[$j],
												 'coupon_package_id'=>$this->request->data['CouponCode']['coupon_package_id']
									);
						$this->CouponCode->create();
						$this->CouponCode->save($arr);
					}
				}
				$this->Session->setFlash(__('The coupon code has been saved.'),'success');
				return $this->redirect($this->referer());
			}
		}
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->CouponCode->exists($id)) {
			throw new NotFoundException(__('Invalid coupon code'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->CouponCode->save($this->request->data)) {
				$this->Session->setFlash(__('The coupon code has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The coupon code could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('CouponCode.' . $this->CouponCode->primaryKey => $id));
			$this->request->data = $this->CouponCode->find('first', $options);
		}
		$couponPackages = $this->CouponCode->CouponPackage->find('list');
		$this->set(compact('couponPackages'));
	}

/**
 * admin_delete method
 */
	public function admin_delete($id = null) {
		$this->CouponCode->id = $id;
		if (!$this->CouponCode->exists()) {
			throw new NotFoundException(__('Invalid coupon code'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->CouponCode->delete()) {
			$this->Session->setFlash(__('The coupon code has been deleted.'),'success');
		} else {
			$this->Session->setFlash(__('The coupon code could not be deleted. Please, try again.'),'error');
		}
		return $this->redirect($this->referer());
	}
}

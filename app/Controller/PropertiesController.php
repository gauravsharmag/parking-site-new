<?php
App::uses('AppController', 'Controller');
App::uses('SimplePasswordHasher','Controller/Component/Auth');

/**
 * Properties Controller
 *
 * @property Property $Property
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class PropertiesController extends AppController {


    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator', 'Session','Auth','RequestHandler','Acl','CloudFlareApi.CloudFlareApi','Email','NewDatatable');

    public $helpers = array('Js');
	 public function beforeFilter()
    { 
        parent::beforeFilter();
         $this->Security->validatePost = false;
        //$this->Auth->allow();
    }


    /**
     * index method
     *
     * @return void
     */

    public function index() {
        $this->Property->recursive = 0;
        $this->set('properties', $this->Paginator->paginate());
    }
    public function admin_index() {
        $this->layout='default';
        $this->Property->recursive = 0;
        $this->set('properties', $this->Paginator->paginate());
    }
    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null) {
        $userRoleDetails=array();
        $i=0;
        if (!$this->Property->exists($id)) {
            throw new NotFoundException(__('Invalid property'));
        }
        $property = $this->Property->find('first', array('conditions' => array('Property.' . $this->Property->primaryKey => $id)));
        $this->set(compact('property'));
        // die();

    }
    public function admin_view($id = null) {
       $this->layout='default';
        $userRoleDetails=array();
        $i=0;
        if (!$this->Property->exists($id)) {
            throw new NotFoundException(__('Invalid property'));
        }
        $this->Property->recursive=-1;
        $property = $this->Property->find('first', array('conditions' => array('Property.' . $this->Property->primaryKey => $id)));
        $this->loadModel('Role');
        $role=new Role();
        $roleArr=$role->ROLES;
        $this->loadModel('User');
        $this->User->recursive=-1;
        $propertyAdmin=$this->User->find('all',array('conditions'=>array('User.id IN (SELECT user_id FROM property_users WHERE property_id ='.$id.') AND role_id=2')));
        $this->User->recursive=-1;
        $propertyManager=$this->User->find('all',array('conditions'=>array('User.id IN (SELECT user_id FROM property_users WHERE property_id ='.$id.') AND role_id=3')));
		$managerNotAssign=$this->User->find('list',array('fields'=>array('id','property_manager'),'conditions'=>array('User.id NOT IN (SELECT user_id from property_users where role_id=3) AND role_id=3')));
        $allProperties=$this->Property->find('list');
        $this->Property->Package->recursive=-1;
        $packages=$this->paginate($this->Property->Package,array('property_id'=>$id));
        $this->Property->Pass->recursive=-1;
        $passes=$this->paginate($this->Property->Pass,array('property_id'=>$id));
        $this->set(compact(array('property','roleArr','propertyAdmin','propertyManager','packages','passes','managerNotAssign','allProperties')));

    }

      /**
     * admin add method
     */

    public function admin_add()
    {
        $this->loadModel('Role');
        if ($this->request->is('post'))
        {
            $this->Property->set($this->request->data);
                if($this->Property->validates())
                {
                    $this->Property->create();
                    $returnPath=$this->Property->getPathNameImage();
                    $this->request->data['Property']['logo']= $returnPath;
					 
                    if ($this->Property->save($this->request->data,false))
                    {
						   $cloudError='';
						   try{
							$httpHost=explode('.',$_SERVER['HTTP_HOST']);
							$httpHoststr='';
							if(count($httpHost)==3){
								$httpHoststr=$httpHost[1].'.'.$httpHost[2];
							}else{
								$httpHoststr=$_SERVER['HTTP_HOST'];
							}
							$domainData=$this->CloudFlareApi->rec_load_all($httpHoststr);
							$domainData = json_decode(json_encode($domainData), true);
							if($domainData){
								if($domainData['result']!='error'){
									if(isset($domainData['response']['recs']['objs'])){
										$content='';
										$arr=$domainData['response']['recs']['objs'];
										for($id=0;$id<count($arr);$id++){
											if($arr[$id]['zone_name']==$arr[$id]['name']){
												if($arr[$id]['type']=='A'){
														$content=$arr[$id]['content'];
												}
											}
										}
										if($content){
											$result=$this->CloudFlareApi->rec_new($httpHoststr,'A',$this->request->data['Property']['sub_domain'],$content);
											$result = json_decode(json_encode($result), true);
											if($result){
												if($result['result']!='error'){
													CakeLog::write('newPropertyCloudFareSuccess','CloudFare Success : Propery : '.$this->request->data['Property']['name'].' with SubDomian: '.$this->request->data['Property']['sub_domain'].' Added To Cloudfare, CONTENT: '.$content);
												}else{
													$cloudError='error';
													CakeLog::write('newPropertyCloudFareError','CloudFare Error : "'.$result['msg'].'" generated while adding Propery : '.$this->request->data['Property']['name'].' To Cloudfare');
												}
											}else{
												$cloudError='error';
												CakeLog::write('newPropertyCloudFareError','CloudFare Error : Error while adding Propery : '.$this->request->data['Property']['name'].' with SubDomian: '.$this->request->data['Property']['sub_domain'].' To Cloudfare');
											}
										}else{
											$cloudError='error';
											CakeLog::write('newPropertyCloudFareError','CloudFare Error : No Content Found while adding Propery : '.$this->request->data['Property']['name']);
										}
									}else{
										$cloudError='error';
										 CakeLog::write('newPropertyCloudFareError','CloudFare Error : No Response Found while adding Propery : '.$this->request->data['Property']['name']);
									}
								}else{
									$cloudError='error';
									 CakeLog::write('newPropertyCloudFareError','CloudFare Error : "'.$domainData['msg'].'" generated while adding Propery : '.$this->request->data['Property']['name']);
								}
							}
							$array = json_decode(json_encode($domainData), true);
						}catch(Exception $e){
							$cloudError='error';
							CakeLog::write('newPropertyCloudFareError','CloudFare Error : Error : "'.$e->getMessage().'" while adding Propery : '.$this->request->data['Property']['name'].' with SubDomian: '.$this->request->data['Property']['sub_domain']. 'To Cloudfare');
						}
						if($cloudError){
							$this->sendCloudFareMail('add',$this->request->data);
						}
                        $lastInsertPropertyID = $this->Property->getLastInsertId();
						CakeLog::write('newPropertyAdded', ''.AuthComponent::user('username').' : New property added with Property ID:  <a href="/admin/properties/view/'.$lastInsertPropertyID.'">'.$lastInsertPropertyID.' </a> by User: '.AuthComponent::user('first_name').' '.AuthComponent::user('last_name').'');
                        $this->loadModel('Role');
                        $role = new Role();
                        $role->unbindModel(array('hasMany'=>'User'));
                        $role=$role->ROLES;
                        $propertyAdmin=$this->request->data['Property']['property_admin'];

                        $passwordHasher=new SimplePasswordHasher();
                       // $this->request->data['Property']['manager_password']=$passwordHasher->hash($this->request->data['Property']['manager_password']);
                        unset($this->request->data['Property']['password_confirmation']);

                        

                         $arr=array('User'=>array('first_name'=>$this->request->data['Property']['first_name'],
                            'last_name'=>$this->request->data['Property']['last_name'],
                            'username'=>$this->request->data['Property']['username'],
                            'email'=>$this->request->data['Property']['email'],
                            'password'=>$this->request->data['Property']['manager_password'],
                            'role_id'=>$this->request->data['Property']['role_id'],
                             ));
                       App::import('Controller','Users');
                       $user=new UsersController();
                       $result=$user->saveUser($arr);
                        if($result!=false)
                        {
                            $propertyMng=$result;

                            if($propertyAdmin!='')
                            {
                                $multipleRows=array(array('user_id' => $propertyAdmin,'property_id' => $lastInsertPropertyID,'role_id'=>$role['propertyAdmin']),
                                    array('user_id' => $propertyMng,'property_id' => $lastInsertPropertyID,'role_id'=>$role['propertyManager']));
                            }
                            else
                            {
                                $multipleRows=array(array('user_id' => $propertyMng,'property_id' => $lastInsertPropertyID,'role_id'=>$role['propertyManager']));
                            }

                            $this->loadModel('PropertyUser');
                            $propertyUser = new PropertyUser();
                            if($propertyUser->saveall($multipleRows))
                            {
                                CakeLog::write('newUserAddedWhileAddingPropertyAdmin', ''.AuthComponent::user('username').' : New users added with by Name: '.AuthComponent::user('first_name').' '.AuthComponent::user('last_name').' ');  
                                $this->Session->setFlash('The property has been saved.','success');
                                return $this->redirect(array('action' => 'index'));
                            }
                            else
                            {
                                echo "Property user error";
                            }
                        }
                        else
                        {
                            $this->Session->setFlash('Manager data is not saved.','error');

                            $this->loadModel('User');
                            $user = new User();
                            $propertyAdmin=$user->find('list',array('fields'=>array('User.id','User.property_manager'),
                                'conditions' => array('User.role_id =' => $this->Role->ROLES['propertyAdmin'])));
                            $this->set(array('propertyAdmin'=> $propertyAdmin));
                        }



                    }
                    else
                    {
                        $this->Session->setFlash('The Property could not be saved. Please, try again.','error');

                        $this->loadModel('User');
                        $user = new User();
                        $propertyAdmin=$user->find('list',array('fields'=>array('User.id','User.property_manager'),
                            'conditions' => array('User.role_id =' => $this->Role->ROLES['propertyAdmin'])));
                        $this->set(array('propertyAdmin'=> $propertyAdmin));
                    }




                }
                else
                {
                    $this->Session->setFlash(__('Validation Failed'));
                    $this->loadModel('User');
                    $user = new User();
                    $propertyAdmin=$user->find('list',array('fields'=>array('User.id','User.property_manager'),
                        'conditions' => array('User.role_id =' => $this->Role->ROLES['propertyAdmin']),


                    ));

                    $this->set(array('propertyAdmin'=> $propertyAdmin));
                }


        }
        else
        {
            $this->loadModel('User');
            $user = new User();
            $propertyAdmin=$user->find('list',array('fields'=>array('User.id','User.property_manager'),
                'conditions' => array('User.role_id =' => $this->Role->ROLES['propertyAdmin']),


            ));

            $this->set(array('propertyAdmin'=> $propertyAdmin));
        }
    }    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */


 
    public function admin_edit($id = null)
    {
       //Edited ON 4th Feb, 2016
        $this->layout='default';

        if (!$this->Property->exists($id)) {
            throw new NotFoundException(__('Invalid property'));
        }
        if ($this->request->is(array('post', 'put'))){
			//debug($this->request->data['Property']['property_admin']);die;
			$this->request->data['Property']['sub_domain']=strtolower(trim($this->request->data['Property']['sub_domain']));
            if($this->request->data['Property']['logo']['size']==0){
                unset($this->request->data['Property']['logo']);
                $this->Property->set($this->request->data);
                if($this->Property->validates()){
					if($this->update_cloudfare($this->request->data)==false){
						$this->sendCloudFareMail('edit',$this->request->data);
						$this->Session->setFlash('Subdomain Cannot Be Updated, Try Again','error');
						unset($this->request->data['Property']['sub_domain']);
					}
					$this->loadModel('PropertyUser');
					$count=count($this->request->data['Property']['property_admin']);
					if($count==1){
						if($this->request->data['Property']['property_admin'][0]==''){
							$this->PropertyUser->query("DELETE from property_users where property_id=".$this->request->data['Property']['id']." AND role_id=2");
						}else{
							$this->PropertyUser->query("DELETE from property_users where property_id=".$this->request->data['Property']['id']." AND role_id=2");
								$arr['PropertyUser']=array('user_id'=>$this->request->data['Property']['property_admin'][0],
														   'property_id'=>$this->request->data['Property']['id'],
														   'role_id'=>2
															);
								$this->PropertyUser->save($arr,false);
						}
					}else{
						//debug(in_array('',$this->request->data['Property']['property_admin']));die;
						if(in_array('',$this->request->data['Property']['property_admin'])){
							$this->PropertyUser->query("DELETE from property_users where property_id=".$this->request->data['Property']['id']." AND role_id=2");
						}
							$propertyUser=array();
							for($j=0;$j<count($this->request->data['Property']['property_admin']);$j++){
								if($this->request->data['Property']['property_admin'][$j]!==''){
									$rslt=$this->PropertyUser->hasAny(array('user_id'=>$this->request->data['Property']['property_admin'][$j],
																	'property_id'=>$this->request->data['Property']['id'],
																	'role_id'=>2
																	));
									if(!$rslt){
										$propertyUser[]=array('user_id'=>$this->request->data['Property']['property_admin'][$j],
														'property_id'=>$this->request->data['Property']['id'],
														  'role_id'=>2
														);
									}
								}
							}
						$this->PropertyUser->saveMany($propertyUser); 
					}
						if ($this->Property->save($this->request->data,false)){
							CakeLog::write('propertyDetailsEdited', ''.AuthComponent::user('username').' : Property details edited with Property ID:  <a href="/admin/properties/view/'.$id.'">'.$id.' </a> by User: '.AuthComponent::user('first_name').' '.AuthComponent::user('last_name').'');
							$this->Session->setFlash('The property has been saved.','success');
							return $this->redirect(array('action' => 'index'));
						}
						else{
							$this->Session->setFlash('Try again.','error');
							return $this->redirect(array('action' => 'index'));
						}
                    }
                
                else{
                    $this->Property->recursive=0;
                    $options = array('conditions' => array('Property.' . $this->Property->primaryKey => $id));
                    $this->request->data = $this->Property->find('first', $options); 
                }
               }
            else
            {
                $this->Property->set($this->request->data);
                if($this->Property->validates()){
					if($this->update_cloudfare($this->request->data)==false){
						$this->sendCloudFareMail('edit',$this->request->data);
						$this->Session->setFlash('Subdomain Cannot Be Updated In Cloudfare, Try Again','error');
						unset($this->request->data['Property']['sub_domain']);
					}
					$this->loadModel('PropertyUser');
					$count=count($this->request->data['Property']['property_admin']);
					if($count==1){
						if($this->request->data['Property']['property_admin'][0]==''){
							$this->PropertyUser->query("DELETE from property_users where property_id=".$this->request->data['Property']['id']." AND role_id=2");
						}else{
							$this->PropertyUser->query("DELETE from property_users where property_id=".$this->request->data['Property']['id']." AND role_id=2");
								$arr['PropertyUser']=array('user_id'=>$this->request->data['Property']['property_admin'][0],
														   'property_id'=>$this->request->data['Property']['id'],
														   'role_id'=>2
															);
								$this->PropertyUser->save($arr,false);
						}
					}else{
						//debug(in_array('',$this->request->data['Property']['property_admin']));die;
						if(in_array('',$this->request->data['Property']['property_admin'])){
							$this->PropertyUser->query("DELETE from property_users where property_id=".$this->request->data['Property']['id']." AND role_id=2");
						}
							$propertyUser=array();
							for($j=0;$j<count($this->request->data['Property']['property_admin']);$j++){
								if($this->request->data['Property']['property_admin'][$j]!==''){
									$rslt=$this->PropertyUser->hasAny(array('user_id'=>$this->request->data['Property']['property_admin'][$j],
																	'property_id'=>$this->request->data['Property']['id'],
																	'role_id'=>2
																	));
									if(!$rslt){
										$propertyUser[]=array('user_id'=>$this->request->data['Property']['property_admin'][$j],
														'property_id'=>$this->request->data['Property']['id'],
														  'role_id'=>2
														);
									}
								}
							}
						$this->PropertyUser->saveMany($propertyUser,array('validate'=>false));
					}
                    $returnPath=$this->Property->getPathNameImage(); 
                    $this->request->data['Property']['logo']= $returnPath;
                    if ($this->Property->save($this->request->data,false)){
                        $this->Session->setFlash('The property has been saved.','success');
                        return $this->redirect(array('action' => 'index'));
                    }
                    else{
                        $this->Session->setFlash('Try again.','error');
                        return $this->redirect(array('action' => 'index'));
                    }
                }
                $this->Property->recursive=0;
                $options = array('conditions' => array('Property.' . $this->Property->primaryKey => $id));
                $this->request->data = $this->Property->find('first', $options);
            }


        }
        else
        {
            $this->Property->recursive=1;
            $options = array('conditions' => array('Property.' . $this->Property->primaryKey => $id));
            $this->Property->unbindModel(array('hasMany'=>array('CustomerPass','Package','Pass','Vehicle')));
            $this->request->data = $this->Property->find('first', $options);
			$adminArray=array();
			$ids=array();
			$this->loadModel('User');
			foreach( $this->request->data['PropertyUser'] as $i){
				if($i['role_id']==2){
					$adminArray[]=$this->User->field('property_manager',array('id'=>$i['user_id']));
					$ids[]=$i['user_id'];
				}
			}
			 $propertyAdmin=$this->User->find('list',array('fields'=>array('User.id','User.property_manager'),
                'conditions' => array('User.role_id =' =>2),
            ));
            $this->set(array('propertyAdmin'=> $propertyAdmin));
			$this->set(compact('adminArray','ids'));
			//debug($adminArray);
			//debug($this->request->data);
			//die;
        }
    }
	 function sendCloudFareMail($type,$data){
		$this->layout=$this->autoRender =false;
		$this->Email->to = array('parkingalerts@netgen.in','shalender@netgen.in');
		$this->Email->from = $_SERVER['HTTP_HOST'].'<noreply@'.$_SERVER['HTTP_HOST'].'>';
		$this->Email->sendAs = 'html';
		$this->Email->smtpOptions=array(
										'host'=>'smtp.mailgun.org',
										'username'=>'postmaster@internetparkingpass.com',
										'password'=>'f68b359e2d4f1380fc32dea8abeb1f5a'
										);
		$this->Email->delivery='smtp';
		if($type=='add'){
			$this->Email->subject = 'New SubDomain Adding Error in '.$_SERVER['HTTP_HOST'];
			$this->Email->send("New Subdomain Not Added <br>
							SITE : ".$_SERVER['HTTP_HOST']."<br>
							PROPERTY : ".$data['Property']['name']."
							SUBDOMIN :".$data['Property']['sub_domain']." 
			");
		}else{
			$oldsubdomain=$this->Property->field('sub_domain',array('id'=>$data['Property']['id']));
			$this->Email->subject = 'SubDomain Editing Error in '.$_SERVER['HTTP_HOST'];
			$this->Email->send("New Subdomain Not edited <br>
							SITE : ".$_SERVER['HTTP_HOST']."<br>
							PROPERTY : ".$data['Property']['name']."<br>
							NEW SUBDOMIN : ".$data['Property']['sub_domain']."<br>
							OLD SUBDOMIN : ".$oldsubdomain."
			");
		}
		
   }
	 function update_cloudfare($data){ 
		$this->layout=$this->autoRender =false;
		$httpHost=explode('.',$_SERVER['HTTP_HOST']);
		$httpHoststr='';
		if(count($httpHost)==3){
			$httpHoststr=$httpHost[1].'.'.$httpHost[2];
		}else{
			$httpHoststr=$_SERVER['HTTP_HOST'];
		}
		$data['Property']['sub_domain']=strtolower(trim($data['Property']['sub_domain']));
		$oldsubdomain=$this->Property->field('sub_domain',array('id'=>$data['Property']['id']));
		if($oldsubdomain!=$data['Property']['sub_domain']){
			try{
				$domainData=$this->CloudFlareApi->rec_load_all($httpHoststr);
				$domainData = json_decode(json_encode($domainData), true);
				
				if($domainData){
					if($domainData['result']!='error'){
						if(isset($domainData['response']['recs']['objs'])){
							$content='';
							$arr=$domainData['response']['recs']['objs'];
							for($id=0;$id<count($arr);$id++){
								if($arr[$id]['zone_name']==$arr[$id]['name']){
									if($arr[$id]['type']=='A'){
											$content=$arr[$id]['content'];
									}
								}
							}
							$rec_id='';
							for($id=0;$id<count($arr);$id++){
								if($arr[$id]['name']==$oldsubdomain.'.'.$httpHoststr){
									$rec_id=$arr[$id]['rec_id'];
								}
							}
							
							if($content){
								if($rec_id){
									$result=$this->CloudFlareApi->rec_edit($httpHoststr,'A',$rec_id,$data['Property']['sub_domain'],$content);
									$result = json_decode(json_encode($result), true);
									/*debug($result);
									debug($content);
									debug($rec_id);
									debug($httpHoststr);
									debug($oldsubdomain.'.'.$httpHoststr);
									debug($domainData);
									
									die;*/
									if($result){
										if($result['result']!='error'){
											CakeLog::write('editPropertyCloudFareSuccess','CloudFare Success : Propery : '.$data['Property']['name'].' with SubDomian: '.$data['Property']['sub_domain'].' edited in Cloudfare, CONTENT: '.$content.' OLD SubDomain: '.$oldsubdomain);
											return true;
										}else{
											CakeLog::write('editPropertyCloudFareError','CloudFare Error : "'.$result['msg'].'" generated while editing Propery : '.$data['Property']['name'].' in Cloudfare');
											return false;
										}
									}else{
										CakeLog::write('editPropertyCloudFareError','CloudFare Error : Error while editing Propery : '.$data['Property']['name'].' with New SubDomian: '.$data['Property']['sub_domain'].' in Cloudfare OLD SubDomain: '.$oldsubdomain);
										return false;
									}
								}else{
									CakeLog::write('editPropertyCloudFareError','CloudFare Error : No Rec_Id Found while editing Propery : '.$data['Property']['name'].' New SubDomian: '.$data['Property']['sub_domain'].' OLD SubDomain: '.$oldsubdomain);
									return false;
								}
							}else{
								CakeLog::write('editPropertyCloudFareError','CloudFare Error : No Content Found while editing Propery : '.$data['Property']['name']);
								return false;
							}
						}else{
							 CakeLog::write('editPropertyCloudFareError','CloudFare Error : No Response Found while editing Propery : '.$data['Property']['name']);
							 return false;
						}
					}else{
						 CakeLog::write('editPropertyCloudFareError','CloudFare Error : "'.$domainData['msg'].'" generated while editing Propery : '.$data['Property']['name']);
						 return false;
					}
				}
			}catch(Exception $e){
				CakeLog::write('editPropertyCloudFareError','CloudFare Error : Error : "'.$e->getMessage().'" while editing Propery : '.$data['name'].' with SubDomian: '.$data['Property']['sub_domain']. ' in Cloudfare');
				return false;
			}
		}else{
			return true;
		}
	  
	}

    /**
     * delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function delete($id = null) {
        $this->Property->id = $id;
        if (!$this->Property->exists()) {
            throw new NotFoundException(__('Invalid property'));
        }
        $this->request->allowMethod('post', 'delete');
        if ($this->Property->delete()) {
            $this->Session->setFlash(__('The property has been deleted.'));
        } else {
            $this->Session->setFlash(__('The property could not be deleted. Please, try again.'));
        }
        return $this->redirect(array('action' => 'index'));
    }

    public function admin_delete($id = null) {
        $this->layout='default';
        $this->Property->id = $id;
        if (!$this->Property->exists()) {
            throw new NotFoundException(__('Invalid property'));
        }
        $this->request->allowMethod('post', 'delete');
        if ($this->Property->delete()) {
            $this->Session->setFlash(__('The property has been deleted.'));
        } else {
            $this->Session->setFlash(__('The property could not be deleted. Please, try again.'));
        }
        return $this->redirect(array('action' => 'index'));
    }
    public function admin_getPropertyName($id=null)
    {
        $assignedProperty=$this->Property->find('list',array('fields'=>array('name'),'conditions'=>array('id in (Select property_id from property_users where user_id = '.$id.')')));
        return ($assignedProperty);
    }
    public function admin_getPropertyNameRFIF($id=null)
    {
        $assignedProperty=$this->Property->field('name',array('id'=>$id));
        return ($assignedProperty);
    }
/***************************************
 * Manager Property View
 */
 public function manager_property_view(){
	$this->layout='manager';
	
	$property = $this->Property->find('first', array('conditions' => array('id'=>$this->Session->read('PropertyId'))));
    $this->loadModel('Package');
    $this->Package->recursive=0;
    $packages=$this->Package->find('all',array('conditions'=>array('Package.property_id'=>$this->Session->read('PropertyId'))));
	$this->loadModel('Pass');
	$this->Pass->recursive=-1;
	$passes=$this->Pass->find('all',array('conditions'=>array('property_id'=>$this->Session->read('PropertyId'))));
    
    //debug($packages);die;
    $this->set(compact('property','packages','passes'));
 }
 /**************************************************
  * Administrator Property View
  */
  public function admin_pa_property_view(){
	$this->layout='administrator';
	$this->Session->write('PropertySelection',true);
	$property = $this->Property->find('first', array('conditions' => array('id'=>$this->Session->read('PropertyId'))));
    $this->loadModel('Package');
    $this->Package->recursive=-1;
    $packages=$this->Package->find('all',array('conditions'=>array('property_id'=>$this->Session->read('PropertyId'))));
	$this->loadModel('Pass');
	$this->Pass->recursive=-1;
	$passes=$this->Pass->find('all',array('conditions'=>array('property_id'=>$this->Session->read('PropertyId'))));
    $this->set(compact('property','packages','passes'));
  }
	/***************************************************
	 * getProperties function to get property list
	 * 
	 */
	public function admin_getProperties(){
		$aColumns = array('name', 'password', 'sub_domain', 'total_passes_per_user', 'total_space','free_guest_credits','archived','id');
        $sIndexColumn = " properties.id ";
        $sTable = " properties ";
        $sJoinTable='';
        $sConditions='';
       
        $returnArr=$this->NewDatatable->getData(array('columns'=>$aColumns,'index_column'=>$sIndexColumn,'table'=>$sTable,'join'=>$sJoinTable,'conditions'=>$sConditions));
        echo json_encode($returnArr);
        die;    
	}
}

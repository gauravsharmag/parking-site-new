<?php
/***************************************
 * Load AWS SNS SDK
 * 
 */
require (APP . 'Vendor' . DS . 'aws' . DS . 'aws-autoloader.php');

class SmsNotificationComponent extends Component{
	/*******************************************************************
	 * Read the configuration abd create Sns Object
	 * 
	 */
	public function __construct(ComponentCollection $collection,$config = array()) {
		$config = Configure::read('SMS_SETTING');
		$this->Sns = new Aws\Sns\SnsClient($config);
	}
	/*******************************************************************
	 * Create the message parameters
	 * 
	 */
	public function sendOTP($phoneNumber,$userID){
		$phoneNumber=$this->correctPhone($phoneNumber);
		$subject= 'OTP-'.strtoupper(Configure::read('SITE'));
		$message= $this->getOTPmessage($userID);
		$messageArr=array(
						'SMSType' => 'Transactional',
						'Message' => $message,
						'PhoneNumber' => $phoneNumber,
						'Subject' =>$subject
		);
		return $this->publish_message($messageArr);
	}
	/*******************************************************************
	 * Correct phone numbers. Attach country code to numbers if not defined
	 * 
	 */
	private function correctPhone($phoneNumber){
		if (strpos($phoneNumber, '+') !== true) {
			$phoneNumber=Configure::read('COUNTRYCODE').$phoneNumber;
		}
		return $phoneNumber;
	}
	/*******************************************************************
	 * Publish message 
	 * 
	 */
	private function publish_message(array $messageArr){
		$success=true;
		$message=true;
		$messageID='';
		$message='';
		try{
			$result = $this->Sns->publish($messageArr);
			$messageID=$result->get('MessageId');
			$message='Otp sent successfuly';
		}catch(Exception $e){
			$success=false;
			$message=$e->getMessage();
		}
		return array(
					 'success'=>$success,
					 'message_id'=>$messageID,
					 'message'=>$message
		);
	}
	/*******************************************************************
	 *Create OTP message
	 * 
	 */
	private function getOTPmessage($userID){
		$this->PropertyUser=ClassRegistry::init('PropertyUser');
		$propertyData=$this->PropertyUser->find('first',array('conditions'=>array(
																	'user_id'=>$userID
												)));
		$digits = 4;
		$opt= rand(pow(10, $digits-1), pow(10, $digits)-1);
		CakeSession::write('CURRENT_OTP',$opt);
		$message='Your Online Parking Pass mobile alerts verification code for property '.$propertyData['Property']['name'].' is '.$opt;
		return $message;
	}
	/*******************************************************************
	 * Send message
	 * 
	 */
	public function sendMessage($userId,$message,$phoneNumber,$subject='Online Parking Pass'){
		$phoneNumber=$this->correctPhone($phoneNumber);
		$this->Notification=ClassRegistry::init('Notification');
		$notificaionArr['Notification']=array(
											  'user_id'=>$userId,
											  'phone_number'=>$phoneNumber,
											  'message'=>$message
											);
		if($this->Notification->save($notificaionArr)){
			$id=$this->Notification->getLastInsertId();
			$messageArr=array(
						'SMSType' => 'Transactional',
						'Message' => $message,
						'PhoneNumber' => $phoneNumber,
						'Subject' =>$subject
			);
			$result=$this->publish_message($messageArr);
			if($result['success']){
				$this->Notification->id=$id;
				$this->Notification->saveField('message_id',$result['message_id']);
			}
		}
	}
}

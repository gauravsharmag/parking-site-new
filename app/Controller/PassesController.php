<?php
App::uses('AppController', 'Controller');
/**
 * Passes Controller
 *
 * @property Pass $Pass
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class PassesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');
    var $helpers = array('Form', 'Html');
    public function beforeFilter()
    { 
        parent::beforeFilter();
        $this->Security->validatePost = false;
        //$this->Auth->allow();
    }

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Pass->recursive = 0;
		$this->set('passes', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Pass->exists($id)) {
			throw new NotFoundException(__('Invalid pass'));
		}
		$options = array('conditions' => array('Pass.' . $this->Pass->primaryKey => $id));
		$this->set('pass', $this->Pass->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Pass->create();
			if ($this->Pass->save($this->request->data)) {
				$this->Session->setFlash(__('The pass has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The pass could not be saved. Please, try again.'));
			}
		}
		$properties = $this->Pass->Property->find('list');
		$this->set(compact('properties'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Pass->exists($id)) {
			throw new NotFoundException(__('Invalid pass'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Pass->save($this->request->data)) {
				$this->Session->setFlash(__('The pass has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The pass could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Pass.' . $this->Pass->primaryKey => $id));
			$this->request->data = $this->Pass->find('first', $options);
		}
		$properties = $this->Pass->Property->find('list');
		$this->set(compact('properties','propertyId','propertyName'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Pass->id = $id;
		if (!$this->Pass->exists()) {
			throw new NotFoundException(__('Invalid pass'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Pass->delete()) {
			$this->Session->setFlash(__('The pass has been deleted.'));
		} else {
			$this->Session->setFlash(__('The pass could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->Pass->recursive = 0;
		$this->set('passes', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view() {

            $this->layout='customer';
            $currentPropertyId=$this->Session->read('PropertyId');
            $currentPropertyName=$this->Session->read('PropertyName');

            /* debug($selectedPackageId);
             debug($currentPropertyId);
             debug($currentPropertyName);
             debug($this->Session->read('Auth'));*/

            $this->Pass->recursive=-1;
            $remainingPasses=$this->Pass->find('all',array('conditions'=>array('id not in (select pass_id from customer_passes where property_id='.$currentPropertyId.' and user_id='.$this->Auth->user('id').') and property_id='.$currentPropertyId)));
            // debug($remainingPasses);
            $countRemainingPasses=count($remainingPasses);
            //debug($countRemainingPasses);
            $dt = new DateTime();
            $currentDateTime= $dt->format('Y-m-d H:m:s');
            $this->Pass->recursive=-1;
            $activePasses=$this->Pass->find('all',array('conditions'=>array('id in(select pass_id from customer_passes where property_id='.$currentPropertyId.' and user_id='.$this->Auth->user('id').' and pass_valid_upto > \''.$currentDateTime.'\' and  membership_vaild_upto >\''.$currentDateTime.'\') and property_id='.$currentPropertyId)));
            //debug($activePasses);
            $countActivePasses=count($activePasses);
            $this->Pass->recursive=-1;
            $validPassesPackagesExpired=$this->Pass->find('all',array('conditions'=>array('id in(select pass_id from customer_passes where property_id='.$currentPropertyId.' and user_id='.$this->Auth->user('id').' and pass_valid_upto > \''.$currentDateTime.'\' and  membership_vaild_upto <\''.$currentDateTime.'\') and property_id='.$currentPropertyId)));
            //debug($validPassesPackagesExpired);
            $countValidPassesPackagesExpired=count($validPassesPackagesExpired);
            $this->Pass->recursive=-1;
            $expiredPasses=$this->Pass->find('all',array('conditions'=>array('id in(select pass_id from customer_passes where property_id='.$currentPropertyId.' and user_id='.$this->Auth->user('id').' and pass_valid_upto < \''.$currentDateTime.'\') and property_id='.$currentPropertyId)));
            //debug($expiredPasses);
            $countExpiredPasses=count($expiredPasses);
            $this->Pass->recursive=-1;
            $totalPasses=$this->Pass->find('count',array('conditions'=>array('property_id'=>$currentPropertyId)));
            //debug($totalPasses);


            $this->set(array(
                'totalPasses'=>$totalPasses,
                'activePasses'=>$activePasses,
                'validPassesPackagesExpired'=>$validPassesPackagesExpired,
                'expiredPasses'=>$expiredPasses,
                'remainingPasses'=>$remainingPasses,
                'countRemainingPasses'=>$countRemainingPasses,
                'countActivePasses'=>$countActivePasses,
                'countValidPassesPackagesExpired'=>$countValidPassesPackagesExpired,
                'countExpiredPasses'=>$countExpiredPasses
                ));


	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add($id=null,$propertyName=null) {

		if ($this->request->is('post')) {

            if($this->request->data['Pass']['is_fixed_duration' ]==0){
                $this->request->data['Pass']['expiration_date']='';
            }
            else{
                $this->request->data['Pass']['duration']='';
                $this->request->data['Pass']['duration_type']='';
            }
           $this->Pass->create();
			if ($this->Pass->save($this->request->data)) {
				$passName=$this->Pass->givePassName($this->Pass->getLastInsertID());
				CakeLog::write('newPassAddedAdmin', ''.AuthComponent::user('username').' : New Pass Added in  Property ID:  <a href="/admin/properties/view/'.$id.'">'.$id.' </a> PropertyName : '.$propertyName.' by User: '.AuthComponent::user('first_name').' '.AuthComponent::user('last_name').' New Pass ID: '.$this->Pass->getLastInsertID().' and Pass Name : '.$passName.'');
				$this->Session->setFlash('The pass has been saved.','success');
				return $this->redirect(array('action' => 'add',$id,$propertyName));
			} else {
				$this->Session->setFlash('The pass could not be saved. Please, try again.','error');
			}
		}
        $this->Pass->Property->recursive=0;
        $allowed_pass=$this->Pass->Property->find('first',array('fields'=>array('total_passes_per_user'),'conditions'=>array('id'=>$id)));
        $this->Pass->recursive = -1;
        $this->paginate = array(
            'limit' => 10,
            'conditions' => array('property_id'=>$id)
        );
        $passes = $this->paginate('Pass');
        $this->Pass->recursive = -1;
        $count_existing_passes=$this->Pass->find('count',array('conditions'=>array('property_id'=>$id)));

        if($count_existing_passes<$allowed_pass['Property']['total_passes_per_user'])
        {
            $new_pass_allowed=true;
        }
        else
        {
            $new_pass_allowed=false;
        }
        $this->set(compact('id','propertyName','passes','new_pass_allowed'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null,$propertyId=null,$propertyName=null) {
		if (!$this->Pass->exists($id)) {
			throw new NotFoundException(__('Invalid pass'));
		}
		if ($this->request->is(array('post', 'put'))) {

            if($this->request->data['Pass']['is_fixed_duration' ]==0)
            {
                $this->request->data['Pass']['expiration_date']='';
            }
            else
            {
                $this->request->data['Pass']['duration']='';
                $this->request->data['Pass']['duration_type']='';
            }

			if ($this->Pass->save($this->request->data)) {
				$passName=$this->Pass->givePassName($id);
				CakeLog::write('passDetailsEditedAdmin', ''.AuthComponent::user('username').' : Pass Details Edited in  Property ID:  <a href="/admin/properties/view/'.$propertyId.'">'.$propertyId.' </a> PropertyName : '.$propertyName.' by User: '.AuthComponent::user('first_name').' '.AuthComponent::user('last_name').'  Pass ID: '.$id.' and Pass Name : '.$passName.'');

				$this->Session->setFlash('The pass has been updated successfully.','success');
				return $this->redirect(array('action' => 'add',$propertyId,$propertyName));
			} else {
				$this->Session->setFlash('The pass could not be saved. Please, try again.','error');
			}
		} else {
			$options = array('conditions' => array('Pass.' . $this->Pass->primaryKey => $id));
			$this->request->data = $this->Pass->find('first', $options);
            $this->request->data['Pass']['expiration_date']= date("m/d/Y",strtotime($this->request->data['Pass']['expiration_date']));
		}
		$properties = $this->Pass->Property->find('list');

        $this->request->data['Pass']['expiration_date']= date("m/d/Y",strtotime($this->request->data['Pass']['expiration_date']));
        $this->set(compact('properties','propertyId','propertyName'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->Pass->id = $id;
		if (!$this->Pass->exists()) {
			throw new NotFoundException(__('Invalid pass'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->Pass->delete()) {
			$this->Session->setFlash(__('The pass has been deleted.'));
		} else {
			$this->Session->setFlash(__('The pass could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
	
	
    public function customer_buy_pass($selectedPackageId=null)
    {
        $this->layout='customer';
        $currentPropertyId=$this->Session->read('PropertyId');

        $currentPropertyName=$this->Session->read('PropertyName');

        $dt = new DateTime();
        $currentDateTime= $dt->format('Y-m-d H:m:s');

        $this->Pass->recursive=-1;
        //$remainingPasses=$this->Pass->find('all',array('conditions'=>array('id not in (select pass_id from customer_passes where property_id='.$currentPropertyId.' and user_id='.$this->Auth->user('id').') and property_id='.$currentPropertyId)));
        $userId=$this->Auth->user('id');
        $remainingPasses=$this->Pass->getRemainingPass($currentPropertyId,$userId);
       //debug($remainingPasses);

        $this->Pass->CustomerPass->recursive=-1;
        $activePasses=$this->Pass->CustomerPass->find('all',array('fields'=>array('pass_id','pass_valid_upto'),'conditions'=>array('property_id='.$currentPropertyId.' and user_id='.$this->Auth->user('id').' and pass_valid_upto > \''.$currentDateTime.'\' and  membership_vaild_upto >\''.$currentDateTime.'\'')));
        //debug($activePasses);
        $this->Pass->CustomerPass->recursive=-1;
        $validPassesPackagesExpired=$this->Pass->CustomerPass->find('all',array('fields'=>array('pass_id','pass_valid_upto'),'conditions'=>array('property_id='.$currentPropertyId.' and user_id='.$this->Auth->user('id').' and pass_valid_upto > \' '.$currentDateTime.'\' and  membership_vaild_upto <\''.$currentDateTime.'\'')));
        //debug($validPassesPackagesExpired);
        $this->Pass->recursive=-1;
        $expiredPassesNonRenewable=$this->Pass->find('all',array('conditions'=>array('id in(select pass_id from customer_passes where property_id='.$currentPropertyId.' and user_id='.$this->Auth->user('id').' and pass_valid_upto <  \' '.$currentDateTime.'\') and property_id='.$currentPropertyId.' and is_fixed_duration = 1 and expiration_date <\''.$currentDateTime.'\'')));
        $this->Pass->recursive=-1;
        $expiredPassesRenewable=$this->Pass->find('all',array('conditions'=>array('id in(select pass_id from customer_passes where property_id='.$currentPropertyId.' and user_id='.$this->Auth->user('id').' and pass_valid_upto <  \' '.$currentDateTime.'\') and property_id='.$currentPropertyId.' and is_fixed_duration = 0')));
        $expiredPasses=array_merge( $expiredPassesNonRenewable,$expiredPassesRenewable);
       // debug($expiredPasses);
        $this->Pass->recursive=-1;
       //die();
        $totalPasses=$this->Pass->find('count',array('conditions'=>array('property_id'=>$currentPropertyId)));



        $this->set(array('selectedPackageID'=>$selectedPackageId,
            'totalPasses'=>$totalPasses,
            'activePasses'=>$activePasses,
            'validPassesPackagesExpired'=>$validPassesPackagesExpired,
            'expiredPasses'=>$expiredPasses,
            'remainingPasses'=>$remainingPasses,




        ));

    }
    public function getDeposit($id=null)
    {
        $this->Pass->recursive=-1;
        return $this->Pass->find('first',array('fields'=>array('deposit'),'conditions'=>array('id'=>$id)));
    }
    public function getCost($id=null)
    {
        $this->Pass->recursive=-1;
        return $this->Pass->find('first',array('fields'=>array('cost_1st_year'),'conditions'=>array('id'=>$id)));
    }
    public function getDuration($id=null)
    {
        $this->Pass->recursive=-1;
        return $this->Pass->find('first',array('fields'=>array('duration','duration_type','is_fixed_duration'),'conditions'=>array('id'=>$id)));
    }
	public function admin_getPassName($id=null)
    {
        return $this->Pass->field('name',array('id'=>$id));
    }
	public function getPassPermit($id=null)
    {
        if (!$this->Pass->exists($id)) {
			throw new NotFoundException(__('Invalid pass'));
		}
		$packageId=$this->Pass->field('package_id',array('id'=>$id));
		if($packageId==null){
			$this->Session->write('PassToBuy',$id);
			$this->redirect(array('controller'=>'Packages','action'=>'index'));
		}else{
			$this->redirect(array('controller'=>'Transactions','action'=>'check_out',$id,$packageId));
		}
    }
	public function getPassName($customerPassId=null)
    {
        $this->loadModel('CustomerPass');
		$i=new CustomerPass();
		$id=(int)$i->field('pass_id',array('id'=>$customerPassId));
		return $this->Pass->field('name',array('id'=>$id));
    }
    public function admin_getPName($customerPassId=null)
    {
        $this->loadModel('CustomerPass');
		$i=new CustomerPass();
		$id=(int)$i->field('pass_id',array('id'=>$customerPassId));
		return $this->Pass->field('name',array('id'=>$id));
    }
    public function admin_get_passes_list($property_id)
    {
        $this->autoRender=false;
		$response=array();
		$response=$this->Pass->find('list',array('fields'=>'name','conditions'=>array('property_id'=>$property_id)));
		echo json_encode($response);
		/*$this->response->type('json');
		$this->response->body(json_encode($response));
		$this->response->send();*/
		
    }
 // Action Updated on 10th Feb 2015
 public function give_selected_passes_details($passNo){
		$this->autoRender=$this->layout=false;
		$this->loadModel('Package');
		$this->Package->recursive=-1;
		$allPasses=$otherRequiredPasses=$otherPasses=$returnArray=array();
		$dt = new DateTime();
		$currentDateTime= $dt->format('Y-m-d H:m:s');
		$totalPassNumber = $this->Pass->find('count', array('conditions' => array(
																						'Pass.property_id' => CakeSession::read('PropertyId')
																					)
																));
		$otherPassesRequiredNumber = $this->Package->find('count', array('conditions' => array(
																						'Package.property_id' => CakeSession::read('PropertyId'), 
																						' Package.is_required' => 1, 
																						'Package.is_guest ' => 0,'Package.pass_id IS NOT NULL AND (Package.expiration_date IS NULL OR Package.expiration_date > \''.$currentDateTime.'\')')
																));
		if($passNo==0 || $passNo>$totalPassNumber){
			$passNo=$totalPassNumber;
		}
		if($passNo==$otherPassesRequiredNumber){
			$otherRequiredPasses = $this->Package->find('all', array(
													'fields' => array('Package.*,Pass.*'),
													'joins' => array(
																	array(
																			'table' => 'passes',
																			'alias' => 'Pass',
																			'type' => 'LEFT',
																			'conditions' => array(
																									'Pass.id=Package.pass_id'
																									)
																		)
																	),
													'conditions' => array('Package.property_id' => CakeSession::read('PropertyId'), 
																		  ' Package.is_guest' => 0,'Package.is_required'=>1,'Package.pass_id IS NOT NULL AND ((Package.expiration_date IS NULL OR Package.expiration_date > \''.$currentDateTime.'\') AND (Pass.expiration_date IS NULL OR Pass.expiration_date > \''.$currentDateTime.'\'))')
											));
		  $allPasses=$otherRequiredPasses ;
		}else{
			$this->Package->recursive=-1;
			$otherRequiredPasses = $this->Package->find('all', array(
													'fields' => array('Package.*,Pass.*'),
													'joins' => array(
																	array(
																			'table' => 'passes',
																			'alias' => 'Pass',
																			'type' => 'LEFT',
																			'conditions' => array(
																									'Pass.id=Package.pass_id'
																									)
																		)
																	),
													'conditions' => array('Package.property_id' => CakeSession::read('PropertyId'), 
																		  ' Package.is_guest' => 0,'Package.is_required'=>1,'Package.pass_id IS NOT NULL AND ((Package.expiration_date IS NULL OR Package.expiration_date > \''.$currentDateTime.'\') AND (Pass.expiration_date IS NULL OR Pass.expiration_date > \''.$currentDateTime.'\'))')
											));
			$countRequiredPasses=count($otherRequiredPasses);
			$nextPassCount=$passNo-$countRequiredPasses;
			$limit=0;
			if($nextPassCount>0){
				
				$otherNonRequiredPasses = $this->Package->find('all', array(
													'fields' => array('Package.*,Pass.*'),
													'joins' => array(
																	array(
																			'table' => 'passes',
																			'alias' => 'Pass',
																			'type' => 'LEFT',
																			'conditions' => array(
																									'Pass.id=Package.pass_id'
																									)
																		)
																	),
													'conditions' => array('Package.property_id' => CakeSession::read('PropertyId'), 
																		  ' Package.is_guest' => 0,'Package.is_required'=>0,'Package.pass_id IS NOT NULL AND ((Package.expiration_date IS NULL OR Package.expiration_date > \''.$currentDateTime.'\') AND (Pass.expiration_date IS NULL OR Pass.expiration_date > \''.$currentDateTime.'\'))'),
													'limit'=>$nextPassCount
											));
			
				$countNonRequiredPasses=count($otherNonRequiredPasses);		
				$limit=$passNo-($countRequiredPasses+$countNonRequiredPasses);
			}
			if($limit>0){
				$this->Pass->recursive=-1;
				$otherPasses = $this->Pass->find('all', array(
													       'conditions' => array('Pass.property_id' => CakeSession::read('PropertyId'),'Pass.package_id'=>NULL,'(Pass.expiration_date IS NULL OR Pass.expiration_date > \''.$currentDateTime.'\')'), 
														  'limit'=>$limit
											));
			}
			if($passNo==1){ 
				$arr=array();
				if(!empty($otherRequiredPasses)){
					$arr[]=$otherRequiredPasses[0];
				}elseif(!empty($otherNonRequiredPasses)){
					$arr[]=$otherNonRequiredPasses[0];
				}elseif(!empty($otherPasses)){
					$arr[]=$otherPasses[0];
				}
				$allPasses=$arr;
			}else{
				$allPasses=array_merge($otherRequiredPasses,$otherNonRequiredPasses,$otherPasses);
			}
		}
		$dt = new DateTime();
        $currentDateTime = $dt->format('Y-m-d H:i:s');
        $totalCost=0;
        if(!empty($allPasses)){
			for($i=0;$i<count($allPasses);$i++){
					if ($allPasses[$i]['Pass']['is_fixed_duration'] == 0) {
                        switch ($allPasses[$i]['Pass']['duration_type']) {
                            case "Hour":
                                $date = new DateTime($currentDateTime);
                                $k = (int) $allPasses[$i]['Pass']['duration'];
                                $date->add(new DateInterval('PT' . $k . 'H'));
                                $allPasses[$i]['Pass']['expiration_date'] = date_format($date, 'Y-m-d H:i:s');
                                break;
                            case "Day":
                                $date = new DateTime($currentDateTime);
                                $k = $allPasses[$i]['Pass']['duration'];
                                $date->add(new DateInterval('P' . $k . 'D'));
                                $allPasses[$i]['Pass']['expiration_date'] = date_format($date, 'Y-m-d H:i:s');
                                break;
                            case "Week":
                                $date = new DateTime($currentDateTime);
                                $k = (int) $allPasses[$i]['Pass']['duration'] * 7;
                                $date->add(new DateInterval('P' . $k . 'D'));
                                $allPasses[$i]['Pass']['expiration_date'] = date_format($date, 'Y-m-d H:i:s');
                                break;
                            case "Month":
                                $date = new DateTime($currentDateTime);
                                $k = (int) $allPasses[$i]['Pass']['duration'];
                                $date->add(new DateInterval('P' . $k . 'M'));
                                $allPasses[$i]['Pass']['expiration_date'] = date_format($date, 'Y-m-d H:i:s');
                                break;
                            case "Year":
                                $date = new DateTime($currentDateTime);
                                $k = (int) $allPasses[$i]['Pass']['duration'];
                                $date->add(new DateInterval('P' . $k . 'Y'));
                                $allPasses[$i]['Pass']['expiration_date'] = date_format($date, 'Y-m-d H:i:s');
                                break;
                        }
                    }
                  $allPasses[$i]['Pass']['expiration_date']= date("m/d/Y",strtotime( $allPasses[$i]['Pass']['expiration_date']));
                  $returnArray[]=array('Pass'=>array($allPasses[$i]['Pass']['name'],
											 '$ '.$allPasses[$i]['Pass']['deposit'],
											 '$ '.$allPasses[$i]['Pass']['cost_1st_year'],
											 $allPasses[$i]['Pass']['expiration_date']
					
                  ));
                    if(isset($allPasses[$i]['Package'])){
						if($allPasses[$i]['Package']['is_required']==1){
							 $returnArray[$i]['Pass'][]="Yes";
						}else{
						    $returnArray[$i]['Pass'][]="No";
						 }
						 $packageNameDetail='$ '.$allPasses[$i]['Package']['cost'];
						 if($allPasses[$i]['Package']['is_fixed_duration']==0){
							 if($allPasses[$i]['Package']['is_recurring'] == 1){
								if($allPasses[$i]['Package']['is_guest'] == 0){
									if($allPasses[$i]['Package']['cost']>0){
										$packageNameDetail=$packageNameDetail.'<br> <font color="red">Recurring Payment*</font><br>
																				Interval : '.$allPasses[$i]['Package']['duration'].' '.$allPasses[$i]['Package']['duration_type'];
									}
								}
							 }
						  }
						 $returnArray[$i]['Pass'][]= $allPasses[$i]['Package']['name'];
						 $returnArray[$i]['Pass'][]= $packageNameDetail;
						 $returnArray[$i]['Pass'][]= $allPasses[$i]['Pass']['deposit']+ $allPasses[$i]['Pass']['cost_1st_year']+ $allPasses[$i]['Package']['cost'];
					}else{
						$returnArray[$i]['Pass'][]="No";
						$returnArray[$i]['Pass'][]= "No Package";
						$returnArray[$i]['Pass'][]= "0";
						$returnArray[$i]['Pass'][]= $allPasses[$i]['Pass']['deposit']+ $allPasses[$i]['Pass']['cost_1st_year'];
					}
                   $totalCost+=$returnArray[$i]['Pass'][7];
           
			}
		}
		//debug($returnArray);die;
		$returnArray[]=$totalCost;
		echo json_encode($returnArray);die;
   }


}

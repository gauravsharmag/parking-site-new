<?php
App::uses('AppController', 'Controller');
/**
 * CouponPackages Controller
 *
 * @property CouponPackage $CouponPackage
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class CouponPackagesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');

/**
 * admin_index method
 */
	public function admin_index() {
		$allCoupons=$this->CouponPackage->find('all',array('fields'=>array('CouponPackage.*','Property.name')));
		//debug($allCoupons);die;
		//$this->CouponPackage->recursive = 0;
		$this->set(compact('allCoupons'));
		
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) { 
		if (!$this->CouponPackage->exists($id)) {
			throw new NotFoundException(__('Invalid coupon package'));
		}
		$options = array('conditions' => array('CouponPackage.' . $this->CouponPackage->primaryKey => $id));
		$this->set('couponPackage', $this->CouponPackage->find('first', $options));
	}

/**
 * admin_add method
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->loadModel('CouponCode');
			//debug($this->request->data);die;
			if(empty($this->request->data['CouponPackage']['current_pass'])){
				$this->Session->setFlash(__('No Passes Selected'),'error');
			}else{
				if(empty($this->request->data['CouponPackage']['coupon_codes'])){
					$this->Session->setFlash(__('Coupon codes cannot be empty'),'error');
				}else{
					$coupons=explode(',',$this->request->data['CouponPackage']['coupon_codes']);
					if($coupons){
						$passes='';
						for($i=0;$i<count($this->request->data['CouponPackage']['current_pass']);$i++){
							$passes=$passes.$this->request->data['CouponPackage']['current_pass'][$i];
							if($i<(count($this->request->data['CouponPackage']['current_pass'])-1)){
								$passes=$passes.',';
							}
						}
						$arrPackage['CouponPackage']=array(	'name'=>$this->request->data['CouponPackage']['name'],
														    'property_id'=>$this->request->data['CouponPackage']['property_id'],
														    'passes'=>$passes
							);
						$this->CouponPackage->create();
						if ($this->CouponPackage->save($arrPackage)) {
							for($j=0;$j<count($coupons);$j++){
								$coupons[$j]=trim($coupons[$j]);
								if($coupons[$j]){
									$arr['CouponCode']=array(
														 'code'=>$coupons[$j],
														 'coupon_package_id'=>$this->CouponPackage->getLastInsertID()
									);
									$this->CouponCode->create();
									$this->CouponCode->save($arr);
								}
							}
							$this->Session->setFlash(__('The coupon package has been saved.'),'success');
							return $this->redirect(array('action' => 'index'));
						} else {
							$this->Session->setFlash(__('The coupon package could not be saved. Please, try again.'),'error');
						}
					}else{
						$this->Session->setFlash(__('Please Check Coupon Codes'),'error');
					}
				}
			}
		}
		$property_list = $this->CouponPackage->Property->find('list');
		$id=array_keys($property_list);
		$selectedId=$id[0];
		$this->loadModel('Pass');
		$passes_list=$this->Pass->find('list',array('fields'=>'name','conditions'=>array('property_id'=>$selectedId)));
		$pass_id=array_keys($property_list);
		$selectedPassId=$pass_id[0];
		$this->set(compact('property_list','selectedId','selectedPassId','passes_list'));
	}

/**
 * admin_edit method
 */
	public function admin_edit($id = null) {
		if (!$this->CouponPackage->exists($id)) {
			throw new NotFoundException(__('Invalid coupon package'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if(empty($this->request->data['CouponPackage']['current_pass'])){
				$this->Session->setFlash(__('No Passes Selected'),'error');
			}else{
				$passes='';
				for($i=0;$i<count($this->request->data['CouponPackage']['current_pass']);$i++){
						$passes=$passes.$this->request->data['CouponPackage']['current_pass'][$i];
						if($i<(count($this->request->data['CouponPackage']['current_pass'])-1)){
								$passes=$passes.',';
						}
				}
				$arrPackage['CouponPackage']=array(	'id'=>$this->request->data['CouponPackage']['id'],
													'name'=>$this->request->data['CouponPackage']['name'],
												    'property_id'=>$this->request->data['CouponPackage']['property_id'],
												    'passes'=>$passes
						);
				if ($this->CouponPackage->save($arrPackage)) {
					$this->Session->setFlash(__('The coupon package has been saved.'),'success');
					return $this->redirect(array('action' => 'index'));
				} else {
					$this->Session->setFlash(__('The coupon package could not be saved. Please, try again.'),'error');
				}	
			}
		} else {
			$this->CouponPackage->unbindModel(array('belongsTo'=>array('Property')));
			$options = array('conditions' => array('CouponPackage.' . $this->CouponPackage->primaryKey => $id));
			$this->request->data = $this->CouponPackage->find('first', $options);
		}
		//debug($this->request->data);die;
		$property_list = $this->CouponPackage->Property->find('list');
		$id=array_keys($property_list);
		$selectedId=$this->request->data['CouponPackage']['property_id'];
		$this->loadModel('Pass');
		$passes_list=$this->Pass->find('list',array('fields'=>'name','conditions'=>array('property_id'=>$selectedId)));
		$pass_id=array_keys($property_list);
		$selectedPassId=explode(',',$this->request->data['CouponPackage']['passes']);
		$this->set(compact('property_list','selectedId','selectedPassId','passes_list'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->CouponPackage->id = $id;
		if (!$this->CouponPackage->exists()) {
			throw new NotFoundException(__('Invalid coupon package'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->CouponPackage->delete()) {
			$this->Session->setFlash(__('The coupon package has been deleted.'));
		} else {
			$this->Session->setFlash(__('The coupon package could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}

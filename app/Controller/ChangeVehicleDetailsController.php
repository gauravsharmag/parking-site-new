<?php

App::uses('AppController', 'Controller');

class ChangeVehicleDetailsController extends AppController {
	public $components = array('Email','NewDatatable');
    public function change($id = null) {
        if (!$this->ChangeVehicleDetail->Vehicle->exists($id)) {
            throw new NotFoundException(__('Not found'));
        } else {
            $this->ChangeVehicleDetail->Vehicle->recursive = -1;
            $changeDetail = $this->ChangeVehicleDetail->Vehicle->findById($id);
            if ($changeDetail['Vehicle']['user_id'] != $this->Auth->user('id')) {
                throw new NotFoundException(__('Not found'));
            }
        }


        $this->layout = 'customer';
        if ($this->request->is('post')) {
            $this->request->data['ChangeVehicleDetail']['customer_pass_id'] = $changeDetail['Vehicle']['customer_pass_id'];
            $this->request->data['ChangeVehicleDetail']['user_id'] = AuthComponent::user('id');
            $this->request->data['ChangeVehicleDetail']['vehicle_id'] = $changeDetail['Vehicle']['id'];
            $this->request->data['ChangeVehicleDetail']['property_id'] = $changeDetail['Vehicle']['property_id'];

            $data = $this->ChangeVehicleDetail->find('all', array(
                'conditions' => array(
                    'ChangeVehicleDetail.vehicle_id' => $changeDetail['Vehicle']['id'],
                    'ChangeVehicleDetail.property_id' => $changeDetail['Vehicle']['property_id'],
                    'ChangeVehicleDetail.changed' => 0,
            )));

            if (!empty($data)) {
                $this->Session->setFlash('The change request for this vehicle has already been sent ', 'warning');
                // debug($vid['ChangeVehicleDetail']['id']); die;
                // $this->request->data['ChangeVehicleDetail']['id'] = $data['ChangeVehicleDetail']['id'];
                // $this->request->data['ChangeVehicleDetail']['changed'] = 0;
                // $this->request->data['ChangeVehicleDetail']['done_by'] = null;
            } else {
                $this->ChangeVehicleDetail->create();
                if ($this->ChangeVehicleDetail->save($this->request->data)) {
					CakeLog::write('changeVehicleRequest', ''.AuthComponent::user('username').' : Vehicle detail change request by Name: '.AuthComponent::user('first_name').' '.AuthComponent::user('last_name').' in Property: '.CakeSession::read('PropertyName').' Request Id : <a href="/admin/ChangeVehicleSetails/view_change/'.$this->ChangeVehicleDetail->getLastInsertId.'">'.$this->ChangeVehicleDetail->getLastInsertId.'</a>');  
                    $this->Session->setFlash('The change request has been made. You will be notified when its approved. You are subject to be towed until change has been approved and you are notified.', 'success');
                    return $this->redirect(array('controller' => 'vehicles', 'action' => 'index'));
                } else {
                    $this->Session->setFlash('The vehicle change request could not be sent', 'error');
                }
            }
        }
        $this->loadModel('State');
        $states = $this->State->find('list');
        $this->set(compact('changeDetail', 'states'));
    }

    public function manager_change($id = null) {
        if (!$this->ChangeVehicleDetail->exists($id)) {
            throw new NotFoundException(__('Not found'));
        } else {
            $changeDetail = $this->ChangeVehicleDetail->findById($id);
        }
        $this->layout = "manager";
        $this->Session->write('PropertySelection', true);
        $this->loadModel('State');
        $states = $this->State->find('list');
        $this->set(compact('changeDetail', 'states'));
        $oldvehiclename = $changeDetail['Vehicle']['owner'];
        $oldvehiclemodel = $changeDetail['Vehicle']['model'];
        $oldvehiclemake = $changeDetail['Vehicle']['make'];
        $oldvehiclecolor = $changeDetail['Vehicle']['color'];
        $oldvehiclenumber = $changeDetail['Vehicle']['license_plate_number'];
        $oldvehiclestate = $changeDetail['Vehicle']['license_plate_state'];
        $oldvehiclevin = $changeDetail['Vehicle']['last_4_digital_of_vin'];
        if ($this->request->is('post')) {
            $this->ChangeVehicleDetail->Vehicle->updateAll(array(
                'Vehicle.owner' => '\'' . $this->request->data['ChangeVehicleDetail']['vehicle_name'] . '\'',
                'Vehicle.make' => '\'' . $this->request->data['ChangeVehicleDetail']['make'] . '\'',
                'Vehicle.model' => '\'' . $this->request->data['ChangeVehicleDetail']['model'] . '\'',
                'Vehicle.color' => '\'' . $this->request->data['ChangeVehicleDetail']['color'] . '\'',
                'Vehicle.license_plate_number' => '\'' . $this->request->data['ChangeVehicleDetail']['plate_number'] . '\'',
                'Vehicle.license_plate_state' => '\'' . $this->request->data['ChangeVehicleDetail']['state'] . '\'',
                'Vehicle.last_4_digital_of_vin' => '\'' . $this->request->data['ChangeVehicleDetail']['vin'] . '\'',
                    ), array('Vehicle.id' => $changeDetail['Vehicle']['id']));
            $this->ChangeVehicleDetail->updateAll(array(
                'ChangeVehicleDetail.changed' => 1,
                'ChangeVehicleDetail.vehicle_name' => '\'' . $oldvehiclename . '\'',
                'ChangeVehicleDetail.make' => '\'' . $oldvehiclemake . '\'',
                'ChangeVehicleDetail.model' => '\'' . $oldvehiclemodel . '\'',
                'ChangeVehicleDetail.color' => '\'' . $oldvehiclecolor . '\'',
                'ChangeVehicleDetail.plate_number' => '\'' . $oldvehiclenumber . '\'',
                'ChangeVehicleDetail.state' => '\'' . $oldvehiclestate . '\'',
                'ChangeVehicleDetail.vin' => '\'' . $oldvehiclevin . '\'',
                'ChangeVehicleDetail.done_by' => '\'' . AuthComponent::user('first_name') . " " . AuthComponent::user('last_name') . " (MANAGER)" . '\''
                    ), array('ChangeVehicleDetail.id' => $id));
            $this->Session->setFlash('Vehicle details updated', 'success');
            return $this->redirect(array('controller' => 'ChangeVehicleDetails', 'action' => 'manager_view_change'));
        }
    }

    public function admin_change($id = null) {

        if (!$this->ChangeVehicleDetail->exists($id)) {
            throw new NotFoundException(__('Not found'));
        } else {
            $changeDetail = $this->ChangeVehicleDetail->findById($id);
        }

        if ($this->Auth->user('role_id') == 2) {
            $this->layout = "administrator";
        }
        $this->loadModel('State');
        $states = $this->State->find('list');
         $this->loadModel('Property');
        $propertyName=$this->Property->givePropertyName($changeDetail['ChangeVehicleDetail']['property_id']);
        $this->set(compact('changeDetail', 'states','propertyName'));
        $oldvehiclename = $changeDetail['Vehicle']['owner'];
        $oldvehiclemodel = $changeDetail['Vehicle']['model'];
        $oldvehiclemake = $changeDetail['Vehicle']['make'];
        $oldvehiclecolor = $changeDetail['Vehicle']['color'];
        $oldvehiclenumber = $changeDetail['Vehicle']['license_plate_number'];
        $oldvehiclestate = $changeDetail['Vehicle']['license_plate_state'];
        $oldvehiclevin = $changeDetail['Vehicle']['last_4_digital_of_vin'];
        if ($this->request->is('post')) {
			
			if($this->request->data['ChangeVehicleDetail']['rejected']==0){	
				
			   $vehicleExists=$this->ChangeVehicleDetail->Vehicle->hasAny(array('license_plate_number'=>$this->request->data['ChangeVehicleDetail']['plate_number'],
																				'license_plate_state'=> $this->request->data['ChangeVehicleDetail']['state'],
																				'property_id'=>$changeDetail['ChangeVehicleDetail']['property_id'],
																				'id != ' => $changeDetail['Vehicle']['id']
																				));
			   if($vehicleExists==false){
					$this->ChangeVehicleDetail->Vehicle->updateAll(array(
						'Vehicle.owner' => '\'' . $this->request->data['ChangeVehicleDetail']['vehicle_name'] . '\'',
						'Vehicle.make' => '\'' . $this->request->data['ChangeVehicleDetail']['make'] . '\'',
						'Vehicle.model' => '\'' . $this->request->data['ChangeVehicleDetail']['model'] . '\'',
						'Vehicle.color' => '\'' . $this->request->data['ChangeVehicleDetail']['color'] . '\'',
						'Vehicle.license_plate_number' => '\'' . $this->request->data['ChangeVehicleDetail']['plate_number'] . '\'',
						'Vehicle.license_plate_state' => '\'' . $this->request->data['ChangeVehicleDetail']['state'] . '\'',
						'Vehicle.last_4_digital_of_vin' => '\'' . $this->request->data['ChangeVehicleDetail']['vin'] . '\'',
							), array('Vehicle.id' => $changeDetail['Vehicle']['id']));
					$this->ChangeVehicleDetail->updateAll(array(
						'ChangeVehicleDetail.changed' => 1,
						'ChangeVehicleDetail.vehicle_name' => '\'' . $oldvehiclename . '\'',
						'ChangeVehicleDetail.make' => '\'' . $oldvehiclemake . '\'',
						'ChangeVehicleDetail.model' => '\'' . $oldvehiclemodel . '\'',
						'ChangeVehicleDetail.color' => '\'' . $oldvehiclecolor . '\'',
						'ChangeVehicleDetail.plate_number' => '\'' . $oldvehiclenumber . '\'',
						'ChangeVehicleDetail.state' => '\'' . $oldvehiclestate . '\'',
						'ChangeVehicleDetail.vin' => '\'' . $oldvehiclevin . '\'',
						'ChangeVehicleDetail.done_by' => '\'' . AuthComponent::user('first_name') . " " . AuthComponent::user('last_name') . " (ADMIN)" . '\''
							), array('ChangeVehicleDetail.id' => $id));
					$this->Email->from = 'parking<noreply@' . $_SERVER['HTTP_HOST'] . '>';
					$this->Email->to = $changeDetail['User']['email'];
					$this->Email->sendAs = 'html';
					$this->Email->subject = 'Change Vehicle Request Approved';
					$this->Email->smtpOptions=Configure::read('SMTP_SETTING');
					$this->Email->delivery='smtp';
					// $this->Email->smtpOptions=array(
					// 				'host'=>'smtp.mailgun.org',
					// 				'username'=>'postmaster@internetparkingpass.com',
					// 				'password'=>'f68b359e2d4f1380fc32dea8abeb1f5a'
					// 				);
					// $this->Email->delivery='smtp';
					$message='Dear '.$changeDetail['User']['first_name'].' '.$changeDetail['User']['last_name'].'<br>
							  This is to inform you that your request to change the vehicle details has been approved: <br>
							  
							  <b>DETAILS: </b>
							  <table border="1">
								<thead>
									<tr>
										<td>OLD VEHICLE DETAIL</td>
										<td>REQUESTED VEHICLE DETAIL</td>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>
											MAKE: '.$changeDetail['Vehicle']['make'].'<br>
											MODEL: '.$changeDetail['Vehicle']['model'].'<br>
											COLOR: '.$changeDetail['Vehicle']['color'].'<br>
											PLATE NUMBER: '.$changeDetail['Vehicle']['license_plate_number'].' '.$changeDetail['Vehicle']['license_plate_state'].'<br>
											VIN: '.$changeDetail['Vehicle']['last_4_digital_of_vin'].'<br>
										</td>
										<td>
											MAKE: '.$changeDetail['ChangeVehicleDetail']['make'].'<br>
											MODEL: '.$changeDetail['ChangeVehicleDetail']['model'].'<br>
											COLOR: '.$changeDetail['ChangeVehicleDetail']['color'].'<br>
											PLATE NUMBER: '.$changeDetail['ChangeVehicleDetail']['plate_number'].' '.$changeDetail['ChangeVehicleDetail']['state'].'<br>
											VIN: '.$changeDetail['ChangeVehicleDetail']['vin'].'<br>
										</td>
									</tr>
								</tbody>
							  </table>
							  <br>
							  Thanks,
							  <br>
							  <b>Administrator</b><br>
							  '.$_SERVER['HTTP_HOST'].'
								';
					$this->Email->send($message);
					$this->loadModel('Ticket');
						$this->loadModel('Property');
						$ticket['Ticket']=array( 'status' => 1,
												 'user_id' => $this->Auth->user('id'),
												 'recipient_id'=>$changeDetail['User']['id'],
												 'property_name' =>  $this->Property->field('name',array('id'=>$changeDetail['Vehicle']['property_id'])),
												 'subject'=>'Change Vehicle Request Approved',
												 'email' => $changeDetail['User']['email'],
												 'phone' => $changeDetail['User']['phone'],
												 'message'=>' Your request to change the vehicle details has been approved : <br>
															  <br>
															  <b>DETAILS: </b> 
															  <table border="1">
																<thead>
																	<tr>
																		<td>OLD VEHICLE DETAIL</td>
																		<td>REQUESTED VEHICLE DETAIL</td>
																	</tr>
																</thead>
																<tbody>
																	<tr>
																		<td>
																			MAKE: '.$changeDetail['Vehicle']['make'].'<br>
																			MODEL: '.$changeDetail['Vehicle']['model'].'<br>
																			COLOR: '.$changeDetail['Vehicle']['color'].'<br>
																			PLATE NUMBER: '.$changeDetail['Vehicle']['license_plate_number'].' '.$changeDetail['Vehicle']['license_plate_state'].'<br>
																			VIN: '.$changeDetail['Vehicle']['last_4_digital_of_vin'].'<br>
																		</td>
																		<td>
																			MAKE: '.$changeDetail['ChangeVehicleDetail']['make'].'<br>
																			MODEL: '.$changeDetail['ChangeVehicleDetail']['model'].'<br>
																			COLOR: '.$changeDetail['ChangeVehicleDetail']['color'].'<br>
																			PLATE NUMBER: '.$changeDetail['ChangeVehicleDetail']['plate_number'].' '.$changeDetail['ChangeVehicleDetail']['state'].'<br>
																			VIN: '.$changeDetail['ChangeVehicleDetail']['vin'].'<br>
																		</td>
																	</tr>
																</tbody>
															  </table>',
												 'reply_admin' => 1,
												 'user_reply' => 0					 
						);
						if($this->Ticket->save($ticket,array('validate'=>false))){
								$this->Session->setFlash('Vehicle details updated and user has been informed.', 'success');
								
						}else{
							$this->Session->setFlash('Error Occured, Please try again', 'error');
						}
						return $this->redirect(array('controller' => 'ChangeVehicleDetails', 'action' => 'admin_view_change'));
				}else{
					$this->Session->setFlash('Vehicle with new details is already present.', 'error');
				}
			}else{
				if(empty($this->request->data['ChangeVehicleDetail']['rejection_reason'])){
					$this->Session->setFlash('Please Provide The Reason To Reject The Request', 'success');
				}else{
					$arr['ChangeVehicleDetail']=array(
														'id'=>$id,
														'changed'=>9,
														'rejection_reason'=>$this->request->data['ChangeVehicleDetail']['rejection_reason']
							
					);
					//debug($changeDetail);die;
					if($this->ChangeVehicleDetail->save($arr,array('validate'=>false))){
						
						$this->Email->from = 'parking<noreply@' . $_SERVER['HTTP_HOST'] . '>';
						$this->Email->to = $changeDetail['User']['email'];
						$this->Email->sendAs = 'html';
						$this->Email->subject = 'Change Vehicle Request Rejected';
						$this->Email->smtpOptions=Configure::read('SMTP_SETTING');
						$this->Email->delivery='smtp';
						// $this->Email->smtpOptions=array(
						// 				'host'=>'smtp.mailgun.org',
						// 				'username'=>'postmaster@internetparkingpass.com',
						// 				'password'=>'f68b359e2d4f1380fc32dea8abeb1f5a'
						// 				);
						// $this->Email->delivery='smtp';
						$message='Dear '.$changeDetail['User']['first_name'].' '.$changeDetail['User']['last_name'].'<br>
								  This is to inform you that your request to change the vehicle details has been rejected due to following reason : <br>
								  <br>
								  <b>REASON :</b> '.$this->request->data['ChangeVehicleDetail']['rejection_reason'].'<br>
								  <b>DETAILS: </b>
								  <table border="1">
									<thead>
										<tr>
											<td>OLD VEHICLE DETAIL</td>
											<td>REQUESTED VEHICLE DETAIL</td>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>
												MAKE: '.$changeDetail['Vehicle']['make'].'<br>
												MODEL: '.$changeDetail['Vehicle']['model'].'<br>
												COLOR: '.$changeDetail['Vehicle']['color'].'<br>
												PLATE NUMBER: '.$changeDetail['Vehicle']['license_plate_number'].' '.$changeDetail['Vehicle']['license_plate_state'].'<br>
												VIN: '.$changeDetail['Vehicle']['last_4_digital_of_vin'].'<br>
											</td>
											<td>
												MAKE: '.$changeDetail['ChangeVehicleDetail']['make'].'<br>
												MODEL: '.$changeDetail['ChangeVehicleDetail']['model'].'<br>
												COLOR: '.$changeDetail['ChangeVehicleDetail']['color'].'<br>
												PLATE NUMBER: '.$changeDetail['ChangeVehicleDetail']['plate_number'].' '.$changeDetail['ChangeVehicleDetail']['state'].'<br>
												VIN: '.$changeDetail['ChangeVehicleDetail']['vin'].'<br>
											</td>
										</tr>
									</tbody>
								  </table>
								  <br>
								  Please login from your account to contact administrator.<br>
								  Thanks,
								  <br>
								  <b>Administrator</b><br>
								  '.$_SERVER['HTTP_HOST'].'
									';
						$this->Email->send($message);
						$this->loadModel('Ticket');
						$this->loadModel('Property');
						$ticket['Ticket']=array( 
												 'user_id' => $this->Auth->user('id'),
												 'recipient_id'=>$changeDetail['User']['id'],
												 'property_name' =>  $this->Property->field('name',array('id'=>$changeDetail['Vehicle']['property_id'])),
												 'subject'=>'Change Vehicle Request Rejected',
												 'email' => $changeDetail['User']['email'],
												 'phone' => $changeDetail['User']['phone'],
												 'message'=>' Your request to change the vehicle details has been rejected due to following reason : <br>
															  <br>
															  <b>REASON :</b> '.$this->request->data['ChangeVehicleDetail']['rejection_reason'].'<br>
															  <b>DETAILS: </b>
															  <table border="1">
																<thead>
																	<tr>
																		<td>OLD VEHICLE DETAIL</td>
																		<td>REQUESTED VEHICLE DETAIL</td>
																	</tr>
																</thead>
																<tbody>
																	<tr>
																		<td>
																			MAKE: '.$changeDetail['Vehicle']['make'].'<br>
																			MODEL: '.$changeDetail['Vehicle']['model'].'<br>
																			COLOR: '.$changeDetail['Vehicle']['color'].'<br>
																			PLATE NUMBER: '.$changeDetail['Vehicle']['license_plate_number'].' '.$changeDetail['Vehicle']['license_plate_state'].'<br>
																			VIN: '.$changeDetail['Vehicle']['last_4_digital_of_vin'].'<br>
																		</td>
																		<td>
																			MAKE: '.$changeDetail['ChangeVehicleDetail']['make'].'<br>
																			MODEL: '.$changeDetail['ChangeVehicleDetail']['model'].'<br>
																			COLOR: '.$changeDetail['ChangeVehicleDetail']['color'].'<br>
																			PLATE NUMBER: '.$changeDetail['ChangeVehicleDetail']['plate_number'].' '.$changeDetail['ChangeVehicleDetail']['state'].'<br>
																			VIN: '.$changeDetail['ChangeVehicleDetail']['vin'].'<br>
																		</td>
																	</tr>
																</tbody>
															  </table>',
												 'reply_admin' => 1,
												 'user_reply' => 0					 
						);
						if($this->Ticket->save($ticket,array('validate'=>false))){
								$this->Session->setFlash('Vehicle details updated and user has been informed.', 'success');
								return $this->redirect(array('controller' => 'ChangeVehicleDetails', 'action' => 'admin_view_change'));
						}
					}else{
						$this->Session->setFlash('Error Occured, Please Try Again', 'error');
					}
				}
			}
              
            
        }
    }

    public function admin_view_change() {
        if ($this->Auth->user('role_id') == 2) {
            $this->layout = "administrator";
        }
        $this->Session->write('PropertySelection', true);
        $this->ChangeVehicleDetail->recursive = 1;
        $this->paginate = array(
            'limit' => 20,
            'order' => array('ChangeVehicleDetail.changed ASC')
        );
        $viewQueries = $this->paginate('ChangeVehicleDetail');

        $this->set(compact('viewQueries'));
    }

    public function manager_view_change() {
        $this->layout = "manager";
        $this->Session->write('PropertySelection', true);
        $this->ChangeVehicleDetail->recursive = 1;
        $this->paginate = array(
            'limit' => 20,
            'order' => array('ChangeVehicleDetail.changed ASC'),
            'conditions' => array('ChangeVehicleDetail.property_id' => $this->Session->read('PropertyId'))
        );
        $viewQueries = $this->paginate('ChangeVehicleDetail');
        $this->set(compact('viewQueries'));
    }

    public function view_notifications($id = null) {

        $this->layout = 'customer';
        $changeDetail = $this->ChangeVehicleDetail->findById($id);
        $this->ChangeVehicleDetail->updateAll(array('changed' => 2), array('ChangeVehicleDetail.id' => $id));
        $this->loadModel('State');
        $states = $this->State->find('list');
        $this->set(compact('changeDetail', 'states'));
    }

    public function admin_done($id = null) {

       // $this->layout = 'administrator';
        $changeDetail = $this->ChangeVehicleDetail->findById($id);
        $this->loadModel('State');
        $states = $this->State->find('list');
       $this->loadModel('Property');
        $propertyName=$this->Property->givePropertyName($changeDetail['ChangeVehicleDetail']['property_id']);
        $this->set(compact('changeDetail', 'states','propertyName'));
    }

    public function manager_done($id = null) {

        $this->layout = 'manager';
        $changeDetail = $this->ChangeVehicleDetail->findById($id);
        $this->loadModel('State');
        $states = $this->State->find('list');
        $this->set(compact('changeDetail', 'states'));
    }
    public function all_requests(){
		$this->layout = 'customer';
		$unapprovedRequest=$this->ChangeVehicleDetail->find('all',array('conditions'=>array('ChangeVehicleDetail.user_id'=>AuthComponent::user('id'),'changed'=>0)));
		//debug($unapprovedRequest);die;
		$this->set(compact('unapprovedRequest'));
	}
    public function delete($id = null) {
		$this->ChangeVehicleDetail->id = $id;
		if (!$this->ChangeVehicleDetail->exists()) {
			throw new NotFoundException(__('Invalid vehicle'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->ChangeVehicleDetail->delete()) {
			$this->Session->setFlash(__('The vehicle has been deleted.'),'success');
		} else {
			$this->ChangeVehicleDetail->setFlash(__('The vehicle could not be deleted. Please, try again.'),'error');
		}
		return $this->redirect($this->referer());
	}
	public function admin_getAllRequests(){
		$aColumns = array('user.first_name', 'property.name', 'vehicle.owner','changeRequest.created', 'changeRequest.id','user.last_name','user.email','user.phone','vehicle.make','vehicle.model','vehicle.color','vehicle.license_plate_number','vehicle.license_plate_state','vehicle.last_4_digital_of_vin');
        $sIndexColumn = " changeRequest.id ";
        $sTable = " change_vehicle_details changeRequest ";
        $sJoinTable=' INNER JOIN users user ON user.id=changeRequest.user_id INNER JOIN properties property ON property.id=changeRequest.property_id INNER JOIN vehicles vehicle ON vehicle.id=changeRequest.vehicle_id';
        $sConditions=' changeRequest.changed=0 ';
       
        $returnArr=$this->NewDatatable->getData(array('columns'=>$aColumns,'index_column'=>$sIndexColumn,'table'=>$sTable,'join'=>$sJoinTable,'conditions'=>$sConditions));
        echo json_encode($returnArr);
        die;
	}
}

?>

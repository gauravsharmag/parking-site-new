<?php
App::uses('AppController', 'Controller');
/**
 * BillingAddresses Controller
 *
 * @property BillingAddress $BillingAddress
 * @property PaginatorComponent $Paginator
 */
class BillingAddressesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->BillingAddress->recursive = 0;
		$this->set('billingAddresses', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->BillingAddress->exists($id)) {
			throw new NotFoundException(__('Invalid billing address'));
		}
		$options = array('conditions' => array('BillingAddress.' . $this->BillingAddress->primaryKey => $id));
		$this->set('billingAddress', $this->BillingAddress->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->BillingAddress->create();
			if ($this->BillingAddress->save($this->request->data)) {
				$this->Session->setFlash(__('The billing address has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The billing address could not be saved. Please, try again.'));
			}
		}
		$users = $this->BillingAddress->User->find('list');
		$this->set(compact('users'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->BillingAddress->exists($id)) {
			throw new NotFoundException(__('Invalid billing address'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->BillingAddress->save($this->request->data)) {
				$this->Session->setFlash(__('The billing address has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The billing address could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('BillingAddress.' . $this->BillingAddress->primaryKey => $id));
			$this->request->data = $this->BillingAddress->find('first', $options);
		}
		$users = $this->BillingAddress->User->find('list');
		$this->set(compact('users'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->BillingAddress->id = $id;
		if (!$this->BillingAddress->exists()) {
			throw new NotFoundException(__('Invalid billing address'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->BillingAddress->delete()) {
			$this->Session->setFlash(__('The billing address has been deleted.'));
		} else {
			$this->Session->setFlash(__('The billing address could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->BillingAddress->recursive = 0;
		$this->set('billingAddresses', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->BillingAddress->exists($id)) {
			throw new NotFoundException(__('Invalid billing address'));
		}
		$options = array('conditions' => array('BillingAddress.' . $this->BillingAddress->primaryKey => $id));
		$this->set('billingAddress', $this->BillingAddress->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->BillingAddress->create();
			if ($this->BillingAddress->save($this->request->data)) {
				$this->Session->setFlash(__('The billing address has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The billing address could not be saved. Please, try again.'));
			}
		}
		$users = $this->BillingAddress->User->find('list');
		$this->set(compact('users'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->BillingAddress->exists($id)) {
			throw new NotFoundException(__('Invalid billing address'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->BillingAddress->save($this->request->data)) {
				$this->Session->setFlash(__('The billing address has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The billing address could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('BillingAddress.' . $this->BillingAddress->primaryKey => $id));
			$this->request->data = $this->BillingAddress->find('first', $options);
		}
		$users = $this->BillingAddress->User->find('list');
		$this->set(compact('users'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->BillingAddress->id = $id;
		if (!$this->BillingAddress->exists()) {
			throw new NotFoundException(__('Invalid billing address'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->BillingAddress->delete()) {
			$this->Session->setFlash(__('The billing address has been deleted.'));
		} else {
			$this->Session->setFlash(__('The billing address could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}

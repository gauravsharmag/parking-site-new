<?php 

	class ChatsController extends AppController{
        public $components = array('Email');    
	public function mailing() {
		$this->layout='myemails';
    
    }
	 public function beforeFilter()
    { 
        parent::beforeFilter();
        $this->Security->validatePost = false;
		//$this->Security->requireSecure();
    }
      
	public function contact_us() {
             
		$this->layout='manager';
        $user_id = $this->Auth->user('id');
		$name = $this->Auth->user('username');
        $email = $this->Auth->user('email');   
		$mobile = $this->Auth->user('phone');  
		$this->loadModel('CustomerPass');
		$this->CustomerPass->recursive=-1;
		$costumerPass= $this->CustomerPass->find('all', array('conditions' => array('CustomerPass.user_id' => $user_id),
            'fields' => 'CustomerPass.pass_id'
        ));
		$costumerPassCount= $this->CustomerPass->find('count', array('conditions' => array('CustomerPass.user_id' => $user_id),
            'fields' => 'CustomerPass.pass_id'
        ));
		
		for($i=0;$i<$costumerPassCount;$i++)
          {
		  $this->loadModel('Pass');
		$this->Pass->recursive=-1;
		$result=$this->Pass->find('all', array('conditions' => array('Pass.id' => $costumerPass[$i]['CustomerPass']['pass_id']),
            'fields' => 'Pass.name'
        ));
		
		
		  $res[]=array($result[0]['Pass']['name']=>$result[0]['Pass']['name']);
          }
		
		
		
		
        $this->set(compact('user_id','name', 'email','mobile','res'));
	}


	 public function add() {
		
        if ($this->request->is('post')) {
            $this->Chat->create();
            if ($this->Chat->save($this->request->data)) {
                $this->Session->setFlash(__('Your message has been delivered, we will contact you soon.'));
                return $this->redirect(array('controller'=>'chats','action' => 'contact_us'));
            }
            $this->Session->setFlash(__('Message not sent.'));
        }
    }
	public function sendMessage(){
		$this->layout=$this->autoRender=false;
		
		//debug($this->request);die;
		
		$this->Email->from = $_SERVER['HTTP_HOST'].'<'.$this->request->query['data']['from'].'>';
		$this->Email->to = $this->request->query['data']['to'];
		$this->Email->sendAs = 'html';
		$this->Email->subject = $this->request->query['data']['subject'];
		$message='Hi, '.$this->request->query['data']['first_name'].' '.$this->request->query['data']['last_name'].'<br><br>'.$this->request->query['data']['message'].'<br><br>Manager<br>'.$_SERVER['HTTP_HOST'];
		// $this->Email->smtpOptions=array(
		// 								'host'=>'smtp.mailgun.org',
		// 								'username'=>'postmaster@internetparkingpass.com',
		// 								'password'=>'f68b359e2d4f1380fc32dea8abeb1f5a'
		// 								);
		// $this->Email->delivery='smtp';
		$this->Email->smtpOptions=Configure::read('SMTP_SETTING');
		$this->Email->delivery='smtp';
		$result=false;
		if($this->Email->send($message)){
			$result=true;
		}
	
		
		
		echo json_encode($result);
	}
        
            
            
            
        }
?>

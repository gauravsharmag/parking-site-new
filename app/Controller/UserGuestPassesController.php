<?php
App::uses('AppController', 'Controller');
App::import('Sanitize');
class UserGuestPassesController extends AppController { 
		public $components = array(
							   'NewDatatable'
							);
		public function admin_user_guest_pass($user_id){
			$this->autoRender=$this->layout=false;
			$output=array();
			$output = $this->UserGuestPass->get_user_pass_details($user_id);      
			echo json_encode($output);
		}

		public function admin_index(){
			$this->loadModel('Property');
			$property_list=$this->Property->find('list',array('fields'=>'name'));
			$this->set(compact('property_list'));
		}
		public function admin_getPasses(){
			$propertyId=$this->request->query['property_id'];
			$fromdate=date('Y-m-d',strtotime($this->request->query['from']));
			$fromdate=$fromdate." 00:00:00";
			$todate=date('Y-m-d',strtotime($this->request->query['toDate']));
			$todate=$todate." 23:59:59";
			$aColumns = array('user.first_name','users.username','properties.name','gp.vehicle_details','gp.days','gp.paid','gp.amount','gp.created','gp.to_date','user.last_name');
			$sIndexColumn = "gp.id";
			$sTable = "user_guest_passes gp";
			$sJoinTable=' INNER JOIN users ON gp.user_id = users.id 
						  INNER JOIN properties ON gp.property_id = properties.id 
						  INNER JOIN vehicles ON gp.vehicle_id = vehicles.id 
						  INNER JOIN customer_passes cp ON gp.customer_pass_id = cp.id
						  INNER JOIN users user ON cp.user_id = user.id';
			$sConditions= " gp.property_id = ".$propertyId." AND to_date BETWEEN '".$fromdate."' AND '".$todate."' AND days>0";
			
			$returnArr=$this->NewDatatable->getData(array('columns'=>$aColumns,'index_column'=>$sIndexColumn,'table'=>$sTable,'join'=>$sJoinTable,'conditions'=>$sConditions));
			echo json_encode($returnArr);
			die;
		}
		public function admin_cost(){
			$propertyId=$this->request->query['property_id'];
			$fromdate=date('Y-m-d',strtotime($this->request->query['from']));
			$fromdate=$fromdate." 00:00:00";
			$todate=date('Y-m-d',strtotime($this->request->query['toDate']));
			$todate=$todate." 23:59:59";
			$conditions[]= " to_date BETWEEN '".$fromdate."' AND '".$todate."' ";
			$conditions['UserGuestPass.property_id']=$propertyId;
			$days = $this->UserGuestPass->field('SUM(days)',array($conditions));
			echo $days;
			die;
		}
}

?>

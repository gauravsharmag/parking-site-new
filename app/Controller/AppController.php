<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');

App::uses('HttpSocket', 'Network/Http');
/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {
    var $uses=array('User');
    public $helpers=array('Session','Html', 'Form');


	public $components = array('Security',
        'Acl','DebugKit.Toolbar','Session','Cookie',
        'Auth' => array(
            'authorize' => array(
                'Actions' => array('actionPath' => 'controllers')
            )
        )
    );

    public function beforeFilter()
    {
       	if (!$this->Auth->loggedIn()) {
			$this->Auth->authError = false;
		}
	   $this->Auth->unauthorizedRedirect=false;
       $this->Components->unload('DebugKit.Toolbar');
       $this->Auth->allow('login',
						  'register',
						  'logout',
						  'logs',
						  'admin_recurringPaymentReponse',
						  'recurring_profile_details',
						  'get_recurring_profiles',
						  'admin_cancel_recurring_profile',
						  'admin_login',
						  'admin_initDB',
						  'admin_rebuildARO',
						  'forgetPwd',
						  'resetPassword',
						  'reset',
						  'vehicle_search',
						  'single_vehicle_search',
						  'get_user',
						  'get_properties',
						  'api_login',
						  'single_vehicle_search',
						  'property_vehicle_details',
						  'vehicle_last_seen',
						  'admin_get_passes_list',
						  'admin_get_passes_detail',
						  'admin_passes_property_wise',
						  'admin_get_passes_detail_interval',
						  'admin_passes_interval',
						  'give_selected_passes_details',
						  'admin_user_guest_pass',
						  'admin_guest_credits_cron',
						  'admin_pass_cost_listing',
						  'admin_guest_pass_cost_listing',
						  'admin_get_passes_expiring_interval',
						  'admin_passes_expiry_interval',
						  'getCouponDetails',
						  'properties',
						  'cost_listing',
						  'passes_list',
						  'passes_interval',
						  'passes_detail_interval',
						  'admin_get_next_date',
						  'admin_update_pass_data',
						  'admin_deactivate_profile',
						  'all_rfids',
						  'admin_get_last_seen',
						  'admin_get_last_seen_propertywise',
						  'get_property_vehicles',
						  'get_all_rfids',
						  'get_vehicle_search',
						  'sendMessage',
						  'resend_approval_link',
						  'verify_email',
						  'properties_api',
						  'admin_get_tickets',
						  'my_vehicle_details',
						  'checkForPayment',
						  'admin_create_csv'
		); 
		 //$this->Auth->allow('*'); 
		 
		if($this->params['controller']!="apps"){
			if($this->params['action']!="admin_recurringPaymentReponse"){
				$this->Security->blackHoleCallback = 'forceSSL';
				$this->Security->requireSecure();
			//	$this-> notification();
				//debug($this->request);die;
				$this->Security->validatePost = false;
			}else{
				 $this->Security->validatePost = false;
				$this->Security->enabled = false;
				$this->Security->csrfCheck = false;
			}
			
		}else{
			 $this->Security->unlockedActions= array('vehicle_search',
													 'single_vehicle_search',
													 'get_user',
													 'get_properties',
													 'token_check',
													 'api_login',
													 'property_vehicle_details',
													 'vehicle_last_seen',
													 'properties',
													 'cost_listing',
													 'passes_list',
													 'passes_detail_interval',
													 'passes_interval',
													 'all_rfids',
													 'get_property_vehicles',
													 'properties_api'
													 );
		}
		
    }
     public function forceSSL() {
	$this->request->addDetector('ssl', array(
			'env' => 'HTTP_X_FORWARDED_PROTO',
			'value' => 'https'
		));
		if(isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == "http") {  
			return $this->redirect('https://' . env('SERVER_NAME') . $this->here); 
		}
	}
	function beforeRender() {
		$currentAction = $this->request->params['action'];
		if($this->name == 'CakeError') {
			 $this->layout = 'custom_error';  
		}elseif($this->Auth->loggedIn()){
			if($this->Auth->user('role_id')==2){
				if($currentAction!='admin_print_details'){
					$this->layout = 'administrator'; 
				}
				
			}elseif($this->Auth->user('role_id')==3){
				//$this->layout='manager';
			}
		}
	}

function notification(){
	if ($this->Auth->loggedIn()) {
            if ($this->Auth->user('role_id') == 2) {
                $sessionId = $this->Session->read('PropertyId');
                $this->loadModel('Pass');
                $this->Pass->recursive = -1;
                $passStatus = $this->Pass->find('all', array('fields' => array('name','id'), 'conditions' => array('Pass.property_id' => $sessionId)));
                $this->set(compact('passStatus'));
                
            }
			$nCount = 0;
            $mCount = 0;
            if ($this->Auth->user('role_id') == 1) {

                $this->loadModel('Ticket');
                $this->Ticket->recursive = 1;
                $messages = $this->Ticket->find('all', array('conditions' =>
                    array(
                        'Ticket.status' => 0
                    ),
                    'order' => array('Ticket.modified DESC')
                ));
                //debug($messages); die;
                $mCount = $this->Ticket->find('count', array('conditions' =>
                    array(
                        'Ticket.status' => 0,
                        'Ticket.reply_admin=0 '
                    )
                ));

                $this->loadModel('ChangeVehicleDetail');

                $notifications = $this->ChangeVehicleDetail->find('all', array('conditions'=>array('ChangeVehicleDetail.changed'=>0),
                    'order' => array('ChangeVehicleDetail.changed ASC')
                ));
                $nCount = $this->ChangeVehicleDetail->find('count', array('conditions' =>
                    array(
                        'ChangeVehicleDetail.changed' => 0
                    )
                ));
            } else if (AuthComponent::user('role_id') == 4) {
                $this->loadModel('Ticket');
                $this->Ticket->recursive = 1;
                $messages = $this->Ticket->find('all', array('conditions' =>
                    array(
                        'Ticket.status' => 0,
                        'OR'=>array('Ticket.user_id' => $this->Auth->user('id'),'Ticket.recipient_id' => $this->Auth->user('id'))
                        ),
                    'order' => array('Ticket.modified DESC')));
                $mCount = $this->Ticket->find('count', array('conditions' =>
                    array(
                        'Ticket.status' => 0,
                        'Ticket.user_reply' => 0,
                        'OR'=>array('Ticket.user_id' => $this->Auth->user('id'),'Ticket.recipient_id' => $this->Auth->user('id')
                ))));

                $this->loadModel('ChangeVehicleDetail');
                $notifications = $this->ChangeVehicleDetail->find('all', array('conditions' =>
                    array(
                        'ChangeVehicleDetail.changed' => 1,
                        'ChangeVehicleDetail.user_id' => $this->Auth->user('id'))));
                $nCount = $this->ChangeVehicleDetail->find('count', array('conditions' =>
                    array(
                        'ChangeVehicleDetail.changed' => 1,
                        'ChangeVehicleDetail.user_id' => $this->Auth->user('id')
                    )
                ));
            }
			
                $role = $this->Auth->user('role_id');
               
                $this->set(compact('mCount', 'messages', 'nCount', 'notifications', 'role'));
            
        }
         //debug($role);die;
	}
	
}

<?php
App::uses('AppController', 'Controller');
/**
 * CouponCodeUsers Controller
 *
 * @property CouponCodeUser $CouponCodeUser
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class CouponCodeUsersController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');

/**
 * admin_index method
 *
 * @return void
 */
	public function admin_index() {
		$this->CouponCodeUser->recursive = 0;
		$this->set('couponCodeUsers', $this->Paginator->paginate());
	}

/**
 * admin_view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_view($id = null) {
		if (!$this->CouponCodeUser->exists($id)) {
			throw new NotFoundException(__('Invalid coupon code user'));
		}
		$options = array('conditions' => array('CouponCodeUser.' . $this->CouponCodeUser->primaryKey => $id));
		$this->set('couponCodeUser', $this->CouponCodeUser->find('first', $options));
	}

/**
 * admin_add method
 *
 * @return void
 */
	public function admin_add() {
		if ($this->request->is('post')) {
			$this->CouponCodeUser->create();
			if ($this->CouponCodeUser->save($this->request->data)) {
				$this->Session->setFlash(__('The coupon code user has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The coupon code user could not be saved. Please, try again.'));
			}
		}
		$couponCodes = $this->CouponCodeUser->CouponCode->find('list');
		$users = $this->CouponCodeUser->User->find('list');
		$this->set(compact('couponCodes', 'users'));
	}

/**
 * admin_edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_edit($id = null) {
		if (!$this->CouponCodeUser->exists($id)) {
			throw new NotFoundException(__('Invalid coupon code user'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->CouponCodeUser->save($this->request->data)) {
				$this->Session->setFlash(__('The coupon code user has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The coupon code user could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('CouponCodeUser.' . $this->CouponCodeUser->primaryKey => $id));
			$this->request->data = $this->CouponCodeUser->find('first', $options);
		}
		$couponCodes = $this->CouponCodeUser->CouponCode->find('list');
		$users = $this->CouponCodeUser->User->find('list');
		$this->set(compact('couponCodes', 'users'));
	}

/**
 * admin_delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function admin_delete($id = null) {
		$this->CouponCodeUser->id = $id;
		if (!$this->CouponCodeUser->exists()) {
			throw new NotFoundException(__('Invalid coupon code user'));
		}
		$this->request->allowMethod('post', 'delete');
		if ($this->CouponCodeUser->delete()) {
			$this->Session->setFlash(__('The coupon code user has been deleted.'));
		} else {
			$this->Session->setFlash(__('The coupon code user could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}

<?php
App::uses('AppModel', 'Model');
class RecurringProfile extends AppModel{
public function get_pass_recurring_profile($customerPassId){
		$aColumns = array('rp.recurring_profile_id','rp.status','rp.created');
        $sIndexColumn = "rp.id";
        $sTable = "recurring_profiles rp";
        $sJoinTable=' ';
        $sConditions= " rp.customer_pass_id = ".$customerPassId." ";
        $outs=$this->recurringProfiles(array('columns'=>$aColumns,'index_column'=>$sIndexColumn,'table'=>$sTable,'join'=>$sJoinTable,'conditions'=>$sConditions));
        return $outs;
	}
	public function recurringProfiles($config=array()){
		if(!isset($config['columns']) || !isset($config['index_column']) || !isset($config['table'])){
				return array();
		}
		$aColumns=$config['columns'];
		$sIndexColumn=$config['index_column'];
		$sTable=$config['table'];
		$sJoinTable=isset($config['join'])? $config['join'] : '';
		$sCondition=isset($config['conditions'])? $config['conditions'] : '';
        App::uses('ConnectionManager', 'Model');
        $dataSource = ConnectionManager::getDataSource('default');
        $gaSql['user']       = $dataSource->config['login'];
        $gaSql['password']   = $dataSource->config['password'];
        $gaSql['db']         = $dataSource->config['database'];
        $gaSql['server']     = $dataSource->config['host'];
       
        function fatal_error ( $sErrorMessage = '' ){
            header( $_SERVER['SERVER_PROTOCOL'] .' 500 Internal Server Error' );
            die( $sErrorMessage );
        }
        /*
         * MySQL connection
        */
        if ( ! $gaSql['link'] = mysqli_connect( $gaSql['server'], $gaSql['user'], $gaSql['password'],$gaSql['db']) ){
            fatal_error( 'Could not open connection to server' );
        }
        
        if ( ! mysqli_select_db($gaSql['link'],$gaSql['db']) ){
            fatal_error( 'Could not select database ' );
        }
         
         
        /*
         * Paging
        */
        $sLimit = "";
        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' ){
            $sLimit = "LIMIT ".intval( $_GET['iDisplayStart'] ).", ".
            intval( $_GET['iDisplayLength'] );
        }
        /*
         * Ordering
        */
        $sOrder = "";
        if ( isset( $_GET['iSortCol_0'] ) ){
            $sOrder = "ORDER BY  ";
            for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ ){
                if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" ){
                    $sOrder .= $aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
                        ($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
                }
            }
            $sOrder = substr_replace( $sOrder, "", -2 );
            if ( $sOrder == "ORDER BY" ){
                $sOrder = "";
            }
        }
        /*
         * Filtering
        * NOTE this does not match the built-in DataTables filtering which does it
        * word by word on any field. It's possible to do here, but concerned about efficiency
        * on very large tables, and MySQL's regex functionality is very limited
        */
        $sWhere = "";
        if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" ){
            $sWhere = "WHERE (";
            for ( $i=0 ; $i<count($aColumns) ; $i++ ){
                $sWhere .= $aColumns[$i]." LIKE '%".mysqli_real_escape_string($gaSql['link'], $_GET['sSearch'] )."%' OR ";
            }
            $sWhere = substr_replace( $sWhere, "", -3 );
            $sWhere .= ')';
        }
        for ( $i=0 ; $i<count($aColumns) ; $i++ ){
            if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' ){
                if ( $sWhere == "" ){
                    $sWhere = "WHERE ";
                } else{
                    $sWhere .= " AND ";
                }
                $sWhere .= $aColumns[$i]." LIKE '%".mysqli_real_escape_string($gaSql['link'],$_GET['sSearch_'.$i])."%' ";
            }
        }
        if($sCondition!=''){
            if ( $sWhere == "" ){
                $sWhere="WHERE ".$sCondition." ";
            }else{
                $sWhere .=" AND ".$sCondition." ";
            }
        }
        /*
         * SQL queries
        * Get data to display 
        */
        $sQuery = "
    SELECT SQL_CALC_FOUND_ROWS ".str_replace(" , ", " ", implode(", ", $aColumns))."
            FROM   $sTable $sJoinTable 
            $sWhere
            $sOrder
            $sLimit
            ";
       //debug($sQuery);die;
        $rResult = mysqli_query( $gaSql['link'],$sQuery) or fatal_error( 'MySQL Error Here 1: ' . mysqli_errno($gaSql['link']) );
         
        /* Data set length after filtering */
        $sQuery = "
    SELECT FOUND_ROWS()
";
        $rResultFilterTotal =mysqli_query( $gaSql['link'],$sQuery)  or fatal_error( 'MySQL Error Here 2: ' . mysqli_errno($gaSql['link']) );
        $aResultFilterTotal = mysqli_fetch_array($rResultFilterTotal);
        $iFilteredTotal = $aResultFilterTotal[0];
         
        /* Total data set length */
        $sQuery = "
    SELECT COUNT(".$sIndexColumn.")
            FROM   $sTable $sJoinTable
            ";
        $rResultTotal = mysqli_query( $gaSql['link'],$sQuery) or fatal_error( 'MySQL Error Here 3: ' . mysqli_errno($gaSql['link']) );
        $aResultTotal = mysqli_fetch_array($rResultTotal);
        $iTotal = $aResultTotal[0];
         
        /*
         * Output
        */
        $output = array(
                "sEcho" => intval($_GET['sEcho']),
                "iTotalRecords" => $iTotal,
                "iTotalDisplayRecords" => $iFilteredTotal,
                "aaData" => array()
        );
        
        while ( $aRow = mysqli_fetch_array( $rResult ) )
        {
            $row = array();
            for ( $i=0 ; $i<count($aColumns); $i++ )
            {  
					if(count(explode('.',$aColumns[$i]))==2){
						$aColumns[$i]=explode('.',$aColumns[$i]);
						$aColumns[$i]=$aColumns[$i][1];
					}  
                   if ( $aColumns[$i] != ' ' ){
						if($aColumns[$i]=="created"){
							$row[] = date("m/d/Y H:i:s", strtotime($aRow[ $aColumns[$i] ]));
						}elseif($aColumns[$i]=="recurring_profile_id"){
							$row[]='<button id="'.$aRow[ $aColumns[$i] ].'" class="btn blue btn-xs" type="button" onclick="getRecurringProfileDetail(this);">'.$aRow[ $aColumns[$i] ].'</button>';
						}else {
							$row[] = $aRow[ $aColumns[$i] ];
						}
					}
            }
            $output['aaData'][] = $row;
        }
        return $output;
	}
public function get_recurring_profile_detail($recurringProfileID){
		//debug($recurringProfileID);die;
		$aColumns = array('rp.recurring_profile_id','rp.paypal_transaction_id','rp.message','rp.created','rp.status');
        $sIndexColumn = "rp.id";
        $sTable = "recurring_profiles rp";
        $sJoinTable=' ';
        $sConditions= " rp.recurring_profile_id = '".$recurringProfileID."' ";
        $outs=$this->recurringProfiles2(array('columns'=>$aColumns,'index_column'=>$sIndexColumn,'table'=>$sTable,'join'=>$sJoinTable,'conditions'=>$sConditions));
        return $outs;
	}
public function recurringProfiles2($config=array()){
		if(!isset($config['columns']) || !isset($config['index_column']) || !isset($config['table'])){
				return array();
		}
		$aColumns=$config['columns'];
		$sIndexColumn=$config['index_column'];
		$sTable=$config['table'];
		$sJoinTable=isset($config['join'])? $config['join'] : '';
		$sCondition=isset($config['conditions'])? $config['conditions'] : '';
        App::uses('ConnectionManager', 'Model');
        $dataSource = ConnectionManager::getDataSource('default');
        $gaSql['user']       = $dataSource->config['login'];
        $gaSql['password']   = $dataSource->config['password'];
        $gaSql['db']         = $dataSource->config['database'];
        $gaSql['server']     = $dataSource->config['host'];
       
        function fatal_error ( $sErrorMessage = '' ){
            header( $_SERVER['SERVER_PROTOCOL'] .' 500 Internal Server Error' );
            die( $sErrorMessage );
        }
        /*
         * MySQL connection
        */
        if ( ! $gaSql['link'] = mysqli_connect( $gaSql['server'], $gaSql['user'], $gaSql['password'],$gaSql['db']) ){
            fatal_error( 'Could not open connection to server' );
        }
        
        if ( ! mysqli_select_db($gaSql['link'],$gaSql['db']) ){
            fatal_error( 'Could not select database ' );
        }
         
         
        /*
         * Paging
        */
        $sLimit = "";
        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' ){
            $sLimit = "LIMIT ".intval( $_GET['iDisplayStart'] ).", ".
            intval( $_GET['iDisplayLength'] );
        }
        /*
         * Ordering
        */
        $sOrder = "";
        if ( isset( $_GET['iSortCol_0'] ) ){
            $sOrder = "ORDER BY  ";
            for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ ){
                if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" ){
                    $sOrder .= $aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
                        ($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
                }
            }
            $sOrder = substr_replace( $sOrder, "", -2 );
            if ( $sOrder == "ORDER BY" ){
                $sOrder = "";
            }
        }
        /*
         * Filtering
        * NOTE this does not match the built-in DataTables filtering which does it
        * word by word on any field. It's possible to do here, but concerned about efficiency
        * on very large tables, and MySQL's regex functionality is very limited
        */
        $sWhere = "";
        if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" ){
            $sWhere = "WHERE (";
            for ( $i=0 ; $i<count($aColumns) ; $i++ ){
                $sWhere .= $aColumns[$i]." LIKE '%".mysqli_real_escape_string($gaSql['link'], $_GET['sSearch'] )."%' OR ";
            }
            $sWhere = substr_replace( $sWhere, "", -3 );
            $sWhere .= ')';
        }
        for ( $i=0 ; $i<count($aColumns) ; $i++ ){
            if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' ){
                if ( $sWhere == "" ){
                    $sWhere = "WHERE ";
                } else{
                    $sWhere .= " AND ";
                }
                $sWhere .= $aColumns[$i]." LIKE '%".mysqli_real_escape_string($gaSql['link'],$_GET['sSearch_'.$i])."%' ";
            }
        }
        if($sCondition!=''){
            if ( $sWhere == "" ){
                $sWhere="WHERE ".$sCondition." ";
            }else{
                $sWhere .=" AND ".$sCondition." ";
            }
        }
        /*
         * SQL queries
        * Get data to display 
        */
        $sQuery = "
    SELECT SQL_CALC_FOUND_ROWS ".str_replace(" , ", " ", implode(", ", $aColumns))."
            FROM   $sTable $sJoinTable 
            $sWhere
            $sOrder
            $sLimit
            ";
       //debug($sQuery);die;
        $rResult = mysqli_query( $gaSql['link'],$sQuery) or fatal_error( 'MySQL Error Here 1: ' . mysqli_errno($gaSql['link']) );
         
        /* Data set length after filtering */
        $sQuery = "
    SELECT FOUND_ROWS()
";
        $rResultFilterTotal =mysqli_query( $gaSql['link'],$sQuery)  or fatal_error( 'MySQL Error Here 2: ' . mysqli_errno($gaSql['link']) );
        $aResultFilterTotal = mysqli_fetch_array($rResultFilterTotal);
        $iFilteredTotal = $aResultFilterTotal[0];
         
        /* Total data set length */
        $sQuery = "
    SELECT COUNT(".$sIndexColumn.")
            FROM   $sTable $sJoinTable
            ";
        $rResultTotal = mysqli_query( $gaSql['link'],$sQuery) or fatal_error( 'MySQL Error Here 3: ' . mysqli_errno($gaSql['link']) );
        $aResultTotal = mysqli_fetch_array($rResultTotal);
        $iTotal = $aResultTotal[0];
         
        /*
         * Output
        */
        $output = array(
                "sEcho" => intval($_GET['sEcho']),
                "iTotalRecords" => $iTotal,
                "iTotalDisplayRecords" => $iFilteredTotal,
                "aaData" => array()
        );
        
        while ( $aRow = mysqli_fetch_array( $rResult ) )
        {
            $row = array();
            for ( $i=0 ; $i<count($aColumns); $i++ )
            {  
					if(count(explode('.',$aColumns[$i]))==2){
						$aColumns[$i]=explode('.',$aColumns[$i]);
						$aColumns[$i]=$aColumns[$i][1];
					}  
                   if ( $aColumns[$i] != ' ' ){
						if($aColumns[$i]=="created"){
							$row[] = date("m/d/Y H:i:s", strtotime($aRow[ $aColumns[$i] ]));
						}else {
							$row[] = $aRow[ $aColumns[$i] ];
						}
					}
            }
            $output['aaData'][] = $row;
        }
        return $output;
	}
	
}

?>

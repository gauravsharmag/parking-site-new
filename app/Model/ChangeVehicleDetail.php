<?php

App::uses('AppModel', 'Model');

/**
 * Vehicle Model
 *
 * @property Property $Property
 * @property CustomerPass $CustomerPass
 * @property User $User

 */
class ChangeVehicleDetail extends AppModel {

    public $belongsTo = array(
        'Vehicle' => array(
            'className' => 'Vehicle',
            'foreignKey' => 'vehicle_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'user_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );
    
    
    var $validate=array(
        'vehicle_name'=>array(
            'rule' => array('custom','/^[a-zA-Z ]*$/'),
            'message' => 'Only alphabets allowed',
            'required'=>true,
	    'allowEmpty'=>false
        ),
        'make'=>array('Alpha numeric'=>array( 'rule' => array('custom','/^[a-zA-Z 0-9 ]*$/'),'allowEmpty'=>false,'required'=>true,'message'=>'Only Alpha Numeric data allowed'),
                      'Minimum Length'=>array('rule'=>array('minLength',2),'message'=>'Minimum 2 character required')
                      ),
        'model'=>array('Alpha numeric'=>array('rule' => array('custom','/^[a-zA-Z 0-9]*$/'),'allowEmpty'=>false,'required'=>true,'message'=>'Only Alpha Numeric data allowed'),
                      'Minimum Length'=>array('rule'=>array('minLength',2),'message'=>'Minimum 2 character required')
                      ),
        'color'=>array('Alpha numeric'=>array('rule' => array('custom','/^[a-zA-Z 0-9]*$/'),'allowEmpty'=>false,'required'=>true,'message'=>'Only Alpha Numeric data allowed'),
                        'Minimum Length'=>array('rule'=>array('minLength',2),'message'=>'Minimum 2 character required')
                      ),
        
        'vin'=>array('Numeric'=>array('rule'=>'numeric','allowEmpty'=>false,'required'=>true,'message'=>'Only numeric data'),
            'Minimum Length'=>array('rule'=>array('minLength',2),'message'=>'Minimum 2 characters required'),
            'Maximum Length'=>array('rule'=>array('maxLength',4),'message'=>'Maximum 4 characters required')
                      ),
        'plate_number'=>array('Alpha numeric'=>array('rule' => array('custom','/^[a-zA-Z 0-9]*$/'),'allowEmpty'=>false,'required'=>true,'message'=>'Only Alpha Numeric data allowed'),
                      'Minimum Length'=>array('rule'=>array('minLength',2),'message'=>'Minimum 2 character required')
                      )
    );

}

?>

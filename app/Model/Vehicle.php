<?php
App::uses('AppModel', 'Model');
/**
 * Vehicle Model
 *
 * @property Property $Property
 * @property CustomerPass $CustomerPass
 * @property User $User

 */
class Vehicle extends AppModel {

	public $virtualFields = array('vehicle_info' => 'CONCAT(Vehicle.owner,"-", Vehicle.make,"-",Vehicle.model,"-",Vehicle.license_plate_number,"-",Vehicle.last_4_digital_of_vin)');
	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Property' => array(
			'className' => 'Property',
			'foreignKey' => 'property_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'CustomerPass' => array(
			'className' => 'CustomerPass',
			'foreignKey' => 'vehicle_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);
    var $validate=array(
		'owner'=>array(
            'rule' => array('custom','/^[a-zA-Z ]*$/'),
            'message' => 'Only alphabets allowed in owner',
            'required'=>true,
			'allowEmpty'=>false 

        ),
        'make'=>array('Alpha numeric'=>array( 'rule' => array('custom','/^[a-zA-Z 0-9]*$/'),
											  'allowEmpty'=>false,
											  'required'=>true,'message'=>'Only Alpha Numeric data allowed in make'),
                      'Minimum Length'=>array('rule'=>array('minLength',2),'message'=>'Minimum 2 character required in make')
                      ),
        'model'=>array('Alpha numeric'=>array('rule' => array('custom','/^[a-zA-Z 0-9]*$/'),
											  'allowEmpty'=>false,'required'=>true,
											  'message'=>'Only Alpha Numeric data allowed in model'),
                      'Minimum Length'=>array('rule'=>array('minLength',1),'message'=>'Minimum 1 character required in model')
                      ),
        'color'=>array('Alpha numeric'=>array('rule' => array('custom','/^[a-zA-Z 0-9]*$/'),
											  'allowEmpty'=>false,'required'=>true,'message'=>'Only Alpha Numeric data allowed in color'),
                        'Minimum Length'=>array('rule'=>array('minLength',2),'message'=>'Minimum 2 character required in color')
                      ),
        'license_plate_number'=>array('Alphanumeric'=>array('rule'=>'alphaNumeric',
															'allowEmpty'=>false,'required'=>true,'message'=>'Only numeric data in plate number'),
                         'Minimum Length'=>array('rule'=>array('minLength',2),'message'=>'Minimum 2 character required'),
                         'Already Registered'=>array('rule'=>'isRegistered','message'=>'This vehicle is already registered in this property')
                      ),
        'last_4_digital_of_vin'=>array('Numeric'=>array('rule'=>'numeric','allowEmpty'=>false,'required'=>true,'message'=>'Only numeric data in vin'),
            'Minimum Length'=>array('rule'=>array('minLength',2),'message'=>'Minimum 2 characters required in vin'),
            'Maximum Length'=>array('rule'=>array('maxLength',4),'message'=>'Maximum 4 characters required in vin')
                      ),
        'license_plate_state'=>array(
            'rule'    => array('minLength', '2'),
            'message' => 'Minimum 2 characters required in plate state',
            'required'=>true
        ),
    );
        var $validateAdmin=array(
		'owner'=>array(
            'rule' => array('custom','/^[a-zA-Z ]*$/'),
            'message' => 'Only alphabets allowed in owner',
            'required'=>true,
			'allowEmpty'=>false

        ),
        'make'=>array('Alpha numeric'=>array( 'rule' => array('custom','/^[a-zA-Z 0-9]*$/'),'allowEmpty'=>false,'required'=>true,'message'=>'Only Alpha Numeric data allowed'),
                      'Minimum Length'=>array('rule'=>array('minLength',2),'message'=>'Minimum 2 character required')
                      ),
        'model'=>array('Alpha numeric'=>array('rule' => array('custom','/^[a-zA-Z 0-9]*$/'),'allowEmpty'=>false,'required'=>true,'message'=>'Only Alpha Numeric data allowed'),
                      'Minimum Length'=>array('rule'=>array('minLength',1),'message'=>'Minimum 1 character required')
                      ),
        'color'=>array('Alpha numeric'=>array('rule' => array('custom','/^[a-zA-Z 0-9]*$/'),'allowEmpty'=>false,'required'=>true,'message'=>'Only Alpha Numeric data allowed'),
                        'Minimum Length'=>array('rule'=>array('minLength',2),'message'=>'Minimum 2 character required')
                      ),
        'license_plate_number'=>array('Alphanumeric'=>array('rule'=>'alphaNumeric','allowEmpty'=>false,'required'=>true,'message'=>'Only numeric data'),
                         'Minimum Length'=>array('rule'=>array('minLength',2),'message'=>'Minimum 2 character required'),
                         'Already Registered'=>array('rule'=>'isRegisteredAdmin','message'=>'This vehicle is already registered in this property')
                      ),
        'last_4_digital_of_vin'=>array(
								'Numeric'=>array('rule'=>'numeric','allowEmpty'=>false,'required'=>true,'message'=>'Only numeric data'),
								'Minimum Length'=>array('rule'=>array('minLength',4),'message'=>'Minimum 4 characters required'),
								'Maximum Length'=>array('rule'=>array('maxLength',5),'message'=>'Maximum 5 characters required')
                      ),
        'license_plate_state'=>array(
            'rule'    => array('minLength', '2'),
            'message' => 'Minimum 2 characters required',
            'required'=>true
        ),
    );
   function isRegisteredAdmin(){
		if(isset($this->data['Vehicle']['id'])){
			$i=$this->hasAny(array('license_plate_number'=>$this->data['Vehicle']['license_plate_number'],'license_plate_state'=>$this->data['Vehicle']['license_plate_state'],'property_id'=>$this->data['Vehicle']['property_id'],'id !='=>$this->data['Vehicle']['id'],'vehicle_archived'=>0));
			if($i){
				$this->invalidate('license_plate_state','Vehicle with this plate number and state is already present');
				return false;
			}else{
				return true;
			}
		}
		else{
			$i=$this->hasAny(array('license_plate_number'=>$this->data['Vehicle']['license_plate_number'],'license_plate_state'=>$this->data['Vehicle']['license_plate_state'],'property_id'=>$this->data['Vehicle']['property_id'],'vehicle_archived'=>0)); 
			if($i){
				$this->invalidate('license_plate_state','Vehicle with this plate number and state is already present');
				return false;
			}else{
				return true;
			}
		}
	}
    function isRegistered(){
		if(isset($this->data['Vehicle']['id'])){
			$i=$this->hasAny(array('license_plate_number'=>$this->data['Vehicle']['license_plate_number'],'license_plate_state'=>$this->data['Vehicle']['license_plate_state'],'property_id'=>CakeSession::read('PropertyId'),'id !='=>$this->data['Vehicle']['id'],'vehicle_archived'=>0));
			if($i){
				$this->invalidate('license_plate_state','Vehicle with this plate number and state is already present');
				return false;
			}else{
				return true;
			}
		}
		else{
			$i=$this->hasAny(array('license_plate_number'=>$this->data['Vehicle']['license_plate_number'],'license_plate_state'=>$this->data['Vehicle']['license_plate_state'],'property_id'=>CakeSession::read('PropertyId'),'vehicle_archived'=>0));
			if($i){
				$this->invalidate('license_plate_state','Vehicle with this plate number and state is already present');
				return false;
			}else{
				return true;
			}
		}
	}
	public function updateValidations()
	{
				$this->validator()->remove('owner');
				$this->validator()->remove('make');
				$this->validator()->remove('model');
				$this->validator()->remove('color');
				$this->validator()->remove('license_plate_number');
				$this->validator()->remove('last_4_digital_of_vin');
				$this->validator()->remove('license_plate_state');
	}
	public function getVehicleData(){
		$aColumns = array('owner','properties.name','users.username','make','model','color','license_plate_number','license_plate_state','last_4_digital_of_vin','customer_pass_id','v.id');
         
        /* Indexed column (used for fast and accurate table cardinality) */
        $sIndexColumn = "v.id";
         
        /* DB table to use */ 
        $sTable = "vehicles v";
        $sJoinTable=' LEFT JOIN users ON v.user_id = users.id LEFT JOIN properties ON v.property_id = properties.id ';
        $sConditions=' v.vehicle_archived=0 ';
        $outs=$this->simplifyDataTableRFIDCorrected(array('columns'=>$aColumns,'index_column'=>$sIndexColumn,'table'=>$sTable,'join'=>$sJoinTable,'conditions'=>$sConditions));
        return $outs;
	}
	public function getManagerAllVehicleData(){
		$aColumns = array('users.first_name','users.address_line_1','make','model','color','license_plate_number','license_plate_state','last_4_digital_of_vin','users.last_name','users.address_line_2','users.city','users.state','users.email','users.phone');
         
        /* Indexed column (used for fast and accurate table cardinality) */
        $sIndexColumn = "v.id";
         
        /* DB table to use */ 
        $sTable = "vehicles v";
        $sJoinTable=' LEFT JOIN users ON v.user_id = users.id AND v.vehicle_archived=0 ';
        $sConditions=' property_id = '.CakeSession::read('PropertyId').' ';
        $outs=$this->simplifyDataTableRFID(array('columns'=>$aColumns,'index_column'=>$sIndexColumn,'table'=>$sTable,'join'=>$sJoinTable,'conditions'=>$sConditions));
        return $outs;
	}
	public function getManagerOtherVehicleData(){
		$aColumns = array('users.first_name','users.address_line_1','make','model','color','license_plate_number','license_plate_state','last_4_digital_of_vin','users.last_name','users.address_line_2','users.city','users.state','users.email','users.phone');
         
        /* Indexed column (used for fast and accurate table cardinality) */
        $sIndexColumn = "v.id";
         
        /* DB table to use */ 
        $sTable = "vehicles v";
        $sJoinTable=' LEFT JOIN users ON v.user_id = users.id ';
        $sConditions=' property_id = '.CakeSession::read('PropertyId').' and customer_pass_id IS NULL AND v.vehicle_archived=0 ';
        $outs=$this->simplifyDataTableRFID(array('columns'=>$aColumns,'index_column'=>$sIndexColumn,'table'=>$sTable,'join'=>$sJoinTable,'conditions'=>$sConditions));
        return $outs;
	}
/*******************************************************************
 * Adminstrator Vehicle DATA
 */	
	public function getAllVehicle(){
		$dt = new DateTime();
		$currentDateTime= $dt->format('Y-m-d H:i:s');
		$aColumns = array('v.make','v.model','v.color','v.license_plate_number','v.license_plate_state','cp.RFID_tag_number','cp.pass_valid_upto','cp.membership_vaild_upto ');
        $sIndexColumn = "v.id";
        $sTable = "vehicles v";
        $sJoinTable=' OUTER JOIN customer_passes cp ON v.customer_pass_id = cp.id ';
        $sConditions= " v.vehicle_archived=0 ";
        $outs=$this->getVehicleSearchResult(array('columns'=>$aColumns,'index_column'=>$sIndexColumn,'table'=>$sTable,'join'=>$sJoinTable,'conditions'=>$sConditions));
        return $outs;
	}
/*******************************************************************
 * Super Admin Property Wise Vehicle DATA
 */
	public function adminPropertyVehicles($propertyId){
		$aColumns = array('u.username','v.make','v.model','v.color','v.license_plate_number','v.license_plate_state','v.last_4_digital_of_vin','v.id');
        $sIndexColumn = "v.id";
        $sTable = "vehicles v";
        $sJoinTable=' LEFT JOIN users u ON v.user_id = u.id ';
        $sConditions= " v.property_id = ".$propertyId." AND v.vehicle_archived=0 ";
        $outs=$this->getVehicleSearchResult(array('columns'=>$aColumns,'index_column'=>$sIndexColumn,'table'=>$sTable,'join'=>$sJoinTable,'conditions'=>$sConditions));
        return $outs;
	}


	public function getVehicleSearchResult($config=array()){
		if(!isset($config['columns']) || !isset($config['index_column']) || !isset($config['table'])){
				return array();
		}
		$aColumns=$config['columns'];
		$sIndexColumn=$config['index_column'];
		$sTable=$config['table'];
		$sJoinTable=isset($config['join'])? $config['join'] : '';
		$sCondition=isset($config['conditions'])? $config['conditions'] : '';
        App::uses('ConnectionManager', 'Model');
        $dataSource = ConnectionManager::getDataSource('default');
        $gaSql['user']       = $dataSource->config['login'];
        $gaSql['password']   = $dataSource->config['password'];
        $gaSql['db']         = $dataSource->config['database'];
        $gaSql['server']     = $dataSource->config['host'];
    
        function fatal_error ( $sErrorMessage = '' ){
            header( $_SERVER['SERVER_PROTOCOL'] .' 500 Internal Server Error' );
            die( $sErrorMessage );
        }
        /*
         * MySQL connection
        */
        if ( ! $gaSql['link'] = mysqli_connect( $gaSql['server'], $gaSql['user'], $gaSql['password'],$gaSql['db']) ){
            fatal_error( 'Could not open connection to server' );
        }
        
        if ( ! mysqli_select_db($gaSql['link'],$gaSql['db']) ){
            fatal_error( 'Could not select database ' );
        }
         
         
        /*
         * Paging
        */
        $sLimit = "";
        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' ){
            $sLimit = "LIMIT ".intval( $_GET['iDisplayStart'] ).", ".
            intval( $_GET['iDisplayLength'] );
        }
        /*
         * Ordering
        */  
        $sOrder = "";
        if ( isset( $_GET['iSortCol_0'] ) ){
            $sOrder = "ORDER BY  ";
            for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ ){
                if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" ){
                    $sOrder .= $aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
                        ($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
                }
            }
            $sOrder = substr_replace( $sOrder, "", -2 );
            if ( $sOrder == "ORDER BY" ){
                $sOrder = "";
            }
        }
        /*
         * Filtering
        * NOTE this does not match the built-in DataTables filtering which does it
        * word by word on any field. It's possible to do here, but concerned about efficiency
        * on very large tables, and MySQL's regex functionality is very limited
        */
        $sWhere = "";
        if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" ){
            $sWhere = "WHERE (";
            for ( $i=0 ; $i<count($aColumns) ; $i++ ){
                $sWhere .= $aColumns[$i]." LIKE '%".mysqli_real_escape_string($gaSql['link'], $_GET['sSearch'] )."%' OR ";
            }
            $sWhere = substr_replace( $sWhere, "", -3 );
            $sWhere .= ')';
        }
        for ( $i=0 ; $i<count($aColumns) ; $i++ ){
            if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' ){
                if ( $sWhere == "" ){
                    $sWhere = "WHERE ";
                } else{
                    $sWhere .= " AND ";
                }
                $sWhere .= $aColumns[$i]." LIKE '%".mysqli_real_escape_string($gaSql['link'],$_GET['sSearch_'.$i])."%' ";
            }
        }
        if($sCondition!=''){
            if ( $sWhere == "" ){
                $sWhere="WHERE ".$sCondition." ";
            }else{
                $sWhere .=" AND ".$sCondition." ";
            }
        }
        /*
         * SQL queries
        * Get data to display 
        */
        $sQuery = "
    SELECT SQL_CALC_FOUND_ROWS ".str_replace(" , ", " ", implode(", ", $aColumns))."
            FROM   $sTable $sJoinTable 
            $sWhere
            $sOrder
            $sLimit
            ";
       //debug($sQuery);die;
        $rResult = mysqli_query( $gaSql['link'],$sQuery) or fatal_error( 'MySQL Error Here 1: ' . mysqli_errno($gaSql['link']) );
         
        /* Data set length after filtering */
        $sQuery = "
    SELECT FOUND_ROWS()
";
        $rResultFilterTotal =mysqli_query( $gaSql['link'],$sQuery)  or fatal_error( 'MySQL Error Here 2: ' . mysqli_errno($gaSql['link']) );
        $aResultFilterTotal = mysqli_fetch_array($rResultFilterTotal);
        $iFilteredTotal = $aResultFilterTotal[0];
         
        /* Total data set length */
        $sQuery = "
    SELECT COUNT(".$sIndexColumn.")
            FROM   $sTable $sJoinTable
            ";
        $rResultTotal = mysqli_query( $gaSql['link'],$sQuery) or fatal_error( 'MySQL Error Here 3: ' . mysqli_errno($gaSql['link']) );
        $aResultTotal = mysqli_fetch_array($rResultTotal);
        $iTotal = $aResultTotal[0];
         
        /*
         * Output
        */
        $output = array(
                "sEcho" => intval($_GET['sEcho']),
                "iTotalRecords" => $iTotal,
                "iTotalDisplayRecords" => $iFilteredTotal,
                "aaData" => array()
        );
        
        while ( $aRow = mysqli_fetch_array( $rResult ) )
        {
            $row = array();
            for ( $i=0 ; $i<count($aColumns); $i++ )
            {  
					if(count(explode('.',$aColumns[$i]))==2){
						$aColumns[$i]=explode('.',$aColumns[$i]);
						$aColumns[$i]=$aColumns[$i][1];
					}  
					if ( $aColumns[$i] != ' ' ){
						if($aColumns[$i]=="pass_valid_upto"||$aColumns[$i]=="membership_vaild_upto"){
							if(is_null($aRow[ $aColumns[$i] ])){
								$row[] = $aRow[ $aColumns[$i] ];
								}else{
									$dt = new DateTime();
									$currentDateTime= $dt->format('Y-m-d H:i:s');
									if($aRow[ $aColumns[$i] ]>$currentDateTime){	
										$row[] = date("m/d/Y H:i:s", strtotime($aRow[ $aColumns[$i] ])).' '.'<span class="label label-sm label-danger">Active</span>';
									}elseif($aRow[ $aColumns[$i] ]<$currentDateTime){
										$row[] = date("m/d/Y H:i:s", strtotime($aRow[ $aColumns[$i] ])).' '.'<span class="label label-sm label-warning">Expired</span>';
									}else{
									$row[] = $aRow[ $aColumns[$i] ];
									}
								}
							}
							elseif($aColumns[$i]=="RFID_tag_number"){
								if(is_null($aRow[ $aColumns[$i]])){
									$row[]="RFID Not Assigned";
								}
							}elseif($aColumns[$i]=='id'){
								$row[] = "<a class='btn green btn-xs green-stripe' href='/admin/Vehicles/complete_vehicle_detail/".$aRow[ $aColumns[$i] ]."'>View Details</a>";
							}
							else {
								$row[] = $aRow[ $aColumns[$i] ];
							}
				 }
            }
            $output['aaData'][] = $row;
        }
        return $output;
	}
	/***********************************************
 * For RFID 
 */

	public function simplifyDataTableRFIDCorrected($config=array()){
		if(!isset($config['columns']) || !isset($config['index_column']) || !isset($config['table'])){
				return array();
		}
		$aColumns=$config['columns'];
		$sIndexColumn=$config['index_column'];
		$sTable=$config['table'];
		/* optional join */
		$sJoinTable=isset($config['join'])? $config['join'] : '';
        /* optional conditions */
		$sCondition=isset($config['conditions'])? $config['conditions'] : '';
        App::uses('ConnectionManager', 'Model');
        $dataSource = ConnectionManager::getDataSource('default');
         
        /* Database connection information */
        $gaSql['user']       = $dataSource->config['login'];
        $gaSql['password']   = $dataSource->config['password'];
        $gaSql['db']         = $dataSource->config['database'];
        $gaSql['server']     = $dataSource->config['host'];
         
         
        /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
         * If you just want to use the basic configuration for DataTables with PHP server-side, there is
        * no need to edit below this line
        */
         
        /*
         * Local functions
        */
        function fatal_error ( $sErrorMessage = '' ){
            header( $_SERVER['SERVER_PROTOCOL'] .' 500 Internal Server Error' );
            die( $sErrorMessage );
        }
        /*
         * MySQL connection
        */
        if ( ! $gaSql['link'] = mysqli_connect( $gaSql['server'], $gaSql['user'], $gaSql['password'],$gaSql['db']) ){
            fatal_error( 'Could not open connection to server' );
        }
        
        if ( ! mysqli_select_db($gaSql['link'],$gaSql['db']) ){
            fatal_error( 'Could not select database ' );
        }
         
         
        /*
         * Paging
        */
        $sLimit = "";
        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' ){
            $sLimit = "LIMIT ".intval( $_GET['iDisplayStart'] ).", ".
            intval( $_GET['iDisplayLength'] );
        }
        /*
         * Ordering
        */
        $sOrder = "";
        if ( isset( $_GET['iSortCol_0'] ) ){
            $sOrder = "ORDER BY  ";
            for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ ){
                if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" ){
                    $sOrder .= $aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
                        ($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
                }
            }
            $sOrder = substr_replace( $sOrder, "", -2 );
            if ( $sOrder == "ORDER BY" ){
                $sOrder = "";
            }
        }
        /*
         * Filtering
        * NOTE this does not match the built-in DataTables filtering which does it
        * word by word on any field. It's possible to do here, but concerned about efficiency
        * on very large tables, and MySQL's regex functionality is very limited
        */
        $sWhere = "";
        if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" ){
            $sWhere = "WHERE (";
            for ( $i=0 ; $i<count($aColumns) ; $i++ ){
                $sWhere .= $aColumns[$i]." LIKE '%".mysqli_real_escape_string($gaSql['link'], $_GET['sSearch'] )."%' OR ";
            }
            $sWhere = substr_replace( $sWhere, "", -3 );
            $sWhere .= ')';
        }
        for ( $i=0 ; $i<count($aColumns) ; $i++ ){
            if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' ){
                if ( $sWhere == "" ){
                    $sWhere = "WHERE ";
                } else{
                    $sWhere .= " AND ";
                }
                $sWhere .= $aColumns[$i]." LIKE '%".mysqli_real_escape_string($gaSql['link'],$_GET['sSearch_'.$i])."%' ";
            }
        }
        if($sCondition!=''){
            if ( $sWhere == "" ){
                $sWhere="WHERE ".$sCondition." ";
            }else{
                $sWhere .=" AND ".$sCondition." ";
            }
        }
        /*
         * SQL queries
        * Get data to display 
        */
        $sQuery = "
    SELECT SQL_CALC_FOUND_ROWS ".str_replace(" , ", " ", implode(", ", $aColumns))."
            FROM   $sTable $sJoinTable 
            $sWhere
            $sOrder
            $sLimit
            ";
       //debug($sQuery);die;
        $rResult = mysqli_query( $gaSql['link'],$sQuery) or fatal_error( 'MySQL Error Here 1: ' . mysqli_errno($gaSql['link']) );
         
        /* Data set length after filtering */
        $sQuery = "
    SELECT FOUND_ROWS()
";
        $rResultFilterTotal =mysqli_query( $gaSql['link'],$sQuery)  or fatal_error( 'MySQL Error Here 2: ' . mysqli_errno($gaSql['link']) );
        $aResultFilterTotal = mysqli_fetch_array($rResultFilterTotal);
        $iFilteredTotal = $aResultFilterTotal[0];
         
        /* Total data set length */
        $sQuery = "
    SELECT COUNT(".$sIndexColumn.")
            FROM   $sTable $sJoinTable
            ";
        $rResultTotal = mysqli_query( $gaSql['link'],$sQuery) or fatal_error( 'MySQL Error Here 3: ' . mysqli_errno($gaSql['link']) );
        $aResultTotal = mysqli_fetch_array($rResultTotal);
        $iTotal = $aResultTotal[0];
         
        /*
         * Output
        */
        $output = array(
                "sEcho" => intval($_GET['sEcho']),
                "iTotalRecords" => $iTotal,
                "iTotalDisplayRecords" => $iFilteredTotal,
                "aaData" => array()
        );
        
        while ( $aRow = mysqli_fetch_array( $rResult ) )
        {
            $row = array();
            //debug($aRow);
            for ( $i=0 ; $i<count($aColumns); $i++ )
            {  
					if(count(explode('.',$aColumns[$i]))==2){
						$aColumns[$i]=explode('.',$aColumns[$i]);
						$aColumns[$i]=$aColumns[$i][1];
					}  
                   if ( $aColumns[$i] != ' ' ){
						if($aColumns[$i]=="customer_pass_id"){
							if(is_null($aRow[ $aColumns[$i] ])){
								$row[] = "<a class='btn green btn-xs green-stripe' href='/admin/Vehicles/edit/".$aRow['id']."'>Edit Vehicle</a>";
							}else{
								$row[] = "<a class='btn green btn-xs green-stripe' href='/admin/CustomerPasses/edit/".$aRow[$aColumns[$i] ]."'>Edit Vehicle</a>";
							}
						}
						elseif($aColumns[$i]=="user_id") {
								$row[] = "<a class='btn green btn-xs green-stripe' href='/admin/CustomerPasses/user_passes/".$aRow[ $aColumns[$i] ]."'>Add/Edit RFID Tag</a>";
						}elseif($aColumns[$i]=="customer_pass_id"){
							$row[] = "<a class='btn green btn-xs green-stripe' href='/admin/CustomerPasses/edit/".$aRow[ $aColumns[$i] ]."'>Edit Vehicle</a>";
						}
						elseif($aColumns[$i]=="pass_valid_upto"||$aColumns[$i]=="membership_vaild_upto"){
							if(is_null($aRow[ $aColumns[$i] ])){
								$row[] = $aRow[ $aColumns[$i] ];
							}else{
								  $dt = new DateTime();
								  $currentDateTime= $dt->format('Y-m-d H:i:s');
								  if($aRow[ $aColumns[$i] ]>$currentDateTime){	
									$row[] = date("m/d/Y H:i:s", strtotime($aRow[ $aColumns[$i] ])).' '.'<span class="label label-sm label-danger">Active</span>';
								  }elseif($aRow[ $aColumns[$i] ]<$currentDateTime){
									$row[] = date("m/d/Y H:i:s", strtotime($aRow[ $aColumns[$i] ])).' '.'<span class="label label-sm label-warning">Expired</span>';
								  }else{
									$row[] = $aRow[ $aColumns[$i] ];
								  }
							}
						}
						else {
							$row[] = $aRow[ $aColumns[$i] ];
						}
					}
					
            }
            $output['aaData'][] = $row;
        }
        return $output;
	}

}

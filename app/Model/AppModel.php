<?php
/**
 * Application model for CakePHP.
 *
 * This file is application-wide model file. You can put all
 * application-wide model-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Model
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Model', 'Model');

/**  
 * Application model for Cake.
 *
 * Add your application-wide methods in the class below, your models
 * will inherit them.
 *
 * @package       app.Model
 */
class AppModel extends Model {
	
	public function simplifyDataTable($config=array()){
		if(!isset($config['columns']) || !isset($config['index_column']) || !isset($config['table'])){
				return array();
		}
		$aColumns=$config['columns'];
		$sIndexColumn=$config['index_column'];
		$sTable=$config['table'];
		/* optional join */
		$sJoinTable=isset($config['join'])? $config['join'] : '';
        /* optional conditions */
		$sCondition=isset($config['conditions'])? $config['conditions'] : '';
        App::uses('ConnectionManager', 'Model');
        $dataSource = ConnectionManager::getDataSource('default');
         
        /* Database connection information */
        $gaSql['user']       = $dataSource->config['login'];
        $gaSql['password']   = $dataSource->config['password'];
        $gaSql['db']         = $dataSource->config['database'];
        $gaSql['server']     = $dataSource->config['host'];
         
         
        /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
         * If you just want to use the basic configuration for DataTables with PHP server-side, there is
        * no need to edit below this line
        */
         
        /*
         * Local functions
        */
        function fatal_error ( $sErrorMessage = '' ){
            header( $_SERVER['SERVER_PROTOCOL'] .' 500 Internal Server Error' );
            die( $sErrorMessage );
        }
        /*
         * MySQL connection
        */
        if ( ! $gaSql['link'] = mysqli_connect( $gaSql['server'], $gaSql['user'], $gaSql['password'],$gaSql['db']) ){
            fatal_error( 'Could not open connection to server' );
        }
        
        if ( ! mysqli_select_db($gaSql['link'],$gaSql['db']) ){
            fatal_error( 'Could not select database ' );
        }
         
         
        /*
         * Paging
        */
        $sLimit = "";
        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' ){
            $sLimit = "LIMIT ".intval( $_GET['iDisplayStart'] ).", ".
            intval( $_GET['iDisplayLength'] );
        }
        /*
         * Ordering
        */
        $sOrder = "";
        if ( isset( $_GET['iSortCol_0'] ) ){
            $sOrder = "ORDER BY  ";
            for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ ){
                if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" ){
                    $sOrder .= $aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
                        ($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
                }
            }
            $sOrder = substr_replace( $sOrder, "", -2 );
            if ( $sOrder == "ORDER BY" ){
                $sOrder = "";
            }
        }
        /*
         * Filtering
        * NOTE this does not match the built-in DataTables filtering which does it
        * word by word on any field. It's possible to do here, but concerned about efficiency
        * on very large tables, and MySQL's regex functionality is very limited
        */
        $sWhere = "";
        if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" ){
            $sWhere = "WHERE (";
            for ( $i=0 ; $i<count($aColumns) ; $i++ ){
                $sWhere .= $aColumns[$i]." LIKE '%".mysqli_real_escape_string($gaSql['link'], $_GET['sSearch'] )."%' OR ";
            }
            $sWhere = substr_replace( $sWhere, "", -3 );
            $sWhere .= ')';
        }
        for ( $i=0 ; $i<count($aColumns) ; $i++ ){
            if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' ){
                if ( $sWhere == "" ){
                    $sWhere = "WHERE ";
                } else{
                    $sWhere .= " AND ";
                }
                $sWhere .= $aColumns[$i]." LIKE '%".mysqli_real_escape_string($gaSql['link'],$_GET['sSearch_'.$i])."%' ";
            }
        }
        if($sCondition!=''){
            if ( $sWhere == "" ){
                $sWhere="WHERE ".$sCondition." ";
            }else{
                $sWhere .=" AND ".$sCondition." ";
            }
        }
        /*
         * SQL queries
        * Get data to display 
        */
        $sQuery = "
    SELECT SQL_CALC_FOUND_ROWS ".str_replace(" , ", " ", implode(", ", $aColumns))."
            FROM   $sTable $sJoinTable 
            $sWhere
            $sOrder
            $sLimit
            ";
       //debug($sQuery);die;
        $rResult = mysqli_query( $gaSql['link'],$sQuery) or fatal_error( 'MySQL Error Here 1: ' . mysqli_errno($gaSql['link']) );
         
        /* Data set length after filtering */
        $sQuery = "
    SELECT FOUND_ROWS()
";
        $rResultFilterTotal =mysqli_query( $gaSql['link'],$sQuery)  or fatal_error( 'MySQL Error Here 2: ' . mysqli_errno($gaSql['link']) );
        $aResultFilterTotal = mysqli_fetch_array($rResultFilterTotal);
        $iFilteredTotal = $aResultFilterTotal[0];
         
        /* Total data set length */
        $sQuery = "
    SELECT COUNT(".$sIndexColumn.")
            FROM   $sTable $sJoinTable
            ";
        $rResultTotal = mysqli_query( $gaSql['link'],$sQuery) or fatal_error( 'MySQL Error Here 3: ' . mysqli_errno($gaSql['link']) );
        $aResultTotal = mysqli_fetch_array($rResultTotal);
        $iTotal = $aResultTotal[0];
         
        /*
         * Output
        */
        $output = array(
                "sEcho" => intval($_GET['sEcho']),
                "iTotalRecords" => $iTotal,
                "iTotalDisplayRecords" => $iFilteredTotal,
                "aaData" => array()
        );
        
        while ( $aRow = mysqli_fetch_array( $rResult ) )
        {
			
            $row = array();
            for ( $i=0 ; $i<count($aColumns) ; $i++ )
            {
				
				if(count(explode('.',$aColumns[$i]))==2){
					$aColumns[$i]=explode('.',$aColumns[$i]);
					$aColumns[$i]=$aColumns[$i][1];
				}
                if ( $aColumns[$i] == "version" )
                {
                    /* Special output formatting for 'version' column */
                    $row[] = ($aRow[ $aColumns[$i] ]=="0") ? '-' : $aRow[ $aColumns[$i] ];
                }
                else if ( $aColumns[$i] != ' ' )
                {
                   if($aColumns[$i]=="user_id") {
							$row[] = "<a class='btn green btn-xs green-stripe' href='/admin/users/edit/".$aRow[ $aColumns[$i] ]."'>Edit</a>&nbsp;
									  &nbsp;<a class='btn default btn-xs green-stripe' href='/admin/users/view_user_details/".$aRow[ $aColumns[$i] ]."'>View</a>&nbsp;
									  &nbsp;<a class='btn yellow btn-xs yellow-stripe' href='/admin/users/print_details/".$aRow[ $aColumns[$i] ]."'target='_blank'>Print</a>";
					}else{
						$row[] = $aRow[ $aColumns[$i] ];
					}
                }
            }
            $output['aaData'][] = $row;
        }
         
        return $output;
	}

/***********************************************
 * For RFID 
 */

	public function simplifyDataTableRFID($config=array()){
		if(!isset($config['columns']) || !isset($config['index_column']) || !isset($config['table'])){
				return array();
		}
		$aColumns=$config['columns'];
		$sIndexColumn=$config['index_column'];
		$sTable=$config['table'];
		/* optional join */
		$sJoinTable=isset($config['join'])? $config['join'] : '';
        /* optional conditions */
		$sCondition=isset($config['conditions'])? $config['conditions'] : '';
        App::uses('ConnectionManager', 'Model');
        $dataSource = ConnectionManager::getDataSource('default');
         
        /* Database connection information */
        $gaSql['user']       = $dataSource->config['login'];
        $gaSql['password']   = $dataSource->config['password'];
        $gaSql['db']         = $dataSource->config['database'];
        $gaSql['server']     = $dataSource->config['host'];
         
         
        /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
         * If you just want to use the basic configuration for DataTables with PHP server-side, there is
        * no need to edit below this line
        */
         
        /*
         * Local functions
        */
        function fatal_error ( $sErrorMessage = '' ){
            header( $_SERVER['SERVER_PROTOCOL'] .' 500 Internal Server Error' );
            die( $sErrorMessage );
        }
        /*
         * MySQL connection
        */
        if ( ! $gaSql['link'] = mysqli_connect( $gaSql['server'], $gaSql['user'], $gaSql['password'],$gaSql['db']) ){
            fatal_error( 'Could not open connection to server' );
        }
        
        if ( ! mysqli_select_db($gaSql['link'],$gaSql['db']) ){
            fatal_error( 'Could not select database ' );
        }
         
         
        /*
         * Paging
        */
        $sLimit = "";
        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' ){
            $sLimit = "LIMIT ".intval( $_GET['iDisplayStart'] ).", ".
            intval( $_GET['iDisplayLength'] );
        }
        /*
         * Ordering
        */
        $sOrder = "";
        if ( isset( $_GET['iSortCol_0'] ) ){
            $sOrder = "ORDER BY  ";
            for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ ){
                if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" ){
                    $sOrder .= $aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
                        ($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
                }
            }
            $sOrder = substr_replace( $sOrder, "", -2 );
            if ( $sOrder == "ORDER BY" ){
                $sOrder = "";
            }
        }
        /*
         * Filtering
        * NOTE this does not match the built-in DataTables filtering which does it
        * word by word on any field. It's possible to do here, but concerned about efficiency
        * on very large tables, and MySQL's regex functionality is very limited
        */
        $sWhere = "";
        if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" ){
            $sWhere = "WHERE (";
            for ( $i=0 ; $i<count($aColumns) ; $i++ ){
                $sWhere .= $aColumns[$i]." LIKE '%".mysqli_real_escape_string($gaSql['link'], $_GET['sSearch'] )."%' OR ";
            }
            $sWhere = substr_replace( $sWhere, "", -3 );
            $sWhere .= ')';
        }
        for ( $i=0 ; $i<count($aColumns) ; $i++ ){
            if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' ){
                if ( $sWhere == "" ){
                    $sWhere = "WHERE ";
                } else{
                    $sWhere .= " AND ";
                }
                $sWhere .= $aColumns[$i]." LIKE '%".mysqli_real_escape_string($gaSql['link'],$_GET['sSearch_'.$i])."%' ";
            }
        }
        if($sCondition!=''){
            if ( $sWhere == "" ){
                $sWhere="WHERE ".$sCondition." ";
            }else{
                $sWhere .=" AND ".$sCondition." ";
            }
        }
        /*
         * SQL queries
        * Get data to display 
        */
        $sQuery = "
    SELECT SQL_CALC_FOUND_ROWS ".str_replace(" , ", " ", implode(", ", $aColumns))."
            FROM   $sTable $sJoinTable 
            $sWhere
            $sOrder
            $sLimit
            ";
       //debug($sQuery);die;
        $rResult = mysqli_query( $gaSql['link'],$sQuery) or fatal_error( 'MySQL Error Here 1: ' . mysqli_errno($gaSql['link']) );
         
        /* Data set length after filtering */
        $sQuery = "
    SELECT FOUND_ROWS()
";
        $rResultFilterTotal =mysqli_query( $gaSql['link'],$sQuery)  or fatal_error( 'MySQL Error Here 2: ' . mysqli_errno($gaSql['link']) );
        $aResultFilterTotal = mysqli_fetch_array($rResultFilterTotal);
        $iFilteredTotal = $aResultFilterTotal[0];
         
        /* Total data set length */
        $sQuery = "
    SELECT COUNT(".$sIndexColumn.")
            FROM   $sTable $sJoinTable
            ";
        $rResultTotal = mysqli_query( $gaSql['link'],$sQuery) or fatal_error( 'MySQL Error Here 3: ' . mysqli_errno($gaSql['link']) );
        $aResultTotal = mysqli_fetch_array($rResultTotal);
        $iTotal = $aResultTotal[0];
         
        /*
         * Output
        */
        $output = array(
                "sEcho" => intval($_GET['sEcho']),
                "iTotalRecords" => $iTotal,
                "iTotalDisplayRecords" => $iFilteredTotal,
                "aaData" => array()
        );
        
        while ( $aRow = mysqli_fetch_array( $rResult ) )
        {
            $row = array();
            //debug($aRow);
            for ( $i=0 ; $i<count($aColumns); $i++ )
            {  
					if(count(explode('.',$aColumns[$i]))==2){
						$aColumns[$i]=explode('.',$aColumns[$i]);
						$aColumns[$i]=$aColumns[$i][1];
					}  
                   if ( $aColumns[$i] != ' ' ){
						if($aColumns[$i]=="id") {
								$row[] = "<a class='btn green btn-xs green-stripe' href='/admin/CustomerPasses/edit/".$aRow[ $aColumns[$i] ]."'>Add/Edit RFID Tag</a>";
						}elseif($aColumns[$i]=="customer_pass_id"){
							$row[] = "<a class='btn green btn-xs green-stripe' href='/admin/CustomerPasses/edit/".$aRow[ $aColumns[$i] ]."'>Edit Vehicle</a>";
						}
						elseif($aColumns[$i]=="user_id") {
								$row[] = "<a class='btn green btn-xs green-stripe' href='/admin/CustomerPasses/user_passes/".$aRow[ $aColumns[$i] ]."'>Add/Edit RFID Tag</a>";
						}elseif($aColumns[$i]=="customer_pass_id"){
							$row[] = "<a class='btn green btn-xs green-stripe' href='/admin/CustomerPasses/edit/".$aRow[ $aColumns[$i] ]."'>Edit Vehicle</a>";
						}
						elseif($aColumns[$i]=="pass_valid_upto"||$aColumns[$i]=="membership_vaild_upto"){
							if(is_null($aRow[ $aColumns[$i] ])){
								$row[] = $aRow[ $aColumns[$i] ];
							}else{
								  $dt = new DateTime();
								  $currentDateTime= $dt->format('Y-m-d H:i:s');
								  if($aRow[ $aColumns[$i] ]>$currentDateTime){	
									$row[] = date("m/d/Y H:i:s", strtotime($aRow[ $aColumns[$i] ])).' '.'<span class="label label-sm label-danger">Active</span>';
								  }elseif($aRow[ $aColumns[$i] ]<$currentDateTime){
									$row[] = date("m/d/Y H:i:s", strtotime($aRow[ $aColumns[$i] ])).' '.'<span class="label label-sm label-warning">Expired</span>';
								  }else{
									$row[] = $aRow[ $aColumns[$i] ];
								  }
							}
						}
						else {
							$row[] = $aRow[ $aColumns[$i] ];
						}
					}
					
            }
            $output['aaData'][] = $row;
        }
        return $output;
	}
/***********************************************
 * For Manager Customer Details 
 */

	public function managerCustomerDetails($config=array()){
		if(!isset($config['columns']) || !isset($config['index_column']) || !isset($config['table'])){
				return array();
		}
		$aColumns=$config['columns'];
		$sIndexColumn=$config['index_column'];
		$sTable=$config['table'];
		$sJoinTable=isset($config['join'])? $config['join'] : '';
		$sCondition=isset($config['conditions'])? $config['conditions'] : '';
        App::uses('ConnectionManager', 'Model');
        $dataSource = ConnectionManager::getDataSource('default');
        $gaSql['user']       = $dataSource->config['login'];
        $gaSql['password']   = $dataSource->config['password'];
        $gaSql['db']         = $dataSource->config['database'];
        $gaSql['server']     = $dataSource->config['host'];
    
        function fatal_error ( $sErrorMessage = '' ){
            header( $_SERVER['SERVER_PROTOCOL'] .' 500 Internal Server Error' );
            die( $sErrorMessage );
        }
        /*
         * MySQL connection
        */
        if ( ! $gaSql['link'] = mysqli_connect( $gaSql['server'], $gaSql['user'], $gaSql['password'],$gaSql['db']) ){
            fatal_error( 'Could not open connection to server' );
        }
        
        if ( ! mysqli_select_db($gaSql['link'],$gaSql['db']) ){
            fatal_error( 'Could not select database ' );
        }
         
         
        /*
         * Paging
        */
        $sLimit = "";
        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' ){
            $sLimit = "LIMIT ".intval( $_GET['iDisplayStart'] ).", ".
            intval( $_GET['iDisplayLength'] );
        }
        /*
         * Ordering
        */
        $sOrder = "";
        if ( isset( $_GET['iSortCol_0'] ) ){
            $sOrder = "ORDER BY  ";
            for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ ){
                if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" ){
                    $sOrder .= $aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
                        ($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
                }
            }
            $sOrder = substr_replace( $sOrder, "", -2 );
            if ( $sOrder == "ORDER BY" ){
                $sOrder = "";
            }
        }
        /*
         * Filtering
        * NOTE this does not match the built-in DataTables filtering which does it
        * word by word on any field. It's possible to do here, but concerned about efficiency
        * on very large tables, and MySQL's regex functionality is very limited
        */
        $sWhere = "";
        if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" ){
            $sWhere = "WHERE (";
            for ( $i=0 ; $i<count($aColumns) ; $i++ ){
                $sWhere .= $aColumns[$i]." LIKE '%".mysqli_real_escape_string($gaSql['link'], $_GET['sSearch'] )."%' OR ";
            }
            $sWhere = substr_replace( $sWhere, "", -3 );
            $sWhere .= ')';
        }
        for ( $i=0 ; $i<count($aColumns) ; $i++ ){
            if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' ){
                if ( $sWhere == "" ){
                    $sWhere = "WHERE ";
                } else{
                    $sWhere .= " AND ";
                }
                $sWhere .= $aColumns[$i]." LIKE '%".mysqli_real_escape_string($gaSql['link'],$_GET['sSearch_'.$i])."%' ";
            }
        }
        if($sCondition!=''){
            if ( $sWhere == "" ){
                $sWhere="WHERE ".$sCondition." ";
            }else{
                $sWhere .=" AND ".$sCondition." ";
            }
        }
        /*
         * SQL queries
        * Get data to display 
        */
        $sQuery = "
    SELECT SQL_CALC_FOUND_ROWS ".str_replace(" , ", " ", implode(", ", $aColumns))."
            FROM   $sTable $sJoinTable 
            $sWhere
            $sOrder
            $sLimit
            ";
       //debug($sQuery);die;
        $rResult = mysqli_query( $gaSql['link'],$sQuery) or fatal_error( 'MySQL Error Here 1: ' . mysqli_errno($gaSql['link']) );
         
        /* Data set length after filtering */
        $sQuery = "
    SELECT FOUND_ROWS()
";
        $rResultFilterTotal =mysqli_query( $gaSql['link'],$sQuery)  or fatal_error( 'MySQL Error Here 2: ' . mysqli_errno($gaSql['link']) );
        $aResultFilterTotal = mysqli_fetch_array($rResultFilterTotal);
        $iFilteredTotal = $aResultFilterTotal[0];
         
        /* Total data set length */
        $sQuery = "
    SELECT COUNT(".$sIndexColumn.")
            FROM   $sTable $sJoinTable
            ";
        $rResultTotal = mysqli_query( $gaSql['link'],$sQuery) or fatal_error( 'MySQL Error Here 3: ' . mysqli_errno($gaSql['link']) );
        $aResultTotal = mysqli_fetch_array($rResultTotal);
        $iTotal = $aResultTotal[0];
         
        /*
         * Output
        */
        $output = array(
                "sEcho" => intval($_GET['sEcho']),
                "iTotalRecords" => $iTotal,
                "iTotalDisplayRecords" => $iFilteredTotal,
                "aaData" => array()
        );
        
        while ( $aRow = mysqli_fetch_array( $rResult ) )
        {
            $row = array();
            for ( $i=0 ; $i<count($aColumns); $i++ )
            {  
					if(count(explode('.',$aColumns[$i]))==2){
						$aColumns[$i]=explode('.',$aColumns[$i]);
						$aColumns[$i]=$aColumns[$i][1];
					}  
					if ( $aColumns[$i] != ' ' ){
						if($sTable=='transactions'){
							if($aColumns[$i]=="date_time") {
								$row[] = date("m/d/Y H:i:s", strtotime($aRow[ $aColumns[$i] ]));
							}
							else {
								$row[] = $aRow[ $aColumns[$i] ];
							}
						}
						else{	
							if($aColumns[$i]=="user_id") {
								$row[] = "<a class='btn green btn-xs green-stripe' href='/Users/manager_view_user_details/".$aRow[ $aColumns[$i] ]."'>View Details</a>&nbsp;
									  &nbsp;<a class='btn yellow btn-xs yellow-stripe' href='/users/manager_print_details/".$aRow[ $aColumns[$i] ]."'target='_blank'>Print</a>";
								
							}
							else {
								$row[] = $aRow[ $aColumns[$i] ];
							}
						}
					}
            }
            $output['aaData'][] = $row;
        }
        return $output;
	}
/*******************************************************************
 * Adminstrator Customer DATA
 */	
	public function adminCustomerDetails($config=array()){
		if(!isset($config['columns']) || !isset($config['index_column']) || !isset($config['table'])){
				return array();
		}
		$aColumns=$config['columns'];
		$sIndexColumn=$config['index_column'];
		$sTable=$config['table'];
		$sJoinTable=isset($config['join'])? $config['join'] : '';
		$sCondition=isset($config['conditions'])? $config['conditions'] : '';
        App::uses('ConnectionManager', 'Model');
        $dataSource = ConnectionManager::getDataSource('default');
        $gaSql['user']       = $dataSource->config['login'];
        $gaSql['password']   = $dataSource->config['password'];
        $gaSql['db']         = $dataSource->config['database'];
        $gaSql['server']     = $dataSource->config['host'];
    
        function fatal_error ( $sErrorMessage = '' ){
            header( $_SERVER['SERVER_PROTOCOL'] .' 500 Internal Server Error' );
            die( $sErrorMessage );
        }
        /*
         * MySQL connection
        */
        if ( ! $gaSql['link'] = mysqli_connect( $gaSql['server'], $gaSql['user'], $gaSql['password'],$gaSql['db']) ){
            fatal_error( 'Could not open connection to server' );
        }
        
        if ( ! mysqli_select_db($gaSql['link'],$gaSql['db']) ){
            fatal_error( 'Could not select database ' );
        }
         
         
        /*
         * Paging
        */
        $sLimit = "";
        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' ){
            $sLimit = "LIMIT ".intval( $_GET['iDisplayStart'] ).", ".
            intval( $_GET['iDisplayLength'] );
        }
        /*
         * Ordering
        */
        $sOrder = "";
        if ( isset( $_GET['iSortCol_0'] ) ){
            $sOrder = "ORDER BY  ";
            for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ ){
                if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" ){
                    $sOrder .= $aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
                        ($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
                }
            }
            $sOrder = substr_replace( $sOrder, "", -2 );
            if ( $sOrder == "ORDER BY" ){
                $sOrder = "";
            }
        }
        /*
         * Filtering
        * NOTE this does not match the built-in DataTables filtering which does it
        * word by word on any field. It's possible to do here, but concerned about efficiency
        * on very large tables, and MySQL's regex functionality is very limited
        */
        $sWhere = "";
        if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" ){
            $sWhere = "WHERE (";
            for ( $i=0 ; $i<count($aColumns) ; $i++ ){
                $sWhere .= $aColumns[$i]." LIKE '%".mysqli_real_escape_string($gaSql['link'], $_GET['sSearch'] )."%' OR ";
            }
            $sWhere = substr_replace( $sWhere, "", -3 );
            $sWhere .= ')';
        }
        for ( $i=0 ; $i<count($aColumns) ; $i++ ){
            if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' ){
                if ( $sWhere == "" ){
                    $sWhere = "WHERE ";
                } else{
                    $sWhere .= " AND ";
                }
                $sWhere .= $aColumns[$i]." LIKE '%".mysqli_real_escape_string($gaSql['link'],$_GET['sSearch_'.$i])."%' ";
            }
        }
        if($sCondition!=''){
            if ( $sWhere == "" ){
                $sWhere="WHERE ".$sCondition." ";
            }else{
                $sWhere .=" AND ".$sCondition." ";
            }
        }
        /*
         * SQL queries
        * Get data to display 
        */
        $sQuery = "
    SELECT SQL_CALC_FOUND_ROWS ".str_replace(" , ", " ", implode(", ", $aColumns))."
            FROM   $sTable $sJoinTable 
            $sWhere
            $sOrder
            $sLimit
            ";
       //debug($sQuery);die;
        $rResult = mysqli_query( $gaSql['link'],$sQuery) or fatal_error( 'MySQL Error Here 1: ' . mysqli_errno($gaSql['link']) );
         
        /* Data set length after filtering */
        $sQuery = "
    SELECT FOUND_ROWS()
";
        $rResultFilterTotal =mysqli_query( $gaSql['link'],$sQuery)  or fatal_error( 'MySQL Error Here 2: ' . mysqli_errno($gaSql['link']) );
        $aResultFilterTotal = mysqli_fetch_array($rResultFilterTotal);
        $iFilteredTotal = $aResultFilterTotal[0];
         
        /* Total data set length */
        $sQuery = "
    SELECT COUNT(".$sIndexColumn.")
            FROM   $sTable $sJoinTable
            ";
        $rResultTotal = mysqli_query( $gaSql['link'],$sQuery) or fatal_error( 'MySQL Error Here 3: ' . mysqli_errno($gaSql['link']) );
        $aResultTotal = mysqli_fetch_array($rResultTotal);
        $iTotal = $aResultTotal[0];
         
        /*
         * Output
        */
        $output = array(
                "sEcho" => intval($_GET['sEcho']),
                "iTotalRecords" => $iTotal,
                "iTotalDisplayRecords" => $iFilteredTotal,
                "aaData" => array()
        );
        
        while ( $aRow = mysqli_fetch_array( $rResult ) )
        {
            $row = array();
            for ( $i=0 ; $i<count($aColumns); $i++ )
            {  
					if(count(explode('.',$aColumns[$i]))==2){
						$aColumns[$i]=explode('.',$aColumns[$i]);
						$aColumns[$i]=$aColumns[$i][1];
					}  
					if ( $aColumns[$i] != ' ' ){
						if($sTable=='transactions'){
							if($aColumns[$i]=="date_time") {
								$row[] = date("m/d/Y H:i:s", strtotime($aRow[ $aColumns[$i] ]));
							}
							else {
								$row[] = $aRow[ $aColumns[$i] ];
							}
						}
						else{	
							if($aColumns[$i]=="user_id") {
								$tempstring = "<a class='btn green btn-xs green-stripe' href='/admin/Users/pa_view_user_details/".$aRow[ $aColumns[$i] ]."'>View Details</a>&nbsp;
									  &nbsp;<a class='btn yellow btn-xs yellow-stripe' href='/admin/users/print_details/".$aRow[ $aColumns[$i] ]."'target='_blank'>Print</a>";
								if(AuthComponent::user('role_id')==2){	 
									 $tempstring.="&nbsp;<a class='btn red btn-xs red-stripe' href='/admin/users/edit/".$aRow[ $aColumns[$i] ]."'>Edit</a>";  
								}
								$row[] =$tempstring;
							}
							else {
								$row[] = $aRow[ $aColumns[$i] ];
							}
						}
					}
            }
            $output['aaData'][] = $row;
        }
        return $output;
	}
public function simplifyDataTable2($config=array()){
		if(!isset($config['columns']) || !isset($config['index_column']) || !isset($config['table'])){
				return array();
		}
		$aColumns=$config['columns'];
		$sIndexColumn=$config['index_column'];
		$sTable=$config['table'];
		/* optional join */
		$sJoinTable=isset($config['join'])? $config['join'] : '';
        /* optional conditions */
		$sCondition=isset($config['conditions'])? $config['conditions'] : '';
        App::uses('ConnectionManager', 'Model');
        $dataSource = ConnectionManager::getDataSource('default');
         
        /* Database connection information */
        $gaSql['user']       = $dataSource->config['login'];
        $gaSql['password']   = $dataSource->config['password'];
        $gaSql['db']         = $dataSource->config['database'];
        $gaSql['server']     = $dataSource->config['host'];
         
         
        /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
         * If you just want to use the basic configuration for DataTables with PHP server-side, there is
        * no need to edit below this line
        */
         
        /*
         * Local functions
        */
        function fatal_error ( $sErrorMessage = '' ){
            header( $_SERVER['SERVER_PROTOCOL'] .' 500 Internal Server Error' );
            die( $sErrorMessage );
        }
        /*
         * MySQL connection
        */
        if ( ! $gaSql['link'] = mysqli_connect( $gaSql['server'], $gaSql['user'], $gaSql['password'],$gaSql['db']) ){
            fatal_error( 'Could not open connection to server' );
        }
        
        if ( ! mysqli_select_db($gaSql['link'],$gaSql['db']) ){
            fatal_error( 'Could not select database ' );
        }
         
         
        /*
         * Paging
        */
        $sLimit = "";
        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' ){
            $sLimit = "LIMIT ".intval( $_GET['iDisplayStart'] ).", ".
            intval( $_GET['iDisplayLength'] );
        }
        /*
         * Ordering
        */
        $sOrder = "";
        if ( isset( $_GET['iSortCol_0'] ) ){
            $sOrder = "ORDER BY  ";
            for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ ){
                if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" ){
                    $sOrder .= $aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
                        ($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
                }
            }
            $sOrder = substr_replace( $sOrder, "", -2 );
            if ( $sOrder == "ORDER BY" ){
                $sOrder = "";
            }
        }
        /*
         * Filtering
        * NOTE this does not match the built-in DataTables filtering which does it
        * word by word on any field. It's possible to do here, but concerned about efficiency
        * on very large tables, and MySQL's regex functionality is very limited
        */
        $sWhere = "";
        if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" ){
            $sWhere = "WHERE (";
            for ( $i=0 ; $i<count($aColumns) ; $i++ ){
                $sWhere .= $aColumns[$i]." LIKE '%".mysqli_real_escape_string($gaSql['link'], $_GET['sSearch'] )."%' OR ";
            }
            $sWhere = substr_replace( $sWhere, "", -3 );
            $sWhere .= ')';
        }
        for ( $i=0 ; $i<count($aColumns) ; $i++ ){
            if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' ){
                if ( $sWhere == "" ){
                    $sWhere = "WHERE ";
                } else{
                    $sWhere .= " AND ";
                }
                $sWhere .= $aColumns[$i]." LIKE '%".mysqli_real_escape_string($gaSql['link'],$_GET['sSearch_'.$i])."%' ";
            }
        }
        if($sCondition!=''){
            if ( $sWhere == "" ){
                $sWhere="WHERE ".$sCondition." ";
            }else{
                $sWhere .=" AND ".$sCondition." ";
            }
        }
        /*
         * SQL queries
        * Get data to display 
        */
        $sQuery = "
    SELECT SQL_CALC_FOUND_ROWS ".str_replace(" , ", " ", implode(", ", $aColumns))."
            FROM   $sTable $sJoinTable 
            $sWhere
            $sOrder
            $sLimit
            ";
       //debug($sQuery);die;
        $rResult = mysqli_query( $gaSql['link'],$sQuery) or fatal_error( 'MySQL Error Here 1: ' . mysqli_errno($gaSql['link']) );
           
        /* Data set length after filtering */
        $sQuery = "
    SELECT FOUND_ROWS()
";
        $rResultFilterTotal =mysqli_query( $gaSql['link'],$sQuery)  or fatal_error( 'MySQL Error Here 2: ' . mysqli_errno($gaSql['link']) );
        $aResultFilterTotal = mysqli_fetch_array($rResultFilterTotal);
        $iFilteredTotal = $aResultFilterTotal[0];
         
        /* Total data set length */
        $sQuery = "
    SELECT COUNT(".$sIndexColumn.")
            FROM   $sTable $sJoinTable
            ";
        $rResultTotal = mysqli_query( $gaSql['link'],$sQuery) or fatal_error( 'MySQL Error Here 3: ' . mysqli_errno($gaSql['link']) );
        $aResultTotal = mysqli_fetch_array($rResultTotal);
        $iTotal = $aResultTotal[0];
         
        /*
         * Output
        */
        $output = array(
                "sEcho" => intval($_GET['sEcho']),
                "iTotalRecords" => $iTotal,
                "iTotalDisplayRecords" => $iFilteredTotal,
                "aaData" => array()
        );
        
        while ( $aRow = mysqli_fetch_array( $rResult ) )
        {
			
            $row = array();
            for ( $i=0 ; $i<count($aColumns) ; $i++ )
            {
				
				if(count(explode('.',$aColumns[$i]))==2){
					$aColumns[$i]=explode('.',$aColumns[$i]);
					$aColumns[$i]=$aColumns[$i][1];
				}
                if ( $aColumns[$i] == "version" )
                {
                    /* Special output formatting for 'version' column */
                    $row[] = ($aRow[ $aColumns[$i] ]=="0") ? '-' : $aRow[ $aColumns[$i] ];
                }
                else if ( $aColumns[$i] != ' ' )
                {
                   if($aColumns[$i]=="user_id") {
							$row[] = "<a class='btn green btn-xs green-stripe' href='/admin/users/edit/".$aRow[ $aColumns[$i] ]."'>Edit</a>&nbsp;
									  &nbsp;<a class='btn default btn-xs green-stripe' href='/admin/users/view_user_details/".$aRow[ $aColumns[$i] ]."'>View</a>&nbsp;
									  &nbsp;<a class='btn yellow btn-xs yellow-stripe' href='/admin/users/print_details/".$aRow[ $aColumns[$i] ]."'target='_blank'>Print</a>
									  &nbsp;<a class='btn red btn-xs red-stripe' href='/admin/CustomerPasses/customer_view/".$aRow[ $aColumns[$i] ]."'target='_blank'>Manage</a>&nbsp;
									  &nbsp;<a class='btn default btn-xs green-stripe' href='/admin/tickets/contact_user/".$aRow[ $aColumns[$i] ]."'>Contact</a>";
					}else{
						$row[] = $aRow[ $aColumns[$i] ];
					}
                }
            }
            $output['aaData'][] = $row;
        }
         
        return $output;
	}
}

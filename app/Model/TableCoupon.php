<?php
App::uses('AppModel', 'Model');

class TableCoupon extends AppModel {
	 public $belongsTo = array(
        'Property' => array(
            'className' => 'Property',
            'foreignKey' => 'property_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );
     public $validate = array(
        'expiration_date'=> array(
            'datetime' => array(
                'rule' => array('date','mdy'),
                'message' => 'Date is either empty or format is incorrect',
                'allowEmpty' => true,
                'required' => false,
            )
        ),
        'start_date'=> array(
            'datetime' => array(
                'rule' => array('date','mdy'),
                'message' => 'Date is either empty or format is incorrect',
                'allowEmpty' => true,
                'required' => false,
            )
        ),
        'discount' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                'message' => 'Only numeric data allowed',
                'allowEmpty' => false,
                'required' => true,
            ),
        ),
        'property_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                'message' => 'Invalid Property',
                'allowEmpty' => false,
                'required' => true,
            ),
        ),
        'coupon_code'=>array(
            'rule' => array('custom','/^[a-zA-Z0-9]*$/'),
            'message' => 'No Special Character Allowed',
            'required'=>true,
            'allowEmpty' => false,
        )
    );
	 public function beforeSave($options = null)
    {
       if(isset($this->data['TableCoupon']['expiration_date' ])){
		  if($this->data['TableCoupon']['expiration_date' ]){
				$this->data['TableCoupon']['expiration_date' ] = date("Y-m-d", strtotime($this->data['TableCoupon']['expiration_date' ]));
				$this->data['TableCoupon']['expiration_date' ] = $this->data['TableCoupon']['expiration_date' ].' 23:59:59';
		  }
       }
        if(isset($this->data['TableCoupon']['start_date' ])){
			if($this->data['TableCoupon']['start_date' ]){
				$this->data['TableCoupon']['start_date' ] = date("Y-m-d", strtotime($this->data['TableCoupon']['start_date' ]));
			}
       }
    }
}

<?php

class Ticket extends AppModel {

    var $name = 'Ticket';
    public $belongsTo = array(
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'user_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
         'Recipient' => array(
            'className' => 'User',
            'foreignKey' => 'recipient_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );
    public $hasMany = array(
        'TicketReply' => array(
            'className' => 'TicketReply',
            'foreignKey' => 'ticket_id',
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
    ));
    public $validate = array
        (
        'subject' => array(
            
            'Minimum 2 character required' => array(
                'required' => true,
                'rule' => array('minLength', 2),
                'message' => 'Minimum 2 characters required')
        ),
        'email' => array(
            'Email Required' => array(
                'rule' => 'email',
                'required' => true,
                'message' => 'Email is not correct')
        ),
        'user_id' => array(
            'Username Required' => array(
                'rule' => 'notBlank',
                'required' => true,
                'message' => 'Username is not selected')
        ),
        
        'recipient_id' => array(
            'Username Required' => array(
                'rule' => 'notBlank',
                'required' => true,
                'message' => 'Username is not selected')
        ),
        /*'username' => array(
            'Only alphabets and numbers allowed' => array(
                'rule' => 'alphaNumeric',
                'allowEmpty' => false),
            'Minimum 3 character required' => array(
                'rule' => array('minLength', 3),
                'message' => 'Minimum 3 characters required')
        ),*/
        'phone' => array(
            'rule' => array('phone', null, 'us'),
            'message' => 'Please check your phone number',
            'required'=>false,
            'allowEmpty'=>true
            
        ),
        'message' => array(
            
            'Minimum 2 character required' => array(
                'required' => true,
                'rule' => array('minLength', 2),
                'message' => 'Minimum 2 characters required')
        )
     );

}

?>

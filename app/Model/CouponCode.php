<?php
App::uses('AppModel', 'Model');
/**
 * CouponCode Model
 *
 * @property CouponPackage $CouponPackage
 * @property CouponCodeUser $CouponCodeUser
 */
class CouponCode extends AppModel {


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'CouponPackage' => array(
			'className' => 'CouponPackage',
			'foreignKey' => 'coupon_package_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'CouponCodeUser' => array(
			'className' => 'CouponCodeUser',
			'foreignKey' => 'coupon_code_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}

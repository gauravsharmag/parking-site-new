<?php
App::uses('AppModel', 'Model');
/**
 * UserGuestPass Model
 *
 * @property User $User
 * @property CustomerPass $CustomerPass
 * @property Vehicle $Vehicle
 */
class UserGuestPass extends AppModel {


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'CustomerPass' => array(
			'className' => 'CustomerPass',
			'foreignKey' => 'customer_pass_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Vehicle' => array(
			'className' => 'Vehicle',
			'foreignKey' => 'vehicle_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
	public function get_user_pass_details($user_id=false,$propertyId=false){
		$aColumns = array('users.username','properties.name','gp.vehicle_details','gp.days','gp.paid','gp.amount','gp.created','gp.to_date');
        $sIndexColumn = "gp.id";
        $sTable = "user_guest_passes gp";
        $sJoinTable=' INNER JOIN users ON gp.user_id = users.id INNER JOIN properties ON gp.property_id = properties.id INNER JOIN vehicles ON gp.vehicle_id = vehicles.id ';
        $sConditions= " gp.customer_pass_id = ".$user_id." ";
        if($propertyId){
			$sConditions= " gp.property_id = ".$propertyId." ";
		}
        $outs=$this->guestPasses(array('columns'=>$aColumns,'index_column'=>$sIndexColumn,'table'=>$sTable,'join'=>$sJoinTable,'conditions'=>$sConditions));
        return $outs;
	}
/***********************************************
 * For Property Wise Passes 
 */

	public function guestPasses($config=array()){
		if(!isset($config['columns']) || !isset($config['index_column']) || !isset($config['table'])){
				return array();
		}
		$aColumns=$config['columns'];
		$sIndexColumn=$config['index_column'];
		$sTable=$config['table'];
		$sJoinTable=isset($config['join'])? $config['join'] : '';
		$sCondition=isset($config['conditions'])? $config['conditions'] : '';
        App::uses('ConnectionManager', 'Model');
        $dataSource = ConnectionManager::getDataSource('default');
        $gaSql['user']       = $dataSource->config['login'];
        $gaSql['password']   = $dataSource->config['password'];
        $gaSql['db']         = $dataSource->config['database'];
        $gaSql['server']     = $dataSource->config['host'];
       
       function fatal_error ( $sErrorMessage = '' ){
            header( $_SERVER['SERVER_PROTOCOL'] .' 500 Internal Server Error' );
            die( $sErrorMessage );
        }  
        /*
         * MySQL connection
        */
        if ( ! $gaSql['link'] = mysqli_connect( $gaSql['server'], $gaSql['user'], $gaSql['password'],$gaSql['db']) ){
            fatal_error( 'Could not open connection to server' );
        }
        
        if ( ! mysqli_select_db($gaSql['link'],$gaSql['db']) ){
            fatal_error( 'Could not select database ' );
        }
         
         
        /*
         * Paging
        */
        $sLimit = "";
        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' ){
            $sLimit = "LIMIT ".intval( $_GET['iDisplayStart'] ).", ".
            intval( $_GET['iDisplayLength'] );
        }
        /*
         * Ordering
        */
        $sOrder = "";
        if ( isset( $_GET['iSortCol_0'] ) ){
            $sOrder = "ORDER BY  ";
            for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ ){
                if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" ){
                    $sOrder .= $aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
                        ($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
                }
            }
            $sOrder = substr_replace( $sOrder, "", -2 );
            if ( $sOrder == "ORDER BY" ){
                $sOrder = "";
            }
        }
        /*
         * Filtering
        * NOTE this does not match the built-in DataTables filtering which does it
        * word by word on any field. It's possible to do here, but concerned about efficiency
        * on very large tables, and MySQL's regex functionality is very limited
        */
        $sWhere = "";
        if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" ){
            $sWhere = "WHERE (";
            for ( $i=0 ; $i<count($aColumns) ; $i++ ){
                $sWhere .= $aColumns[$i]." LIKE '%".mysqli_real_escape_string($gaSql['link'], $_GET['sSearch'] )."%' OR ";
            }
            $sWhere = substr_replace( $sWhere, "", -3 );
            $sWhere .= ')';
        }
        for ( $i=0 ; $i<count($aColumns) ; $i++ ){
            if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' ){
                if ( $sWhere == "" ){
                    $sWhere = "WHERE ";
                } else{
                    $sWhere .= " AND ";
                }
                $sWhere .= $aColumns[$i]." LIKE '%".mysqli_real_escape_string($gaSql['link'],$_GET['sSearch_'.$i])."%' ";
            }
        }
        if($sCondition!=''){
            if ( $sWhere == "" ){
                $sWhere="WHERE ".$sCondition." ";
            }else{
                $sWhere .=" AND ".$sCondition." ";
            }
        }
        /*
         * SQL queries
        * Get data to display 
        */
        $sQuery = "
    SELECT SQL_CALC_FOUND_ROWS ".str_replace(" , ", " ", implode(", ", $aColumns))."
            FROM   $sTable $sJoinTable 
            $sWhere
            $sOrder
            $sLimit
            ";
       //debug($sQuery);die;
        $rResult = mysqli_query( $gaSql['link'],$sQuery) or fatal_error( 'MySQL Error Here 1: ' . mysqli_errno($gaSql['link']) );
         
        /* Data set length after filtering */
        $sQuery = "
    SELECT FOUND_ROWS()
";
        $rResultFilterTotal =mysqli_query( $gaSql['link'],$sQuery)  or fatal_error( 'MySQL Error Here 2: ' . mysqli_errno($gaSql['link']) );
        $aResultFilterTotal = mysqli_fetch_array($rResultFilterTotal);
        $iFilteredTotal = $aResultFilterTotal[0];
         
        /* Total data set length */
        $sQuery = "
    SELECT COUNT(".$sIndexColumn.")
            FROM   $sTable $sJoinTable
            ";
        $rResultTotal = mysqli_query( $gaSql['link'],$sQuery) or fatal_error( 'MySQL Error Here 3: ' . mysqli_errno($gaSql['link']) );
        $aResultTotal = mysqli_fetch_array($rResultTotal);
        $iTotal = $aResultTotal[0];
         
        /*
         * Output
        */
        $output = array(
                "sEcho" => intval($_GET['sEcho']),
                "iTotalRecords" => $iTotal,
                "iTotalDisplayRecords" => $iFilteredTotal,
                "aaData" => array()
        );
        
        while ( $aRow = mysqli_fetch_array( $rResult ) )
        {
            $row = array();
            for ( $i=0 ; $i<count($aColumns); $i++ )
            {  
					if(count(explode('.',$aColumns[$i]))==2){
						$aColumns[$i]=explode('.',$aColumns[$i]);
						$aColumns[$i]=$aColumns[$i][1];
					}  
                   if ( $aColumns[$i] != ' ' ){
						if($aColumns[$i]=="created"||$aColumns[$i]=="to_date"){
						 if($aColumns[$i]=="to_date"){
							if(is_null($aRow[ $aColumns[$i] ])){
								$row[] = $aRow[ $aColumns[$i] ];
							}else{
								  $dt = new DateTime();
								  $currentDateTime= $dt->format('Y-m-d H:i:s');
								  if($aRow[ $aColumns[$i] ]>$currentDateTime){	
									$row[] = date("m/d/Y H:i:s", strtotime($aRow[ $aColumns[$i] ])).' '.'<span class="label label-sm label-danger">Active</span>';
								  }elseif($aRow[ $aColumns[$i] ]<$currentDateTime){
									$row[] = date("m/d/Y H:i:s", strtotime($aRow[ $aColumns[$i] ])).' '.'<span class="label label-sm label-warning">Expired</span>';
								  }else{
									$row[] = $aRow[ $aColumns[$i] ];
								  }
							}
						 }else{
							$row[] = date("m/d/Y H:i:s", strtotime($aRow[ $aColumns[$i] ]));
						 }
						}
						else {
							$row[] = $aRow[ $aColumns[$i] ];
						}
					}
            }
            $output['aaData'][] = $row;
        }
        return $output;
	}	
}

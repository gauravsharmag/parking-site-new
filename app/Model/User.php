<?php
App::uses('AppModel', 'Model');
App::uses('SimplePasswordHasher','Controller/Component/Auth');
/**
 * User Model
 *
 * @property Role $Role
 * @property CustomerPass $CustomerPass
 * @property Transaction $Transaction
 * @property Vehicle $Vehicle
 */
class User extends AppModel {
    public $virtualFields = array('property_manager' => 'CONCAT(User.first_name, " ", User.last_name," -- ", User.email)');

    //The Associations below have been created with all possible keys, those that are not needed can be removed
 
 public $belongsTo = array(
        'Role' => array(
            'className' => 'Role',
            'foreignKey' => 'role_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );
/************************************* 
 * ACL Implementation
 * 
 */
   public $actsAs = array('Acl' => array('type' => 'requester'));

   public function parentNode() {
        if (!$this->id && empty($this->data)) {
            return null;
        }
        if (isset($this->data['User']['role_id'])) {
            $roleId = $this->data['User']['role_id'];
        } else {
            $roleId = $this->field('role_id');
        }
        if (!$roleId) {
            return null;
        } else {
            return array('Role' => array('id' => $roleId));
        }
    }
    /**
     * hasMany associations
     *
     * @var array
     */
    public $hasMany = array(
        'CustomerPass' => array(
            'className' => 'CustomerPass',
            'foreignKey' => 'user_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        'Transaction' => array(
            'className' => 'Transaction',
            'foreignKey' => 'user_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        'Vehicle' => array(
            'className' => 'Vehicle',
            'foreignKey' => 'user_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        'PropertyUser' => array(
            'className' => 'PropertyUser',
            'foreignKey' => 'user_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        'BillingAddress' => array(
            'className' => 'BillingAddress',
            'foreignKey' => 'user_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        )
    );
    var $validate=array
    (
        'username'=>array(
            'Only alphabets and numbers allowed'=>array('rule'=>'alphaNumeric','required'=>true,'allowEmpty'=>false),
            'Minimum 8 character required'=>array('rule'=>array('minLength',3),'message'=>'Minimum 3 characters required'),
            'isUnique'=>array('rule'=>'isUnique','message'=>'Sorry, username has already been taken')

        ),
        'password'=>array( 
            'No spaces allowed, Only ~`!@#$%^&.* allowed' => array('rule' => array('custom','/^[a-zA-Z0-9~`!_@#$%.^&*]*$/'),'required' => true,'allowEmpty'=>false),
            'matchPasswords'=>array('rule'=>'matchPasswords','message'=>'Password does not match'),
            'Minimum Length'=>array('rule'=>array('minLength',8),'message'=>'Minimum 8 characters required'),

        ),
        'password_confirmation'=>array(
             //'Only alphabets and number allowed' => array('rule' => 'alphaNumeric','required' => true),
			 'Minimum Length'=>array('rule'=>array('minLength',8),'message'=>'Minimum 8 characters required','required' =>true)
        ),
		 'current_password'=>array(
			'Cannot contain spaces' => array('rule' => 'notBlank','required' => false,'allowEmpty'=>false),
            'Minimum Length'=>array('rule'=>array('minLength',8),'message'=>'Minimum 8 characters required'),
			'Verify Password'=>array('rule'=>'changePassWord','message'=>'Current Password Not Matched')
        ),
		'new_password'=>array(
            'No spaces allowed, Only ~`!@#$%^&.* allowed' => array('rule' => array('custom','/^[a-zA-Z0-9~`!@#$%^&*.]*$/'),'required' => false,'allowEmpty'=>false),
            'matchPasswords'=>array('rule'=>'matchNewPassword','message'=>'Password does not match'),
            'Minimum Length'=>array('rule'=>array('minLength',8),'message'=>'Minimum 8 characters required'),
        ),
		'confirm_password'=>array(
             'required'=>false,
			 'allowEmpty'=>false,
            'rule'=>array('minLength',8),
            'message'=>'Min 8 character required'
        ),
        'email'=>array(
            'Email Required'=>array('rule'=>'email','required'=>true,'message'=>'Email is not correct',),
            'isUnique'=>array('rule'=>'isUnique','message'=>'An account with this email is already registered')
        ),
        'property_password'=>array(
            'Not Empty'=>array('rule'=>'notBlank','required'=>'true'),
            'Verify Details'=>array('rule'=>'validatePropertyDetails','message'=>'Property name and password does not match')
        ),
        'property_id'=>array(
            'Not Empty'=>array('rule'=>'notBlank','required'=>'true')
        ),
        'first_name'=>array(
            'rule' => array('custom','/^[a-zA-Z ]*$/'),
            'message' => 'Only alphabets allowed',
            'required'=>true,
			'allowEmpty'=>false

        ),
        'last_name'=>array(
            'rule' => array('custom','/^[a-zA-Z ]*$/'),
            'message' => 'Only alphabets allowed',
            'required'=>true,
			'allowEmpty'=>false

        ),
        'address_line_1'=>array(
            'rule'    => array('minLength', '2'),
            'message' => 'Minimum 2 characters required',
            'required'=>true,
			'allowEmpty'=>false
        ),
        'address_line_2'=>array(
            'rule'    => array('minLength', '2'),
            'message' => 'Minimum 2 characters required',
			'required'=>true,
			'allowEmpty'=>true
        ),
        'zip' => array(
            'rule' => array('postal', null, 'us'),
            'message'=>'Please check your zip code',
            'required'=>true,
			'allowEmpty'=>false
        ),
        'phone' => array(
            'rule' => array('phone', null, 'us'),
            'message'=>'Please check your phone number',
            'required'=>true
        ),
        'city'=>array(
            'rule' => array('custom','/^[a-zA-Z 0-9]*$/'),
            'message' => 'No special characters allowed',
            'required'=>true,
			'allowEmpty'=>false
        ),
        'state'=>array(
            'rule'    => array('minLength', '2'),
            'message' => 'Minimum 2 characters required',
            'required'=>true,
			'allowEmpty'=>false
        ),
		
        'tos' => array(
            'notBlank' => array(
                'rule'     => array('comparison', '!=', 0),
                'required' => true,
                'message'  => 'Please check this box if you want to proceed.'
            ))


    );

    var $userAdd=array(
        'username'=>array(
            'Only alphabets and numbers allowed'=>array('rule'=>'alphaNumeric','required'=>true),
            'Minimum 8 character required'=>array('rule'=>array('minLength',3),'message'=>'Minimum 3 characters required'),
            'isUnique'=>array('rule'=>'isUnique','message'=>'Sorry, username has already been taken')

        ),
        'password'=>array(
            'No spaces allowed, Only ~`!@#$%^&.* allowed' => array('rule' => array('custom','/^[a-zA-Z0-9~`!@#$%^&.*]*$/'),'required' => true,'allowEmpty'=>false),
            'matchPasswords'=>array('rule'=>'matchPasswords','message'=>'Password does not match'),
            'Minimum Length'=>array('rule'=>array('minLength',8),'message'=>'Minimum 8 characters required'),

        ),
        'password_confirmation'=>array(
            'required'=>'true',
            'rule'=>array('minLength',8),
            'message'=>'Minimum 8 characters required',
			'allowEmpty'=>false
        ),
        'email'=>array(
            'Email Required'=>array('rule'=>'email','required'=>true,'allowEmpty'=>false,'message'=>'Email is not correct',),
            'isUnique'=>array('rule'=>'isUnique','message'=>'An account with this email is already registered')
        ),
         'first_name'=>array(
            'rule' => array('custom','/^[a-zA-Z ]*$/'),
            'message' => 'Only Alphabets Allowed',
            'required'=>true,
			'allowEmpty'=>false

        ),
         'last_name'=>array(
            'rule' => array('custom','/^[a-zA-Z ]*$/'),
            'message' => 'Only Alphabets Allowed',
            'required'=>true,
			'allowEmpty'=>false
        ),

        'role'=> array(
            'rule' => array('notBlank'),
            'message' => 'Please select role '),

    ); 
    public function validateCoupon(){
		App::import('Model', 'CouponCode');
		$coupon = new CouponCode();
		$checkCoupon=$coupon->hasAny(array('code'=>$this->data['User']['coupon']));
		if($checkCoupon){
			$allPackages=$coupon->find('all',array('conditions'=>array('code'=>$this->data['User']['coupon'])));
			$thisPropertyCoupon=false;
			for($i=0;$i<count($allPackages);$i++){
				if($allPackages[$i]['CouponPackage']['property_id']==$this->data['User']['property_id']){
					CakeSession::write('CurrentPackage',$allPackages[$i]['CouponPackage']['id']);
					$thisPropertyCoupon=true;
				}
			}
			return $thisPropertyCoupon;
		}else{
			return false;
		}
	}
	public function getTableData($prop_id){
		$aColumns = array('first_name', 'last_name', 'username', 'email', 'phone','address_line_1','address_line_2','user_id','property_users.id' );
         
        /* Indexed column (used for fast and accurate table cardinality) */
        $sIndexColumn = "property_users.id";
         
        /* DB table to use */ 
        $sTable = "property_users";
        $sJoinTable=' INNER JOIN users ON users.id=property_users.user_id AND property_users.property_id='.$prop_id.' ';
        $sConditions=' property_users.role_id=4 ';
        $outs=$this->simplifyDataTable2(array('columns'=>$aColumns,'index_column'=>$sIndexColumn,'table'=>$sTable,'join'=>$sJoinTable,'conditions'=>$sConditions));
        return $outs;
	}
	public function matchPasswords($data)
    {
        if($data['password']==$this->data['User']['password_confirmation'])
        {
           // unset($this->data['User']['password_confirmation']);
            return true;
        }else{
			$this->invalidate('password_confirmation','Password does not match');
			return false;
		}
    }
   /* function beforeValidate($options = null)
    {
       if(CakeSession::check('PropertyId')==true){
		 return true;
	   }
	   elseif(!isset($this->data['User']['property_id']))
        {
            $this->validate=$this->userAdd;
            return true;
        }else{
			return true;
		}
    }*/
    public function beforeSave($options=array())
    {
        if(isset($this->data['User']['password_changed'])){
					if($this->data['User']['password_changed']==0){
						unset($this->data['User']['password']);
						unset($this->data['User']['password_confirmation']);
					}
		}
        if(isset($this->data['User']['password']))
        {
            $passwordHasher=new SimplePasswordHasher();
            $this->data['User']['password']=$passwordHasher->hash($this->data['User']['password']);
            unset($this->data['User']['password_confirmation']);
        }
        return true;
    }

   public function validatePropertyDetails($options=array())
    {
        $attr = new Property();
        $array=$attr->find('first',array('conditions'=>array('Property.id'=>$this->data['User']['property_id'],'AND'=>array('Property.password'=>$this->data['User']['property_password']))));
        if(!empty($array))
        {
            return true;
        }
        else
        {
            $this->invalidate('property_id','Property name and password does not match');
            return false;
        }
    }
	public function changePassWord()
	{
			$passwordHasher=new SimplePasswordHasher();
			$currentPassword=$passwordHasher->hash($this->data['User']['current_password']);
			$existingPass=$this->field('password',array('id'=>CakeSession::read("Auth.User.id")));
			if($existingPass!=$currentPassword){
				return false;
			}else{
				return true;
			}
	}
	 public function matchNewPassword()
    {
        if($this->data['User']['new_password']==$this->data['User']['confirm_password'])
        {
            return true;
        }
        $this->invalidate('confirm_password','Password does not match');
        return false;
    }
    public function getCustomerData(){
		$aColumns = array('username','first_name','last_name','email','phone','user_id');
        $sIndexColumn = "property_users.id";
        $sTable = "property_users";
        $sJoinTable=' INNER JOIN users ON users.id=property_users.user_id AND property_users.property_id='.CakeSession::read('PropertyId').' ';
         $sConditions=' property_users.role_id=4 ';
        $outs=$this->managerCustomerDetails(array('columns'=>$aColumns,'index_column'=>$sIndexColumn,'table'=>$sTable,'join'=>$sJoinTable,'conditions'=>$sConditions));
        return $outs;
	}
	public function adminGetCustomerData(){
		$aColumns = array('username','first_name','last_name','email','phone','user_id');
        $sIndexColumn = "property_users.id";
        $sTable = "property_users";
        $sJoinTable=' INNER JOIN users ON users.id=property_users.user_id AND property_users.property_id='.CakeSession::read('PropertyId').' ';
         $sConditions=' property_users.role_id=4 ';
        $outs=$this->adminCustomerDetails(array('columns'=>$aColumns,'index_column'=>$sIndexColumn,'table'=>$sTable,'join'=>$sJoinTable,'conditions'=>$sConditions));
        return $outs;
	}
	
	 public function managergetCustomerData(){
		$aColumns = array('username','first_name','last_name','email','phone','address_line_1','address_line_2','user_id');
        $sIndexColumn = "property_users.id";
        $sTable = "property_users";
        $sJoinTable=' INNER JOIN users ON users.id=property_users.user_id AND property_users.property_id='.CakeSession::read('PropertyId').' ';
         $sConditions=' property_users.role_id=4 ';
        $outs=$this->managerCustomerDetails(array('columns'=>$aColumns,'index_column'=>$sIndexColumn,'table'=>$sTable,'join'=>$sJoinTable,'conditions'=>$sConditions));
        return $outs;
	}

}

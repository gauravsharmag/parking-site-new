<?php
App::uses('AppModel', 'Model');
/**
 * CouponCodeUser Model
 *
 * @property CouponCode $CouponCode
 * @property User $User
 */
class CouponCodeUser extends AppModel {


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'CouponCode' => array(
			'className' => 'CouponCode',
			'foreignKey' => 'coupon_code_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}

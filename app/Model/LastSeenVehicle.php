<?php
App::uses('AppModel', 'Model');
/**
 * UserGuestPass Model
 *
 * @property User $User
 * @property CustomerPass $CustomerPass
 * @property Vehicle $Vehicle
 */
class LastSeenVehicle extends AppModel {
	public function getLastSeenStatus($rfidTag){
		$aColumns = array('lsv.rfid','user.username','property.name','lsv.vehicle_details','lsv.last_seen','lsv.created','p.name as passname','cp.pass_valid_upto','cp.id','cp.user_id');
         
        /* Indexed column (used for fast and accurate table cardinality) */
        $sIndexColumn = "lsv.id";
         
        /* DB table to use */ 
        $sTable = "last_seen_vehicles lsv";
        $sJoinTable=' LEFT JOIN customer_passes cp ON lsv.customer_pass_id = cp.id LEFT JOIN users user ON cp.user_id = user.id LEFT JOIN passes p ON cp.pass_id = p.id LEFT JOIN properties property ON lsv.property_id = property.id ';
        $sConditions=' lsv.rfid= "'.$rfidTag.'" ';
        $outs=$this->simplifyDataTableRFID(array('columns'=>$aColumns,'index_column'=>$sIndexColumn,'table'=>$sTable,'join'=>$sJoinTable,'conditions'=>$sConditions));
        return $outs;
	}
	public function getLastSeenStatusProppertyWise($fromdate,$toDate,$propertyId){
		$aColumns = array('lsv.rfid','user.username','property.name','lsv.vehicle_details','lsv.last_seen','lsv.created','p.name as passname','cp.pass_valid_upto','cp.id','cp.user_id');
          
        /* Indexed column (used for fast and accurate table cardinality) */
        $sIndexColumn = "lsv.id";
         
        /* DB table to use */ 
        $sTable = "last_seen_vehicles lsv";
        $sJoinTable=' LEFT JOIN customer_passes cp ON lsv.customer_pass_id = cp.id LEFT JOIN users user ON cp.user_id = user.id LEFT JOIN passes p ON cp.pass_id = p.id LEFT JOIN properties property ON lsv.property_id = property.id ';
        $sConditions=' lsv.property_id= "'.$propertyId.'" AND (lsv.last_seen BETWEEN "'.$fromdate.'" AND "'.$toDate.'") ';
        $outs=$this->simplifyDataTableRFID(array('columns'=>$aColumns,'index_column'=>$sIndexColumn,'table'=>$sTable,'join'=>$sJoinTable,'conditions'=>$sConditions));
        return $outs;
	}
	
	public function simplifyDataTableRFID($config=array()){
		if(!isset($config['columns']) || !isset($config['index_column']) || !isset($config['table'])){
				return array();
		}
		$aColumns=$config['columns'];
		$sIndexColumn=$config['index_column'];
		$sTable=$config['table'];
		$sJoinTable=isset($config['join'])? $config['join'] : '';
		$sCondition=isset($config['conditions'])? $config['conditions'] : '';
        App::uses('ConnectionManager', 'Model');
        $dataSource = ConnectionManager::getDataSource('default');
        $gaSql['user']       = $dataSource->config['login'];
        $gaSql['password']   = $dataSource->config['password'];
        $gaSql['db']         = $dataSource->config['database'];
        $gaSql['server']     = $dataSource->config['host'];
       
        function fatal_error ( $sErrorMessage = '' ){
            header( $_SERVER['SERVER_PROTOCOL'] .' 500 Internal Server Error' );
            die( $sErrorMessage );
        }
        /*
         * MySQL connection
        */
        if ( ! $gaSql['link'] = mysqli_connect( $gaSql['server'], $gaSql['user'], $gaSql['password'],$gaSql['db']) ){
            fatal_error( 'Could not open connection to server' );
        }
        
        if ( ! mysqli_select_db($gaSql['link'],$gaSql['db']) ){
            fatal_error( 'Could not select database ' );
        }
         
         
        /*
         * Paging
        */
        $sLimit = "";
        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' ){
            $sLimit = "LIMIT ".intval( $_GET['iDisplayStart'] ).", ".
            intval( $_GET['iDisplayLength'] );
        }
        /*
         * Ordering
        */
        $sOrder = "";
        if ( isset( $_GET['iSortCol_0'] ) ){
            $sOrder = "ORDER BY  ";
            for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ ){
                if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" ){
                    $sOrder .= $aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
                        ($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
                }
            }
            $sOrder = substr_replace( $sOrder, "", -2 );
            if ( $sOrder == "ORDER BY" ){
                $sOrder = "";
            }
        }
        /*
         * Filtering
        * NOTE this does not match the built-in DataTables filtering which does it
        * word by word on any field. It's possible to do here, but concerned about efficiency
        * on very large tables, and MySQL's regex functionality is very limited
        */
        $sWhere = "";
        if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" ){
            $sWhere = "WHERE (";
            for ( $i=0 ; $i<count($aColumns) ; $i++ ){
                $sWhere .= $aColumns[$i]." LIKE '%".mysqli_real_escape_string($gaSql['link'], $_GET['sSearch'] )."%' OR ";
            }
            $sWhere = substr_replace( $sWhere, "", -3 );
            $sWhere .= ')';
        }
        for ( $i=0 ; $i<count($aColumns) ; $i++ ){
            if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' ){
                if ( $sWhere == "" ){
                    $sWhere = "WHERE ";
                } else{
                    $sWhere .= " AND ";
                }
                $sWhere .= $aColumns[$i]." LIKE '%".mysqli_real_escape_string($gaSql['link'],$_GET['sSearch_'.$i])."%' ";
            }
        }
        if($sCondition!=''){
            if ( $sWhere == "" ){
                $sWhere="WHERE ".$sCondition." ";
            }else{
                $sWhere .=" AND ".$sCondition." ";
            }
        }
        /*
         * SQL queries
        * Get data to display 
        */
        $sQuery = "
    SELECT SQL_CALC_FOUND_ROWS ".str_replace(" , ", " ", implode(", ", $aColumns))."
            FROM   $sTable $sJoinTable 
            $sWhere
            $sOrder
            $sLimit
            ";
       //debug($sQuery);die;
        $rResult = mysqli_query( $gaSql['link'],$sQuery) or fatal_error( 'MySQL Error Here 1: ' . mysqli_errno($gaSql['link']) );
         
        /* Data set length after filtering */
        $sQuery = "
    SELECT FOUND_ROWS()
";
        $rResultFilterTotal =mysqli_query( $gaSql['link'],$sQuery)  or fatal_error( 'MySQL Error Here 2: ' . mysqli_errno($gaSql['link']) );
        $aResultFilterTotal = mysqli_fetch_array($rResultFilterTotal);
        $iFilteredTotal = $aResultFilterTotal[0];
         
        /* Total data set length */
        $sQuery = "
    SELECT COUNT(".$sIndexColumn.")
            FROM   $sTable $sJoinTable
            ";
        $rResultTotal = mysqli_query( $gaSql['link'],$sQuery) or fatal_error( 'MySQL Error Here 3: ' . mysqli_errno($gaSql['link']) );
        $aResultTotal = mysqli_fetch_array($rResultTotal);
        $iTotal = $aResultTotal[0];
         
        /*
         * Output
        */
        $output = array(
                "sEcho" => intval($_GET['sEcho']),
                "iTotalRecords" => $iTotal,
                "iTotalDisplayRecords" => $iFilteredTotal,
                "aaData" => array()
        );
         
        while ( $aRow = mysqli_fetch_array( $rResult ) )
        {
            //debug($aRow);die;
            $row = array();
            for ( $i=0 ; $i<count($aColumns); $i++ )
            {  
					if(count(explode(' ',$aColumns[$i]))==3){
						$aColumns[$i]=explode(' ',$aColumns[$i]);
						$aColumns[$i]=$aColumns[$i][2];
					}
					if(count(explode('.',$aColumns[$i]))==2){
							$aColumns[$i]=explode('.',$aColumns[$i]);
							$aColumns[$i]=$aColumns[$i][1];
					}					  
                   if ( $aColumns[$i] != ' ' ){
						if($aColumns[$i]=="id") {
								$row[] = "<a class='btn green btn-xs green-stripe' href='/admin/CustomerPasses/edit/".$aRow[ $aColumns[$i] ]."'>Edit RFID Tag</a>";
						}elseif($aColumns[$i]=="username"){
								$row[] = '<a href="/admin/users/view_user_details/'.$aRow['user_id'].'">'.$aRow[ $aColumns[$i] ].'</a>';
						}elseif($aColumns[$i]=="pass_valid_upto"||$aColumns[$i]=="membership_vaild_upto"){
							if(is_null($aRow[ $aColumns[$i] ])){
								$row[] = $aRow[ $aColumns[$i] ];
							}else{
								  $dt = new DateTime();
								  $currentDateTime= $dt->format('Y-m-d H:i:s');
								  if($aRow[ $aColumns[$i] ]>$currentDateTime){	
									$row[] = date("m/d/Y H:i:s", strtotime($aRow[ $aColumns[$i] ])).' '.'<span class="label label-sm label-danger">Active</span>';
								  }elseif($aRow[ $aColumns[$i] ]<$currentDateTime){
									$row[] = date("m/d/Y H:i:s", strtotime($aRow[ $aColumns[$i] ])).' '.'<span class="label label-sm label-warning">Expired</span>';
								  }else{
									$row[] = $aRow[ $aColumns[$i] ];
								  }
							}
						}elseif($aColumns[$i]=="last_seen"||$aColumns[$i]=="created"){
							$row[] = date("m/d/Y H:i:s", strtotime($aRow[ $aColumns[$i] ]));
						}
						else {
							$row[] = $aRow[ $aColumns[$i] ];
						}
					}
            }
            $output['aaData'][] = $row;
        }
        return $output;
	}
}

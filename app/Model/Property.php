<?php
App::uses('AppModel', 'Model');
App::uses('SimplePasswordHasher','Controller/Component/Auth');
/**
 * Property Model
 *
 * @property CustomerPass $CustomerPass
 * @property Package $Package
 * @property Pass $Pass
 * @property Vehicle $Vehicle
 * @property PropertyUser $PropertyUser
 */
class Property extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';

/**
 * Validation rules
 *
 * @var array
 */
    public $hasMany = array(
        'CustomerPass' => array(
            'className' => 'CustomerPass',
            'foreignKey' => 'property_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        'Package' => array(
            'className' => 'Package',
            'foreignKey' => 'property_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        'Pass' => array(
            'className' => 'Pass',
            'foreignKey' => 'property_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        'Vehicle' => array(
            'className' => 'Vehicle',
            'foreignKey' => 'property_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        'PropertyUser' => array(
            'className' => 'PropertyUser',
            'foreignKey' => 'property_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        )
    );
	public $validate = array(
        'name' => array(
                'minLength' => array(
                'rule' => array('minLength',2),
                'message' => 'Minimum 2 characters required',
                'required' => true,
                'allowEmpty' => false
               ),
        'Unique'=>array( 'rule'=>'isUnique','message'=>'This property already exists'),
			'notBlank' => array(
				'rule' => array('notBlank'),
			    'message' => 'Property name is required',

			),


		),
        'sub_domain' => array(
            'No Space'=>array( 'rule' => '/^[a-z0-9]{3,}$/i','message'  => 'Sub domain should not contain blank spaces and special characters'),
            'isUnique'=>array('rule'=>'isUnique','message'=>'This sub domain already exists'),
            'notBlank' => array(
                'rule' => 'notBlank',
                'message' => 'This value may not be left empty!'
            ),
        ),
        'logo'=>array(
            'uploadError'=>array(
                'rule'=>'uploadError',
                'message'=>'Upload Fail',
                'allowEmpty'=>true
            ),
            'extension'=>array(
                'rule'=>array('extension',array('png','jpg','gif','jpeg')),
                'message'=>'Please upload images(gif, jpg, png )',
                'allowEmpty'=>true
            ),
            'fileSize'=>array(
                'rule'=>array('fileSize','<=','1MB'),
                'message'=>'Image size must be less than 1MB',
                'allowEmpty'=>true
            ),
            'processPhotoUpload'=>array(
                'rule'=>'processPhotoUpload',
                'message'=>'Unable to process image',
                'allowEmpty'=>true
            )
        ),
        'password'=>array('Only alphabets and number allowed' => array(
            'rule' => 'alphaNumeric','required' => true,'message'=>'Password must be alphanumeric'),
            'Minimum Length'=>array('rule'=>array('minLength',3),'message'=>'Minimum 3 characters required'),
            'notBlank' => array(
                'rule' => 'notBlank',
                'message' => 'This value may not be left empty!'
            ),

        ),
        'terms_and_conditions'=>array('rule' => 'notBlank','required'=>true,'message'=>'Terms and conditions are required '),
		'total_passes_per_user'=> array(
			'numeric' => array(
				'rule' => array('numeric'),
				'message' => 'Only numeric data allowed',
				'allowEmpty' => false,
				'required' => true
			),
		),
		'free_guest_credits'=> array(
			'numeric' => array(
				'rule' => array('numeric'),
				'message' => 'Only numeric data allowed',
				'allowEmpty' => false,
				'required' => true
			),
		),
		'total_space' => array(
			'numeric' => array(
				'rule' => array('numeric'),
                'message' => 'Only numeric data allowed',
                'allowEmpty' => true,
                'required' => true
			),
		),




    );
    var $validatePropertyManager=array(
        'username'=>array(
            'Only alphabets and numbers allowed'=>array('rule'=>'alphaNumeric','required'=>true),
            'Minimum 8 character required'=>array('rule'=>array('minLength',3),'message'=>'Minimum 3 characters required'),
            'isUnique'=>array('rule'=>'isPasswordUnique','message'=>'Sorry, username has already been taken')

        ),
        'manager_password'=>array(
            'Only alphabets and number allowed' => array('rule' => 'alphaNumeric','required' => true),
            'matchPasswords'=>array('rule'=>'matchPasswords','message'=>'Password does not match'),
            'Minimum Length'=>array('rule'=>array('minLength',8),'message'=>'Minimum 8 characters required'),

        ),
        'password_confirmation'=>array(
            'required'=>'true',
            'rule'=>array('minLength',8),
            'message'=>'Min 8 character required'
        ),
        'email'=>array(
            'Email Required'=>array('rule'=>'email','required'=>true,'message'=>'Email is not correct',),

            'isUnique'=>array('rule'=>'isEmailUnique','message'=>'An account with this email is already registered')
        ),
        'first_name'=>array(
            'rule'    => array('minLength', '2'),
            'message' => 'Minimum 2 characters required',
            'required'=>true

        ),
        'last_name'=>array(
            'rule'    => array('minLength', '2'),
            'message' => 'Minimum 2 characters required',
            'required'=>true

        ),
    );

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */

    public function processPhotoUpload($check=array())
    {
        if(!is_uploaded_file($check['logo']['tmp_name'])){
            return false;
        }
        if(!(move_uploaded_file($check['logo']['tmp_name'],WWW_ROOT.'img'.DS.'logo'.DS.$check['logo']['name']))){
            return false;
        }
        else{
            return true;
        }
    }
    public function beforeValidate($options = array())
    {
       if(!isset($this->data['Property']['id']))
       {
           $this->validate=array_merge($this->validate,$this->validatePropertyManager);
       }
        if(isset($this->data['Property']['logo']))
        {
        if($this->data['Property']['logo']['size']!=0){
            $arr=array('image/jpg','image/gif','image/png','image/jpeg');
            if(in_array($this->data['Property']['logo']['type'],$arr)){
                list($a,$ext)=explode('/',$this->data['Property']['logo']['type']);
                list($c,$b)=explode(' ',microtime());
                $name=$a.$c.$b;
                list($e,$f)=explode('.',$name);
                $photoName=$e.$f;
                $this->data['Property']['logo']['name']=$photoName.".$ext";
                $this->pathNameImage = $photoName.".$ext";
                return true;
            }
        }

        else{
            return true;
        }
        }
        else{
            return true;
        }

    }
    public function beforeSave($options=null)
    {
        if(isset($this->data['Property']['logo'])){
            if(isset($this->data['Property']['id'])){
                $this->unbindModel(array('hasMany'=>array('Pass','PropertyUser','VehicleType','Vehicle')));
                //$this->recursive=-1;
                $oldImage=$this->find('first',array('fields'=>array('logo'),'conditions'=>array('id'=>$this->data['Property']['id'])));
                $image= new File(WWW_ROOT."img/logo/".$oldImage['Property']['logo'],false, 0777);

                if($image->exists()){
                    $image->delete();
                    return true;
                }
            }
            return true;
        }
        return true;

    }
    public function getPathNameImage()
    {
        return $this->pathNameImage;
    }
    public function isPasswordUnique($username=array())
    {

        App::import('model','User');
        $user = new User();
        $user->recursive=-1;
        $result=$user->find('first',array('conditions'=>array('username'=>$username)));
        if(!empty($result)){
            return false;
        }
        else{
            return true;
        }
    }
    public function isEmailUnique($email=array())
    {
        App::import('model','User');
        $user = new User();
        $user->recursive=-1;
        $result=$user->find('first',array('conditions'=>array('email'=>$email)));
        if(!empty($result)){
            return false;
        }
        else{
            return true;
        }
    }
    public function matchPasswords($data)
    {
        if($this->data['Property']['manager_password']==$this->data['Property']['password_confirmation'])
        {
            unset($this->data['Property']['password_confirmation']);
            return true;
        }
        $this->invalidate('password_confirmation','Password does not match');
        return false;
    }
    public function givePropertyName($id){
		if (!$this->exists($id)) {
            return "No Property Found";
        }else{
			return $this->field('Property.name',array('Property.id'=>$id));
		}
	}



}

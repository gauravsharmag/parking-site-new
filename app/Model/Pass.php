<?php
App::uses('AppModel', 'Model');
/**
 * Pass Model
 *
 * @property Property $Property
 * @property Transaction $Transaction
 */
class Pass extends AppModel {

    /**
     * Validation rules
     *
     * @var array
     */
    public $validate = array(

        'cost_1st_year' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                'message' => 'Only Numeric Data Allowed',
                'allowEmpty' => false,
                'required' => true,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'cost_after_1st_year' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                'message' => 'Only Numeric Data Allowed',
                'allowEmpty' => false,
                'required' => true,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'deposit' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                'message' => 'Only numeric data allowed',
                'allowEmpty' => true,
                'required' => true,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'name'=>array(
            'rule' => array('custom','/^[a-zA-Z 0-9]*$/'),
            'message' => 'No Special Character Allowed',
            'required'=>true,
            'allowEmpty' => false,
        ),

    );
    public $validateDate = array(
        'expiration_date'=> array(
            'datetime' => array(
                'rule' => array('date','mdy'),
                'message' => 'Date is either empty or format is incorrect',
                'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),



    );
    public $validateDuration=array(
        'duration_type'=> array(
            'rule' => array('notBlank'),
            'message' => 'Please select a duration type '),
        'duration' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                'message' => 'Duration is either empty or contain non numeric data',
                'allowEmpty' => false
            ))

    );

    //The Associations below have been created with all possible keys, those that are not needed can be removed
    public $hasOne = array(
        'Package' => array(
            'className' => 'Package',
            'foreignKey' => 'pass_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );
    /**
     * belongsTo associations
     *
     * @var array
     */
    public $belongsTo = array(
        'Property' => array(
            'className' => 'Property',
            'foreignKey' => 'property_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),

    );

    /**
     * hasMany associations
     *
     * @var array
     */
    public $hasMany = array(
        'Transaction' => array(
            'className' => 'Transaction',
            'foreignKey' => 'pass_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
        'CustomerPass' => array(
            'className' => 'CustomerPass',
            'foreignKey' => 'pass_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        )
    );
    public function beforeValidate($options=null)
    {
        if($this->data['Pass']['is_fixed_duration']==0){
            $this->validate=array_merge($this->validate,$this->validateDuration);

        }
        else{
            $this->validate=array_merge($this->validate,$this->validateDate);
        }
    }
    public function beforeSave($options = null)
    {
       if(isset($this->data['Pass']['is_fixed_duration' ])){
        if($this->data['Pass']['is_fixed_duration' ]==1){
            $this->data['Pass']['expiration_date'] = date("Y-m-d", strtotime($this->data['Pass']['expiration_date']));
        }
       }
        if(!isset($this->data['Pass']['id'])){
            $this->recursive=-1;
            $total_no_of_pass= $this->find('count',array('conditions'=>array('property_id'=>$this->data['Pass']['property_id'])));
            $this->data['Pass']['sort_order']=$total_no_of_pass+1;
        }
    }
    public function getRemainingPass($propertyId=null,$userId=null)
    {
        //debug($userId);
        $dt = new DateTime();
        $currentDateTime= $dt->format('Y-m-d H:i:s');
        //$result=$this->find('all',array('conditions'=>array( 'property_id'=>$propertyId,'AND'=>array('expiration_date > \''.$currentDateTime.'\''))));
        //$result1=$this->find('all',array('conditions'=>array('property_id'=>$propertyId,'AND'=>array( 'expiration_date' => null))));
        $this->recursive=-1;
        $result=$this->find('all',array('conditions'=>array('id not in(select pass_id from customer_passes where property_id='.$propertyId.' and user_id='.$userId.') and property_id='.$propertyId.' and is_fixed_duration = 1 and expiration_date >\''.$currentDateTime.'\'')));
        $this->recursive=-1;
        $result1=$this->find('all',array('conditions'=>array('id not in(select pass_id from customer_passes where property_id='.$propertyId.' and user_id='.$userId.') and property_id='.$propertyId.' and is_fixed_duration = 0')));
        $i=count($result1);
        for($j=0;$j<$i;$j++){
            if($result1[$j]['Pass']['is_fixed_duration']==0){
                switch ($result1[$j]['Pass']['duration_type']) {
                    case "Hour":
                        $date = new DateTime($currentDateTime);
                        $k=(int)$result1[$j]['Pass']['duration'];
                        $date->add(new DateInterval('PT'.$k.'H'));
                        $result1[$j]['Pass']['expiration_date']=date_format($date, 'Y-m-d H:i:s');
                        break;
                    case "Day":
                        $date = new DateTime($currentDateTime);
                        $k=$result1[$j]['Pass']['duration'];
                        $date->add(new DateInterval('P'.$k.'D'));
                        $result1[$j]['Pass']['expiration_date']=date_format($date, 'Y-m-d H:i:s');
                        break;
                    case "Week":
                        $date = new DateTime($currentDateTime);
                        $k=(int)$result1[$j]['Pass']['duration']*7;
                        $date->add(new DateInterval('P'.$k.'D'));
                        $result1[$j]['Pass']['expiration_date']=date_format($date, 'Y-m-d H:i:s');
                        break;
                    case "Month":
                        $date = new DateTime($currentDateTime);
                        $k=(int)$result1[$j]['Pass']['duration'];
                        $date->add(new DateInterval('P'.$k.'M'));
                        $result1[$j]['Pass']['expiration_date']=date_format($date, 'Y-m-d H:i:s');
                        break;
                    case "Year":
                        $date = new DateTime($currentDateTime);
                        $k=(int)$result1[$j]['Pass']['duration'];
                        $date->add(new DateInterval('P'.$k.'Y'));
                        $result1[$j]['Pass']['expiration_date']=date_format($date, 'Y-m-d H:i:s');
                        break;
                }
            }

        }
        $finalResult=array_merge($result,$result1);
        //debug( $finalResult);
        // die();
        return $finalResult;
    }
    public function givePassName($id){
		if (!$this->exists($id)) {
            return "No Pass Found";
        }else{
			return $this->field('name',array('id'=>$id));
		}
	}


}

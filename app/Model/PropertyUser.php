<?php
App::uses('AppModel', 'Model');
/**
 * PropertyUser Model
 *
 * @property User $User
 * @property Property $Property
 * @property Role $Role
 */
class PropertyUser extends AppModel {
	

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'User' => array(
			'className' => 'User',
			'foreignKey' => 'user_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Property' => array(
			'className' => 'Property',
			'foreignKey' => 'property_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Role' => array(
			'className' => 'Role',
			'foreignKey' => 'role_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
	public function getTableData(){
		$aColumns = array('first_name', 'last_name', 'username', 'email', 'phone','address_line_1','address_line_2','roles.role_name','properties.name','property_users.user_id','roles.id' );
         
        /* Indexed column (used for fast and accurate table cardinality) */
        $sIndexColumn = "property_users.id";
         
        /* DB table to use */ 
        $sTable = "property_users";
        $sJoinTable=' LEFT JOIN users ON users.id=property_users.user_id LEFT JOIN roles ON roles.id=property_users.role_id LEFT JOIN properties ON properties.id=property_users.property_id';
        $sConditions=' property_users.role_id!=1 ';
        $outs=$this->getAllUsersData(array('columns'=>$aColumns,'index_column'=>$sIndexColumn,'table'=>$sTable,'join'=>$sJoinTable,'conditions'=>$sConditions));
        return $outs;
	}
	public function getTableDataCorrected(){
		$aColumns = array('users.first_name', 'users.last_name', 'users.username', 'users.email', 'users.phone','users.address_line_1','users.address_line_2','roles.role_name','properties.name','users.id','users.role_id' );
         
        /* Indexed column (used for fast and accurate table cardinality) */
        $sIndexColumn = "users.id";
         
        /* DB table to use */ 
        $sTable = "users";
        $sJoinTable=' LEFT JOIN roles ON roles.id=users.role_id LEFT JOIN property_users ON users.id=property_users.user_id LEFT JOIN properties ON properties.id=property_users.property_id';
        $sConditions=' users.id != '.AuthComponent::user('id');
        $outs=$this->getAllUsersDataCorrected(array('columns'=>$aColumns,'index_column'=>$sIndexColumn,'table'=>$sTable,'join'=>$sJoinTable,'conditions'=>$sConditions));
        return $outs;
	}
	public function getAllUsersDataCorrected($config=array()){
		if(!isset($config['columns']) || !isset($config['index_column']) || !isset($config['table'])){
				return array();
		}
		$aColumns=$config['columns'];
		$sIndexColumn=$config['index_column'];
		$sTable=$config['table'];
		$sJoinTable=isset($config['join'])? $config['join'] : '';
        
		$sCondition=isset($config['conditions'])? $config['conditions'] : '';
        App::uses('ConnectionManager', 'Model');
        $dataSource = ConnectionManager::getDataSource('default');
         
        /* Database connection information */
        $gaSql['user']       = $dataSource->config['login'];
        $gaSql['password']   = $dataSource->config['password'];
        $gaSql['db']         = $dataSource->config['database'];
        $gaSql['server']     = $dataSource->config['host'];
         
         
        /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
         * If you just want to use the basic configuration for DataTables with PHP server-side, there is
        * no need to edit below this line
        */
          
        /*
         * Local functions
        */
        function fatal_error ( $sErrorMessage = '' ){
            header( $_SERVER['SERVER_PROTOCOL'] .' 500 Internal Server Error' );
            die( $sErrorMessage );
        }
        /*
         * MySQL connection
        */
        if ( ! $gaSql['link'] = mysqli_connect( $gaSql['server'], $gaSql['user'], $gaSql['password'],$gaSql['db']) ){
            fatal_error( 'Could not open connection to server' );
        }
        
        if ( ! mysqli_select_db($gaSql['link'],$gaSql['db']) ){
            fatal_error( 'Could not select database ' );
        }
         
         
        /*
         * Paging
        */
        $sLimit = "";
        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' ){
            $sLimit = "LIMIT ".intval( $_GET['iDisplayStart'] ).", ".
            intval( $_GET['iDisplayLength'] );
        }
        /*
         * Ordering
        */
        $sOrder = "";
        if ( isset( $_GET['iSortCol_0'] ) ){
            $sOrder = "ORDER BY  ";
            for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ ){
                if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" ){
                    $sOrder .= $aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
                        ($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
                }
            }
            $sOrder = substr_replace( $sOrder, "", -2 );
            if ( $sOrder == "ORDER BY" ){
                $sOrder = "";
            }
        }
        /*
         * Filtering
        * NOTE this does not match the built-in DataTables filtering which does it
        * word by word on any field. It's possible to do here, but concerned about efficiency
        * on very large tables, and MySQL's regex functionality is very limited
        */
        $sWhere = "";
        if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" ){
            $sWhere = "WHERE (";
            for ( $i=0 ; $i<count($aColumns) ; $i++ ){
                $sWhere .= $aColumns[$i]." LIKE '%".mysqli_real_escape_string($gaSql['link'], $_GET['sSearch'] )."%' OR ";
            }
            $sWhere = substr_replace( $sWhere, "", -3 );
            $sWhere .= ')';
        }
        for ( $i=0 ; $i<count($aColumns) ; $i++ ){
            if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' ){
                if ( $sWhere == "" ){
                    $sWhere = "WHERE ";
                } else{
                    $sWhere .= " AND ";
                }
                $sWhere .= $aColumns[$i]." LIKE '%".mysqli_real_escape_string($gaSql['link'],$_GET['sSearch_'.$i])."%' ";
            }
        }
        if($sCondition!=''){
            if ( $sWhere == "" ){
                $sWhere="WHERE ".$sCondition." ";
            }else{
                $sWhere .=" AND ".$sCondition." ";
            }
        }
        /*
         * SQL queries
        * Get data to display 
        */
        $sQuery = "
    SELECT SQL_CALC_FOUND_ROWS ".str_replace(" , ", " ", implode(", ", $aColumns))."
            FROM   $sTable $sJoinTable 
            $sWhere
            $sOrder
            $sLimit
            ";
       //debug($sQuery);die;
        $rResult = mysqli_query( $gaSql['link'],$sQuery) or fatal_error( 'MySQL Error Here 1: ' . mysqli_errno($gaSql['link']) );
         
        /* Data set length after filtering */
        $sQuery = "
    SELECT FOUND_ROWS()
";
        $rResultFilterTotal =mysqli_query( $gaSql['link'],$sQuery)  or fatal_error( 'MySQL Error Here 2: ' . mysqli_errno($gaSql['link']) );
        $aResultFilterTotal = mysqli_fetch_array($rResultFilterTotal);
        $iFilteredTotal = $aResultFilterTotal[0];
         
        /* Total data set length */
        $sQuery = "
    SELECT COUNT(".$sIndexColumn.")
            FROM   $sTable $sJoinTable
            ";
        $rResultTotal = mysqli_query( $gaSql['link'],$sQuery) or fatal_error( 'MySQL Error Here 3: ' . mysqli_errno($gaSql['link']) );
        $aResultTotal = mysqli_fetch_array($rResultTotal);
        $iTotal = $aResultTotal[0];
         
        /*
         * Output
        */
        $output = array(
                "sEcho" => intval($_GET['sEcho']),
                "iTotalRecords" => $iTotal,
                "iTotalDisplayRecords" => $iFilteredTotal,
                "aaData" => array()
        );
        
        while ( $aRow = mysqli_fetch_array( $rResult ) )
        {
			
            $row = array();
            for ( $i=0 ; $i<count($aColumns) ; $i++ )
            {
				
				if(count(explode('.',$aColumns[$i]))==2){
					$aColumns[$i]=explode('.',$aColumns[$i]);
					$aColumns[$i]=$aColumns[$i][1];
				}
                if ( $aColumns[$i] == "version" )
                {
                    /* Special output formatting for 'version' column */
                    $row[] = ($aRow[ $aColumns[$i] ]=="0") ? '-' : $aRow[ $aColumns[$i] ];
                }
                else if ( $aColumns[$i] != ' ' )
                {
                   if($aColumns[$i]=="id") {
					  $str = "<a class='btn green btn-xs green-stripe' href='/admin/users/edit/".$aRow[ $aColumns[$i] ]."'>Edit</a>&nbsp;
									  &nbsp;<a class='btn default btn-xs ' href='/admin/users/view_user_details/".$aRow[ $aColumns[$i] ]."'>View</a>&nbsp;
									  &nbsp;<a class='btn yellow btn-xs yellow-stripe' href='/admin/users/print_details/".$aRow[ $aColumns[$i] ]."'target='_blank'>Print</a>&nbsp;
									  ";
					    if($aRow['role_name']!='customer'){
					     $uniqID=uniqid();
					     $str= $str.'<form action="/admin/Users/delete/'.$aRow[ $aColumns[$i] ].'" name="post_'.$uniqID.'" id="post_'.$uniqID.'" style="display:none;" method="post"><input type="hidden" name="_method" value="POST"></form><a href="#" onclick="if (confirm(&quot;Are you sure?&quot;)) { document.post_'.$uniqID.'.submit(); } event.returnValue = false; return false;" class="btn red btn-xs red-stripe">Delete</a>';
						 
					   }
					   if($aRow['role_name']=='customer'){
							 $str .= "<a class='btn red btn-xs red-stripe' href='/admin/CustomerPasses/customer_view/".$aRow[ $aColumns[$i] ]."'>Manage</a>&nbsp;
									  &nbsp;<a class='btn default btn-xs green-stripe' href='/admin/tickets/contact_user/".$aRow[ $aColumns[$i] ]."'>Contact</a>";
							}
					  $row[] = $str;
					}else{
						$row[] = $aRow[ $aColumns[$i] ];
					}
                }
            }
            $output['aaData'][] = $row;
        }
         
        return $output;
	}
	public function getAllUsersData($config=array()){
		if(!isset($config['columns']) || !isset($config['index_column']) || !isset($config['table'])){
				return array();
		}
		$aColumns=$config['columns'];
		$sIndexColumn=$config['index_column'];
		$sTable=$config['table'];
		$sJoinTable=isset($config['join'])? $config['join'] : '';
        
		$sCondition=isset($config['conditions'])? $config['conditions'] : '';
        App::uses('ConnectionManager', 'Model');
        $dataSource = ConnectionManager::getDataSource('default');
         
        /* Database connection information */
        $gaSql['user']       = $dataSource->config['login'];
        $gaSql['password']   = $dataSource->config['password'];
        $gaSql['db']         = $dataSource->config['database'];
        $gaSql['server']     = $dataSource->config['host'];
         
         
        /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
         * If you just want to use the basic configuration for DataTables with PHP server-side, there is
        * no need to edit below this line
        */
         
        /*
         * Local functions
        */
        function fatal_error ( $sErrorMessage = '' ){
            header( $_SERVER['SERVER_PROTOCOL'] .' 500 Internal Server Error' );
            die( $sErrorMessage );
        }
        /*
         * MySQL connection
        */
        if ( ! $gaSql['link'] = mysqli_connect( $gaSql['server'], $gaSql['user'], $gaSql['password'],$gaSql['db']) ){
            fatal_error( 'Could not open connection to server' );
        }
        
        if ( ! mysqli_select_db($gaSql['link'],$gaSql['db']) ){
            fatal_error( 'Could not select database ' );
        }
         
         
        /*
         * Paging
        */
        $sLimit = "";
        if ( isset( $_GET['iDisplayStart'] ) && $_GET['iDisplayLength'] != '-1' ){
            $sLimit = "LIMIT ".intval( $_GET['iDisplayStart'] ).", ".
            intval( $_GET['iDisplayLength'] );
        }
        /*
         * Ordering
        */
        $sOrder = "";
        if ( isset( $_GET['iSortCol_0'] ) ){
            $sOrder = "ORDER BY  ";
            for ( $i=0 ; $i<intval( $_GET['iSortingCols'] ) ; $i++ ){
                if ( $_GET[ 'bSortable_'.intval($_GET['iSortCol_'.$i]) ] == "true" ){
                    $sOrder .= $aColumns[ intval( $_GET['iSortCol_'.$i] ) ]." ".
                        ($_GET['sSortDir_'.$i]==='asc' ? 'asc' : 'desc') .", ";
                }
            }
            $sOrder = substr_replace( $sOrder, "", -2 );
            if ( $sOrder == "ORDER BY" ){
                $sOrder = "";
            }
        }
        /*
         * Filtering
        * NOTE this does not match the built-in DataTables filtering which does it
        * word by word on any field. It's possible to do here, but concerned about efficiency
        * on very large tables, and MySQL's regex functionality is very limited
        */
        $sWhere = "";
        if ( isset($_GET['sSearch']) && $_GET['sSearch'] != "" ){
            $sWhere = "WHERE (";
            for ( $i=0 ; $i<count($aColumns) ; $i++ ){
                $sWhere .= $aColumns[$i]." LIKE '%".mysqli_real_escape_string($gaSql['link'], $_GET['sSearch'] )."%' OR ";
            }
            $sWhere = substr_replace( $sWhere, "", -3 );
            $sWhere .= ')';
        }
        for ( $i=0 ; $i<count($aColumns) ; $i++ ){
            if ( isset($_GET['bSearchable_'.$i]) && $_GET['bSearchable_'.$i] == "true" && $_GET['sSearch_'.$i] != '' ){
                if ( $sWhere == "" ){
                    $sWhere = "WHERE ";
                } else{
                    $sWhere .= " AND ";
                }
                $sWhere .= $aColumns[$i]." LIKE '%".mysqli_real_escape_string($gaSql['link'],$_GET['sSearch_'.$i])."%' ";
            }
        }
        if($sCondition!=''){
            if ( $sWhere == "" ){
                $sWhere="WHERE ".$sCondition." ";
            }else{
                $sWhere .=" AND ".$sCondition." ";
            }
        }
        /*
         * SQL queries
        * Get data to display 
        */
        $sQuery = "
    SELECT SQL_CALC_FOUND_ROWS ".str_replace(" , ", " ", implode(", ", $aColumns))."
            FROM   $sTable $sJoinTable 
            $sWhere
            $sOrder
            $sLimit
            ";
       //debug($sQuery);die;
        $rResult = mysqli_query( $gaSql['link'],$sQuery) or fatal_error( 'MySQL Error Here 1: ' . mysqli_errno($gaSql['link']) );
         
        /* Data set length after filtering */
        $sQuery = "
    SELECT FOUND_ROWS()
";
        $rResultFilterTotal =mysqli_query( $gaSql['link'],$sQuery)  or fatal_error( 'MySQL Error Here 2: ' . mysqli_errno($gaSql['link']) );
        $aResultFilterTotal = mysqli_fetch_array($rResultFilterTotal);
        $iFilteredTotal = $aResultFilterTotal[0];
         
        /* Total data set length */
        $sQuery = "
    SELECT COUNT(".$sIndexColumn.")
            FROM   $sTable $sJoinTable
            ";
        $rResultTotal = mysqli_query( $gaSql['link'],$sQuery) or fatal_error( 'MySQL Error Here 3: ' . mysqli_errno($gaSql['link']) );
        $aResultTotal = mysqli_fetch_array($rResultTotal);
        $iTotal = $aResultTotal[0];
         
        /*
         * Output
        */
        $output = array(
                "sEcho" => intval($_GET['sEcho']),
                "iTotalRecords" => $iTotal,
                "iTotalDisplayRecords" => $iFilteredTotal,
                "aaData" => array()
        );
        
        while ( $aRow = mysqli_fetch_array( $rResult ) )
        {
			
            $row = array();
            for ( $i=0 ; $i<count($aColumns) ; $i++ )
            {
				
				if(count(explode('.',$aColumns[$i]))==2){
					$aColumns[$i]=explode('.',$aColumns[$i]);
					$aColumns[$i]=$aColumns[$i][1];
				}
                if ( $aColumns[$i] == "version" )
                {
                    /* Special output formatting for 'version' column */
                    $row[] = ($aRow[ $aColumns[$i] ]=="0") ? '-' : $aRow[ $aColumns[$i] ];
                }
                else if ( $aColumns[$i] != ' ' )
                {
                   if($aColumns[$i]=="user_id") {
					  $str = "<a class='btn green btn-xs green-stripe' href='/admin/users/edit/".$aRow[ $aColumns[$i] ]."'>Edit</a>&nbsp;
									  &nbsp;<a class='btn default btn-xs green-stripe' href='/admin/users/view_user_details/".$aRow[ $aColumns[$i] ]."'>View</a>&nbsp;
									  &nbsp;<a class='btn yellow btn-xs yellow-stripe' href='/admin/users/print_details/".$aRow[ $aColumns[$i] ]."'target='_blank'>Print</a>&nbsp;";
					   if($aRow['role_name']=='customer'){
							 $str .= "<a class='btn red btn-xs red-stripe' href='/admin/CustomerPasses/customer_view/".$aRow[ $aColumns[$i] ]."'>Manage</a>&nbsp;
									  &nbsp;<a class='btn default btn-xs green-stripe' href='/admin/tickets/contact_user/".$aRow[ $aColumns[$i] ]."'>Contact</a>";
							}
					  $row[] = $str;
					}else{
						$row[] = $aRow[ $aColumns[$i] ];
					}
                }
            }
            $output['aaData'][] = $row;
        }
          
        return $output;
	}

}

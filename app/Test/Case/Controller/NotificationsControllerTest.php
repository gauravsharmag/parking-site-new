<?php
App::uses('NotificationsController', 'Controller');

/**
 * NotificationsController Test Case
 *
 */
class NotificationsControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.notification',
		'app.user',
		'app.role',
		'app.property_user',
		'app.property',
		'app.customer_pass',
		'app.vehicle',
		'app.transaction',
		'app.pass',
		'app.package',
		'app.billing_address'
	);

}

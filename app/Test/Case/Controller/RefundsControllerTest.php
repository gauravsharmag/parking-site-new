<?php
App::uses('RefundsController', 'Controller');

/**
 * RefundsController Test Case
 *
 */
class RefundsControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.refund',
		'app.user',
		'app.role',
		'app.property_user',
		'app.property',
		'app.customer_pass',
		'app.vehicle',
		'app.transaction',
		'app.pass',
		'app.package',
		'app.billing_address'
	);

}

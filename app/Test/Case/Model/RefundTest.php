<?php
App::uses('Refund', 'Model');

/**
 * Refund Test Case
 *
 */
class RefundTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.refund',
		'app.user',
		'app.role',
		'app.property_user',
		'app.property',
		'app.customer_pass',
		'app.vehicle',
		'app.transaction',
		'app.pass',
		'app.package',
		'app.billing_address'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Refund = ClassRegistry::init('Refund');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Refund);

		parent::tearDown();
	}

}

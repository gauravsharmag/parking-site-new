<?php
App::uses('CustomerPass', 'Model');

/**
 * CustomerPass Test Case
 *
 */
class CustomerPassTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.customer_pass',
		'app.user',
		'app.vehicle',
		'app.property',
		'app.transaction',
		'app.package'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->CustomerPass = ClassRegistry::init('CustomerPass');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->CustomerPass);

		parent::tearDown();
	}

}

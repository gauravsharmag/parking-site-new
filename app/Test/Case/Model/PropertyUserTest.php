<?php
App::uses('PropertyUser', 'Model');

/**
 * PropertyUser Test Case
 *
 */
class PropertyUserTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.property_user',
		'app.user',
		'app.role',
		'app.customer_pass',
		'app.vehicle',
		'app.property',
		'app.package',
		'app.pass',
		'app.transaction'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->PropertyUser = ClassRegistry::init('PropertyUser');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->PropertyUser);

		parent::tearDown();
	}

}

<?php
/**
 * UserGuestPassFixture
 *
 */
class UserGuestPassFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'user_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'customer_pass_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'vehicle_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'vehicle_details' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 150, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'days' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'amount' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'indexes' => array(
			
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'user_id' => 1,
			'customer_pass_id' => 1,
			'vehicle_id' => 1,
			'vehicle_details' => 'Lorem ipsum dolor sit amet',
			'days' => 1,
			'amount' => 1
		),
	);

}

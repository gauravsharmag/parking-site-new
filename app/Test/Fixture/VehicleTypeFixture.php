<?php
/**
 * VehicleTypeFixture
 *
 */
class VehicleTypeFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'vehicle_type_name' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 15, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'property_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'deposit' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'pass_cost' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'pass_cost_after_1_year' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'recurring_cost_duration' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'recurring_cost' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'vehicle_password' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 60, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'is_recurring' => array('type' => 'boolean', 'null' => false, 'default' => null),
		'total_number_of_spaces' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'vehicle_type_name' => 'Lorem ipsum d',
			'property_id' => 1,
			'deposit' => 1,
			'pass_cost' => 1,
			'pass_cost_after_1_year' => 1,
			'recurring_cost_duration' => 1,
			'recurring_cost' => 1,
			'vehicle_password' => 'Lorem ipsum dolor sit amet',
			'is_recurring' => 1,
			'total_number_of_spaces' => 1
		),
	);

}


<script>
$(document).ready(function(){
	$("#NewPass").hide();
	$("#AddNew").click(function(){
		$("#NewPass").slideToggle();
	});
/*	
    $("#panelDate").hide("slow");
    $("#PassDuration").attr("required",true );
    $("#PassDurationType").attr("required",true );
    $("#PassExpirationDate").attr("required",false );
    $("#panelDuration").show("slow");
    $("#TableCouponFixedDuration").change(function()
    {
        if ($(this).is(':checked'))
        {
            $("#panelDate").show("slow");
            $("#PassExpirationDate").attr("required",true );
            $("#panelDuration").hide("slow");
            $("#PassDuration").attr("required",false);
            $("#PassDurationType").attr("required",false);

        }
        else
        {
             $("#panelDate").hide("slow");
             $("#PassExpirationDate").attr("required",false );
             $("#panelDuration").show("slow");
             $("#PassDuration").attr("required",true );
             $("#PassDurationType").attr("required",true );
        }
    });*/
});
</script>

<div class="page-content-wrapper">
        <div class="page-content" style="min-height:1089px">
            
            <!-- BEGIN PAGE HEADER-->
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                    <h3 class="page-title">
                                    <?php echo 'Add/View Coupons'?> <small></small>
                     </h3>

                    <!-- END PAGE TITLE & BREADCRUMB-->
                </div>
            </div>
            <!-- END PAGE HEADER-->
            <div id="flashMessages">
                	<h2><?php  echo $this->Session->flash();?></h2>
            </div>
             <!--Start Row-->
            <div class="row">
                <!--Start ColUmn-->
                <div class="col-md-12">
                  <!-- Start TAB-->
                    <div class="tabbable tabbable-custom boxless tabbable-reversed">
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a data-toggle="tab" href="#tab_0">
                                Coupons List </a>
                            </li>
                            
                        </ul>
                         <!-- Start Tab CONTENT-->
                        <div class="tab-content">
                            <div id="tab_0" class="tab-pane active">
								<div class="nav-pills">
									<button class="btn btn-primary" id="AddNew">Add new Coupon</button>
								</div>
                               <!-- 1 Portlet-->
                                <div class="portlet box red" id="NewPass" style="display:none;">
                                    <div class="portlet-title">
										 <div class="caption">
                                            <i class="fa fa-gift"></i><?php echo __('Add New Coupon'); ?>
                                        </div>
                                        <div class="tools">
                                            <a class="collapse" href="javascript:;">
                                            </a>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                            <div class="form">
                                            <?php 
                                                 
                                                        echo $this->Form->create('TableCoupon',array('class'=>'form-horizontal'));
                                                       
                                                        echo $this->Form->input('coupon_code',array('required'=>true,'div'=>array('class'=>'form-group'),
                                                                                'label'=>array('class'=>'col-md-3 control-label','text'=>'Coupon Code'),
                                                                                'error'=>array('attributes'=>array('class'=>'model-error')),
                                                                                'between'=>'<div class="col-md-4">',
                                                                                'after'=>'</div>',
                                                                                'class'=>'form-control'));

                                                        echo $this->Form->input('discount',array('required'=>true,'div'=>array('class'=>'form-group'),
                                                                                'label'=>array('class'=>'col-md-3 control-label','text'=>'Discount in %'),
                                                                                'error'=>array('attributes'=>array('class'=>'model-error')),
                                                                                'min'=>'1',
                                                                                'max'=>'99',
                                                                                'between'=>'<div class="col-md-4">',
                                                                                'after'=>'</div>',
                                                                                'class'=>'form-control'));
                                                        echo $this->Form->input('property_id',array('required'=>true,'div'=>array('class'=>'form-group'),
                                                                                 'label'=>array('class'=>'col-md-3 control-label','text'=>'Property'),
                                                                                 'error'=>array('attributes'=>array('class'=>'model-error')),
                                                                                 'options'=>$property,
                                                                                 'type'=>'select',
                                                                                 'empty'=>'Select Property',
                                                                                 'between'=>'<div class="col-md-4">',
                                                                                 'after'=>'</div>',
                                                                                 'class'=>'form-control'));
                                                        /*echo $this->Form->input('fixed_duration',array('type'=>'checkbox','div'=>array('class'=>'form-group'),
                                                                                'label'=>false,
                                                                                'error'=>array('attributes'=>array('class'=>'model-error')),
                                                                                 'before'=>'<label class="col-md-3 control-label">Is Fixed Duration?</label><div class="col-md-4">',
                                                                                'after'=>'</div>'
                                                                                 ));*/
                                                       // echo"<div id='panelDate'>";
                                                                    echo $this->Form->input('start_date',array('required'=>false,'type'=>'text','div'=>array('class'=>'form-group'),
                                                                                'label'=>array('class'=>'col-md-3 control-label','text'=>'Start Date'),
																				'error'=>array('attributes'=>array('class'=>'model-error')),
                                                                                'between'=>'<div class="col-md-4"><div class="input-group date date-picker" data-date-format="mm/dd/yyyy" data-date-start-date="+0d">',
                                                                                'after'=>'<span class="input-group-btn"><button type="button" class="btn default"><i class="fa fa-calendar"></i></button></span></div></div>',
                                                                                'class'=>'form-control'));
                                                                     echo $this->Form->input('expiration_date',array('required'=>false,'type'=>'text','div'=>array('class'=>'form-group'),
                                                                                'label'=>array('class'=>'col-md-3 control-label','text'=>'Expiration Date'),
																				'error'=>array('attributes'=>array('class'=>'model-error')),
                                                                                'between'=>'<div class="col-md-4"><div class="input-group date date-picker" data-date-format="mm/dd/yyyy" data-date-start-date="+0d">',
                                                                                'after'=>'<span class="input-group-btn"><button type="button" class="btn default"><i class="fa fa-calendar"></i></button></span></div></div>',
                                                                                'class'=>'form-control'));
                                                        // echo"</div>";
                                            ?>
                                           
                                             <div class="form-actions fluid">
                                                <div class="col-md-offset-3 col-md-9">
                                                    <button type="submit" class="btn blue">Submit</button> 
                                                </div>
                                            </div>
                                           
                                          <?php echo $this->Form->end();?>
										  </div>
                                    </div>
                                </div>
                                 <!-- 1 Portlet-->
                                 <!-- 2 Portlet-->
                                <div class="portlet box green">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fa fa-gift"></i><?php echo __('Existing Coupons '); ?>
                                        </div>
                                        <div class="tools">
                                            <a class="collapse" href="javascript:;">
                                            </a>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <?php if(!empty($coupons))
                                          {
                                        ?>
                                        
                                           <div class="table-responsive">
                                              <table class="table table-striped table-bordered table-hover">
                                               <thead>
                                                    <tr>
                                                        <th><?php echo "Coupon Code"; ?></th>
                                                        <th><?php echo "Property"; ?></th>
                                                        <th><?php echo "Discount"; ?></th>
                                                        <th><?php echo "Start Date"; ?></th>
                                                        <th><?php echo "End Date"; ?></th>
                                                        <th><?php echo "Status"; ?></th>
                                                        <th class="actions"><?php echo __('Actions'); ?></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                <?php foreach ($coupons as $coupon): ?>
                                                    <tr>
                                                       
                                                        <td>
                                                            <?php echo h($coupon['TableCoupon']['coupon_code']); ?>
                                                        </td>
                                                        <td><?php echo h($coupon['Property']['name']); ?>&nbsp;</td>
                                                        <td><?php echo h($coupon['TableCoupon']['discount']); ?>%&nbsp;</td>
                                                         <td> <?php  if($coupon['TableCoupon']['start_date']==null || empty($coupon['TableCoupon']['start_date'])){ echo "NA";}else{
                                                                    $dateStart= date("m/d/Y H:i:s", strtotime($coupon['TableCoupon']['start_date']));
                                                                     echo $dateStart;
                                                                     $dt = new DateTime();
																	 $currentDateTime= $dt->format('Y-m-d H:i:s');
																	 if($coupon['TableCoupon']['start_date']>$currentDateTime){
																		echo '<br><span class="label label-sm label-danger">Not Valid</span>';
																	 }else{
																		echo '<br><span class="label label-sm label-success">Valid</span>';
																	 }
                                                                }
                                                            ?>
                                                        </td>
                                                        <td> <?php  if($coupon['TableCoupon']['expiration_date']==null || empty($coupon['TableCoupon']['expiration_date'])){ echo "NA";}else{
                                                                     $dateExpiry= date("m/d/Y H:i:s", strtotime($coupon['TableCoupon']['expiration_date']));
                                                                     echo $dateExpiry;
                                                                     $dt = new DateTime();
																	 $currentDateTime= $dt->format('Y-m-d H:i:s');
																	 if($coupon['TableCoupon']['expiration_date']<$currentDateTime){
																		echo '<br><span class="label label-sm label-danger">Expired</span>';
																	 }else{
																		echo '<br><span class="label label-sm label-success">Valid</span>';
																	 }
                                                                }
                                                            ?>
                                                        </td>
                                                        <td>
                                                            <?php if($coupon['TableCoupon']['valid']==0){
																	echo '<span class="label label-sm label-danger">Deactivated</span>';
																  }else{
																	echo '<span class="label label-sm label-success">Active</span>';
																  }
                                                            ?>
                                                        </td>
                                                        <td class="actions">
                                                        <?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $coupon['TableCoupon']['id']),array('class'=>"btn btn-xs blue")); ?>
                                                         <?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $coupon['TableCoupon']['id']), array('class'=>"btn btn-xs red"), __('Are you sure you want to delete?', $coupon['TableCoupon']['id'])); ?>
														 <?php if($coupon['TableCoupon']['valid']==0){
																  echo $this->Form->postLink(__('Activate'), array('action' => 'activate', $coupon['TableCoupon']['id']), array('class'=>"btn btn-xs green"), __('Are you sure you want to activate this coupon?', $coupon['TableCoupon']['id'])); 
																}else{
																	echo $this->Form->postLink(__('De-Activate'), array('action' => 'deactivate', $coupon['TableCoupon']['id']), array('class'=>"btn btn-xs purple"), __('Are you sure you want to de-activate this coupon?', $coupon['TableCoupon']['id'])); 
																}	
															?>

                                                        </td>
                                                     </tr>
                                                <?php endforeach; ?>
                                                </tbody>
                                            </table>
                                                <p>
                                                    <?php
                                                        echo $this->Paginator->counter(array(
                                                        'format' => __('Page {:page} of {:pages}, Total Coupons :  {:count}')
                                                        ));
                                                    ?>  
                                                </p>
                                                <div class="paging">
                                                    <?php
                                                        echo $this->Paginator->prev('< ' . __('Previous   '), array(), null, array('class' => 'prev disabled'));
                                                        echo $this->Paginator->numbers(array('separator' => '|'));
                                                        echo $this->Paginator->next(__('   Next') . ' >', array(), null, array('class' => 'next disabled'));
                                                    ?>
                                                </div>
                                        <?php }?>
                                    </div>           
                                </div>
                            </div>
                             <!-- 2 Portlet-->
                        </div>
                    </div>
                     <!-- END TabCONTENT-->
                </div>
                <!-- END TAB-->
            </div>
             <!--End ColUmn-->
        </div>
         <!--End Row-->
    </div>
    <!--End Page Content-->
</div>
    <!--End Page -->
    
<?php //echo $this->Html->script('/assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js'); ?>
<?php echo $this->Html->script('//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.min.js'); ?> 
<?php echo $this->Html->script('/assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js'); ?>
<?php echo $this->Html->script('/assets/admin/pages/scripts/components-pickers.js');   ?>
<script>
    jQuery(document).ready(function() {     
      ComponentsPickers.init(); 
    });
</script>




<script>
$(document).ready(function(){
/*	
    $("#panelDate").hide("slow");
    $("#PassDuration").attr("required",true );
    $("#PassDurationType").attr("required",true );
    $("#PassExpirationDate").attr("required",false );
    $("#panelDuration").show("slow");
    $("#TableCouponFixedDuration").change(function()
    {
        if ($(this).is(':checked'))
        {
            $("#panelDate").show("slow");
            $("#PassExpirationDate").attr("required",true );
            $("#panelDuration").hide("slow");
            $("#PassDuration").attr("required",false);
            $("#PassDurationType").attr("required",false);

        }
        else
        {
             $("#panelDate").hide("slow");
             $("#PassExpirationDate").attr("required",false );
             $("#panelDuration").show("slow");
             $("#PassDuration").attr("required",true );
             $("#PassDurationType").attr("required",true );
        }
    });*/
});
</script>

<div class="page-content-wrapper">
        <div class="page-content" style="min-height:1089px">
            
            <!-- BEGIN PAGE HEADER-->
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                    <h3 class="page-title">
                                    <?php echo 'Edit Coupons'?> <small></small>
                     </h3>

                    <!-- END PAGE TITLE & BREADCRUMB-->
                </div>
            </div>
            <!-- END PAGE HEADER-->
            <div id="flashMessages">
                	<h2><?php  echo $this->Session->flash();?></h2>
            </div>
             <!--Start Row-->
            <div class="row">
                <!--Start ColUmn-->
                <div class="col-md-12">
                  <!-- Start TAB-->
                    <div class="tabbable tabbable-custom boxless tabbable-reversed">
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a data-toggle="tab" href="#tab_0">
                                Coupons</a>
                            </li>
                            
                        </ul>
                         <!-- Start Tab CONTENT-->
                        <div class="tab-content">
                            <div id="tab_0" class="tab-pane active">
                               <!-- 1 Portlet-->
                                <div class="portlet box red" id="NewPass">
                                    <div class="portlet-title">
										 <div class="caption">
                                            <i class="fa fa-gift"></i><?php echo __('Edit Coupon'); ?>
                                        </div>
                                        <div class="tools">
                                            <a class="collapse" href="javascript:;">
                                            </a>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                            <div class="form">
                                            <?php 
                                                 
                                                        echo $this->Form->create('TableCoupon',array('class'=>'form-horizontal'));
                                                        echo $this->Form->hidden('id');
                                                        echo $this->Form->input('coupon_code',array('required'=>true,'div'=>array('class'=>'form-group'),
                                                                                'label'=>array('class'=>'col-md-3 control-label','text'=>'Coupon Code'),
                                                                                'error'=>array('attributes'=>array('class'=>'model-error')),
                                                                                'between'=>'<div class="col-md-4">',
                                                                                'after'=>'</div>',
                                                                                'class'=>'form-control'));

                                                        echo $this->Form->input('discount',array('required'=>true,'div'=>array('class'=>'form-group'),
                                                                                'label'=>array('class'=>'col-md-3 control-label','text'=>'Discount in %'),
                                                                                'error'=>array('attributes'=>array('class'=>'model-error')),
                                                                                'min'=>'1',
                                                                                'max'=>'99',
                                                                                'between'=>'<div class="col-md-4">',
                                                                                'after'=>'</div>',
                                                                                'class'=>'form-control'));
                                                        echo $this->Form->input('property_id',array('required'=>true,'div'=>array('class'=>'form-group'),
                                                                                 'label'=>array('class'=>'col-md-3 control-label','text'=>'Property'),
                                                                                 'error'=>array('attributes'=>array('class'=>'model-error')),
                                                                                 'options'=>$property,
                                                                                 'type'=>'select',
                                                                                 'empty'=>'Select Property',
                                                                                 'value'=>$this->request->data['TableCoupon']['property_id'],
                                                                                 'between'=>'<div class="col-md-4">',
                                                                                 'after'=>'</div>',
                                                                                 'class'=>'form-control'));
                                                        /*echo $this->Form->input('fixed_duration',array('type'=>'checkbox','div'=>array('class'=>'form-group'),
                                                                                'label'=>false,
                                                                                'error'=>array('attributes'=>array('class'=>'model-error')),
                                                                                 'before'=>'<label class="col-md-3 control-label">Is Fixed Duration?</label><div class="col-md-4">',
                                                                                'after'=>'</div>'
                                                                                 ));*/
                                                       // echo"<div id='panelDate'>";
                                                                    echo $this->Form->input('start_date',array('required'=>false,'type'=>'text','div'=>array('class'=>'form-group'),
                                                                                'label'=>array('class'=>'col-md-3 control-label','text'=>'Start Date'),
																				'error'=>array('attributes'=>array('class'=>'model-error')),
                                                                                'between'=>'<div class="col-md-4"><div class="input-group date date-picker" data-date-format="mm/dd/yyyy" data-date-start-date="+0d">',
                                                                                'after'=>'<span class="input-group-btn"><button type="button" class="btn default"><i class="fa fa-calendar"></i></button></span></div></div>',
                                                                                'class'=>'form-control'));
                                                                     echo $this->Form->input('expiration_date',array('required'=>false,'type'=>'text','div'=>array('class'=>'form-group'),
                                                                                'label'=>array('class'=>'col-md-3 control-label','text'=>'Expiration Date'),
																				'error'=>array('attributes'=>array('class'=>'model-error')),
                                                                                'between'=>'<div class="col-md-4"><div class="input-group date date-picker" data-date-format="mm/dd/yyyy" data-date-start-date="+0d">',
                                                                                'after'=>'<span class="input-group-btn"><button type="button" class="btn default"><i class="fa fa-calendar"></i></button></span></div></div>',
                                                                                'class'=>'form-control'));
                                                        // echo"</div>";
                                            ?>
                                           
                                             <div class="form-actions fluid">
                                                <div class="col-md-offset-3 col-md-9">
                                                    <button type="submit" class="btn blue">Submit</button> 
                                                </div>
                                            </div>
                                           
                                          <?php echo $this->Form->end();?>
										  </div>
                                    </div>
                                </div>
                                 <!-- 1 Portlet-->
                            </div>
                             <!-- 2 Portlet-->
                        </div>
                    </div>
                     <!-- END TabCONTENT-->
                </div>
                <!-- END TAB-->
            </div>
             <!--End ColUmn-->
        </div>
         <!--End Row-->
    </div>
    <!--End Page Content-->
</div>
    <!--End Page -->
    <?php echo $this->Html->script('//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.min.js'); ?> 
    <?php echo $this->Html->script('/assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js'); ?>
    <?php echo $this->Html->script('/assets/admin/pages/scripts/components-pickers.js');   ?>
    <script>
        jQuery(document).ready(function() {     
          ComponentsPickers.init(); 
        });
    </script>
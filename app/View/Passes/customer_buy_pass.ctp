
<div class="page-content-wrapper">
		<div class="page-content" style="min-height:996px">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="portlet-config" class="modal fade">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button aria-hidden="true" data-dismiss="modal" class="close" type="button"></button>
							<h4 class="modal-title">Modal title</h4>
						</div>
						<div class="modal-body">
							 Widget settings form goes here
						</div>
						<div class="modal-footer">
							<button class="btn blue" type="button">Save changes</button>
							<button data-dismiss="modal" class="btn default" type="button">Close</button>
						</div>
					</div>
					<!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			</div>
			<!-- /.modal -->
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN STYLE CUSTOMIZER -->

			<!-- END STYLE CUSTOMIZER -->
			<!-- BEGIN PAGE HEADER-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
					<?php //debug($this->Session->params);
					//debug($this->Session->read('Auth'));
					//debug($this->Session->read('PropertyId')['Property']['id']);
					//debug($this->Session->read('PropertyName'));
					?>
					<?php echo $this->Session->read('PropertyName')." ";?>Passes <small></small>
					</h3>
					<ul class="page-breadcrumb breadcrumb">

						<li>
							<i class="fa fa-home"></i>
							<?php echo $this->Html->link(
                             'Home',
                              array(
                                    'controller' => 'Users',
                                    'action' => 'myAccount',
                                    'full_base' => true
                                                                                              )
                              );?>
							<i class="fa fa-angle-right"></i>
						</li>

						<li>
							<a href="#">Passes Details</a>
						</li>
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>

			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<!-- Begin: life time stats -->
					<div class="portlet">
						<div class="portlet-title">

						</div>
						<div class="portlet-body">
							<div class="tabbable">
								<ul class="nav nav-tabs nav-tabs-lg">
									<li class="active">
										<a data-toggle="tab" href="#tab_1">
										   Select Pass
                                        </a>
									</li>


								</ul>
								<div class="tab-content">
									<div id="tab_1" class="tab-pane active">
                                                <div class="portlet box yellow">
                                                						<div class="portlet-title">
                                                							<div class="caption">
                                                								<i class="fa fa-building"></i>Passes
                                                							</div>
                                                							<div class="tools">
                                                                            	<a class="collapse" href="javascript:;">
                                                                            	</a>

                                                                            </div>

                                                						</div>
                                                						<div class="portlet-body">                                              							<div class="table-responsive">

                                                								<table class="table table-bordered">
                                                								<thead>
                                                								<tr>
                                                									<th>
                                                										 #
                                                									</th>
                                                									<th>
                                                										 Status
                                                									</th>
                                                									<th>
                                                										 Deposit
                                                									</th>
                                                									<th>
                                                										 Cost
                                                									</th>
                                                									<th>
                                                										 Valid Upto
                                                									</th>
                                                                                    <th>
                                                										 Select
                                                									</th>

                                                								</tr>
                                                								</thead>
                                                								<tbody>
                                                				<?php $srNo=1;foreach($remainingPasses as $remainingPass){?>
                                                								<tr>
                                                									<td>
                                                										<?php echo $srNo++;?>
                                                									</td>
                                                									<td>
                                                                                         <span class="label label-sm label-info">Remaining</span>

                                                									</td>
                                                									<td>
                                                										 <?php echo "$".' '.$remainingPass['Pass']['deposit']?>
                                                									</td>
                                                									<td>
                                                										 <?php echo "$".' '.$remainingPass['Pass']['cost_1st_year']?>
                                                									</td>
                                                									<td>
                                                										 <?php	echo $remainingPass['Pass']['expiration_date']= date("d-m-Y H:i:s", strtotime($remainingPass['Pass']['expiration_date']));?>
                                                									</td>
                                                									<td>
                                                                                        <?php  echo $this->Html->link('Select', array('controller' => 'Transactions', 'action' => 'check_out', $remainingPass['Pass']['id'],$selectedPackageID));?>
                                                                                    </td>
                                                								</tr>
                                                				<?php }?>
                                                                <?php

                                                                foreach($validPassesPackagesExpired as $activePass){?>
                                                								<tr>
                                                									<td>
                                                										<?php echo $srNo++;?>
                                                									</td>
                                                									<td>
                                                                                         <span class="label label-sm label-success">Valid Passes </span>
                                                									</td>
                                                									<td>
                                                										 <?php //$deposit=$this->requestAction(array('controller'=>'Passes','action'=>'getDeposit',$activePass['CustomerPass']['pass_id']));
                                                										       //echo "$".' '.$deposit['Pass']['deposit']
                                                										        echo '--';


                                                										 ?>
                                                									</td>
                                                									<td>
                                                										 <?php //$cost=$this->requestAction(array('controller'=>'Passes','action'=>'getCost',$activePass['CustomerPass']['pass_id']));
                                                                                                //echo "$".' '.$cost['Pass']['cost_1st_year']
                                                                                                echo '--';
                                                                                         ?>
                                                									</td>
                                                									<td>
                                                										 <?php echo $activePass['CustomerPass']['pass_valid_upto']= date("d-m-Y H:i:s", strtotime( $activePass['CustomerPass']['pass_valid_upto']));?>
                                                									</td>
                                                									<td>
                                                                                        <?php echo $this->Html->link('Select', array('controller' => 'Transactions', 'action' => 'check_out', $activePass['CustomerPass']['pass_id'],$selectedPackageID));?>
                                                                                    </td>
                                                								</tr>
                                                				<?php }?>
                                                                <?php
                                                                foreach($activePasses as $activePass){?>
                                                								<tr>
                                                									<td>
                                                										<?php echo $srNo++;?>
                                                									</td>
                                                									<td>
                                                                                         <span class="label label-sm label-warning">Active </span>


                                                									</td>
                                                									<td>
                                                										 <?php $deposit=$this->requestAction(array('controller'=>'Passes','action'=>'getDeposit',$activePass['CustomerPass']['pass_id']));
                                                										     echo "$".' '.$deposit['Pass']['deposit']?>
                                                									</td>
                                                									<td>
                                                										 <?php $cost=$this->requestAction(array('controller'=>'Passes','action'=>'getCost',$activePass['CustomerPass']['pass_id']));
                                                										 echo "$".' '.$cost['Pass']['cost_1st_year']?>
                                                									</td>
                                                									<td>
                                                										 <?php	 echo $activePass['CustomerPass']['pass_valid_upto']= date("d-m-Y H:i:s", strtotime( $activePass['CustomerPass']['pass_valid_upto']));?>
                                                									</td>
                                                									<td>
                                                                                        <?php echo "-----";?>
                                                                                    </td>
                                                								</tr>
                                                				<?php }?>
                                                                <?php foreach($expiredPasses as $expiredPass){?>
                                                								<tr>
                                                									<td>
                                                										<?php echo $srNo++;?>
                                                									</td>
                                                									<td>
                                                                                         <span class="label label-sm label-danger">Expired </span>
                                                									</td>
                                                									<td>
                                                										 <?php //echo "$".' '.$expiredPass['Pass']['deposit']
                                                										       echo '--';
                                                										 ?>
                                                									</td>
                                                									<td>
                                                										 <?php echo "$".' '.$expiredPass['Pass']['cost_1st_year']?>
                                                									</td>
                                                									<td>
                                                										 <?php	$expiryDate=$this->requestAction(array('controller'=>'CustomerPasses','action'=>'getExpiryDate',$expiredPass['Pass']['id']));
                                                										        //debug($expiryDate);
                                                										        echo $expiryDate['CustomerPass']['pass_valid_upto']= date("d-m-Y H:i:s", strtotime($expiryDate['CustomerPass']['pass_valid_upto'])).' ';
                                                										 if($expiredPass['Pass']['is_fixed_duration']==0){?>
                                                										 <span class="label label-sm label-warning">Renewable </span>
                                                										 <?php }else{?>
                                                										 <span class="label label-sm label-warning">Non Renewable </span>
                                                										 <?php }
                                                										 ?>

                                                									</td>
                                                									<td>
                                                                                        <?php	if($expiredPass['Pass']['is_fixed_duration']==0){
                                                                                                     echo $this->Html->link('Select', array('controller' => 'Transactions', 'action' => 'check_out', $expiredPass['Pass']['id'],$selectedPackageID));
                                                										        }
                                                										        else{
                                                										                echo "-----";
                                                										        }?>
                                                                                    </td>
                                                								</tr>
                                                				<?php }?>
                                                								</tbody>
                                                								</table>

                                                							</div>
                                                						</div>
                                                					</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- End: life time stats -->
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>


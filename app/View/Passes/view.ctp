
<div class="page-content-wrapper">
		<div class="page-content" style="min-height:996px">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="portlet-config" class="modal fade">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button aria-hidden="true" data-dismiss="modal" class="close" type="button"></button>
							<h4 class="modal-title">Modal title</h4>
						</div>
						<div class="modal-body">
							 Widget settings form goes here
						</div>
						<div class="modal-footer">
							<button class="btn blue" type="button">Save changes</button>
							<button data-dismiss="modal" class="btn default" type="button">Close</button>
						</div>
					</div>
					<!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			</div>
			<!-- /.modal -->
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN STYLE CUSTOMIZER -->

			<!-- END STYLE CUSTOMIZER -->
			<!-- BEGIN PAGE HEADER-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
					<?php //debug($this->Session->params);
					//debug($this->Session->read('Auth'));
					//debug($this->Session->read('PropertyId')['Property']['id']);
					//debug($this->Session->read('PropertyName'));
					?>
					<?php echo $this->Session->read('PropertyName')." ";?>Passes <small></small>
					</h3>
					<ul class="page-breadcrumb breadcrumb">

						<li>
							<i class="fa fa-home"></i>
							<?php echo $this->Html->link(
                             'Home',
                              array(
                                    'controller' => 'Users',
                                    'action' => 'myAccount',
                                    'full_base' => true
                                                                                              )
                              );?>
							<i class="fa fa-angle-right"></i>
						</li>

						<li>
							<a href="#">Passes Details</a>
						</li>
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>

			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<!-- Begin: life time stats -->
					<div class="portlet">
						<div class="portlet-title">

						</div>
						<div class="portlet-body">
							<div class="tabbable">
								<ul class="nav nav-tabs nav-tabs-lg">
									<li class="active">
										<a data-toggle="tab" href="#tab_1">
										    Remaining Passes <span class="badge badge-success">
                                                        <?php echo($countRemainingPasses);?></span>
                                        </a>
									</li>
									<li class="">
										<a data-toggle="tab" href="#tab_2">
										Active Passes <span class="badge badge-danger">
										<?php echo($countActivePasses);?></span>
										</a>
									</li>
									<li class="">
										<a data-toggle="tab" href="#tab_3">
										Valid Passes<span class="badge badge-success">
                                        <?php echo($countValidPassesPackagesExpired);?></span>
										</a>
									</li>
									<li class="">
										<a data-toggle="tab" href="#tab_4">
										Expired Passes <span class="badge badge-danger">
										 <?php echo($countExpiredPasses);?></span>
										</a>
									</li>

								</ul>
								<div class="tab-content">
									<div id="tab_1" class="tab-pane active">
                                                <div class="portlet box yellow">
                                                						<div class="portlet-title">
                                                							<div class="caption">
                                                								<i class="fa fa-building"></i>Remaining Passes
                                                							</div>

                                                						</div>
                                                						<div class="portlet-body">
                                                							<div class="table-responsive">
                                                	<?php if($countRemainingPasses!=0){?>
                                                								<table class="table table-bordered">
                                                								<thead>
                                                								<tr>
                                                									<th>
                                                										 #
                                                									</th>
                                                									<th>
                                                										 Table heading
                                                									</th>
                                                									<th>
                                                										 Table heading
                                                									</th>
                                                									<th>
                                                										 Table heading
                                                									</th>
                                                									<th>
                                                										 Table heading
                                                									</th>
                                                									<th>
                                                										 Table heading
                                                									</th>
                                                									<th>
                                                										 Table heading
                                                									</th>
                                                								</tr>
                                                								</thead>
                                                								<tbody>
                                                								<tr>
                                                									<td>
                                                										 1
                                                									</td>
                                                									<td>
                                                										 Table cell
                                                									</td>
                                                									<td>
                                                										 Table cell
                                                									</td>
                                                									<td>
                                                										 Table cell
                                                									</td>
                                                									<td>
                                                										 Table cell
                                                									</td>
                                                									<td>
                                                										 Table cell
                                                									</td>
                                                									<td>
                                                										 Table cell
                                                									</td>
                                                								</tr>
                                                								<tr>
                                                									<td>
                                                										 2
                                                									</td>
                                                									<td>
                                                										 Table cell
                                                									</td>
                                                									<td>
                                                										 Table cell
                                                									</td>
                                                									<td>
                                                										 Table cell
                                                									</td>
                                                									<td>
                                                										 Table cell
                                                									</td>
                                                									<td>
                                                										 Table cell
                                                									</td>
                                                									<td>
                                                										 Table cell
                                                									</td>
                                                								</tr>
                                                								<tr>
                                                									<td>
                                                										 3
                                                									</td>
                                                									<td>
                                                										 Table cell
                                                									</td>
                                                									<td>
                                                										 Table cell
                                                									</td>
                                                									<td>
                                                										 Table cell
                                                									</td>
                                                									<td>
                                                										 Table cell
                                                									</td>
                                                									<td>
                                                										 Table cell
                                                									</td>
                                                									<td>
                                                										 Table cell
                                                									</td>
                                                								</tr>
                                                								</tbody>
                                                								</table>
                                                	<?php }else{?>
                                                	<h2>No Remaining Passes </h2>
                                                    <?php }?>
                                                							</div>
                                                						</div>
                                                					</div>
									</div>
									<div id="tab_2" class="tab-pane">
                                            <div class="portlet box green">
                                            						<div class="portlet-title">
                                            							<div class="caption">
                                            								<i class="fa fa-building"></i>Bordered Bootstrap 3.0 Responsive Table
                                            							</div>

                                            						</div>
                                            						<div class="portlet-body">
                                            							<div class="table-responsive">
                                            								<table class="table table-bordered">
                                            								<thead>
                                            								<tr>
                                            									<th>
                                            										 #
                                            									</th>
                                            									<th>
                                            										 Table heading
                                            									</th>
                                            									<th>
                                            										 Table heading
                                            									</th>
                                            									<th>
                                            										 Table heading
                                            									</th>
                                            									<th>
                                            										 Table heading
                                            									</th>
                                            									<th>
                                            										 Table heading
                                            									</th>
                                            									<th>
                                            										 Table heading
                                            									</th>
                                            								</tr>
                                            								</thead>
                                            								<tbody>
                                            								<tr>
                                            									<td>
                                            										 1
                                            									</td>
                                            									<td>
                                            										 Table cell
                                            									</td>
                                            									<td>
                                            										 Table cell
                                            									</td>
                                            									<td>
                                            										 Table cell
                                            									</td>
                                            									<td>
                                            										 Table cell
                                            									</td>
                                            									<td>
                                            										 Table cell
                                            									</td>
                                            									<td>
                                            										 Table cell
                                            									</td>
                                            								</tr>
                                            								<tr>
                                            									<td>
                                            										 2
                                            									</td>
                                            									<td>
                                            										 Table cell
                                            									</td>
                                            									<td>
                                            										 Table cell
                                            									</td>
                                            									<td>
                                            										 Table cell
                                            									</td>
                                            									<td>
                                            										 Table cell
                                            									</td>
                                            									<td>
                                            										 Table cell
                                            									</td>
                                            									<td>
                                            										 Table cell
                                            									</td>
                                            								</tr>
                                            								<tr>
                                            									<td>
                                            										 3
                                            									</td>
                                            									<td>
                                            										 Table cell
                                            									</td>
                                            									<td>
                                            										 Table cell
                                            									</td>
                                            									<td>
                                            										 Table cell
                                            									</td>
                                            									<td>
                                            										 Table cell
                                            									</td>
                                            									<td>
                                            										 Table cell
                                            									</td>
                                            									<td>
                                            										 Table cell
                                            									</td>
                                            								</tr>
                                            								</tbody>
                                            								</table>
                                            							</div>
                                            						</div>
                                            					</div>
                                    </div>
									<div id="tab_3" class="tab-pane ">
										        <div class="portlet box red">
                                                						<div class="portlet-title">
                                                							<div class="caption">
                                                								<i class="fa fa-building"></i>Bordered Bootstrap 3.0 Responsive Table
                                                							</div>

                                                						</div>
                                                						<div class="portlet-body">
                                                							<div class="table-responsive">
                                                								<table class="table table-bordered">
                                                								<thead>
                                                								<tr>
                                                									<th>
                                                										 #
                                                									</th>
                                                									<th>
                                                										 Table heading
                                                									</th>
                                                									<th>
                                                										 Table heading
                                                									</th>
                                                									<th>
                                                										 Table heading
                                                									</th>
                                                									<th>
                                                										 Table heading
                                                									</th>
                                                									<th>
                                                										 Table heading
                                                									</th>
                                                									<th>
                                                										 Table heading
                                                									</th>
                                                								</tr>
                                                								</thead>
                                                								<tbody>
                                                								<tr>
                                                									<td>
                                                										 1
                                                									</td>
                                                									<td>
                                                										 Table cell
                                                									</td>
                                                									<td>
                                                										 Table cell
                                                									</td>
                                                									<td>
                                                										 Table cell
                                                									</td>
                                                									<td>
                                                										 Table cell
                                                									</td>
                                                									<td>
                                                										 Table cell
                                                									</td>
                                                									<td>
                                                										 Table cell
                                                									</td>
                                                								</tr>
                                                								<tr>
                                                									<td>
                                                										 2
                                                									</td>
                                                									<td>
                                                										 Table cell
                                                									</td>
                                                									<td>
                                                										 Table cell
                                                									</td>
                                                									<td>
                                                										 Table cell
                                                									</td>
                                                									<td>
                                                										 Table cell
                                                									</td>
                                                									<td>
                                                										 Table cell
                                                									</td>
                                                									<td>
                                                										 Table cell
                                                									</td>
                                                								</tr>
                                                								<tr>
                                                									<td>
                                                										 3
                                                									</td>
                                                									<td>
                                                										 Table cell
                                                									</td>
                                                									<td>
                                                										 Table cell
                                                									</td>
                                                									<td>
                                                										 Table cell
                                                									</td>
                                                									<td>
                                                										 Table cell
                                                									</td>
                                                									<td>
                                                										 Table cell
                                                									</td>
                                                									<td>
                                                										 Table cell
                                                									</td>
                                                								</tr>
                                                								</tbody>
                                                								</table>
                                                							</div>
                                                						</div>
                                                					</div>
									</div>
									<div id="tab_4" class="tab-pane">
                                                            <div class="portlet box blue">
                                                						<div class="portlet-title">
                                                							<div class="caption">
                                                								<i class="fa fa-building"></i>Bordered Bootstrap 3.0 Responsive Table
                                                							</div>

                                                						</div>
                                                						<div class="portlet-body">
                                                							<div class="table-responsive">
                                                								<table class="table table-bordered">
                                                								<thead>
                                                								<tr>
                                                									<th>
                                                										 #
                                                									</th>
                                                									<th>
                                                										 Table heading
                                                									</th>
                                                									<th>
                                                										 Table heading
                                                									</th>
                                                									<th>
                                                										 Table heading
                                                									</th>
                                                									<th>
                                                										 Table heading
                                                									</th>
                                                									<th>
                                                										 Table heading
                                                									</th>
                                                									<th>
                                                										 Table heading
                                                									</th>
                                                								</tr>
                                                								</thead>
                                                								<tbody>
                                                								<tr>
                                                									<td>
                                                										 1
                                                									</td>
                                                									<td>
                                                										 Table cell
                                                									</td>
                                                									<td>
                                                										 Table cell
                                                									</td>
                                                									<td>
                                                										 Table cell
                                                									</td>
                                                									<td>
                                                										 Table cell
                                                									</td>
                                                									<td>
                                                										 Table cell
                                                									</td>
                                                									<td>
                                                										 Table cell
                                                									</td>
                                                								</tr>
                                                								<tr>
                                                									<td>
                                                										 2
                                                									</td>
                                                									<td>
                                                										 Table cell
                                                									</td>
                                                									<td>
                                                										 Table cell
                                                									</td>
                                                									<td>
                                                										 Table cell
                                                									</td>
                                                									<td>
                                                										 Table cell
                                                									</td>
                                                									<td>
                                                										 Table cell
                                                									</td>
                                                									<td>
                                                										 Table cell
                                                									</td>
                                                								</tr>
                                                								<tr>
                                                									<td>
                                                										 3
                                                									</td>
                                                									<td>
                                                										 Table cell
                                                									</td>
                                                									<td>
                                                										 Table cell
                                                									</td>
                                                									<td>
                                                										 Table cell
                                                									</td>
                                                									<td>
                                                										 Table cell
                                                									</td>
                                                									<td>
                                                										 Table cell
                                                									</td>
                                                									<td>
                                                										 Table cell
                                                									</td>
                                                								</tr>
                                                								</tbody>
                                                								</table>
                                                							</div>
                                                						</div>
                                                					</div>
									</div>

								</div>
							</div>
						</div>
					</div>
					<!-- End: life time stats -->
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>



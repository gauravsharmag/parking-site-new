<div class="passes form">
<?php echo $this->Form->create(array('url'=>array('controller'=>'Passes','action'=>'simple_user_buy_pass')));
//debug($vehicle_type);
//debug($property_name);

 
?>
	<fieldset>
		<legend><?php echo __('Pass Details'); ?></legend>
	<table>
	    <tr><td><b>Parking Place</b></td><td> <?php echo h($property_name);?></td> </tr>
	    <tr><td><b>Vehicle Type</b></td><td> <?php echo h($vehicle_type[0]['VehicleType']['vehicle_type_name']);?></td> </tr>
	 </table>
	 <legend>Registration Fees</legend>
	 <br>
	 <table>
	    <tr><td><b>Initial Deposit</b></td><td>&#36; <?php echo h($vehicle_type[0]['VehicleType']['deposit']);?></td> </tr>
	    <tr><td><b>Pass Cost for 1 year</b></td><td> &#36; <?php echo h($vehicle_type[0]['VehicleType']['pass_cost']);?></td> </tr>

	    <tr><td><b>Net Amount Total Amount</b></td><td>&#36; <?php echo h(($vehicle_type[0]['VehicleType']['deposit']+$vehicle_type[0]['VehicleType']['pass_cost']));?></td> </tr>
	</table>
	<table>

    	    <tr><td><legend>Payment Information</legend></td></tr>

                  <tr>
                        <td><?php echo $this->Form->input('Billing Address');?></td>
                        <td><?php echo $this->Form->input('email');?></td>
                  </tr>
                  <tr>
                        <td><?php echo $this->Form->input('street');?></td>
                        <td><?php echo $this->Form->input('city');?></td>
                  </tr>
                  <tr>
                        <td><?php echo $this->Form->input('state');?></td>
                        <td><?php echo $this->Form->input('zip');?></td>
                  </tr>
                  <tr>
                         <td><?php echo $this->Form->input('phone');?></td>
                         <td></td>
                  </tr>
                  <tr>
                         <td><?php echo $this->Form->input('state');?></td>
                         <td><?php echo $this->Form->input('zip');?></td>
                  </tr>
    </table>
    <legend>Credit Card Details</legend>
    <br>
    <table>

                  <tr>
                         <td><?php echo $this->Form->input('Credit Card Number');?></td>
                         <td><?php echo $this->Form->input('Credit CVC');?></td>
                  </tr>
                   <tr>
                         <td><?php echo $this->Form->input('Expiry Date',array('type'=>'date'));?></td>
                         <td><?php ?></td>
                  </tr>
                  <tr>
                         <td><?php echo $this->Form->input('tos', array('type'=>'checkbox', 'label'=>__('I confirm that I have read terms and conditions.', true))); ?></td>
                        <td></td>
                  </tr>
    	</table>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div>
    <?php echo $this->element('homePage');?>
</div>
<div class="actions">
	<h3><?php echo __(''); ?></h3>
	<ul>
         <li><?php echo $this->Html->link(__('My Account'), array('controller'=>'users','action' => 'myAccount')); ?></li>
         <li><?php echo $this->Html->link(__('New Vehicle'), array('controller' => 'Vehicles', 'action' => 'registerNewVehicle')); ?> </li>
         <li><?php echo $this->Html->link(__('Guest Vehicle'), array('controller' => 'Vehicles', 'action' => 'registerGuestVehicle')); ?> </li>

	</ul>
</div>

<div class="passes view">


<h2><?php echo __('Your Pass'); //debug($passes);?></h2>
<table>
   <?php
        if(!empty($passes))
        {
         ?>
            <tr>
                        			<th><?php echo h('Sr. No.'); ?></th>
                        			<th><?php echo h('Pass Ref. No.'); ?></th>
                        			<th><?php echo h('Vehicle'); ?></th>
                        			<th><?php echo h('Vehicle Type'); ?></th>
                        			<th><?php echo h('Licence Plate No.'); ?></th>
                        			<th><?php echo h('Property'); ?></th>
                        			<th><?php echo h('Transaction Id'); ?></th>
                        			<th><?php echo h('Valid Upto'); ?></th>
                        	</tr>

        <?php
        $srNo=0;
            foreach($passes as $pass):
            ?>

            <tr>
            		<td><?php echo h(++$srNo); ?>&nbsp;</td>
            		<td>
            			<?php echo h($pass['Pass']['id']); ?>
            		</td>
            		<td>
            			<?php echo h($pass['Vehicle']['make']).' '.h($pass['Vehicle']['model']); ?>
            		</td>
            		<td>
                        <?php echo h($pass['Vehicle']['vehicle_type_name']); ?>
                    </td>
            		<td>
                        <?php echo h($pass['Vehicle']['license_plate_number']); ?>
                    </td>

            		<td><?php echo h($pass['Property']['name']); ?>&nbsp;</td>
            	    <td><?php
            	                foreach($pass['Transaction'] as $transaction):
            	                            echo h($transaction['id']);
            	                            echo "<br>";
            	                endforeach;
            	         ?>

            	    </td>
            		<td><?php echo h($pass['Pass']['pass_valid_upto']); ?>&nbsp;</td>
            </tr>
      <?php endforeach; ?>
    <?php

        }
           else
           {
                   echo __('You have not applied for any pass');
            }
    ?>

</table>

</div>

<div>
    <?php echo $this->element('homePage');?>
</div>
<div class="actions">
	<h3><?php echo __(''); ?></h3>
	<ul>
         <li><?php echo $this->Html->link(__('My Account'), array('controller'=>'Users','action' => 'myAccount')); ?></li>
	     <li><?php echo $this->Html->link(__('Properties'), array('controller' => 'Properties', 'action' => 'index')); ?> </li>
	     <li><?php echo $this->Html->link(__('Users'), array('controller' => 'Users', 'action' => 'index')); ?> </li>
    </ul>
</div>


<div class="related">
<?php  //debug($passes);
if (!empty($passes)):
?>
	<h3><?php echo __('Transaction Details'); //debug($passes['Transaction']); ?></h3>

	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('User Id'); ?></th>
		<th><?php echo __('Date Time'); ?></th>
		<th><?php echo __('Amount'); ?></th>
		<th><?php echo __('Result'); ?></th>
		<th><?php echo __('Pass Id'); ?></th>
		<th><?php echo __('Message'); ?></th>
		<th><?php echo __('Credits Used'); ?></th>
		<th><?php echo __('Payment Method Used'); ?></th>
		<th class="actions"><?php// echo __('Actions'); ?></th>
	</tr>
	<?php foreach($passes as $pass):
	                foreach($pass['Transaction'] as $transaction):
	?>
		<tr>
			<td><?php echo $transaction['id']; ?></td>
			<td><?php echo $transaction['user_id']; ?></td>
			<td><?php echo $transaction['date_time']; ?></td>
			<td><?php echo $transaction['amount']; ?></td>
			<td><?php echo $transaction['result']; ?></td>
			<td><?php echo $transaction['pass_id']; ?></td>
			<td><?php echo $transaction['message']; ?></td>
			<td><?php echo $transaction['credits_used']; ?></td>
			<td><?php echo $transaction['payment_method_used']; ?></td>
			<td class="actions">
				<?php //echo $this->Html->link(__('View'), array('controller' => 'transactions', 'action' => 'view', $transaction['id'])); ?>
				<?php //echo $this->Html->link(__('Edit'), array('controller' => 'transactions', 'action' => 'edit', $transaction['id'])); ?>
				<?php //echo $this->Form->postLink(__('Delete'), array('controller' => 'transactions', 'action' => 'delete', $transaction['id']), array(), __('Are you sure you want to delete # %s?', $transaction['id'])); ?>
			</td>
		</tr>
	<?php endforeach;
         endforeach; ?>
	</table>
<?php endif; ?>


	<div class="actions">
		<ul>
			<li><?php //echo $this->Html->link(__('New Transaction'), array('controller' => 'transactions', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>

<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" >
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
<meta charset="utf-8"/>
<title>My Parking Pass | Powered by Digital6</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta content="" name="description"/>
<meta content="" name="author"/>
<link href="//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
<?php 
  echo $this->Html->charset();
  echo $this->Html->css('//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css'); 
  echo $this->Html->css('/assets/global/plugins/bootstrap/css/bootstrap.min.css');
  echo $this->Html->css('/assets/global/css/components.css');
  echo $this->Html->css('/assets/global/css/plugins.css');
  echo $this->Html->css('/assets/admin/layout/css/layout.css');
  echo $this->Html->css('/assets/admin/layout/css/themes/default.css',array('id'=>'style_color'));
  echo $this->Html->css('/assets/admin/layout/css/custom.css');
  echo $this->Html->css('custom.css');
  echo $this->Html->script('//code.jquery.com/jquery-1.12.0.min.js');
?>
</head>
<body class="page-header-fixed page-quick-sidebar-over-content">
<!-- BEGIN HEADER -->

<?php echo $this->element('header'); ?>
<div class="clearfix">
</div>
<!-- END HEADER-->
<!-- BEGIN CONTAINER -->
<div class="page-container">
<!-- BEGIN SIDEBAR -->

<?php echo $this->element('sidebar'); ?>
<?php echo $this->fetch('content'); ?>
<!-- END SIDEBAR -->
</div>
<!-- END CONTAINER -->

<!-- BEGIN COPYRIGHT -->
<div class="copyright">
	 <?php echo date('Y'); ?> &copy; <a href="http://digital6technologies.com" target="_blank">Powered by Digital6 Technologies</a>
</div>
<?php 
	echo $this->Html->script('/assets/global/plugins/bootstrap/js/bootstrap.min.js');
	echo $this->Html->script('/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js');
	echo $this->Html->script('/assets/global/scripts/metronic.js');
	echo $this->Html->script('/assets/admin/layout/scripts/layout.js');
	echo $this->Html->script('/assets/admin/layout/scripts/quick-sidebar.js');     
?>
<script>
	jQuery(document).ready(function() {     
			Metronic.init(); // init metronic core components
			Layout.init(); // init current layout
			QuickSidebar.init() // init quick sidebar
			//ComponentsPickers.init();
	});
</script>
<!-- END JAVASCRIPTS -->
<?php ?> <?php //echo $this->element('sql_dump'); ?> <?php  ?>
</body>
<!-- END BODY -->
</html>   

<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" >
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
<meta charset="utf-8"/>
<title>My Parking Pass | Powered by Digital6</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
<?php
echo $this->Html->charset();
echo $this->Html->css('/assets/global/plugins/bootstrap/css/bootstrap.min.css');
echo $this->Html->css('/assets/global/plugins/uniform/css/uniform.default.css');
echo $this->Html->css('/assets/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.css');
echo $this->Html->css('/assets/admin/layout/css/layout.css');
?>
</head>
<body class="">
<div class="container">
<?php echo $this->fetch('content'); ?>
<div class="copyright">
	<?php echo date('Y'); ?> &copy; <a href="http://digital6technologies.com" target="_blank" style="color:black;">Powered by Digital6 Technologies</a>
</div>

</div>
</body>
</html>

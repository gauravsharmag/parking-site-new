<div class="couponCodes view">
<h2><?php echo __('Coupon Code'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($couponCode['CouponCode']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Code'); ?></dt>
		<dd>
			<?php echo h($couponCode['CouponCode']['code']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Coupon Package'); ?></dt>
		<dd>
			<?php echo $this->Html->link($couponCode['CouponPackage']['name'], array('controller' => 'coupon_packages', 'action' => 'view', $couponCode['CouponPackage']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($couponCode['CouponCode']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($couponCode['CouponCode']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Coupon Code'), array('action' => 'edit', $couponCode['CouponCode']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Coupon Code'), array('action' => 'delete', $couponCode['CouponCode']['id']), array(), __('Are you sure you want to delete # %s?', $couponCode['CouponCode']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Coupon Codes'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Coupon Code'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Coupon Packages'), array('controller' => 'coupon_packages', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Coupon Package'), array('controller' => 'coupon_packages', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Coupon Code Users'), array('controller' => 'coupon_code_users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Coupon Code User'), array('controller' => 'coupon_code_users', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Coupon Code Users'); ?></h3>
	<?php if (!empty($couponCode['CouponCodeUser'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th><?php echo __('Coupon Code Id'); ?></th>
		<th><?php echo __('User Id'); ?></th>
		<th><?php echo __('Completed'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($couponCode['CouponCodeUser'] as $couponCodeUser): ?>
		<tr>
			<td><?php echo $couponCodeUser['id']; ?></td>
			<td><?php echo $couponCodeUser['created']; ?></td>
			<td><?php echo $couponCodeUser['modified']; ?></td>
			<td><?php echo $couponCodeUser['coupon_code_id']; ?></td>
			<td><?php echo $couponCodeUser['user_id']; ?></td>
			<td><?php echo $couponCodeUser['completed']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'coupon_code_users', 'action' => 'view', $couponCodeUser['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'coupon_code_users', 'action' => 'edit', $couponCodeUser['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'coupon_code_users', 'action' => 'delete', $couponCodeUser['id']), array(), __('Are you sure you want to delete # %s?', $couponCodeUser['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Coupon Code User'), array('controller' => 'coupon_code_users', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>


<div class="page-content-wrapper">
		<div class="page-content">
		   <div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
					Users<small>List of archived users</small>
					</h3>
					<ul class="page-breadcrumb breadcrumb">
					
						<li>
							<i class="fa fa-home"></i>
							<?php echo $this->Html->link('Users',array('controller'=>'users','action'=>'index')); ?>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<?php echo $this->Html->link('List Other Users',array('controller'=>'users','action'=>'index')); ?>
							
						</li>
						
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
			<div id="flashMessages">
                	<h2><?php  echo $this->Session->flash();?></h2>
            </div>
            <div class="row">
				<div class="col-md-12">
					<!-- BEGIN EXAMPLE TABLE PORTLET-->
					<div class="portlet box red">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-users"></i>Archived Users List
							</div>
							<div class="tools">
								<a href="javascript:;" class="collapse">
								</a>
							</div>
						</div>
						<div class="portlet-body">
							<div class="table-toolbar">
								<div class="btn-group">
                                <?php echo $this->Html->link('Add New <i class="fa fa-plus"></i>',array('controller'=>'users','action'=>'add'),array('escape'=>false,'class'=>'btn green')); ?>
								</div>
							</div>
							<div class="nav-pills">
								<h3>Search Options</h3>
								<?php echo $this->Form->create('User',array('class'=>'form-inline'))?>
									 
									  <div class="form-group">
										<?php echo $this->Form->input('search_option',array('type'=>'select',
																						    'options'=>array(
																									'first_name'=>'First Name',
																									'last_name'=>'Last Name',
																									'username'=>'Username',
																									'email'=>'Email',
																									'address_line_1'=>'Address',
																									'address_line_2'=>'Apartment #'
																								),
																							'div'=>false,'class'=>'form-control','label'=>false,'required'=>true));?>
									  </div>
									   <div class="form-group">
										<?php echo $this->Form->input('search_key_to_search',array('placeholder'=>'Search...','div'=>false,'class'=>'form-control','label'=>false,'required'=>true));?>
									  </div>	
									 <button class="btn submit yellow" type="button" onclick="createTable();"><i class="fa fa-search" aria-hidden="true"></i></button>
								<?php echo $this->Form->end();?>
							</div>
							<hr>
							<div class="table-scrollable">
							<table id="all_customer_list"class="table table-striped table-bordered table-hover"> 
								<thead>
								<tr>
									<th>
										First Name
									</th>
									<th>
										Last Name
									</th>
									<th>
										Username
									</th>
									<th>
										 Email
									</th>
									<th>
										 Phone
									</th>
									<th>
										 Address Line 1
									</th>
									<th>
										 Apartment # 
									</th>
									<th>
										 Role
									</th>
									<th>
										Property
									</th>
									<th>
										Actions
									</th>
									<!--<th>
										Action
									</th>-->
								</tr>
								</thead>
								<tbody>
									<tr>
										<td colspan="10" class="dataTables_empty">Loading Data...</td>
									</tr>
								</tbody>
							</table>
							</div>
							
						</div>
					</div>
					<!-- END EXAMPLE TABLE PORTLET-->
				</div>
			</div>
</div>
<?php echo $this->Html->script('//code.jquery.com/jquery-1.12.0.min.js'); ?>
<?php echo $this->Html->script('//cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js'); ?>
<?php echo $this->Html->script('//cdn.datatables.net/buttons/1.1.2/js/dataTables.buttons.min.js'); ?>
<?php echo $this->Html->script('//cdn.datatables.net/buttons/1.1.2/js/buttons.print.min.js'); ?>
<?php echo $this->Html->script('//cdn.datatables.net/buttons/1.1.2/js/buttons.flash.min.js'); ?>
<?php echo $this->Html->script('//cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js'); ?>
<?php echo $this->Html->script('//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js'); ?>
<?php echo $this->Html->script('//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js'); ?>
<?php echo $this->Html->script('//cdn.datatables.net/buttons/1.1.2/js/buttons.html5.min.js'); ?>
<?php echo $this->Html->script('//cdn.datatables.net/buttons/1.1.2/js/buttons.colVis.min.js'); ?>
<?php echo $this->Html->css('//cdn.datatables.net/1.10.11/css/jquery.dataTables.min.css'); ?>
 <?php echo $this->Html->css('//cdn.datatables.net/buttons/1.1.2/css/buttons.dataTables.min.css'); ?> 
<script type="text/javascript">
createTable();
function createTable(){	
	$('#all_customer_list').dataTable({ 
			"bProcessing": false,
			"bServerSide": true,
			"bDestroy": true,
			"dom": 'lBrftip',
			"columnDefs": [{"className": "dt-center", "targets": "_all"},
							
						    {
								"targets": 9,
								"data": function ( row, type, val, meta ) {
										str = "<a class='btn green btn-xs green-stripe' href='/admin/users/edit/"+row.id+"'>Edit</a>&nbsp";
										str += "<a class='btn default btn-xs ' href='/admin/users/view_user_details/"+row.id+"'>View</a>&nbsp";
										str += "<a class='btn yellow btn-xs yellow-stripe' href='/admin/users/print_details/"+row.id+"'target='_blank'>Print</a>&nbsp;";
										if(row.role_name!='customer'){
											var token = Math.floor((Math.random() * 100000000111111) + 1);
												str+= '&nbsp;<form action="/admin/Users/delete/'+row.id+'" name="post_'+token+'" id="post_'+token+'" style="display:none;" method="post"><input type="hidden" name="_method" value="POST"></form><a href="#" onclick="if (confirm(&quot;Are you sure?&quot;)) { document.post_'+token+'.submit(); } event.returnValue = false; return false;" class="btn red btn-xs red-stripe">Delete</a>';
											
										}else{
											 str += "<a class='btn red btn-xs red-stripe' href='/admin/CustomerPasses/customer_view/"+row.id+"'>Manage</a>&nbsp";
											 str += "<a class='btn default btn-xs green-stripe' href='/admin/tickets/contact_user/"+row.id+"'>Contact</a>";
										}
										return str;
								}
						   }
						],
			// "sAjaxSource": "<?php echo $this->Html->Url(array('controller' => 'Users', 'action' => 'admin_user_details_corrected',true)); ?>",
			 "sAjaxSource": "/admin/Users/user_details_corrected/1?search_option="+$('#UserSearchOption').val()+"&key="+$('#UserSearchKeyToSearch').val(),
			 buttons: [
            {
                extend: 'print',
                exportOptions: {
                    columns: [ 0, ':visible' ]
                },
                customize: function ( win ) {
                    $(win.document.body)
                        .css( 'font-size', '10pt' )
                        .prepend(
                            '<img src="<?php echo "https://internetparkingpass.com/img/onlineparking-logo.png" ?>" alt="" style="position:absolute; opacity:0.3; top:40%; left:30%;" />'
                        );
 
                    $(win.document.body).find( 'table' )
                        .addClass( 'compact' )
                        .css( 'font-size', 'inherit' );
                }
            },
            {
				extend: 'csv',
				 exportOptions: {
                    columns: [ 0, ':visible' ]
                }
			},
			{
				extend: 'pdf',
				 exportOptions: {
                    columns: [ 0, ':visible' ]
                }
			},
			{
				extend: 'excel',
				 exportOptions: {
                    columns: [ 0, ':visible' ]
                }
			},
			{
				extend: 'colvis',
				 text: "Select Columns"
			}
			
        ],
       "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
       oLanguage: {
			sInfoFiltered: "",
			sLengthMenu: "Show _MENU_ entries.&nbsp;&nbsp;",
		},
	});
}
</script>

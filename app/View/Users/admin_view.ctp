

<div class="page-content-wrapper">
		<div class="page-content">
<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">View User Info <small><?php echo h($user['User']['username']); ?> </small>
					</h3>
					<ul class="page-breadcrumb breadcrumb">
						
						<li>
							<i class="fa fa-home"></i>
							<?php echo $this->Html->link('Users',array('controller'=>'Users','action'=>'index')); ?>    
							<i class="fa fa-angle-right"></i>
                            
						</li>
						<li>
                         View User
							<?php //echo $this->Html->link('List Properties',array('controller'=>'properties','action'=>'index')); ?>
							
						</li>
						
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
            <div class="row">
				<div class="col-md-12">
					<!-- BEGIN EXAMPLE TABLE PORTLET-->
					<div class="portlet box grey-cascade">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-users"></i>View User - <?php echo h($user['User']['username']); ?>
							</div>
							<div class="tools">
								<a href="javascript:;" class="collapse">
								</a>
								<?php /*?><a href="#portlet-config" data-toggle="modal" class="config">
								</a>
								<a href="javascript:;" class="reload">
								</a>
								<a href="javascript:;" class="remove">
								</a><?php */?>
							</div>
						</div>
						<div class="portlet-body">
							 <div class="form-body">
               <!-- <h2 class="margin-bottom-20"> View Property Info -  </h2>
                <h3 class="form-section">Property Info</h3>-->
                
				 <!--/row-->
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">First Name:</label>
                            <div class="col-md-9">
                                <p class="form-control-static">
                                    <?php echo h($user['User']['first_name']); ?>
                                </p>
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Last Name:</label>
                            <div class="col-md-9">
                                <p class="form-control-static">
                                    <?php echo h($user['User']['last_name']); ?>
                                </p>
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                </div>
                   
            	
                
                <!--/row-->
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Username:</label>
                            <div class="col-md-9">
                                <p class="form-control-static">
                                    <?php echo h($user['User']['username']); ?>
                                </p>
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">ID:</label>
                            <div class="col-md-9">
                                <p class="form-control-static">
                                    <?php echo h($user['User']['id']); ?>
                                </p>
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                </div>
                   
                <!--/row-->
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Email :</label>
                            <div class="col-md-9">
                                <p class="form-control-static">
                                    <?php echo h($user['User']['email']); ?>
                                </p>
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Role:</label>
                            <div class="col-md-9">
                                <p class="form-control-static">
                                   <?php echo h($user['Role']['role_name']); ?>
                                </p>
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                </div> 
                <!--/row-->
                   
                <!--/row-->
                
                <!--/row-->
               
               <br />

                
                
            </div>
           
           <div class="form-actions fluid">
                <div class="row">
                    <div class="col-md-6">
                        <div class="col-md-offset-3 col-md-9">
                          <?php echo $this->Html->link("<i class='fa fa-pencil'></i> Edit",array('controller'=>'users','action'=>'edit',$user['User']['id']),array('class'=>'btn green','escape'=>false)); ?>
                            <!--<button type="submit" class="btn green"><i class="fa fa-pencil"></i> Edit</button>
                            <button type="button" class="btn default">Cancel</button>-->
                        </div>
                    </div>
                    <div class="col-md-6">
                    </div>
                </div>
            </div>
					</div>
					<!-- END EXAMPLE TABLE PORTLET-->
				</div>
			</div>

    



<!-- BEGIN LOGIN FORM -->
<?php
        echo $this->Form->create('User',array('class'=>'login-form','url'=>array('action'=>'resend_approval_link/'.$key)));
?>
<h3 class="form-title">Sorry, your email is not verified.</h3>
<div id="flashMessages">
                 <?php  echo $this->Session->flash();?>
</div>
<div class="alert alert-success">
                                        <strong>Current Email:</strong> <?php echo $user['User']['email']; ?> </div>
<?php
        echo $this->Form->input('email',array('div'=>array('class'=>'form-group'),'error'=>array('attributes'=>array('class'=>'model-error')),'required'=>false,'label'=>array('class'=>'control-label visible-ie8 visible-ie9','text'=>'Username'),'before'=>'<div class="input-icon"><i class="fa fa-envelope-o"></i>','after'=>'</div>','class'=>'form-control placeholder-no-fix'));
		
 ?>
	<p class="help-block"> Add new email address to change current email address. <br>OR <br>Click below to resend link.</p>
	<button type="submit" class="btn blue btn-block">Click Here To Resend Verification Link</button>
<?php echo $this->Form->end(); ?>
<!-- END LOGIN FORM -->



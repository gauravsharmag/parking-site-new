
<?php echo $this->Form->create();
    // debug($users);
     $user=$users;

?>
	
<div class="page-content-wrapper">
<div class="page-content" style="min-height:1213px">
			
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			
			<!-- BEGIN PAGE HEADER-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
					User Profile <small><?php echo ($user['User']['first_name']." ".$user['User']['last_name']);?></small>
					</h3>
					<ul class="page-breadcrumb breadcrumb">
						<li>
							<i class="fa fa-home"></i>
							<?php 
								if(AuthComponent::user('role_id')==1){
										echo $this->Html->link('Home',array(
                                          'controller' => 'Users',
                                          'action' => 'superAdminHome',
                                          'full_base' => true
                                      ));
                                    }elseif(AuthComponent::user('role_id')==2){
										echo $this->Html->link('Home',array(
                                          'controller' => 'Users',
                                          'action' => 'pa_home_page',
                                          'full_base' => true
                                      ));
                                    }
                                  
                               ?>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="#">User Profile</a>
						</li>
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
			<!-- END PAGE HEADER-->
			<div id="flashMessages">
                	<h2 id="checkError"><?php  echo $this->Session->flash();?></h2>
            </div>
			<!-- BEGIN PAGE CONTENT-->
			<div class="row profile">
				<div class="col-md-12">
					<!--BEGIN TABS-->
					<div class="tabbable tabbable-custom tabbable-full-width">
						<ul class="nav nav-tabs">
							<li class="">
								<a data-toggle="tab" href="#tab_1_1">
								Account </a>
							</li>

						</ul>
						<div class="tab-content">
	                    <div id="tab_1_1" class="tab-pane active">
								<div class="row profile-account">
									<div class="col-md-3">
										<ul class="ver-inline-menu tabbable margin-bottom-10">
											<li id="li1" class="active">
												<a href="#tab_1-1" data-toggle="tab">
												<i class="fa fa-cog"></i> Personal info </a>
												<span class="after">
												</span>
											</li>
											<li id="li2" class="">
												<a href="#tab_3-3" data-toggle="tab">
												<i class="fa fa-lock"></i> Change Password </a>
											</li>
										</ul>
									</div>
									<div class="col-md-9">
										<div class="tab-content">
											<div class="tab-pane active" id="tab_1-1">
                                                  
                                                    <?php echo $this->Form->create();
                                                        echo $this->Form->hidden('id',array('default'=>$user['User']['id']));
                                                    ?>
													<div class="form-group">
													    <?php  echo $this->Form->input('first_name',array('class'=>'form-control','error'=>array('attributes'=>array('class'=>'model-error')),'value'=>$user['User']['first_name']));?>
                                                    </div>
													<div class="form-group">
														 <?php  echo $this->Form->input('last_name',array('class'=>'form-control','error'=>array('attributes'=>array('class'=>'model-error')),'value'=>$user['User']['last_name']));?>
													</div>
													<div class="form-group">
														<?php  echo $this->Form->input('email',array('class'=>'form-control','error'=>array('attributes'=>array('class'=>'model-error')),'value'=>$user['User']['email']));?>
													</div>
													<div class="form-group">
														<?php  echo $this->Form->input('address_line_1',array('class'=>'form-control','error'=>array('attributes'=>array('class'=>'model-error')),'value'=>$user['User']['address_line_1']));?>
                                                          													</div>
													<div class="form-group">
														<?php  echo $this->Form->input('address_line_2',array('label'=>array('text'=>'Appartment/Suite #'),'class'=>'form-control','error'=>array('attributes'=>array('class'=>'model-error')),'value'=>$user['User']['address_line_2']));?>
													</div>
													<div class="form-group">
														<?php  echo $this->Form->input('city',array('label'=>array('text'=>'City'),'class'=>'form-control','error'=>array('attributes'=>array('class'=>'model-error')),'value'=>$user['User']['city']));?>
													</div>
													<div class="form-group">
                                                        <?php  echo $this->Form->input('state',array('type' => 'select',
														            'options' => $states,
														            'empty'=>'State','error'=>array('attributes'=>array('class'=>'model-error')),'class'=>'form-control','value'=>$user['User']['state']) );
														?>
													</div>
													<div class="form-group">
														<?php  echo $this->Form->input('zip',array('class'=>'form-control','error'=>array('attributes'=>array('class'=>'model-error')),'value'=>$user['User']['zip']));?>
													</div>
                                                    <div class="form-group">
														<?php  echo $this->Form->input('phone',array('class'=>'form-control','error'=>array('attributes'=>array('class'=>'model-error')),'value'=>$user['User']['phone']));?>
													</div>
													 <div class="form-actions fluid">
                                                           <div class="col-md-offset-3 col-md-9">
                                                                  <button type="submit" class="btn green">Submit</button>
                                                           </div>
                                                     </div>
                                                 <?php echo $this->Form->end( ); ?>
											</div>
											
											<div class="tab-pane" id="tab_3-3">
											  <?php echo $this->Form->create();?>
													<div class="form-group">
														<?php  echo $this->Form->input('current_password',array('class'=>'form-control','type'=>'password','text'=>'Current Password','error'=>array('attributes'=>array('class'=>'model-error')),'required'=>'true','error'=>array('attributes'=>array('class'=>'model-error'))));?>
													</div>
													<div class="form-group">
														<?php  echo $this->Form->input('new_password',array('class'=>'form-control','type'=>'password','required'=>'true','label'=>'New Password','error'=>array('attributes'=>array('class'=>'model-error')),'required'=>'true','error'=>array('attributes'=>array('class'=>'model-error'))));?>
													</div>
													<div class="form-group">
														<?php  echo $this->Form->input('confirm_password',array('class'=>'form-control','type'=>'password','required'=>'true','label'=>'Retype New Password','required'=>'true','error'=>array('attributes'=>array('class'=>'model-error')),'error'=>array('attributes'=>array('class'=>'model-error'))));?>
													</div>
													 <div class="form-actions fluid">
                                                           <div class="col-md-offset-3 col-md-9">
                                                                 <button type="submit" class="btn green">Change</button>
                                                           </div>
                                                     </div>
                                                 <?php echo $this->Form->end( ); ?>
											</div>
											
										</div>
									</div>
									<!--end col-md-9-->
								</div>
							</div>
					    </div>
					<!--END TABS-->
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>

<script>
jQuery( document ).ready(function(){
	var parent = document.getElementById("checkError");
    var division = parent.getElementsByTagName("div");
    if(division){
		if(division[0]){
		 if(division[0].innerHTML=="Check Password Details"){
			$("#tab_3-3").addClass('active');
			$('#tab_1-1').removeClass('active');
			$('#li1').removeClass('active');
			$('#li2').addClass('active');
		}
	  }
	}
});	
</script>

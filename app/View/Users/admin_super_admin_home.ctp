
<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
        
        	<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
					Dashboard <small>statistics and more</small>
					</h3>
					<ul class="page-breadcrumb breadcrumb">
						<li>
							<i class="fa fa-home"></i>
							<a href="#">Home</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="#">Dashboard</a>
						</li>
						<li class="pull-right">
							<div id="dashboard-report-range" class="dashboard-date-range tooltips" data-placement="top" data-original-title="Change dashboard date range">
								<i class="icon-calendar"></i>
								<span></span>
								<i class="fa fa-angle-down"></i>
							</div>
						</li>
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
            <div class="row">
				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
					<div class="dashboard-stat blue-madison">
						<div class="visual">
							<i class="fa fa-building-o"></i>
						</div>
						<div class="details">
							<div class="number">
								 <?php echo $propertyCount;?>
							</div>
							<div class="desc">
								Total Properties
							</div>
						</div>
						<a class="more" href="/admin/properties">
						View Properties <i class="m-icon-swapright m-icon-white"></i>
						</a>
					</div>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
					<div class="dashboard-stat red-intense">
						<div class="visual">
							<i class="fa fa-taxi"></i>
						</div>
						<div class="details">
							<div class="number">
								  <?php echo $vehicleCount;?>
							</div>
							<div class="desc">
								Active Vehicles
							</div>
						</div>
						<a class="more" href="/admin/Vehicles/list_vehicles">
						View Vehicles <i class="m-icon-swapright m-icon-white"></i>
						</a>
					</div>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
					<div class="dashboard-stat green-haze">
						<div class="visual">
							<i class="fa fa-group"></i>
						</div>
						<div class="details">
							<div class="number">
								 <?php echo $userCount;?>
							</div>
							<div class="desc">
								 Total Users
							</div>
						</div>
						<a class="more" href="/admin/Users">
						List Users <i class="m-icon-swapright m-icon-white"></i>
						</a>
					</div>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
					<div class="dashboard-stat purple-plum">
						<div class="visual">
							<i class="fa fa-dollar"></i>
						</div>
						<div class="details">
							<div class="number">
								$ <?php echo $transactionAmount;?>
							</div>
							<div class="desc">
								 Transaction Amount (Last Month)
							</div>
						</div>
						<a class="more" href="/admin/CustomerPasses/property_wise_passes">
								List Passes <i class="m-icon-swapright m-icon-white"></i>
						</a>
					</div>
				</div>
			</div>
			
            <div class="clearfix">
			</div>
			<div class="row">
				<div class="col-md-12">
					 <div class="tabbable tabbable-tabdrop">
						<ul class="nav nav-tabs">
						
							<li class="active">
								<a href="#tab_0" data-toggle="tab" aria-expanded="true"><?php echo __('Change Vehicle Requests'); ?>&nbsp;<span class="badge badge-info" id="new_tag"></span></a>
							</li>
							<li class="">
								<a href="#tab_1" data-toggle="tab" aria-expanded="false"><?php echo __('Messages'); ?>&nbsp;<span class="badge badge-warning" id="message_tag"></span></a>
							</li>
						</ul>
						<div class="tab-content">
							<!---TAB 0 START-->
							<div class="tab-pane fade active in" id="tab_0">
								<div class="portlet portlet box blue">	
										<div class="portlet-title" >
											<div class="caption" >
												<i class="fa fa-gift"></i>Change Vehicle Requests
											</div>
										</div>
										<div class="portlet-body " >
											<div class="table-responsive">
												<div class="table-toolbar">
													<a href="/admin/ChangeVehicleDetails/view_change" class="btn green">View All</a>
												</div>
												<table class="table _table-bordered" id="change_list" width="100%">
													<thead>
														<tr>
															<th>
																Customer
															</th>
															<th>
																Property
															</th>
															<th>
																Vehicle
															</th>
															<th>
																Created
															</th>
															<th>
																Actions
															</th>
														</tr>
														</thead>
														<tbody>
															<tr>
																<td colspan="4" class="dataTables_empty">Loading Data...</td>
															</tr>
														</tbody>
												</table>
											</div>
										</div>
									</div>
							</div>
							<!--TAB 0 END-->
							<!---TAB 1 START-->
							<div class="tab-pane fade" id="tab_1">
								<div class="portlet portlet box green">	
									<div class="portlet-title" >
										<div class="caption" >
											<i class="fa fa-gift"></i> Messages
										</div>
									</div>
									<div class="portlet-body " >
										<div class="table-toolbar">
											<div class="btn-group">
												<?php echo $this->Html->link('View All ',array('controller'=>'tickets','action'=>'view'),array('escape'=>false,'class'=>'btn red')); ?>
											</div>
											
										</div>
										 <div class="table-responsive">
											<table class="table _table-bordered" id="tickets" width="100%">
												<thead>
													<tr>

														<th>Subject</th>
														<th>From</th>
														<th>To</th>
														<th>Sent On</th>
														<th>Status</th>
														<th>Action</th>
													</tr>
												</thead>
												<tbody>
												   

												</tbody>
											</table>
									  </div>
								  </div>
							  </div>
						     
							<!--TAB 1 END-->
						</div>
					 </div>
				</div>
			</div>
		</div>
    </div>
<?php echo $this->Html->script('//cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js'); ?>
<?php echo $this->Html->css('//cdn.datatables.net/1.10.11/css/jquery.dataTables.min.css'); ?>
<?php 
	echo $this->Html->script(array('moment.min.js')); 
?>
<script type="text/javascript">
 $('#change_list').dataTable({  
			"bProcessing": false,
			"bServerSide": true,
			 "bDestroy": true,
			 "initComplete": function(settings, json) {
						$('#new_tag').html(json.iTotalDisplayRecords);
					 },
			 "columnDefs": [{"className": "dt-center", "targets": "_all"},
							{
								"targets": 0,
								"data": function ( row, type, val, meta ) {
												    str = row.first_name+" "+row.last_name+"<br>"+row.email+"<br>"+row.phone;			
												    return str;
								}
						   },
						   {
								"targets": 2,
								"data": function ( row, type, val, meta ) {
												    str = ' <span class="label label-sm label-danger ">'+row.license_plate_state+" "+row.license_plate_number+"</span>";			
												    return str;
								}
						   },
						   {
								"targets": 3,
								"data": function ( row, type, val, meta ) {
											var date = moment(row.created);
											var newDate = date.format(" DD MMM  YYYY HH:mm:ss"); 
											return newDate;
								}
						   },
						    {
								"targets": 4,
								"data": function ( row, type, val, meta ) {
												    str = "&nbsp;<a class='label label-sm label-success' href='/admin/ChangeVehicleDetails/change/"+row.id+"'>View</a>";	
												    return str;
								}
						   }
						],
			"sAjaxSource":"/admin/ChangeVehicleDetails/getAllRequests"
	});
	$('#tickets').dataTable({
			"bProcessing": false,
			"bServerSide": true,
			"bDestroy": true,
			"initComplete": function(settings, json) {
						$('#message_tag').html(json.iTotalDisplayRecords);
					 },
			"aoColumns": [
				{"mData":"Ticket.subject"},
				{"mData":"User.first_name"},
				{"mData":"Recipient.first_name"},
				{"mData":"Ticket.created"},
				{"mData":"Ticket.status"},
				{"mData":"Ticket.id"},
			],
			"sAjaxSource":"/admin/Tickets/get_tickets/notSeen"
		});
</script>   

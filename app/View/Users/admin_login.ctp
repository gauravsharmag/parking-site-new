<?php
        echo $this->Form->create(array('url'=>array('controller'=>'Users','action'=>'admin_login')));
        ?>
        <h3 class="form-title">Login to your account</h3>
        <div id="flashMessages">
                 <?php  echo $this->Session->flash();?>
		</div>
        <?php
		//echo $this->Session->flash();
		echo $this->Form->hidden('requestedUrl',array('default'=>$requestedURL));
        echo $this->Form->input('username',array('div'=>array('class'=>'form-group'),'label'=>array('class'=>'control-label visible-ie8 visible-ie9','text'=>'Username'),'before'=>'<div class="input-icon"><i class="fa fa-user"></i>','after'=>'</div>','class'=>'form-control placeholder-no-fix'));
		echo $this->Form->input('password',array('div'=>array('class'=>'form-group'),'label'=>array('class'=>'control-label visible-ie8 visible-ie9','text'=>'Password'),'before'=>'<div class="input-icon"><i class="fa fa-key"></i>','after'=>'</div>','class'=>'form-control placeholder-no-fix'));
            
 ?>
 <div class="form-actions">
			<label class="checkbox">
			<!--<input type="checkbox" name="remember" value="1"/> Remember me </label>-->
			<button type="submit" class="btn green pull-right">
			Login <i class="m-icon-swapright m-icon-white"></i>
			</button>
		</div>
<?php echo $this->Form->end(); 
		//CakeSession::destroy();
?>

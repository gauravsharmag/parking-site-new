<div class="page-content-wrapper">
        <div class="page-content">
<div class="row">
<div class="col-md-12">
<?php echo $this->element('useractions'); ?>
<div class="portlet box green ">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-gift"></i> Edit User Details
                            </div>
                            <div class="tools">
                                <a href="" class="collapse">
                                </a>
                            </div>
                        </div>
                        <div class="portlet-body form">
                           <?php echo $this->Form->create(array('class'=>'form-horizontal')); ?>
                                <div class="form-body">
    	<?php
        		echo $this->Form->input('id');
echo $this->Form->input('first_name',array('div'=>array('class'=>'form-group'),'label'=>array('class'=>'col-md-3 control-label','text'=>'First Name'),'between'=>'<div class="col-md-9">','after'=>'</div>','class'=>'form-control input-inline input-medium','placeholder'=>'First Name'));

echo $this->Form->input('last_name',array('div'=>array('class'=>'form-group'),'label'=>array('class'=>'col-md-3 control-label','text'=>'Last Name'),'between'=>'<div class="col-md-9">','after'=>'</div>','class'=>'form-control input-inline input-medium','placeholder'=>'Last Name'));
echo $this->Form->input('email',array('div'=>array('class'=>'form-group'),'label'=>array('class'=>'col-md-3 control-label','text'=>'E-mail'),'between'=>'<div class="col-md-9">','after'=>'</div>','class'=>'form-control input-inline input-medium','placeholder'=>'E-mail'));
echo $this->Form->input('address_line_1',array('div'=>array('class'=>'form-group'),'label'=>array('class'=>'col-md-3 control-label','text'=>'Address 1'),'between'=>'<div class="col-md-9">','after'=>'</div>','class'=>'form-control input-inline input-medium','placeholder'=>'Address 1'));

echo $this->Form->input('address_line_2',array('div'=>array('class'=>'form-group'),'label'=>array('class'=>'col-md-3 control-label','text'=>'Address 2'),'between'=>'<div class="col-md-9">','after'=>'</div>','class'=>'form-control input-inline input-medium','placeholder'=>'Address 2'));
echo $this->Form->input('city',array('div'=>array('class'=>'form-group'),'label'=>array('class'=>'col-md-3 control-label','text'=>'City'),'between'=>'<div class="col-md-9">','after'=>'</div>','class'=>'form-control input-inline input-medium','placeholder'=>'City'));
echo $this->Form->input('state',array('div'=>array('class'=>'form-group'),'label'=>array('class'=>'col-md-3 control-label','text'=>'State'),'between'=>'<div class="col-md-9">','after'=>'</div>','class'=>'form-control input-inline input-medium','placeholder'=>'State'));
echo $this->Form->input('zip',array('div'=>array('class'=>'form-group'),'label'=>array('class'=>'col-md-3 control-label','text'=>'Zip'),'between'=>'<div class="col-md-9">','after'=>'</div>','class'=>'form-control input-inline input-medium','placeholder'=>'Zip'));
echo $this->Form->input('phone',array('div'=>array('class'=>'form-group'),'label'=>array('class'=>'col-md-3 control-label','text'=>'Phone Number'),'between'=>'<div class="col-md-9">','after'=>'</div>','class'=>'form-control input-inline input-medium','placeholder'=>'Phone Number'));
        	?>

</div>
                                    <div class="form-actions fluid">
                                        <div class="col-md-offset-3 col-md-9">
                                            <button type="submit" class="btn green">Submit</button>
                                         </div>
                                     </div>
                                <?php echo $this->Form->end( ); ?> 
</div>
</div></div></div></div>
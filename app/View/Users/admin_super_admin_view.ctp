
<div class="page-content-wrapper">
    <div class="page-content">
			
    	<!-- BEGIN FORM-->
        
            <div class="form-body">
                <h2 class="margin-bottom-20"> View User Info - <?php echo h($user['User']['username']); ?> </h2>
                <h3 class="form-section">Person Info</h3>
                
                <!--/row-->
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Username:</label>
                            <div class="col-md-9">
                                <p class="form-control-static">
                                    <?php echo h($user['User']['username']); ?>
                                </p>
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">ID:</label>
                            <div class="col-md-9">
                                <p class="form-control-static">
                                    <?php echo h($user['User']['id']); ?>
                                </p>
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                </div>
                <!--/row-->
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Email Address:</label>
                            <div class="col-md-9">
                                <p class="form-control-static">
                                    <?php echo h($user['User']['email']); ?>
                                </p>
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Role:</label>
                            <div class="col-md-9">
                                <p class="form-control-static">
                                   <?php echo h($user['Role']['name']); ?>
                                </p>
                            </div>
                        </div>
                    </div>
                    <!--/span-->
                </div>
                <!--/row-->
               
               <br />

                
                
            </div>
           
            <!--<div class="form-actions fluid">
                <div class="row">
                    <div class="col-md-6">
                        <div class="col-md-offset-3 col-md-9">
                          <?php echo $this->Html->link("<i class='fa fa-pencil'></i> Edit",array('controller'=>'users','action'=>'edit',$user['User']['id']),array('class'=>'btn green','escape'=>false)); ?>
                            <!--<button type="submit" class="btn green"><i class="fa fa-pencil"></i> Edit</button>
                            <button type="button" class="btn default">Cancel</button>-->
                        </div>
                    </div>
                    <div class="col-md-6">
                    </div>
                </div>
            </div>-->
     
        <!-- END FORM-->


<!--
<div class="users view">
<h2><?php echo __('User'); //debug($user); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($user['User']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('name'); ?></dt>
        		<dd>
        			<?php echo h($user['User']['first_name'].' '.$user['User']['last_name']); ?>
        			&nbsp;
        		</dd>
		<dt><?php echo __('Username'); ?></dt>
		<dd>
			<?php echo h($user['User']['username']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Email'); ?></dt>
		<dd>
			<?php echo h($user['User']['email']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Password'); ?></dt>
		<dd>
			<?php echo h($user['User']['password']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Role'); ?></dt>
		<dd>
			<?php echo $this->Html->link($user['Role']['name'], array('controller' => 'roles', 'action' => 'view', $user['Role']['id'])); ?>
			&nbsp;
		</dd>
		  <dt><?php echo __('Address') ?></dt>
                                            <dd>
                                                <?php echo h($user['User']['address_line_1']); echo"<br>";?>
                                                &nbsp;
                                            </dd>
                                            <dd>
                                                <?php echo h($user['User']['address_line_2']); echo"<br>"; ?>
                                                &nbsp;
                                            </dd>
                                            <dd>
                                                <?php echo h($user['User']['street']).' '.h($user['User']['city']); ?>
                                                &nbsp;
                                            </dd>
                     <dt><?php echo __('State') ?></dt>
                                                        <dd>
                                                            <?php echo h($user['User']['state']);?>
                                                            &nbsp;
                                                        </dd>
                    <dt><?php echo __('Zip') ?></dt>
                                                        <dd>
                                                            <?php echo h($user['User']['zip']); echo"<br>"; ?>

                                                        </dd>
                    <dt><?php echo __('Mobile') ?></dt>
                                                                    <dd>
                                                                        <?php echo h($user['User']['phone']); echo"<br>"; ?>
                                                                        &nbsp;
                                                                    </dd>

	</dl>
</div>
<div>
    <?php echo $this->element('homePage');?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit User'), array('action' => 'edit', $user['User']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete User'), array('action' => 'delete', $user['User']['id']), array(), __('Are you sure you want to delete # %s?', $user['User']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('action' => 'add')); ?> </li>



</div>
<?php if (!empty($user['Pass'])): ?>
        <div class="related">
	            <h3><?php echo __('Passes Details'); ?></h3>

	            <table cellpadding = "0" cellspacing = "0">
	            <tr>
		            <th><?php echo __('Id'); ?></th>
		            <th><?php //echo __('User Id'); ?></th>
		            <th><?php //echo __('Vehicle Id'); ?></th>
		            <th><?php echo __('Property Id'); ?></th>
		            <th><?php echo __('Transaction Id'); ?></th>
		            <th><?php echo __('Status'); ?></th>
		            <th><?php echo __('Membership Vaild Upto'); ?></th>
		            <th><?php echo __('Pass Valid Upto'); ?></th>
		            <th><?php echo __('RFID Tag Number'); ?></th>
		            <th class="actions"><?php echo __('Actions'); ?></th>
	            </tr>
	            <?php foreach ($user['Pass'] as $pass): ?>
		            <tr>
			            <td><?php echo $pass['id']; ?></td>
			            <td><?php //echo $pass['user_id']; ?></td>
			            <td><?php //echo $pass['vehicle_id']; ?></td>
			            <td><?php echo $pass['property_id']; ?></td>
			            <td><?php echo $pass['transaction_id']; ?></td>
			            <td><?php echo $pass['status']; ?></td>
			            <td><?php echo $pass['membership_vaild_upto']; ?></td>
			            <td><?php echo $pass['pass_valid_upto']; ?></td>
			            <td><?php echo $pass['RFID_tag_number']; ?></td>
			            <td class="actions">
				            <?php echo $this->Html->link(__('View'), array('controller' => 'passes', 'action' => 'view', $pass['id'])); ?>
				            <?php echo $this->Html->link(__('Edit'), array('controller' => 'passes', 'action' => 'edit', $pass['id'])); ?>
				            <?php echo $this->Form->postLink(__('Delete'), array('controller' => 'passes', 'action' => 'delete', $pass['id']), array(), __('Are you sure you want to delete # %s?', $pass['id'])); ?>
			            </td>
		            </tr>
	            <?php endforeach; ?>
	            </table>

	        <div class="actions">
		        <ul>
			            <li><?php echo $this->Html->link(__('New Pass'), array('controller' => 'passes', 'action' => 'add')); ?> </li>
		        </ul>
	        </div>
        </div>
<?php endif; ?>

<?php if (!empty($user['Transaction'])): ?>
    <div class="related">
	        <h3><?php echo __('Previous Transactions'); ?></h3>

	        <table cellpadding = "0" cellspacing = "0">
	        <tr>
		        <th><?php echo __('Id'); ?></th>
		        <th><?php echo __('User Id'); ?></th>
		        <th><?php echo __('Date Time'); ?></th>
		        <th><?php echo __('Amount'); ?></th>
		        <th><?php echo __('Result'); ?></th>
		        <th><?php echo __('Pass Id'); ?></th>
		        <th><?php echo __('Message'); ?></th>
		        <th><?php echo __('Credits Used'); ?></th>
		        <th><?php echo __('Payment Method Used'); ?></th>
		        <th class="actions"><?php echo __('Actions'); ?></th>
	        </tr>
	        <?php foreach ($user['Transaction'] as $transaction): ?>
		    <tr>
			    <td><?php echo $transaction['id']; ?></td>
			    <td><?php echo $transaction['user_id']; ?></td>
			    <td><?php echo $transaction['date_time']; ?></td>
			    <td><?php echo $transaction['amount']; ?></td>
			    <td><?php echo $transaction['result']; ?></td>
			    <td><?php echo $transaction['pass_id']; ?></td>
			    <td><?php echo $transaction['message']; ?></td>
			    <td><?php echo $transaction['credits_used']; ?></td>
			    <td><?php echo $transaction['payment_method_used']; ?></td>
			    <td class="actions">
				    <?php echo $this->Html->link(__('View'), array('controller' => 'transactions', 'action' => 'view', $transaction['id'])); ?>
				    <?php echo $this->Html->link(__('Edit'), array('controller' => 'transactions', 'action' => 'edit', $transaction['id'])); ?>
				    <?php echo $this->Form->postLink(__('Delete'), array('controller' => 'transactions', 'action' => 'delete', $transaction['id']), array(), __('Are you sure you want to delete # %s?', $transaction['id'])); ?>
			    </td>
		    </tr>
	        <?php endforeach; ?>
	        </table>

	    <div class="actions">
		    <ul>
			    <li><?php //echo $this->Html->link(__('New Transaction'), array('controller' => 'transactions', 'action' => 'add')); ?> </li>
		    </ul>
	    </div>
</div>
<?php endif; ?>

<?php if (!empty($user['Vehicle'])): ?>
<div class="related">
	<h3><?php echo __('Registered Vehicles'); ?></h3>

	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Sr.No.'); ?></th>
		<th><?php echo __('Id'); ?></th>
		<th><?php //echo __('Property Id'); ?></th>
		<th><?php// echo __('User Id'); ?></th>
		<th><?php echo __('Vehicle Type'); ?></th>
		<th><?php echo __('Make'); ?></th>
		<th><?php echo __('Model'); ?></th>
		<th><?php echo __('Color'); ?></th>
		<th><?php echo __('License Plate Number'); ?></th>
		<th><?php echo __('License Plate State'); ?></th>
		<th><?php echo __('Reserved Space'); ?></th>
		<th><?php echo __('Last 4 Digital Of Vin'); ?></th>
		<th class="actions"><?php// echo __('Actions'); ?></th>
	</tr>
	<?php $srNo=0;foreach ($user['Vehicle'] as $vehicle): ?>
		<tr>
			<td><?php echo ++$srNo; ?></td>
			<td><?php echo $vehicle['id']; ?></td>
			<td><?php //echo $vehicle['property_id']; ?></td>
			<td><?php //echo $vehicle['user_id']; ?></td>
			<td><?php echo $vehicle['vehicle_type_name']; ?></td>
			<td><?php echo $vehicle['make']; ?></td>
			<td><?php echo $vehicle['model']; ?></td>
			<td><?php echo $vehicle['color']; ?></td>
			<td><?php echo $vehicle['license_plate_number']; ?></td>
			<td><?php echo $vehicle['license_plate_state']; ?></td>
			<td><?php echo $vehicle['reserved_space']; ?></td>
			<td><?php echo $vehicle['last_4_digital_of_vin']; ?></td>
			<td class="actions">
				<?php //echo $this->Html->link(__('View'), array('controller' => 'vehicles', 'action' => 'view', $vehicle['id'])); ?>
				<?php //echo $this->Html->link(__('Edit'), array('controller' => 'vehicles', 'action' => 'edit', $vehicle['id'])); ?>
				<?php //echo $this->Form->postLink(__('Delete'), array('controller' => 'vehicles', 'action' => 'delete', $vehicle['id']), array(), __('Are you sure you want to delete # %s?', $vehicle['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php //echo $this->Html->link(__('New Vehicle'), array('controller' => 'vehicles', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
-->


    </div>	        
</div>

<div class="page-content-wrapper">
		<div class="page-content" style="min-height:1089px">
			
			<!-- BEGIN PAGE HEADER-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
					<?php echo CakeSession::read('PropertyName');?> <small>Users</small>
					</h3>
					<ul class="page-breadcrumb breadcrumb">
						<li>
							<i class="fa fa-home"></i>
							<?php echo $this->Html->link(
                                              'Home',
                                              array(
                                                 'controller' => 'Users',
                                                 'action' => 'manager_home_page',
                                                 'full_base' => true
                                                                  )
                                              );?>
							<i class="fa fa-angle-right"></i>
						</li>

						<li>
							<a href="#">Users Listing</a>
						</li>
						
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<div class="tabbable tabbable-custom boxless tabbable-reversed">
						<ul class="nav nav-tabs">
							<li class="active">
								<a data-toggle="tab" href="#tab_0">
								Users Listing </a>
							</li>
							
						</ul>
						<div class="tab-content">
						  <!----------------------------------------TAB Start------------------------->
							<div id="tab_0" class="tab-pane active">
								<div class="portlet box blue">
									<div class="portlet-title">
										<div class="caption">
											<i class="fa fa-gift"></i><?php echo CakeSession::read('PropertyName');?> User Details
										</div>
										<div class="tools">
											<a class="collapse" href="javascript:;">
											</a>
										</div>
									</div>
									<div class="portlet-body">
										<div class="table-container">		
											<div class="table-responsive">
                                              <table id="customer_list" class="table table-striped table-bordered table-hover" width="100%">
                                            	 <thead>
                                            	   	<tr>
                                                       <th>Username</th>
                                                       <th>First Name</th>
                                                       <th>Last Name</th>
                                                       <th>Email</th>
                                                       <th>Phone</th>
                                                       <th>Address Line 1</th>
                                                       <th>Apartment/Suite #</th>
                                                       <th>View All</th> 
                                                    </tr>
                        						  </thead>
                                            	  <tbody>
                                                       <tr>
															<td colspan="8" class="dataTables_empty">Loading Data...</td>
														</tr>
                                            	   </tbody>
                                            	</table>
											</div>
										</div>
									</div>
								</div>
							</div>
			             <!----------------------------------------TAB END------------------------->
						</div>
					</div>
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
<div id="myModalForMessage" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h3 class="modal-title">Send Message</h3>
         <h4 id='returnMessage' style=''></h3>
      </div>
      <div class="modal-body">
        <form class='form-horizontal' id='reportErrorForm'>
			<div class="form-body">
				<?php echo $this->Form->hidden('from',array('value'=>AuthComponent::user('email'))); 
					  echo $this->Form->hidden('to');
					  echo $this->Form->hidden('last_name');
					  echo $this->Form->hidden('first_name');
				?>
				<div class="form-group" >   
					<label class="col-md-3 control-label">Subject<font>*</font></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('subject', array('required' => true,'type'=>'text','label' => false,'div' => false, 'class' => 'form-control')); ?>    
					</div> 
				</div>
				<div class="form-group" >   
					<label class="col-md-3 control-label">Message<font>*</font></label>
					<div class="col-md-9">
						<?php echo $this->Form->input('message', array('required' => true,'type'=>'textarea','label' => false,'div' => false, 'class' => 'form-control')); ?>    
					</div> 
				</div>
			</div>
			 <div class="form-actions">
				<div class="row">
					<div class="col-md-offset-3 col-md-9">
						<button type="submit" class="btn green">Send</button>
					</div>
				</div>
			</div>
		</form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
<?php echo $this->Html->script('//code.jquery.com/jquery-1.12.0.min.js'); ?>
<?php echo $this->Html->script('//cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js'); ?>
<?php echo $this->Html->script('//cdn.datatables.net/buttons/1.1.2/js/dataTables.buttons.min.js'); ?>
<?php echo $this->Html->script('//cdn.datatables.net/buttons/1.1.2/js/buttons.print.min.js'); ?>
<?php echo $this->Html->script('//cdn.datatables.net/buttons/1.1.2/js/buttons.flash.min.js'); ?>
<?php echo $this->Html->script('//cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js'); ?>
<?php echo $this->Html->script('//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js'); ?>
<?php echo $this->Html->script('//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js'); ?>
<?php echo $this->Html->script('//cdn.datatables.net/buttons/1.1.2/js/buttons.html5.min.js'); ?>
<?php echo $this->Html->script('//cdn.datatables.net/buttons/1.1.2/js/buttons.colVis.min.js'); ?>
<?php echo $this->Html->css('//cdn.datatables.net/1.10.11/css/jquery.dataTables.min.css'); ?>
 <?php echo $this->Html->css('//cdn.datatables.net/buttons/1.1.2/css/buttons.dataTables.min.css'); ?> 
 <?php echo $this->Html->script('tinymce/tinymce.min.js'); ?> 
<script type="text/javascript">
 $(function(){
	  $('#customer_list').DataTable( {
		bProcessing: false,
        bServerSide: true,
        dom: 'lBfrtip',
        sAjaxSource: "<?php echo $this->Html->Url(array('controller' => 'Users', 'action' => 'manager_customer_details')); ?>",
        buttons: [
            {
                extend: 'print',
                exportOptions: {
                    columns: [ 0, 1, 2,3,4,5,6 ]
                },
                customize: function ( win ) {
                    $(win.document.body)
                        .css( 'font-size', '10pt' )
                        .prepend(
                            '<img src="<?php echo "https://netparkingpass.com/img/logo/".CakeSession::read('PropertyLogo')?>" alt="" style="position:absolute; opacity:0.3; top:40%; left:30%;" />'
                        );
 
                    $(win.document.body).find( 'table' )
                        .addClass( 'compact' )
                        .css( 'font-size', 'inherit' );
                }
            },
            {
				extend: 'csv',
				 exportOptions: {
                    columns: [ 0, 1, 2,3,4,5,6 ]
                }
			},
			{
				extend: 'pdf',
				 exportOptions: {
                    columns: [ 0, 1, 2,3,4,5,6 ]
                }
			},
			{
				extend: 'excel',
				 exportOptions: {
                    columns: [ 0, 1, 2,3,4,5,6 ]
                }
			}
        ],
       oLanguage: {
			sInfoFiltered: "",
			sLengthMenu: "Show_MENU_entries.&nbsp;&nbsp;<b>Download Current Table As :</b>&nbsp;",
		},
		"columnDefs": [
            {
                // The `data` parameter refers to the data for the cell (defined by the
                // `data` option, which defaults to the column being worked with, in
                // this case `data: 0`.
                "render": function ( data, type, row ) {
                    return data +'&nbsp;<a href="javascript:;" class="btn red btn-xs red-stripe" onclick="contactUser(\''+row[1]+'\',\''+row[2]+'\',\''+row[3]+'\');">Send Message</a>';
                },
                "targets": 7
            }
        ]
    } );
});
function contactUser(firstName,lastName,email){
	tinyMCE.get('message').setContent('');

	$("#returnMessage").html('');
	$("#subject").val('');
	$("#message").val('');
	$("#to").val(email);
	$("#first_name").val(firstName);
	$("#last_name").val(lastName);
	$("#myModalForMessage").modal();
}
$("#reportErrorForm").submit(function(e)
	{
		e.preventDefault();
		var postData = $(this).serializeArray();
		$.ajax(
		{
			url : '/Chats/sendMessage',
			type: "GET",
			data : postData,
			success:function(data, textStatus, jqXHR) 
			{
				if(data=='true'){
					$("#returnMessage").html('Message Sent Successfully');
					$("#returnMessage").attr('style','color:green;');
				}else{
					$("#returnMessage").html('Message Cannot Be Sent, Please Try Later');
					$("#returnMessage").attr('style','color:red;');
				}
				console.log(data);
			},
			error: function(jqXHR, textStatus, errorThrown) 
			{
				//if fails      
			}
		});
		
	});
	 
	$( document ).ready(function() {
			$('#returnMessage').delay(1000).fadeOut(); 
			 tinymce.init({
				  selector: 'textarea',
				  height: 300,
				  theme: 'modern',
				  plugins: [
					'advlist autolink lists link image charmap print preview hr anchor pagebreak',  
					'searchreplace wordcount visualblocks visualchars code fullscreen',
					'insertdatetime media nonbreaking save table contextmenu directionality',
					'emoticons template paste textcolor colorpicker textpattern'
				  ],
				  toolbar1: ' undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link',
				  
				  image_advtab: true,
				  setup: function (editor) {
						editor.on('change', function () {
							editor.save();
						});
					}
				  /*templates: [
					{ title: 'Test template 1', content: 'Test 1' },
					{ title: 'Test template 2', content: 'Test 2' }
				  ],*/
				  /*content_css: [
					'//fast.fonts.net/cssapi/e6dc9b99-64fe-4292-ad98-6974f93cd2a2.css',
					'//www.tinymce.com/css/codepen.min.css'
				  ]*/
			 });
});
</script>


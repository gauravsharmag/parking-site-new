<div class="users view">
<h2><?php echo __('User'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($user['User']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Username'); ?></dt>
		<dd>
			<?php echo h($user['User']['username']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Email'); ?></dt>
		<dd>
			<?php echo h($user['User']['email']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Password'); ?></dt>
		<dd>
			<?php echo h($user['User']['password']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Role'); ?></dt>
		<dd>
			<?php echo $this->Html->link($user['Role']['name'], array('controller' => 'roles', 'action' => 'view', $user['Role']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('First Name'); ?></dt>
		<dd>
			<?php echo h($user['User']['first_name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Last Name'); ?></dt>
		<dd>
			<?php echo h($user['User']['last_name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Address Line 1'); ?></dt>
		<dd>
			<?php echo h($user['User']['address_line_1']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Address Line 2'); ?></dt>
		<dd>
			<?php echo h($user['User']['address_line_2']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('City'); ?></dt>
		<dd>
			<?php echo h($user['User']['city']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('State'); ?></dt>
		<dd>
			<?php echo h($user['User']['state']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Zip'); ?></dt>
		<dd>
			<?php echo h($user['User']['zip']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Phone'); ?></dt>
		<dd>
			<?php echo h($user['User']['phone']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit User'), array('action' => 'edit', $user['User']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete User'), array('action' => 'delete', $user['User']['id']), array(), __('Are you sure you want to delete # %s?', $user['User']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Roles'), array('controller' => 'roles', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Role'), array('controller' => 'roles', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Customer Passes'), array('controller' => 'customer_passes', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Customer Pass'), array('controller' => 'customer_passes', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Transactions'), array('controller' => 'transactions', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Transaction'), array('controller' => 'transactions', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Vehicles'), array('controller' => 'vehicles', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Vehicle'), array('controller' => 'vehicles', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Property Users'), array('controller' => 'property_users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Property User'), array('controller' => 'property_users', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Customer Passes'); ?></h3>
	<?php if (!empty($user['CustomerPass'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('User Id'); ?></th>
		<th><?php echo __('Vehicle Id'); ?></th>
		<th><?php echo __('Property Id'); ?></th>
		<th><?php echo __('Transaction Id'); ?></th>
		<th><?php echo __('Status'); ?></th>
		<th><?php echo __('Membership Vaild Upto'); ?></th>
		<th><?php echo __('Pass Valid Upto'); ?></th>
		<th><?php echo __('RFID Tag Number'); ?></th>
		<th><?php echo __('Assigned Location'); ?></th>
		<th><?php echo __('Package Id'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($user['CustomerPass'] as $customerPass): ?>
		<tr>
			<td><?php echo $customerPass['id']; ?></td>
			<td><?php echo $customerPass['user_id']; ?></td>
			<td><?php echo $customerPass['vehicle_id']; ?></td>
			<td><?php echo $customerPass['property_id']; ?></td>
			<td><?php echo $customerPass['transaction_id']; ?></td>
			<td><?php echo $customerPass['status']; ?></td>
			<td><?php echo $customerPass['membership_vaild_upto']; ?></td>
			<td><?php echo $customerPass['pass_valid_upto']; ?></td>
			<td><?php echo $customerPass['RFID_tag_number']; ?></td>
			<td><?php echo $customerPass['assigned_location']; ?></td>
			<td><?php echo $customerPass['package_id']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'customer_passes', 'action' => 'view', $customerPass['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'customer_passes', 'action' => 'edit', $customerPass['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'customer_passes', 'action' => 'delete', $customerPass['id']), array(), __('Are you sure you want to delete # %s?', $customerPass['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Customer Pass'), array('controller' => 'customer_passes', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Transactions'); ?></h3>
	<?php if (!empty($user['Transaction'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('User Id'); ?></th>
		<th><?php echo __('Date Time'); ?></th>
		<th><?php echo __('Amount'); ?></th>
		<th><?php echo __('Result'); ?></th>
		<th><?php echo __('Pass Id'); ?></th>
		<th><?php echo __('Message'); ?></th>
		<th><?php echo __('Credits Used'); ?></th>
		<th><?php echo __('Payment Method Used'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($user['Transaction'] as $transaction): ?>
		<tr>
			<td><?php echo $transaction['id']; ?></td>
			<td><?php echo $transaction['user_id']; ?></td>
			<td><?php echo $transaction['date_time']; ?></td>
			<td><?php echo $transaction['amount']; ?></td>
			<td><?php echo $transaction['result']; ?></td>
			<td><?php echo $transaction['pass_id']; ?></td>
			<td><?php echo $transaction['message']; ?></td>
			<td><?php echo $transaction['credits_used']; ?></td>
			<td><?php echo $transaction['payment_method_used']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'transactions', 'action' => 'view', $transaction['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'transactions', 'action' => 'edit', $transaction['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'transactions', 'action' => 'delete', $transaction['id']), array(), __('Are you sure you want to delete # %s?', $transaction['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Transaction'), array('controller' => 'transactions', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Vehicles'); ?></h3>
	<?php if (!empty($user['Vehicle'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Property Id'); ?></th>
		<th><?php echo __('Package Id'); ?></th>
		<th><?php echo __('User Id'); ?></th>
		<th><?php echo __('Make'); ?></th>
		<th><?php echo __('Model'); ?></th>
		<th><?php echo __('Color'); ?></th>
		<th><?php echo __('License Plate Number'); ?></th>
		<th><?php echo __('License Plate State'); ?></th>
		<th><?php echo __('Reserved Space'); ?></th>
		<th><?php echo __('Last 4 Digital Of Vin'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($user['Vehicle'] as $vehicle): ?>
		<tr>
			<td><?php echo $vehicle['id']; ?></td>
			<td><?php echo $vehicle['property_id']; ?></td>
			<td><?php echo $vehicle['package_id']; ?></td>
			<td><?php echo $vehicle['user_id']; ?></td>
			<td><?php echo $vehicle['make']; ?></td>
			<td><?php echo $vehicle['model']; ?></td>
			<td><?php echo $vehicle['color']; ?></td>
			<td><?php echo $vehicle['license_plate_number']; ?></td>
			<td><?php echo $vehicle['license_plate_state']; ?></td>
			<td><?php echo $vehicle['reserved_space']; ?></td>
			<td><?php echo $vehicle['last_4_digital_of_vin']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'vehicles', 'action' => 'view', $vehicle['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'vehicles', 'action' => 'edit', $vehicle['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'vehicles', 'action' => 'delete', $vehicle['id']), array(), __('Are you sure you want to delete # %s?', $vehicle['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Vehicle'), array('controller' => 'vehicles', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
<div class="related">
	<h3><?php echo __('Related Property Users'); ?></h3>
	<?php if (!empty($user['PropertyUser'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('User Id'); ?></th>
		<th><?php echo __('Property Id'); ?></th>
		<th><?php echo __('Role Id'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($user['PropertyUser'] as $propertyUser): ?>
		<tr>
			<td><?php echo $propertyUser['id']; ?></td>
			<td><?php echo $propertyUser['user_id']; ?></td>
			<td><?php echo $propertyUser['property_id']; ?></td>
			<td><?php echo $propertyUser['role_id']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'property_users', 'action' => 'view', $propertyUser['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'property_users', 'action' => 'edit', $propertyUser['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'property_users', 'action' => 'delete', $propertyUser['id']), array(), __('Are you sure you want to delete # %s?', $propertyUser['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Property User'), array('controller' => 'property_users', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>

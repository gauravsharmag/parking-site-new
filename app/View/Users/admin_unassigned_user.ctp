
<div class="page-content-wrapper">
		<div class="page-content">
		   <div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
					Users<small>List of users</small>
					</h3>
					<ul class="page-breadcrumb breadcrumb">
					
						<li>
							<i class="fa fa-home"></i>
							<?php echo $this->Html->link('Users',array('controller'=>'users','action'=>'index')); ?>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<?php echo $this->Html->link('List Users',array('controller'=>'users','action'=>'index')); ?>
							
						</li>
						
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
			<div id="flashMessages">
                	<h2><?php  echo $this->Session->flash();?></h2>
            </div>
            <div class="row">
				<div class="col-md-12">
					<!-- BEGIN EXAMPLE TABLE PORTLET-->
					<div class="portlet box grey-cascade">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-users"></i>Un-Assigned Users List
							</div>
							<div class="tools">
								<a href="javascript:;" class="collapse">
								</a>
								
							</div>
						</div>
						<div class="portlet-body">
							<div class="table-toolbar">
								<div class="alert alert-danger">
                                        Please Refer To <strong>Users->Assigned Users</strong> option if user is already attached to a property. Thanks </div>
								<div class="btn-group">
                                <?php echo $this->Html->link('Add New <i class="fa fa-plus"></i>',array('controller'=>'users','action'=>'add'),array('escape'=>false,'class'=>'btn green')); ?>
									
								</div>
								
							</div>
							<div class="table-scrollable">
							<table id="all_customer_list"class="table table-striped table-bordered table-hover">
							<thead>
							<tr>
								<th>
									First Name
								</th>
								<th>
									Last Name
								</th>
								<th>
									Username
								</th>
								<th>
									 Email
								</th>
								<th>
									 Phone
								</th>
								<th>
									 Address Line 1
								</th>
								<th>
									 Apartment # 
								</th>
								<th>
									 Role
								</th>
								
                                <th>
                                	Actions
                                </th>
                                <!--<th>
									Action
								</th>-->
							</tr>
							</thead>
							<tbody>
								<?php for($i=0;$i<count($result);$i++){ ?>
								<tr>
									<td>
										<?php echo $result[$i]['User']['first_name']; ?>
									</td>
									<td>
										<?php echo $result[$i]['User']['last_name']; ?>
									</td>
									<td>
										<?php echo $result[$i]['User']['username']; ?>
									</td>
									<td>
										<?php echo $result[$i]['User']['email']; ?>
									</td>
									<td>
										<?php echo $result[$i]['User']['phone']; ?>
									</td>
									<td>
										<?php echo $result[$i]['User']['address_line_1']; ?>
									</td>
									<td>  
										<?php echo $result[$i]['User']['address_line_2']; ?>
									</td>
									<td>
										<?php echo $result[$i]['Role']['role_name']; ?>
									</td>
									<td>
										<?php echo $result[$i]['User']['id']; ?>
									</td>
									
								</tr>
								<?php } ?>
							</tbody>
							</table>
							</div>
						</div>
					</div>
					<!-- END EXAMPLE TABLE PORTLET-->
				</div>
			</div>
</div>
</div>
<?php echo $this->Html->script('//cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js'); ?>
<?php echo $this->Html->css('//cdn.datatables.net/1.10.11/css/jquery.dataTables.min.css'); ?>
<script type="text/javascript">
 $(function(){
	$('#all_customer_list').dataTable();
	
});
</script>

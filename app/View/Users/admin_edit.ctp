<script>
$(document).ready(function(){
	if ($(UserPasswordChanged).is(':checked'))
        {
            $("#passwordPanel").show("slow");
            $("#UserPassword").attr("required",true);
			$("#UserPasswordConfirmation").attr("required",true);

        }else{
			$("#passwordPanel").hide("slow");
			$("#UserPassword").val('');
			$("#UserPasswordConfirmation").val('');
			$("#UserPassword").attr("required",false );
			$("#UserPasswordConfirmation").attr("required",false );
	}
	$("#UserPasswordChanged").change(function()
    {
        if ($(this).is(':checked'))
        {
            $("#passwordPanel").show("slow");
            $("#UserPassword").attr("required",true);
			$("#UserPasswordConfirmation").attr("required",true);

        }
        else
        {
             $("#passwordPanel").hide("slow");
             $("#UserPassword").attr("required",false);
			 $("#UserPasswordConfirmation").attr("required",false);
        }
    });
});
</script>
<div class="page-content-wrapper">
    <div class="page-content">
		   <div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
					Users<small> Edit User</small>
					</h3>
					<ul class="page-breadcrumb breadcrumb">
					
						<li>
							<i class="fa fa-home"></i>
							<?php echo $this->Html->link('Home',array('controller'=>'users','action'=>'superAdminHome')); ?>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<?php echo $this->Html->link('List Users',array('controller'=>'users','action'=>'index')); ?>
							
						</li>
						
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
		    <div id="flashMessages">
                	<h2><?php  echo $this->Session->flash();?></h2>
            </div>    
     <!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<div class="tabbable tabbable-custom boxless tabbable-reversed">
						
						
							<div class="tab-pane active" id="tab_0">
								<div class="portlet box green">
									<div class="portlet-title">
										<div class="caption">
											<i class="fa fa-gift"></i><?php echo __('Edit User'); ?>
										</div>
										
									</div>
                                   
										<!-- BEGIN FORM-->
										
									<div class="portlet-body form">

									<?php echo $this->Form->create('User',array('class'=>'form-horizontal'));// debug($roles);?>
                                    <div class="form-body">
												
                                                
                                            <?php
											    echo $this->Form->input('id');
											     echo $this->Form->hidden('role_id');

                                       			echo $this->Form->input('first_name',array('error'=>array('attributes'=>array('class'=>'model-error')),'required'=>true,'div'=>array('class'=>'form-group'),'label'=>array('class'=>'col-md-3 control-label','text'=>'First Name'),'between'=>'<div class="col-md-4">','after'=>'</div>','class'=>'form-control','placeholder'=>'First Name'));
												echo $this->Form->input('last_name',array('error'=>array('attributes'=>array('class'=>'model-error')),'required'=>true,'div'=>array('class'=>'form-group'),'label'=>array('class'=>'col-md-3 control-label','text'=>'Last Name'),'between'=>'<div class="col-md-4">','after'=>'</div>','class'=>'form-control','placeholder'=>'Last Name'));
                                       			echo $this->Form->input('username',array('error'=>array('attributes'=>array('class'=>'model-error')),'required'=>true,'div'=>array('class'=>'form-group'),'label'=>array('class'=>'col-md-3 control-label','text'=>'Username'),'between'=>'<div class="col-md-4">','after'=>'</div>','class'=>'form-control','placeholder'=>'Username'));
												
												echo $this->Form->input('email',array('error'=>array('attributes'=>array('class'=>'model-error')),'required'=>true,'div'=>array('class'=>'form-group'),'label'=>array('class'=>'col-md-3 control-label','text'=>'Email Address'),'between'=>'<div class="col-md-4"><div class="input-group"><span class="input-group-addon"><i class="fa fa-envelope"></i>														</span>','after'=>'</div></div>','class'=>'form-control','placeholder'=>'Email Address','type'=>'email'));
												 echo $this->Form->input('password_changed',array('type'=>'checkbox','div'=>array('class'=>'form-group'),
                                                                                'label'=>false,
                                                                                 'before'=>'<label class="col-md-3 control-label">Change Password ?</label><div class="col-md-4">',
                                                                                'after'=>'</div>'
                                                                                 ));
											echo "<div id='passwordPanel' style='display: none'>"	;
												echo $this->Form->input('password',array('error'=>array('attributes'=>array('class'=>'model-error')),'required'=>true,'div'=>array('class'=>'form-group'),'label'=>array('class'=>'col-md-3 control-label','text'=>'Password'),'between'=>'<div class="col-md-4"><div class="input-group">','after'=>'<span class="input-group-addon"><i class="fa fa-user"></i>													</span></div></div>','class'=>'form-control','placeholder'=>'Password'));
												
												echo $this->Form->input('password_confirmation',array('value'=>$this->request->data['User']['password'],'error'=>array('attributes'=>array('class'=>'model-error')),'required'=>true,'div'=>array('class'=>'form-group'),'label'=>array('class'=>'col-md-3 control-label','text'=>'Password Confirmation'),'between'=>'<div class="col-md-4"><div class="input-group">','after'=>'<span class="input-group-addon"><i class="fa fa-user"></i>													</span></div></div>','class'=>'form-control','placeholder'=>'Password Confirmation','type'=>'password'));
											echo "</div>";
												//echo $this->Form->input('role_id',array('required'=>true,'empty'=>'Select One','div'=>array('class'=>'form-group'),'label'=>array('class'=>'col-md-3 control-label','text'=>'Role'),'between'=>'<div class="col-md-4"><div class="input-group">','after'=>'</div></div>','class'=>'form-control'));
										    ?>
                                           <div class="form-actions fluid">
												<div class="col-md-offset-3 col-md-9">
													<button type="submit" class="btn blue">Submit</button>
													<!--<button type="button" class="btn default">Cancel</button>-->
												</div>
											</div>
                                         <?php echo $this->Form->end(); ?>   
                                     </div>   
                                   
										<!-- END FORM-->
									</div>
								</div>							
								
							</div>
							
							
						
					</div>
				</div>
			</div>
			<!-- END PAGE CONTENT-->   
        
        
    </div>
</div>








<!--<div class="users form">
<?php echo $this->Form->create('User'); //debug($this->request->data); ?>
	<fieldset>
		<legend><?php echo __('Edit User'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('username',array('disabled'=>'disabled'));
		echo $this->Form->input('email',array('disabled'=>'disabled'));
		echo $this->Form->input('password',array('type'=>'hidden','disabled'=>'disabled'));
		echo $this->Form->input('role_id');
		echo $this->Form->input('credits');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div>
    <?php echo $this->element('homePage');?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('User.id')), array(), __('Are you sure you want to delete # %s?', $this->Form->value('User.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Users'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Roles'), array('controller' => 'roles', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Role'), array('controller' => 'roles', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Addresses'), array('controller' => 'addresses', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Address'), array('controller' => 'addresses', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Passes'), array('controller' => 'passes', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Pass'), array('controller' => 'passes', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Transactions'), array('controller' => 'transactions', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Transaction'), array('controller' => 'transactions', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Vehicles'), array('controller' => 'vehicles', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Vehicle'), array('controller' => 'vehicles', 'action' => 'add')); ?> </li>
	</ul>
</div>-->

<div class="page-sidebar-wrapper">
		<!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
		<!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
		<div class="page-sidebar navbar-collapse collapse">
			<!-- BEGIN SIDEBAR MENU -->
			<ul class="page-sidebar-menu" data-auto-scroll="true" data-slide-speed="200">
				<!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
				<li class="sidebar-toggler-wrapper">
					<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
					<div class="sidebar-toggler">
					</div>
					<!-- END SIDEBAR TOGGLER BUTTON -->
				</li>
				 
				<!-- DOC: To remove the search box from the sidebar you just need to completely remove the below "sidebar-search-wrapper" LI element -->
				<li class="sidebar-search-wrapper">
					<?php echo $this->Form->create('Vehicle',array('url'=>array('controller'=>'vehicles','action'=>'search'),'class'=>'sidebar-search'))?>
					<!--<form class="sidebar-search" action="/admin/Vehicles/search" method="POST">-->
						<a href="javascript:;" class="remove">
						<i class="icon-close"></i>
						</a>
						<div class="input-group">
						<?php echo $this->Form->input('search_key',array('placeholder'=>'Search...','div'=>false,'class'=>'form-control','label'=>false));?>
							
						</div>
					<?php echo $this->Form->end();?>
				</li>
				<li class="sidebar-search-wrapper">
					<?php echo $this->Form->create('Transaction',array('url'=>array('controller'=>'transactions','action'=>'search'),'class'=>'sidebar-search2'))?>
					<!--<form class="sidebar-search" action="/admin/Vehicles/search" method="POST">-->
						<a href="javascript:;" class="remove">
						<i class="icon-close"></i> 
						</a>
						<div class="input-group">
						<?php echo $this->Form->input('search_key',array('placeholder'=>'Search Transaction...','div'=>false,'class'=>'form-control','label'=>false));?>
							
						</div>
					<?php echo $this->Form->end();?>
					<!-- END RESPONSIVE QUICK SEARCH FORM -->
				</li>
				<li class="start active ">
                <?php 
				$action='';
				if(AuthComponent::user('role_id')==1)
				{
					$action='superAdminHome';
				}
				else
				{
					$action='customerHomePage';
				}
				echo $this->Html->link('<i class="fa fa-home"></i><span class="title">Dashboard</span><span class="selected"></span>',array('controller'=>'users','action'=>$action),array('escape'=>false)); ?>
					
				</li>
				<li>
                
					<a href="javascript:;">
					<i class="fa fa-users"></i>
					<span class="title">Users</span>
					<span class="arrow "></span>
					</a>
					<ul class="sub-menu">
						<li>     
                        
                        <?php 
							echo $this->Html->link('<i class="fa fa-users"></i>Assigned Users',array('controller'=>'users','action'=>'index'),array('escape'=>false)); ?>
							
						</li>
						<li>
                        
                        <?php 
							echo $this->Html->link('<i class="fa fa-users"></i>Un-Assigned Users',array('controller'=>'Users','action'=>'unassigned_user'),array('escape'=>false)); ?>
							
						</li>
						<li>
                        
                        <?php 
							echo $this->Html->link('<i class="fa fa-users"></i>Archived Users',array('controller'=>'Users','action'=>'archive_list'),array('escape'=>false)); ?>
							
						</li>
						<li>
                        
                        <?php 
							echo $this->Html->link('<i class="fa fa-user "></i>Add User',array('controller'=>'users','action'=>'add'),array('escape'=>false)); ?>
							
						</li>
					</ul>
				</li>
				
				<li>
					<a href="javascript:;">
					<i class="fa fa-cab"></i>
					<span class="title">Vehicle</span>
					<span class="arrow "></span>
					</a> 
					<ul class="sub-menu">
                  		<li>
							 <?php 
							echo $this->Html->link('<i class="fa fa-taxi"></i>List Vehicle',array('controller'=>'vehicles','action'=>'index'),array('escape'=>false)); ?>
						</li>								
					</ul>
				</li>
				<!-- BEGIN FRONTEND THEME LINKS -->
				<li>
					<a href="javascript:;">
					<i class="fa  fa-building "></i>
					<span class="title">
					Properties </span>
					<span class="arrow">
					</span>
					</a>
					<ul class="sub-menu">
						<li>
							 <?php 
							echo $this->Html->link('<i class="fa fa-building-o"></i>List Properties',array('controller'=>'properties','action'=>'index'),array('escape'=>false)); ?>
						</li>
						<li>
							<?php 
							echo $this->Html->link('<i class="fa fa-plus-square-o"></i>Add Properties',array('controller'=>'properties','action'=>'add'),array('escape'=>false)); ?>
						</li>
					
					</ul>
				</li>
				<!-- END FRONTEND THEME LINKS -->
				<li>
					<a href="javascript:;">
					<i class="fa fa-sitemap"></i>
					<span class="title">Manage</span>
					<span class="arrow "></span>
					</a>
					<ul class="sub-menu"> 
						<li>
							 <?php 
							echo $this->Html->link('<i class="fa fa-sitemap"></i>Unassigned Passes',array('controller'=>'CustomerPasses','action'=>'addRfid'),array('escape'=>false)); ?>
						</li>
						<li>
							 <?php 
							echo $this->Html->link('<i class="fa fa-sitemap"></i>View/Edit RFID Tags',array('controller'=>'CustomerPasses','action'=>'viewRfid'),array('escape'=>false)); ?>
						</li>
						<li>
							 <?php 
							echo $this->Html->link('<i class="fa fa-sitemap"></i>Coupons',array('controller'=>'TableCoupons','action'=>'add'),array('escape'=>false)); ?>
						</li>
						<li>
							 <?php 
							echo $this->Html->link('<i class="fa fa-sitemap"></i>Property Permit Packages',array('controller'=>'CouponPackages','action'=>'index'),array('escape'=>false)); ?>
						</li>
					</ul>
				</li>
				
				<li>
					<a href="javascript:;">
					<i class="fa fa-sitemap"></i>
					<span class="title">Reports</span>
					<span class="arrow "></span>
					</a>
					<ul class="sub-menu"> 
						<li>  
							 <?php 
							echo $this->Html->link('<i class="fa fa-sitemap"></i> Property Wide Vehicles',array('controller'=>'Vehicles','action'=>'list_vehicles'),array('escape'=>false)); ?>
						</li>
						<li>
							 <?php 
							echo $this->Html->link('<i class="fa fa-users"></i> Property Wide Users',array('controller'=>'Users','action'=>'property_wise_users'),array('escape'=>false)); ?>
						</li>
						<li>
							 <?php 
								echo $this->Html->link('<i class="fa fa-building"></i> Property Wide Passes',array('controller'=>'CustomerPasses','action'=>'property_wise_passes'),array('escape'=>false)); 
							?> 
						</li>
						<li>
							 <?php 
								echo $this->Html->link('<i class="fa fa-building"></i>Pass Details',array('controller'=>'CustomerPasses','action'=>'property_wise_pass_details'),array('escape'=>false)); 
							?>
						</li>
						<li>
							 <?php 
								echo $this->Html->link('<i class="fa fa-sitemap"></i>Pass Cost Details',array('controller'=>'Transactions','action'=>'pass_costs'),array('escape'=>false)); 
							?>
						</li>
						<li>
							 <?php 
								echo $this->Html->link('<i class="fa fa-sitemap"></i>Guest Pass Details',array('controller'=>'Transactions','action'=>'guest_pass_costs'),array('escape'=>false)); 
							?>
						</li>
						<li>
							 <?php 
								echo $this->Html->link('<i class="fa fa-building"></i> Property Wide Expiring Passes',array('controller'=>'CustomerPasses','action'=>'pass_expiry'),array('escape'=>false)); 
							?>
						</li>
						<li>
							<?php
							echo $this->Html->link('<i class="fa fa-building"></i> ' . (__('Last Seen Reports')), array('controller' => 'CustomerPasses', 'action' => 'last_seen'), array('escape' => false));
							?>
						</li>
						<li>
							 <?php 
								echo $this->Html->link('<i class="fa fa-sitemap"></i>Pass Cost Details (RENEWAL)',array('controller'=>'Transactions','action'=>'renewal_passes'),array('escape'=>false)); 
							?>
						</li> 
						<li>
							 <?php 
								echo $this->Html->link('<i class="fa fa-sitemap"></i>No Vehicle',array('controller'=>'CustomerPasses','action'=>'no_vehicle'),array('escape'=>false)); 
							?>
						</li> 
						<li>
							 <?php 
								echo $this->Html->link('<i class="fa fa-sitemap"></i>Guest Activation Report',array('controller'=>'UserGuestPasses'),array('escape'=>false)); 
							?>
						</li> 
						<li>
							 <?php 
								echo $this->Html->link('<i class="fa fa-sitemap"></i>Renewed Pass Report',array('controller'=>'CustomerPasses','action'=>'renewed_pass'),array('escape'=>false)); 
							?>
						</li> 
						<li>
							 <?php 
								echo $this->Html->link('<i class="fa fa-sitemap"></i>Deleted Passes',array('controller'=>'Refunds','action'=>'index'),array('escape'=>false)); 
							?>
						</li> 
					</ul>
				</li>
				<li>
					<a href="javascript:;">
					<i class="fa  fa-building "></i>
					<span class="title">
					Messages </span>
					<span class="arrow">
					</span>
					</a>
					<ul class="sub-menu">
						<li>
							 <?php 
							echo $this->Html->link('<i class="icon-users"></i>View All Messages',array('controller'=>'tickets','action'=>'view'),array('escape'=>false)); ?>
						</li>
						<!--<li>
							 <?php 
							//echo $this->Html->link('<i class="icon-users"></i>New Message',array('controller'=>'tickets','action'=>'super_admin_contact'),array('escape'=>false)); ?>
						</li>-->
                                                
					
					</ul>
				</li>
				<li>
					<a href="/admin/vehicles/optional_search">
					<i class="fa fa-search"></i>
					<span class="title">
					Search With Options </span>
					</a>
				</li>
				
			</ul>
			<!-- END SIDEBAR MENU -->
		</div>
	</div>

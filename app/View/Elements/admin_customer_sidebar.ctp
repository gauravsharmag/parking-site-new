<div class="page-sidebar-wrapper">
		<!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
		<!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
		<div class="page-sidebar navbar-collapse collapse">
			<!-- BEGIN SIDEBAR MENU -->
			<ul class="page-sidebar-menu" data-auto-scroll="true" data-slide-speed="200">
				<!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
				<li class="sidebar-toggler-wrapper">
					<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
					<div class="sidebar-toggler">
					</div>
					<!-- END SIDEBAR TOGGLER BUTTON -->
				</li>
				<!-- DOC: To remove the search box from the sidebar you just need to completely remove the below "sidebar-search-wrapper" LI element -->
				<li class="sidebar-search-wrapper">
					<?php echo $this->Form->create('Vehicle',array('url'=>array('controller'=>'vehicles','action'=>'search'),'class'=>'sidebar-search'))?>
					<!--<form class="sidebar-search" action="/admin/Vehicles/search" method="POST">-->
						<a href="javascript:;" class="remove">
						<i class="icon-close"></i>
						</a>
						<div class="input-group">
						<?php echo $this->Form->input('search_key',array('placeholder'=>'Search...','div'=>false,'class'=>'form-control','label'=>false));?>
							<!--<input type="text" class="form-control" placeholder="Search...">-->
							<span class="input-group-btn">
							<a href="javascript:;" class="btn submit"><i class="icon-magnifier"></i></a>
							</span>
						</div>
					<?php echo $this->Form->end();?>
					<!-- END RESPONSIVE QUICK SEARCH FORM -->
				</li>
				<li class="start active ">
                <?php 
				$action='';
				if(AuthComponent::user('role_id')==1)
				{
					$action='superAdminHome';
				}
				else
				{
					$action='customerHomePage';
				}
				echo $this->Html->link('<i class="icon-home"></i><span class="title">Dashboard</span><span class="selected"></span>',array('controller'=>'users','action'=>$action),array('escape'=>false)); ?>
					
				</li>
				<li>
                
					<a href="javascript:;">
					<i class="icon-user"></i>
					<span class="title">Users</span>
					<span class="arrow "></span>
					</a>
					<ul class="sub-menu">
						<li>
                        
                        <?php 
							echo $this->Html->link('<i class="icon-users"></i>List Users',array('controller'=>'users','action'=>'index'),array('escape'=>false)); ?>
							
						</li>
						<li>
                        
                        <?php 
							echo $this->Html->link('<i class="icon-user-follow "></i>Add User',array('controller'=>'users','action'=>'add'),array('escape'=>false)); ?>
							
						</li>
					</ul>
				</li>
				<li>
					<a href="javascript:;">
					<i class="fa fa-cab"></i>
					<span class="title">Vehicle</span>
					<span class="arrow "></span>
					</a> 
					<ul class="sub-menu">
                  		<li>
							 <?php 
							echo $this->Html->link('<i class="fa fa-taxi"></i>List Vehicle',array('controller'=>'vehicles','action'=>'index'),array('escape'=>false)); ?>
						</li>								
					</ul>
				</li>
				<!-- BEGIN FRONTEND THEME LINKS -->
				<li>
					<a href="javascript:;">
					<i class="fa  fa-building "></i>
					<span class="title">
					Properties </span>
					<span class="arrow">
					</span>
					</a>
					<ul class="sub-menu">
						<li>
							 <?php 
							echo $this->Html->link('<i class="fa fa-building-o"></i>List Properties',array('controller'=>'properties','action'=>'index'),array('escape'=>false)); ?>
						</li>
						<li>
							<?php 
							echo $this->Html->link('<i class="fa fa-plus-square-o"></i>Add Properties',array('controller'=>'properties','action'=>'add'),array('escape'=>false)); ?>
						</li>
					
					</ul>
				</li>
				<!-- END FRONTEND THEME LINKS -->
				<li>
					<a href="javascript:;">
					<i class="fa fa-sitemap"></i>
					<span class="title">Manage</span>
					<span class="arrow "></span>
					</a>
					<ul class="sub-menu"> 
						<li>
							 <?php 
							echo $this->Html->link('<i class="fa fa-sitemap"></i>Unassigned Passes',array('controller'=>'CustomerPasses','action'=>'addRfid'),array('escape'=>false)); ?>
						</li>
						<li>
							 <?php 
							echo $this->Html->link('<i class="fa fa-sitemap"></i>View/Edit RFID Tags',array('controller'=>'CustomerPasses','action'=>'viewRfid'),array('escape'=>false)); ?>
						</li>
						
					</ul>
				</li>
				<!--<li>
					<a href="javascript:;">
					<i class="fa fa-credit-card"></i>
					<span class="title">Passes Creation Details</span>
					<span class="arrow "></span>
					</a> 
					<ul class="sub-menu">
                  		<li>
							 <?php 
							echo $this->Html->link('<i class="fa fa-credit-card"></i>View Created Trends',array('controller'=>'CustomerPasses','action'=>'admin_view_passes_created'),array('escape'=>false)); ?>
						</li>
														
					</ul>
				</li>-->
				<!--<li>
					<a href="javascript:;">
					<i class="fa fa-credit-card"></i>
					<span class="title">Passes Expiration Details</span>
					<span class="arrow "></span>
					</a> 
					<ul class="sub-menu">
                  		<li>
							 <?php 
							echo $this->Html->link('<i class="fa fa-credit-card"></i>Expiring Today',array('controller'=>'CustomerPasses','action'=>'admin_view_expirations_today'),array('escape'=>false)); ?>
						</li>
						<li>
							 <?php 
							echo $this->Html->link('<i class="fa fa-credit-card"></i>Expiring In Next 30 Days',array('controller'=>'CustomerPasses','action'=>'admin_view_expirations_next'),array('escape'=>false)); ?>
						</li>
						<li>
							 <?php 
							echo $this->Html->link('<i class="fa fa-credit-card"></i>Expired In Last 30 Days',array('controller'=>'CustomerPasses','action'=>'admin_view_expirations_last'),array('escape'=>false)); ?>
						</li>
						
														
					</ul>
				</li>-->
				<li>
					<a href="javascript:;">
					<i class="fa fa-sitemap"></i>
					<span class="title">Reports</span>
					<span class="arrow "></span>
					</a>
					<ul class="sub-menu"> 
						<li>
							 <?php 
							echo $this->Html->link('<i class="fa fa-sitemap"></i> Property Wide Vehicles',array('controller'=>'Vehicles','action'=>'list_vehicles'),array('escape'=>false)); ?>
						</li>
						<li>
							 <?php 
							echo $this->Html->link('<i class="fa fa-users"></i> Property Wide Users',array('controller'=>'Users','action'=>'property_wise_users'),array('escape'=>false)); ?>
						</li>
						<li>
							 <?php 
								echo $this->Html->link('<i class="fa fa-building"></i> Property Wide Passes',array('controller'=>'CustomerPasses','action'=>'property_wise_passes'),array('escape'=>false)); 
							?>
						</li>
						<li>
							 <?php 
								echo $this->Html->link('<i class="fa fa-building"></i>Pass Details',array('controller'=>'CustomerPasses','action'=>'property_wise_pass_details'),array('escape'=>false)); 
							?>
						</li>
						<li>
							 <?php 
								echo $this->Html->link('<i class="fa fa-sitemap"></i>Pass Cost Details',array('controller'=>'Transactions','action'=>'pass_costs'),array('escape'=>false)); 
							?>
						</li>
						<li>
							 <?php 
								echo $this->Html->link('<i class="fa fa-sitemap"></i>Guest Pass Details',array('controller'=>'Transactions','action'=>'guest_pass_costs'),array('escape'=>false)); 
							?>
						</li>
					</ul>
				</li>
				<li>
					<a href="javascript:;">
					<i class="fa  fa-building "></i>
					<span class="title">
					Messages </span>
					<span class="arrow">
					</span>
					</a>
					<ul class="sub-menu">
						<li>
							 <?php 
							echo $this->Html->link('<i class="icon-users"></i>View All Messages',array('controller'=>'tickets','action'=>'view'),array('escape'=>false)); ?>
						</li>
						<li>
							 <?php 
							echo $this->Html->link('<i class="icon-users"></i>New Message',array('controller'=>'tickets','action'=>'super_admin_contact'),array('escape'=>false)); ?>
						</li>
                                                
					
					</ul>
				</li>
				<li>
					<a href="javascript:;">
					<i class="fa  fa-sitemap "></i>
					<span class="title">
					Export </span>
					<span class="arrow">
					</span>
					</a>
					<ul class="sub-menu">
						<li>
							 <?php 
							echo $this->Html->link('<i class="fa fa-sitemap"></i>Export Property',array('controller'=>'Properties','action'=>'export_property_data'),array('escape'=>false)); ?>
						</li>
					</ul>
				</li>
				
			</ul>
			<!-- END SIDEBAR MENU -->
		</div>
	</div>

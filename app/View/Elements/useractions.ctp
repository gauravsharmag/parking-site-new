<ul class="page-breadcrumb breadcrumb">
                        <li class="btn-group">
                            <button type="button" class="btn blue dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true">
                            <span>Actions</span><i class="fa fa-angle-down"></i>
                            </button>
                            <ul class="dropdown-menu pull-right" role="menu">
                                <li>
                                    <?php echo $this->Html->link(__('My Account'), array('action' => 'myAccount')); ?>
                                </li>
                                <li>
                                   <?php echo $this->Html->link(__('New Vehicle'), array('controller' => 'Vehicles', 'action' => 'registerNewVehicle')); ?>
                                </li>
                            </ul>
                        </li>
                    </ul>
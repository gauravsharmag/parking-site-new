

<div class="page-content-wrapper">
		<div class="page-content">
<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">Roles <small> List of roles</small>
					</h3>
					<ul class="page-breadcrumb breadcrumb">
						<?php /*?><li class="btn-group">
							<button type="button" class="btn blue dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true">
							<span>Actions</span><i class="fa fa-angle-down"></i>
							</button>
							<ul class="dropdown-menu pull-right" role="menu">
								<li>
									<a href="#">Action</a>
								</li>
								<li>
									<a href="#">Another action</a>
								</li>
								<li>
									<a href="#">Something else here</a>
								</li>
								<li class="divider">
								</li>
								<li>
									<a href="#">Separated link</a>
								</li>
							</ul>
						</li><?php */?>
						<li>
							<i class="fa fa-home"></i>
							<?php echo $this->Html->link('Roles',array('controller'=>'roles','action'=>'index')); ?>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<?php echo $this->Html->link('List Roles',array('controller'=>'roles','action'=>'index')); ?>
							
						</li>
						
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
            <div class="row">
				<div class="col-md-12">
					<!-- BEGIN EXAMPLE TABLE PORTLET-->
					<div class="portlet box grey-cascade">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-users"></i>Roles List
							</div>
							<div class="tools">
								<a href="javascript:;" class="collapse">
								</a>
								<?php /*?><a href="#portlet-config" data-toggle="modal" class="config">
								</a>
								<a href="javascript:;" class="reload">
								</a>
								<a href="javascript:;" class="remove">
								</a><?php */?>
							</div>
						</div>
						<div class="portlet-body">
							<div class="table-toolbar">
								<div class="btn-group">
                                <?php echo $this->Html->link('Add New <i class="fa fa-plus"></i>',array('controller'=>'roles','action'=>'add'),array('escape'=>false,'class'=>'btn green')); ?>
									
								</div>
								<?php /*?><div class="btn-group pull-right">
									<button class="btn dropdown-toggle" data-toggle="dropdown">Tools <i class="fa fa-angle-down"></i>
									</button>
									<ul class="dropdown-menu pull-right">
										<li>
											<a href="#">
											Print </a>
										</li>
										<li>
											<a href="#">
											Save as PDF </a>
										</li>
										<li>
											<a href="#">
											Export to Excel </a>
										</li>
									</ul>
								</div><?php */?>
							</div>
                            <?php $srNo=0;  ?>
							<table class="table table-striped table-bordered table-hover">
							<thead>
							<tr>
                                    <th><?php echo $this->Paginator->sort('id'); ?></th>
                                    <th><?php echo $this->Paginator->sort('role_type'); ?></th>
                                    <th><?php echo $this->Paginator->sort('role_name'); ?></th>
                                    <th class="actions"><?php echo __('Actions'); ?></th>
                            </tr>
							</thead>
							<tbody>
                            <?php foreach ($roles as $role): ?>
                                    <tr>
                                        <td><?php echo h($role['Role']['id']); ?>&nbsp;</td>
                                        <td><?php echo h($role['Role']['role_type']); ?>&nbsp;</td>
                                        <td><?php echo h($role['Role']['role_name']); ?>&nbsp;</td>
                                        <td class="actions">
                                        
                                        
                                        
                                        
                                            <?php echo $this->Html->link(__('View'), array('action' => 'view', $role['Role']['id']),array('class'=>'btn default btn-xs green-stripe')); ?>&nbsp;
                                            <?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $role['Role']['id']),array('class'=>'btn green btn-xs green-stripe')); ?>&nbsp;
                                            <?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $role['Role']['id']), array('class'=>'btn btn-danger  btn-xs green-stripe'), __('Are you sure you want to delete # %s?', $role['Role']['id'])); ?>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
							
							</tbody>
							</table>
						</div>
					</div>
					<!-- END EXAMPLE TABLE PORTLET-->
			


















<!--


<div class="roles index">
	<h2><?php echo __('Roles'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('role_type'); ?></th>
			<th><?php echo $this->Paginator->sort('role_name'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($roles as $role): ?>
	<tr>
		<td><?php echo h($role['Role']['id']); ?>&nbsp;</td>
		<td><?php echo h($role['Role']['role_type']); ?>&nbsp;</td>
		<td><?php echo h($role['Role']['role_name']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $role['Role']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $role['Role']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $role['Role']['id']), array(), __('Are you sure you want to delete # %s?', $role['Role']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div>
    <?php echo $this->element('homePage');?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Role'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
	</ul>
</div>-->
</div>
			</div>
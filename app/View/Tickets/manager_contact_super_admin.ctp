<div class="page-content-wrapper">
    <div class="page-content">

        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title">
                    <?php echo CakeSession::read('PropertyName'); ?><small> Contact Super Admin</small>
                </h3>
                <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <?php echo $this->Html->link('Home',array('controller'=>'users','action'=>'manager_home_page')); ?>
                         <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="">Contact Super Admin</a>
                    </li>
                    <li class="pull-right">
                        <div id="dashboard-report-range" class="dashboard-date-range tooltips" data-placement="top" data-original-title="Change dashboard date range">
                            <i class="icon-calendar"></i>
                            <span></span>
                            <i class="fa fa-angle-down"></i>
                        </div>
                    </li>
                </ul>
                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>
        <div id="flashMessages">
            <h2><?php echo $this->Session->flash(); ?></h2>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="portlet box blue">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-gift"></i>Contact Super Admin
                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse">
                            </a>


                        </div>
                    </div>
                    <div class="portlet-body form">
                        <!-- BEGIN FORM-->
                        <?php echo $this->Form->create('Ticket', array('class' => 'form-horizontal')) ?>
    <div class="form-body">
                            
                                
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Subject<span class="required">
                                            * </span></label>
                                    <div class="col-md-4">
                                        <?php
                                        echo $this->Form->input('subject', array('label' => false,
                                            'required' => true,
                                            'errorMessage' => false,
                                            'class' => 'form-control'));
                                        if ($this->Form->isFieldError('subject')) {
                                            echo $this->Form->error('subject', array('div' => false, 'attributes' => array('wrap' => 'span', 'class' => 'model-error')));
                                        }
                                        ?>
                                    </div>

                                </div>
                                
                              
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Message<span class="required">
                                            * </span> </label>
                                    <div class="col-md-4">
                                        <?php
                                        echo $this->Form->textarea('question', array('label' => false,
                                            'errorMessage' => false,
                                            'required' => true,
                                            'class' => 'ckeditor'));
                                        if ($this->Form->isFieldError('question')) {
                                            echo $this->Form->error('question', array('div' => false, 'attributes' => array('wrap' => 'span', 'class' => 'model-error')));
                                        }
                                        ?>
                                    </div>
                                </div>





                            </div>
                        
                        <div class="form-actions fluid">
                                <div class="col-md-offset-3 col-md-9">
                                    <button type="submit" class="btn green" id="submitB">Send Message</button>
                                </div>
                            </div>
                            <?php echo $this->Form->end(); ?>
                            <!-- END FORM-->
                   
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
        $('#submitB').click(function(){
            
             $('#submitB').attr("disabled", true);
             $('#TicketManagerContactSuperAdminForm').submit();
        });
        
        
        
    });


</script>


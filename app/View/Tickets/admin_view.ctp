<div class="page-content-wrapper">
    <div class="page-content">
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title">
                    <?php echo CakeSession::read('PropertyName'); ?><small> View Queries</small>
                </h3>
                <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <?php echo $this->Html->link('Home',array('controller'=>'users','action'=>'admin_pa_home_page')); ?>
                        <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="">View Queries</a>
                    </li>
                    <li class="pull-right">
                        <div id="dashboard-report-range" class="dashboard-date-range tooltips" data-placement="top" data-original-title="Change dashboard date range">
                            <i class="icon-calendar"></i>
                            <span></span>
                            <i class="fa fa-angle-down"></i>
                        </div>
                    </li>
                   <!-- <li class="btn-group">
                       <a href="/admin/tickets/super_admin_contact" ><button type="button" class="btn blue">
							<span>Send a New Message</span>
							</button></a>
                    </li>-->
                </ul>
                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>
        <div id="flashMessages">
            <h2><?php echo $this->Session->flash(); ?></h2>
        </div>
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN SAMPLE TABLE PORTLET-->
                <div class="portlet box purple">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-cogs"></i>All Queries<?php echo CakeSession::read('PropertyName'); ?>
                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse">
                            </a>

                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="table-scrollable">
                            <table class="table table-hover" id="tickets">
                                <thead>
                                    <tr>

                                        <th>Subject</th>
                                        <th>From</th>
                                        <th>To</th>
                                        <th>Sent On</th>
                                        <th>Status</th>
										<th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                   

                                </tbody>
                            </table>
                        </div>
                        
                        
                    </div>
                </div>
                <!-- END SAMPLE TABLE PORTLET-->
            </div>
        </div>
    </div>
</div>
<?php echo $this->Html->script('//code.jquery.com/jquery-1.12.0.min.js'); ?>
<?php echo $this->Html->script('//cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js'); ?>
<?php echo $this->Html->css('//cdn.datatables.net/1.10.11/css/jquery.dataTables.min.css'); ?>

<script>
	$('#tickets').dataTable({
			"bProcessing": false,
			"bServerSide": true,
			"bDestroy": true,
			"aoColumns": [
				{"mData":"Ticket.subject"},
				{"mData":"User.first_name"},
				{"mData":"Recipient.first_name"},
				{"mData":"Ticket.created"},
				{"mData":"Ticket.status"},
				{"mData":"Ticket.id"},
			],
			"sAjaxSource":"/admin/Tickets/get_tickets"
			//"sAjaxSource":"<?php echo $this->Html->Url(array('controller' => 'SubscriptionPayments', 'action' => 'sub_revenew_table',1)); ?>"
		});
</script>

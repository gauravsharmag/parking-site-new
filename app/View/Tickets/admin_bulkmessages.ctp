<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.4/jquery.min.js" type="text/javascript"></script>-->
<?php
echo $this->Html->css('chosen.css');
echo $this->Html->script('chosen.jquery.js');
?>

<style type="text/css" media="all">
    /* fix rtl for demo */
    .chosen-rtl .chosen-drop { left: -9000px; }
</style>
<div class="page-content-wrapper">
    <div class="page-content">

        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title">
                    <?php echo CakeSession::read('PropertyName'); ?><small> Contact User</small>
                </h3>
                <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <?php echo $this->Html->link('Home', array('controller' => 'users', 'action' => 'superAdminHome')); ?>
                        <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="">Contact User</a>
                    </li>
                    <li class="pull-right">
                        <div id="dashboard-report-range" class="dashboard-date-range tooltips" data-placement="top" data-original-title="Change dashboard date range">
                            <i class="icon-calendar"></i>
                            <span></span>
                            <i class="fa fa-angle-down"></i>
                        </div>
                    </li>
                </ul>
                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>
        <div id="flashMessages">
            <h2><?php echo $this->Session->flash(); ?></h2>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="portlet box blue">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-gift"></i>Contact User
                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse">
                            </a>


                        </div>
                    </div>
                    <div class="portlet-body form">
                        <!-- BEGIN FORM-->
                        <?php echo $this->Form->create('Ticket', array('class' => 'form-horizontal')) ?>

                        <div class="form-body">
                            <div class="form-body">
                                <div class="form-group">
                                    <label class="col-md-3 control-label">To</label>
                                    <div class="col-md-4">
										
                                        <?php
                                         echo $this->Form->input('recipient', array('label' => false,'type'=>'select','options'=>$usernameVal,'empty'=>'','multiple'=>true,'data-placeholder'=>'Select User OR leave empty for all','class' => 'form-control'));
                                 
                                        ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Subject<span class="required">
                                            * </span></label>
                                    <div class="col-md-4">
                                        <?php
                                        echo $this->Form->input('subject', array('label' => false,
                                            'required' => true,
                                            'errorMessage' => false,
                                            'class' => 'form-control'));
                                        if ($this->Form->isFieldError('subject')) {
                                            echo $this->Form->error('subject', array('div' => false, 'attributes' => array('wrap' => 'span', 'class' => 'model-error')));
                                        }
                                        ?>
                                    </div>

                                </div>


                                <div class="form-group">
                                    <label class="col-md-3 control-label">Message<span class="required">
                                            * </span> </label>
                                    <div class="col-md-4">
                                        <?php
                                        echo $this->Form->textarea('message', array('label' => false,
                                            'errorMessage' => false,
                                            'required' => true,
                                            'class' => 'ckeditor'));
                                        if ($this->Form->isFieldError('message')) {
                                            echo $this->Form->error('message', array('div' => false,'required'=>true,'attributes' => array('wrap' => 'span', 'class' => 'model-error')));
                                        }
                                        ?>
                                    </div>
                                </div>





                            </div>
                            <div class="form-actions fluid">
                                <div class="col-md-offset-3 col-md-9">
                                    <button type="submit" class="btn green" id="submitB">Send Message</button>
                                </div>
                            </div>
                            <?php echo $this->Form->end(); ?>
                            <!-- END FORM-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
     $('#TicketRecipient').chosen();
    
</script>

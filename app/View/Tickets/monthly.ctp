<?php //debug($allProperty);   ?>

<h1>Reports "<?php echo date('F d, Y',  strtotime($fromDate)); ?> TO <?php echo date('F d, Y',  strtotime($toDate)); ?>"</h1>
<h2>New Passes And Cost Report</h2>
<table border="1" >
    <thead align="center">
    <th>
        Sr. No.
    </th>
    <th>
        Property
    </th>
    <th>
        Transaction Amount
    </th>
    <th>
        New Pass Purchased
    </th>
</thead>
<tbody>
    <?php
    $i = 1;
    foreach ($newPassProperty as $newPass) {
        ?>
        <tr>
            <td><?php echo $i++; ?></td>
            <td><?php echo $newPass['Property']['name']; ?></td>
            <td align="right">$ <?php echo $newPass['Property']['cost']; ?></td>
            <td align="right"><?php echo $newPass['Property']['passCount']; ?></td>
        </tr>
    <?php } ?>
    <tr>
        <td></td>
        <td><b>Total</b></td>
        <td align="right"><b>$ <?php echo $totalAmount; ?></b></td>
        <td align="right"><b><?php echo $totalPasses; ?></b></td>
    </tr>
</tbody>
</table>


<h2>Renewal Cost</h2>
<table border="1" >
  <thead align="center">
        <tr>
            <th>Sr. No.</th>
            <th>Property</th>
            <th>Total Pass Renewed</th>
            <th>Free</th>
            <th>Paid</th>
            <th>Amount</th>
        </tr>
    </thead>
    <tbody>
        <?php
        $sr = 1;
        $totalRenewal = $paidRenewal = $freeRenewal = $totalCost = 0;
        for ($i = 0; $i < count($array); $i++) {
            ?>
            <tr>
                <td align="left">
                    <?php echo $sr++; ?>
                </td>
                <td>
                    <?php echo $array[$i]['PropertyName']; ?>
                </td>
                <td  align="right">
                    <?php
                    $totalRenewal = $totalRenewal + ($array[$i]['FreePassCount'] + $array[$i]['PaidPassCount']);
                    echo $totalRenewal;
                    ?>
                </td>
                <td align="right">
                    <?php
                    $freeRenewal = $freeRenewal + $array[$i]['FreePassCount'];
                    echo $array[$i]['FreePassCount'];
                    ?>
                </td>
                <td align="right">
                    <?php
                    $paidRenewal = $paidRenewal + $array[$i]['PaidPassCount'];
                    echo $array[$i]['PaidPassCount'];
                    ?>
                </td>
                <td align="right">
                    <?php
                    echo '$ ' . $array[$i]['PaidPassAmount'];
                    $totalCost = $totalCost + $array[$i]['PaidPassAmount'];
                    ?>
                </td>
            </tr>
<?php } ?>
        <tr>
            <td></td>
            <td><b>TOTAL</b></td>
            <td align="right"><b><?php echo $totalRenewal; ?></b></td>
            <td align="right"><b><?php echo $freeRenewal; ?></b></td>
            <td align="right"><b><?php echo $paidRenewal; ?></b></td>
            <td  align="right"><b><?php echo '$ ' . $totalCost; ?></b></td>
        </tr>
    </tbody>
</table>

<h2>Guest Pass Report</h2>
<table border="1" >
    <thead align="center">
    <th>
        Sr. No.
    </th>
    <th>
        Property
    </th>
    <th>
        Total Days
    </th>
    <th>
        Free Days
    </th>
    <th>
        Paid Days
    </th>
    <th>
        Paid Amount
    </th>
</thead>
<tbody>
    <?php
    $i = 1;
    foreach ($allProperty as $guestPass) {
        ?>
        <tr>
            <td><?php echo $i++; ?></td>
            <td><?php echo $guestPass['Property']['name']; ?></td>
            <td align="right"><?php echo $guestPass['Property']['days']?$guestPass['Property']['days']:0; ?></td>
            <td align="right"><?php echo $guestPass['Property']['freeDays']?$guestPass['Property']['freeDays']:0; ?></td>
            <td align="right"><?php echo $guestPass['Property']['paidDays']?$guestPass['Property']['paidDays']:0; ?></td>
            <td align="right">$ <?php echo $guestPass['Property']['paidAmount']?$guestPass['Property']['paidAmount']:0; ?></td>
        </tr>
<?php } ?>
    <tr>
        <td></td>
        <td><b>Total</b></td>
        <td align="right"><b><?php echo $totalDays; ?></b></td>
        <td align="right"><b><?php echo $totalFreedays; ?></b></td>
        <td align="right"><b><?php echo $totalPaid; ?></b></td>
        <td align="right"><b>$ <?php echo $totalAmt; ?></b></td>
    </tr>
</tbody>
</table>
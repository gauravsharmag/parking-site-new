<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.4/jquery.min.js" type="text/javascript"></script>-->
<?php
echo $this->Html->css('chosen.css');
echo $this->Html->script('chosen.jquery.js');
?>
<?php echo $this->Html->script(array('//cdnjs.cloudflare.com/ajax/libs/ckeditor/4.4.1/ckeditor.js'))?>

<style type="text/css" media="all">
    /* fix rtl for demo */
    .chosen-rtl .chosen-drop { left: -9000px; }
</style>
<div class="page-content-wrapper">
    <div class="page-content">

        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title">
                    <?php echo CakeSession::read('PropertyName'); ?><small> Contact User</small>
                </h3>
                <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <?php echo $this->Html->link('Home', array('controller' => 'users', 'action' => 'superAdminHome')); ?>
                        <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="">Contact User</a>
                    </li>
                    <li class="pull-right">
                        <div id="dashboard-report-range" class="dashboard-date-range tooltips" data-placement="top" data-original-title="Change dashboard date range">
                            <i class="icon-calendar"></i>
                            <span></span>
                            <i class="fa fa-angle-down"></i>
                        </div>
                    </li>
                </ul>
                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>
        <div id="flashMessages">
            <h2><?php echo $this->Session->flash(); ?></h2>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="portlet box blue">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-gift"></i>Contact <?php echo $userName; ?>
                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse">
                            </a>


                        </div>
                    </div>
                    <div class="portlet-body form">
                        <!-- BEGIN FORM-->
                        <?php echo $this->Form->create('Ticket', array('url'=>array('action'=>'contact_user/'.$user_id),'class' => 'form-horizontal', 'novalidate' => 'novalidate')) ?>

                        <div class="form-body">
                            <div class="form-body">
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Subject<span class="required">
                                            * </span></label>
                                    <div class="col-md-9">
                                        <?php
                                        echo $this->Form->input('subject', array('label' => false,
                                            'required' => true,
                                            'errorMessage' => false,
                                            'class' => 'form-control'));
                                        if ($this->Form->isFieldError('subject')) {
                                            echo $this->Form->error('subject', array('div' => false, 'attributes' => array('wrap' => 'span', 'class' => 'model-error')));
                                        }
                                        ?>
                                    </div>

                                </div>


                                <div class="form-group">
                                    <label class="col-md-3 control-label">Message<span class="required">
                                            * </span> </label>
                                    <div class="col-md-9">
                                        <?php
                                        echo $this->Form->textarea('message', array('label' => false,
                                            'errorMessage' => false,
                                            'required' => true,
                                            'class' => 'ckeditor'));
                                        if ($this->Form->isFieldError('message')) {
                                            echo $this->Form->error('message', array('div' => false, 'attributes' => array('wrap' => 'span', 'class' => 'model-error')));
                                        }
                                        ?>
                                    </div>
                                </div>





                            </div>
                            <div class="form-actions fluid">
                                <div class="col-md-offset-3 col-md-9">
                                    <button type="submit" class="btn green" id="submitB">Send Message</button>
                                </div>
                            </div>
                            <?php echo $this->Form->end(); ?>
                            <!-- END FORM-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
    var config = {
        '.chosen-select': {},
        '.chosen-select-deselect': {allow_single_deselect: true},
        '.chosen-select-no-single': {disable_search_threshold: 10},
        '.chosen-select-no-results': {no_results_text: 'Oops, nothing found!'},
        '.chosen-select-width': {width: "95%"}
    }
    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }
</script>

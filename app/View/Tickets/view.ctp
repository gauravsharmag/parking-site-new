<div class="page-content-wrapper">
    <div class="page-content">
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title">
                   <?php echo CakeSession::read('PropertyName'); ?><small> View Queries</small>
                </h3>
                <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <?php echo $this->Html->link('Home',array('controller'=>'users','action'=>'customerHomePage')); ?>
                        <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="">View Queries</a>
                    </li>
                    <li class="pull-right">
                        <div id="dashboard-report-range" class="dashboard-date-range tooltips" data-placement="top" data-original-title="Change dashboard date range">
                            <i class="icon-calendar"></i>
                            <span></span>
                            <i class="fa fa-angle-down"></i>
                        </div>
                    </li>
                </ul>
                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>
        <div id="flashMessages">
            <h2><?php echo $this->Session->flash(); ?></h2>
        </div>
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN SAMPLE TABLE PORTLET-->
                <div class="portlet box purple">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-cogs"></i>All Queries
                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse">
                            </a>

                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="table-scrollable">
                            <table class="table table-hover">
                                <thead>
                                    <tr>

                                        <th><?php echo $this->Paginator->sort('subject', 'Subject'); ?></th>
                                        <th><?php echo $this->Paginator->sort('username', 'Username'); ?></th>
                                        <th><?php echo $this->Paginator->sort('created', 'Sent On'); ?></th>
                                        <th><?php echo $this->Paginator->sort('status', 'Status'); ?></th>
                                        <th><?php echo $this->Paginator->sort('Replies'); ?></th>

                                    </tr>

                                </thead>
                                <tbody>
                                    <?php foreach ($viewQueries as $view) { ?>

                                        <tr>
                                            <td>
                                                <strong>  <?php echo $this->Html->link($view['Ticket']['subject'], array('controller' => 'TicketReplies', 'action' => 'view_single', $view['Ticket']['id'])); ?>
                                                </strong>	

                                            </td>
                                            <td>
                                                <?php echo $view['User']['username']; ?>
                                            </td>
                                            <td>
                                               
                                                <?php echo $this->Time->niceShort($view['Ticket']['created']); ?>
                                                
                                            </td>
                                            <td>
                                                <?php if ($view['Ticket']['status'] == 0) { ?>
                                                    <span class="label label-warning">
                                                    <?php } else { ?>
                                                        <span class="label label-default">
                                                        <?php } ?>
                                                        <?php
                                                        if ($view['Ticket']['status'] == 0) {
                                                            echo "open";
                                                        } else {
                                                            echo "closed";
                                                        }
                                                        ?> </span>
                                            </td>
                                            <td>


                                                <?php
                                                $answer_count = count($view['TicketReply']);
                                                if (!$answer_count) {
                                                    ?>
                                                    <span class="badge badge-default">
                                                    <?php echo ("(no threads yet)"); ?>
                                                    </span>
                                                    <?php } else if ($answer_count == 1) { ?>
                                                    <span class="badge badge-danger">
                                                    <?php echo ("(1 thread)"); ?>
                                                    </span>
                                                    <?php } else { ?>
                                                    <span class="badge badge-info">
                                                    <?php echo("(" . $answer_count . " threads)"); ?>
                                                    </span>
    <?php } ?>

                                            </td>
                                        </tr>
<?php } ?>

                                </tbody>
                            </table>
                        </div>
                        <p>
                            <?php
                            echo $this->Paginator->counter(array(
                                'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
                            ));
                            ?>  
                        </p>
                        <div class="paging">
                            <?php
                            echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
                            echo " ";
                            echo $this->Paginator->numbers(array('separator' => ''));
                            echo " ";
                            echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
                            ?>
                        </div>
                    </div>
                </div>
                <!-- END SAMPLE TABLE PORTLET-->
            </div>
        </div>
    </div>
</div>
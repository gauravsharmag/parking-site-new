
  <div class="page-content-wrapper">
	<div class="page-content" style="min-height:996px">
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
					<?php //debug($this->Session->params);
					//debug($this->Session->read('Auth'));
					//debug($this->Session->read('PropertyId')['Property']['id']);
					//debug($this->Session->read('PropertyName'));
					?>
					<?php echo $this->Session->read('PropertyName')." ";?>Permits <small></small>
					</h3>
					<ul class="page-breadcrumb breadcrumb">

						<li>
							<i class="fa fa-home"></i>
							<?php echo $this->Html->link(
                             'Home',
                              array(
                                    'controller' => 'Users',
                                    'action' => 'myAccount',
                                    'full_base' => true
                                                                                              )
                              );?>
							<i class="fa fa-angle-right"></i>
						</li>

						<li>
							<a href="#">Permit Details</a>
						</li>
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->

      <div class="row">
        <div class="col-md-12">
          <!-- Begin: life time stats -->
            <div class="portlet">
              <div class="portlet-body">
                <div class="tabbable">
                   <ul class="nav nav-tabs nav-tabs-lg">
					    <li class="active">
						    <a data-toggle="tab" href="#tab_1">All Permits</a>
						</li>
					
                   </ul>
                    <div class="tab-content">
<!-------------------------Tab Content Begin---------------------------->
                       <div id="tab_1" class="tab-pane active">

                         <?php if($packagesFixed!=null){?>

                         <?php for($i=0;$i<(round($countFixed/2,0,PHP_ROUND_HALF_DOWN)+1)*2;$i=$i+2){?>
                            <div class="row">
                               <?php if(array_key_exists($i,$packagesFixed)){?>
                                  <div class="col-md-6 col-sm-12">
    								 <div class="portlet yellow-crusta box">
    													<div class="portlet-title">
    														<div class="caption">
    															<i class="fa fa-cogs"></i><?php echo $packagesFixed[$i]['Package']['name'];?>
    														</div>
    														
    													</div>
    													<div class="portlet-body">
    														<div class="row static-info">
    															<div class="col-md-5 name">
    																 Cost :
    															</div>
    															<div class="col-md-7 value">
    																<?php echo '$ '.$packagesFixed[$i]['Package']['cost'];?><!--<span class="label label-info label-sm">
    																Email confirmation was sent </span>-->
    															</div>
    														</div>
    														<?php If($packagesFixed[$i]['Package']['is_fixed_duration']==1){?>
    														        <div class="row static-info">
    															        <div class="col-md-5 name">
    																        Start Date:
    															        </div>
    															        <div class="col-md-7 value">
    															        <?php	echo $packagesFixed[$i]['Package']['start_date']= date("d-m-Y", strtotime($packagesFixed[$i]['Package']['start_date']));?>
    															        </div>
    														        </div>
                                                                    <div class="row static-info">
    															        <div class="col-md-5 name">
    																        End Date:
    															        </div>
    															        <div class="col-md-7 value">
    															        <?php	echo $packagesFixed[$i]['Package']['expiration_date']= date("d-m-Y", strtotime($packagesFixed[$i]['Package']['expiration_date']));?>
    															        </div>
    														        </div>
    														<?php }else{?>
                                                                    <div class="row static-info">
    															        <div class="col-md-5 name">
    																        Valid For:
    															        </div>
    															        <div class="col-md-7 value">
    															        <?php	echo $packagesFixed[$i]['Package']['duration'].' '.$packagesFixed[$i]['Package']['duration_type'];?>
    															        </div>
    														        </div>
    														<?php }?>
    														<?php If($packagesFixed[$i]['Package']['is_recurring']==1){?>
    														        <div class="row static-info">
    															        <div class="col-md-5 name">
    																        Recurring Type:
    															        </div>
    															        <div class="col-md-7 value">
    														                Yes
    														            </div>
    														        </div>
    														<?php }?>
                                                            <?php If($packagesFixed[$i]['Package']['is_guest']==1){?>
    														        <div class="row static-info">
    															        <div class="col-md-5 name">
    																        Guest Type:
    															        </div>
    															        <div class="col-md-7 value">
    														                Yes
    														            </div>
    														        </div>
    														<?php }?>
    													</div>
    								 </div>
    							  </div>
                               <?php }?>
                               <?php if(array_key_exists($i+1,$packagesFixed)){?>
			                        <div class="col-md-6 col-sm-12">
												<div class="portlet blue-hoki box">
                                                        <div class="portlet-title">
    														<div class="caption">
    															<i class="fa fa-cogs"></i><?php echo $packagesFixed[$i+1]['Package']['name'];?>
    														</div>
    														
    													</div>
    													<div class="portlet-body">
    														<div class="row static-info">
    															<div class="col-md-5 name">
    																 Cost :
    															</div>
    															<div class="col-md-7 value">
    																<?php echo '$ '.$packagesFixed[$i+1]['Package']['cost'];?><!--<span class="label label-info label-sm">
    																Email confirmation was sent </span>-->
    															</div>
    														</div>
    														<?php If($packagesFixed[$i+1]['Package']['is_fixed_duration']==1){?>
    														        <div class="row static-info">
    															        <div class="col-md-5 name">
    																        Start Date:
    															        </div>
    															        <div class="col-md-7 value">
    															        <?php	echo $packagesFixed[$i+1]['Package']['start_date']= date("d-m-Y", strtotime($packagesFixed[$i+1]['Package']['start_date']));?>
    															        </div>
    														        </div>
                                                                    <div class="row static-info">
    															        <div class="col-md-5 name">
    																        End Date:
    															        </div>
    															        <div class="col-md-7 value">
    															        <?php	echo $packagesFixed[$i+1]['Package']['expiration_date']= date("d-m-Y", strtotime($packagesFixed[$i+1]['Package']['expiration_date']));?>
    															        </div>
    														        </div>
    														<?php }else{?>
                                                                    <div class="row static-info">
    															        <div class="col-md-5 name">
    																        Valid For:
    															        </div>
    															        <div class="col-md-7 value">
    															        <?php	echo $packagesFixed[$i+1]['Package']['duration'].' '.$packagesFixed[$i+1]['Package']['duration_type'];?>
    															        </div>
    														        </div>
    														<?php }?>
    														<?php If($packagesFixed[$i+1]['Package']['is_recurring']==1){?>
    														        <div class="row static-info">
    															        <div class="col-md-5 name">
    																        Recurring Type:
    															        </div>
    															        <div class="col-md-7 value">
    														                Yes
    														        </div>
    														<?php }?>
                                                            <?php If($packagesFixed[$i+1]['Package']['is_guest']==1){?>
    														        <div class="row static-info">
    															        <div class="col-md-5 name">
    																        Guest Type:
    															        </div>
    															        <div class="col-md-7 value">
    														                Yes
    														            </div>
    														        </div>
    														    <?php }?>

    												    </div>
												</div>
								    </div>
							   <?php }?>
                            </div>
                         <?php }?>
                         <?php }?>
					
                             
                                             </div>
                       

                       </div>
                      
<!-------------------------Tab Content End-------------------------------->
                    </div>










                </div>
              </div>
            </div>
	      <!-- End: life time stats -->
        </div>
      </div>

























			<!-- END PAGE CONTENT-->
	 </div>
  </div>

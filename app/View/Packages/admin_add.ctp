<script>
$(document).ready(function(){
	$("#NewPass").hide();
	$("#AddNew").click(function(){
		$("#NewPass").slideToggle();
	});
    $("#panelDate").hide("slow");
    $("#panelGuest").hide("slow");
    $("#PackageDuration").attr("required",true );
    $("#PackageDurationType").attr("required",true );
    $("#PackageStartDate").attr("required",false );
    $("#PackageExpirationDate").attr("required",false );
    $("#panelTime").show("slow");

    $("#PackageIsFixedDuration").change(function()
    {
        if ($(this).is(':checked'))
        {
            $("#panelDate").show("slow");
            $("#PackageStartDate").attr("required",true );
            $("#PackageExpirationDate").attr("required",true );
            $("#panelTime").hide("slow");
            $("#PackageDuration").attr("required",false);
            $("#PackageDurationType").attr("required",false);

        }
        else
        {
             $("#panelDate").hide("slow");
             $("#PackageStartDate").attr("required",false );
             $("#PackageExpirationDate").attr("required",false );
             $("#panelTime").show("slow");
             $("#PackageDuration").attr("required",true );
             $("#PackageDurationType").attr("required",true );
        }
    });
    $('#PackageIsGuest').click(function ()
    {
         if ($(this).is(':checked'))
         {
           $("#panelGuest").show("slow");
           $("#PackageFreeGuestCredits").attr("required",true );
         }
         else
         {
           $("#panelGuest").hide("slow");
           $("#PackageFreeGuestCredits").attr("required",false );
         }
    });
});
</script>


<div class="page-content-wrapper">
	<div class="page-content">
	<!-- BEGIN PAGE CONTENT-->
		<div class="row">
			<div class="col-md-12">
				<h3 class="page-title">
					Parking Locations  <small><?php echo $propertyName;?></small>
                </h3>
                <!--BREADCRUMB START-->
				<ul class="page-breadcrumb breadcrumb">
				   <li class="btn-group">
						<button type="button" class="btn blue dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true">
						<span><?php $srNo=0; echo __('Actions'); ?></span><i class="fa fa-angle-down"></i>
						</button>
						<ul class="dropdown-menu pull-right" role="menu">
							<li>
								<a href="#"><?php echo $this->Html->link(__('View Property'), array('controller'=>'Properties','action' => 'view',$id)); ?> </a>
							</li>
							<li>
								<a href="#"><?php echo $this->Html->link(__('Edit Property'), array('controller'=>'Properties','action' => 'edit',$id,$propertyName)); ?> </a>
							</li>
							<li>
								<a href="#"><?php echo $this->Html->link(__('Manage Parking Locations'), array('controller' => 'Packages', 'action' => 'add',$id,$propertyName)); ?></a>
							</li>
							<li>
								<a href="#"><?php echo $this->Html->link(__('Manage Passes'), array('controller' => 'Passes', 'action' => 'add',$id,$propertyName)); ?> </a>
							</li>
						</ul>
					</li>
					<li>
						<i class="fa fa-home"></i>
						<?php echo $this->Html->link('Properties',array('controller'=>'properties','action'=>'index')); ?>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<?php echo $this->Html->link($propertyName,array('controller'=>'properties','action'=>'view',$id)); ?>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<?php echo __('Manage Parking Location'); ?>

					</li>
				</ul>
				<!--BREADCRUMB END-->
				<div id="flashMessages">
					<h2><?php  echo $this->Session->flash();?></h2>
				</div>
				<div class="nav-pills">
					<button class="btn btn-primary" id="AddNew">Add new </button>
				</div>
				<!-- ADD PORTLET START-->
				<div class="portlet box red" id="NewPass"  style="display:none;">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-gift"></i><?php echo __('Add New Parking Locations At'.' '.$propertyName); ?>
						</div>
					</div>
					<div class="portlet-body form">	
						<!-- BEGIN FORM-->
							<?php echo $this->Form->create('Package',array('class'=>'form-horizontal')); ?>
								<div class="form-body">
									 <?php
                                                  echo $this->Form->hidden('property_id',array('default'=>$id));
                                                  echo $this->Form->input('name',array('div'=>array('class'=>'form-group'),
                                                          'label'=>array('class'=>'col-md-3 control-label','text'=>'Name'),
                                                           'between'=>'<div class="col-md-4">',
                                                           'after'=>'</div>',
                                                           'class'=>'form-control'));
                                                  echo $this->Form->input('pass_id',array('div'=>array('class'=>'form-group'),
                                                                    'empty'=>'Select',
                                                                    'type'=>'select','options'=>$passes,
                                                                    'label'=>array('class'=>'col-md-3 control-label','text'=>'Assign Pass'),
                                                                    'between'=>'<div class="col-md-4">',
                                                                    'after'=>'</div>',
                                                                    'class'=>'form-control'));
                                                   echo $this->Form->input('cost',array('div'=>array('class'=>'form-group'),'label'=>array('class'=>'col-md-3 control-label','text'=>'Cost'),'between'=>'<div class="col-md-4">','after'=>'</div>','class'=>'form-control'));
                                             echo"<div id='panelTime'>";
                                                    echo $this->Form->input('duration',array('div'=>array('class'=>'form-group'),
                                                                'label'=>array('class'=>'col-md-3 control-label','text'=>'Duration'),
                                                                'between'=>'<div class="col-md-4">',
                                                                'after'=>'</div>',
                                                                'class'=>'form-control'));

                                                    echo $this->Form->input('duration_type',array('type'=>'select','options'=>array('Day'=>'Day','Month'=>'Month','Year'=>'Year'),'empty'=>'Select One','div'=>array('class'=>'form-group'),
                                                                'label'=>array('class'=>'col-md-3 control-label','text'=>'Duration Type'),
                                                                 'between'=>'<div class="col-md-4">',
                                                                  'after'=>'</div>',
                                                                  'class'=>'form-control'));
                                             echo"</div>";
                                                    echo $this->Form->input('is_fixed_duration',array('type'=>'checkbox','div'=>array('class'=>'form-group'),
                                                             'label'=>false,
                                                              'before'=>'<label class="col-md-3 control-label">Is Fixed Duration?</label><div class="col-md-4">',
                                                              'after'=>'</div>'
                                                              ));
                                             echo"<div id='panelDate'>";
                                        	        echo $this->Form->input('start_date',array('type'=>'text','div'=>array('class'=>'form-group'),
                                                          'label'=>array('class'=>'col-md-3 control-label','text'=>'Start Date'),
                                                          'between'=>'<div class="col-md-4"><div class="input-group date date-picker" data-date-format="mm/dd/yyyy" data-date-start-date="+0d">',
                                                           'after'=>'<span class="input-group-btn"><button type="button" class="btn default"><i class="fa fa-calendar"></i></button></span></div></div>',
                                                           'class'=>'form-control'));

                                                    echo $this->Form->input('expiration_date',array('type'=>'text','div'=>array('class'=>'form-group'),
                                                            'label'=>array('class'=>'col-md-3 control-label','text'=>'Valid Upto'),
                                                             'between'=>'<div class="col-md-4"><div class="input-group date date-picker" data-date-format="mm/dd/yyyy" data-date-start-date="+0d">',
                                                             'after'=>'<span class="input-group-btn"><button type="button" class="btn default"><i class="fa fa-calendar"></i></button></span></div></div>',
                                                             'class'=>'form-control'));
                                             echo"</div>";

                                                   echo $this->Form->input('total_spaces_each_package',array('required'=>true,'div'=>array('class'=>'form-group'),
                                                              'label'=>array('class'=>'col-md-3 control-label','text'=>'Allocate space'),
                                                              'between'=>'<div class="col-md-4">',
                                                               'after'=>'</div>',
                                                               'class'=>'form-control'));

                                                    echo $this->Form->input('is_recurring',array('type'=>'checkbox','div'=>array('class'=>'form-group'),
                                                                  'label'=>false,
                                                                  'before'=>'<label class="col-md-3 control-label">Is Recurring ?</label><div class="col-md-4">',
                                                                   'after'=>'</div>'
                                                                    ));
                                                    echo $this->Form->input('is_guest',array('type'=>'checkbox','div'=>array('class'=>'form-group'),
                                                                    'label'=>false,
                                                                    'before'=>'<label class="col-md-3 control-label">Is Guest ?</label><div class="col-md-4">',
                                                                     'after'=>'</div>'
                                                                     ));
                                              echo"<div id='panelGuest' style='display:none'>";
                                                    echo $this->Form->input('free_guest_credits',array('div'=>array('class'=>'form-group'),
                                                                                     'label'=>array('class'=>'col-md-3 control-label','text'=>'Free Guest Credits'),
                                                                                     'between'=>'<div class="col-md-4">',
                                                                                     'after'=>'</div>',
                                                                                     'class'=>'form-control'));
                                               echo"</div>";
                                                     echo $this->Form->input('is_required',array('type'=>'checkbox','div'=>array('class'=>'form-group'),
                                                                     'label'=>false,
                                                                     'before'=>'<label class="col-md-3 control-label">Is Required ? </label><div class="col-md-4">',
                                                                     'after'=>'</div>'
                                                                      ));


                                           ?>
								 </div>
								 <div class="form-actions fluid">
									<div class="col-md-offset-3 col-md-9">
										<button type="submit" class="btn blue">Submit</button>
									</div>
								 </div>
							<?php echo $this->Form->end(); ?>
							<!-- END FORM-->
					</div>
				</div>
				<!-- ADD PORTLET END-->	
				<!--PORTLET START-->
					<div class="portlet box green">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-gift"></i><?php echo __('Parking Locations At'.' '.$propertyName); ?>
							</div>
						</div>
						<div class="portlet-body">	
							  <?php $srNo=0;  ?>
							  <div class="table-responsive">
								<table class="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											   <th><?php echo "Sr.No."; ?></th>
											   <th><?php echo h('Name'); ?></th>
											   <th><?php echo h('Cost'); ?></th>
											   <th>From - To</th>
											   <th><?php echo h('Pass Assigned'); ?></th>
											   <th><?php echo h('Fixed Duration'); ?></th>
											   <th><?php echo h('Duration'); ?></th>
											   <th><?php echo h('Recurring'); ?></th>
											   <th><?php echo h('Guest'); ?></th>
											   <th><?php echo h('Free Guest Credits'); ?></th>
											   <th><?php echo h('Required'); ?></th>
											   <th><?php echo h('Total Space'); ?></th>
											   <th><?php echo __('Actions'); ?></th>
										 </tr>
									</thead>
									<tbody>
									<?php 
                                        foreach ($packages as $package): ?>
										   <tr>
												<td><?php echo h(++$srNo); ?>&nbsp;</td>
                                                <td><?php echo h($package['Package']['name']); ?>&nbsp;</td>
												<td><?php echo h($package['Package']['cost']); ?>&nbsp;</td>
												<td><?php     
														if($package['Package']['start_date']==null){ echo "NA";}else{
														echo date("m/d/Y", strtotime($package['Package']['start_date']))."<br>"."To"."<br>".date("m/d/Y", strtotime($package['Package']['expiration_date']));
														} ?>
												</td>         		
												<td><?php
														  echo $this->requestAction(array('controller'=>'Passes','action'=>'getPassName',$package['Package']['pass_id']));
													 ?>&nbsp;
												</td>
												<td><?php 
														if($package['Package']['is_fixed_duration']==1){echo h('Yes');}else{echo h('No');} 
													?>&nbsp;
												</td>
												<td><?php 
														echo $package['Package']['duration'].' '.$package['Package']['duration_type']; 
													?>&nbsp;
												</td>
                                                <td><?php 
														 if($package['Package']['is_recurring']==1){
															 echo h('Yes');
														  }else{
															  echo h('No');
														  } ?>&nbsp;
												</td>
												<td><?php 
														if($package['Package']['is_guest']==1){echo h('Yes');}else{echo h('No');} 
													 ?>&nbsp;
												</td>
                                                <td><?php 
														echo h($package['Package']['free_guest_credits']); 
													  ?>&nbsp;
												</td>
                                                <td><?php 
														if($package['Package']['is_required']==1){echo h('Yes');}else{echo h('No');} 
													?>&nbsp;
												</td>
                                                <td><?php echo h($package['Package']['total_spaces_each_package']); ?>&nbsp;
												</td>
												<td class="actions">
													<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $package['Package']['id'],$propertyName,$propertyId),array('class' => 'btn green btn-xs green-stripe')); ?>
													<?php
															echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $package['Package']['id'],$propertyName,$propertyId), array('class' => 'btn red btn-xs red-stripe'), __(' Are you sure to delete this package ?', $package['Package']['id']));
													?>
												</td>                           		
										</tr>
										<?php endforeach; ?>
								</tbody>
							</table>
							<p>
                            	<?php
                            	echo $this->Paginator->counter(array(
                            	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
                            	));
                            	?>	</p>
                            	<div class="paging">
                            	    <?php
                            		    echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
                            		    echo $this->Paginator->numbers(array('separator' => ''));
                            		    echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
                            	    ?>
                                </div>
							
						</div>
					</div>
				<!--PORTLET END-->
				
			</div>
		</div>
	<!-- END PAGE CONTENT-->
	</div>
</div>
			



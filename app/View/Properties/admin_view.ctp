<?php echo $this->Html->script('//code.jquery.com/jquery-1.12.0.min.js'); ?>
<?php echo $this->Html->script('//cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js'); ?>
<?php echo $this->Html->script('//cdn.datatables.net/buttons/1.1.2/js/dataTables.buttons.min.js'); ?>
<?php echo $this->Html->script('//cdn.datatables.net/buttons/1.1.2/js/buttons.print.min.js'); ?>
<?php echo $this->Html->script('//cdn.datatables.net/buttons/1.1.2/js/buttons.flash.min.js'); ?>
<?php echo $this->Html->script('//cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js'); ?>
<?php echo $this->Html->script('//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js'); ?>
<?php echo $this->Html->script('//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js'); ?>
<?php echo $this->Html->script('//cdn.datatables.net/buttons/1.1.2/js/buttons.html5.min.js'); ?>
<?php echo $this->Html->script('//cdn.datatables.net/buttons/1.1.2/js/buttons.colVis.min.js'); ?>
<?php echo $this->Html->css('//cdn.datatables.net/1.10.11/css/jquery.dataTables.min.css'); ?>
 <?php echo $this->Html->css('//cdn.datatables.net/buttons/1.1.2/css/buttons.dataTables.min.css'); 
  ?> 
<div class="page-content-wrapper">
	<div class="page-content">
<!--Two Outer Divs-->
		<div class="row">
			<div class="col-md-12">
				<!-- BEGIN PAGE TITLE & BREADCRUMB-->
				<h3 class="page-title">
					<h2><?php echo __('Property Details: '); //debug($property); //debug($roleArr);?> <?php echo h($property['Property']['name']); ?></h2> <small></small>
				</h3>
				<ul class="page-breadcrumb breadcrumb">
					<li class="btn-group">
						<button type="button" class="btn blue dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true">
						<span><?php $srNo=0; echo __('Actions'); ?></span><i class="fa fa-angle-down"></i>
						</button>
						<ul class="dropdown-menu pull-right" role="menu">
							<li>
								<a href="#"><?php echo $this->Html->link(__('Edit Property'), array('action' => 'edit', $property['Property']['id'],$property['Property']['name'])); ?> </a>
							</li>


							<li>
								<a href="#"><?php echo $this->Html->link(__('Manage Parking Locations'), array('controller' => 'Packages', 'action' => 'add',$property['Property']['id'],$property['Property']['name'])); ?></a>
							</li>

							<li>
								<a href="#"><?php echo $this->Html->link(__('Manage Passes'), array('controller' => 'Passes', 'action' => 'add',$property['Property']['id'],$property['Property']['name'])); ?> </a>
							</li>
							<li>
								 <?php
									if($property['Property']['archived']){
										//echo $this->Form->postLink('Restore',['controller'=>'Users','action'=>'restore_all',$property['Property']['id']],['confirm' => __('Are you sure you want to restore this property ?')]);
									}else{
										echo $this->Form->postLink('Archive',['controller'=>'Users','action'=>'archive_all',$property['Property']['id']],['confirm' => __('Are you sure you want to archive this property ?')]);
									}
								?>
							</li>
						</ul>
					</li>
					<li>
						<i class="fa fa-home"></i>
						<?php echo $this->Html->link('Properties',array('controller'=>'properties','action'=>'index')); ?>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<?php echo $this->Html->link('List Properties',array('controller'=>'properties','action'=>'index')); ?>

					</li>
				</ul>
				<!-- END PAGE TITLE & BREADCRUMB-->
			</div>
		</div>
		<div id="flashMessages">
				<h2><?php  echo $this->Session->flash();?></h2>
		</div>
   
<!------------------------------------------------- BEGIN PAGE CONTENT------------------------------------------------------------------------------->
		<div class="row">
			<div class="col-md-12">	
				<div class="portlet box green">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-gift"></i>Property Details
						</div>
						<div class="tools">
							<a href="javascript:;" class="collapse">
							</a>
							<a href="#portlet-config" data-toggle="modal" class="config">
							</a>
						</div>
					</div>
					<div class="portlet-body">
						 <div class="tabbable tabbable-tabdrop">
						<ul class="nav nav-tabs">
							<li class="active">
								<a href="#tab_0" data-toggle="tab" aria-expanded="true"><?php echo __('Property'); ?></a>
							</li>
							<li class="">
								<a href="#tab_1" data-toggle="tab" aria-expanded="false">Parking Locations</a>
							</li>
							<li class="">
								<a href="#tab_2" data-toggle="tab" aria-expanded="false">Passes</a>
							</li>
							<li class="">
								<a href="#tab_3" data-toggle="tab" aria-expanded="false">Administrator</a>
							</li>
							<li class="">
								<a href="#tab_4" data-toggle="tab" aria-expanded="false">Managers</a>
							</li>
							<li class="">
								<a href="#tab_5" data-toggle="tab" aria-expanded="false">Customers</a>
							</li>
							<li class="">
								<a href="#tab_6" data-toggle="tab" aria-expanded="false">Registered Vehicles</a>
							</li>
							<li class="">
								<a href="#tab_7" data-toggle="tab" aria-expanded="false">Documents</a>
							</li>
						</ul>
						<div class="tab-content">
							<!---TAB 0 START--->
							<div class="tab-pane fade active in" id="tab_0">
								<!-- BEGIN FORM-->
								<form class="form-horizontal" role="form">
									<div class="form-body">
										<h2 class="margin-bottom-20">  <?php echo h($property['Property']['name']); ?> <?php
											if($property['Property']['archived']){
												echo '(Archived)';
											}
										?> </h2>
										<h3 class="form-section">General Details</h3>
										<div class="row">
											<div class="col-md-6">
												<div class="form-group">
													<label class="control-label col-md-3">Property Name:</label>
													<div class="col-md-9">
														<p class="form-control-static">
															 <?php echo h($property['Property']['name']); 
																
															 ?>
															 
														</p>
													</div>
												</div>
											</div>
											<!--/span-->
											<div class="col-md-6">
												<div class="form-group">
													<label class="control-label col-md-3">Property logo:</label>
													<div class="col-md-9">
														<p class="form-control-static">
															 <img style="max-height:200px; max-width:400px;" src=<?php echo $this->webroot."img/logo/".h($property['Property']['logo']);?> alt='property logo'?>
														</p>
													</div>
												</div>
											</div>
											<!--/span-->
										</div>
										<!--/row-->
										<div class="row">
											<div class="col-md-6">
												<div class="form-group">
													<label class="control-label col-md-3">Property Sub-Domain:</label>
													<div class="col-md-9">
														<p class="form-control-static">
															 <?php echo h($property['Property']['sub_domain']); ?>
														</p>
													</div>
												</div>
											</div>
										</div>
										<!--/row-->
										<div class="row">
											<div class="col-md-6">
												<div class="form-group">
													<label class="control-label col-md-3">Property Password:</label>
													<div class="col-md-9">
														<p class="form-control-static">
															 <?php echo h($property['Property']['password']); ?>
														</p>
													</div>
												</div>
											</div>
											<!--/span-->
											<div class="col-md-6">
												<div class="form-group">
													<label class="control-label col-md-3">Terms and Conditions:</label>
													<div class="col-md-9">
														<p class="form-control-static">
															  <a class="btn red btn-xs red-stripe" href="javascript:;" data-toggle="modal" data-target="#myModal">Click To View </a>
														</p>
													</div>
												</div>
											</div>
											<!--/span-->
										</div>
										<!--/row-->
										<h3 class="form-section">Property User Settings</h3>
										<div class="row">
											<div class="col-md-6">
												<div class="form-group">
													<label class="control-label col-md-3">Total Passes Per User:</label>
													<div class="col-md-9">
														<p class="form-control-static">
															<?php echo h($property['Property']['total_passes_per_user']); ?>
														</p>
													</div>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label class="control-label col-md-3">Free Guest Credits:</label>
													<div class="col-md-9">
														<p class="form-control-static">
															<?php echo h($property['Property']['free_guest_credits']); ?>
														</p>
													</div>
												</div>
											</div>
										</div>
										<div class="row">
											<!--/span-->
											<div class="col-md-6">
												<div class="form-group">
													<label class="control-label col-md-3">Total Number of Spaces:</label>
													<div class="col-md-9">
														<p class="form-control-static">
															<?php echo h($property['Property']['total_space']); ?>
														</p>
													</div>
												</div>
											</div>
											<div class="col-md-6">
												<div class="form-group">
													<label class="control-label col-md-4">Coupon Registeration Required:</label>
													<div class="col-md-8">
														<p class="form-control-static">
															<?php if($property['Property']['coupon_selection']){
																	echo "YES"; 
																  }else{
																	echo "NO"; 
																  }
															?>
														</p>
													</div>
												</div>
											</div>
											<!--/span-->
										</div>
										<!--/row-->
									</div>
								  </form>
								<!-- END FORM-->
							</div>
							<!---TAB 0 END--->
							<!---TAB 1 START--->
							<div class="tab-pane fade" id="tab_1">
								<?php 
								if(!empty($packages)){
								 ?>
								   <?php $srNo=0;  ?>
								   <h2><?php echo __('Existing Parking Locations'); ?></h2>
									<div class="table-responsive">
										<table id="packages_listing" class="table table-striped table-bordered table-hover">
											<thead>
												<tr>
													<th><?php echo "Sr.No."; ?></th>
													<th><?php echo h('Name'); ?></th>
													<th><?php echo h('Cost'); ?></th>
													<th>From - To</th>
													<th><?php echo h('Pass Assigned'); ?></th>
													<th><?php echo h('Fixed Duration'); ?></th>
													<th><?php echo h('Duration'); ?></th>
													<th><?php echo h('Recurring'); ?></th>
													<th><?php echo h('Guest'); ?></th>
													<th><?php echo h('Free Guest Credits'); ?></th>
													<th><?php echo h('Required'); ?></th>
													<th><?php echo h('Total Space'); ?></th>
													<th><?php echo __('Actions'); ?></th>

												</tr>
											</thead>
											<tbody>                                                                          
												<?php //debug($packages);
													foreach ($packages as $package): ?>
													<tr>
														<td><?php echo h(++$srNo); ?>&nbsp;</td>

														<td><?php echo h($package['Package']['name']); ?>&nbsp;</td>
														<td><?php echo h($package['Package']['cost']); ?>&nbsp;</td>
														<td><?php     
														if($package['Package']['start_date']==null){ echo "NA";}else{
														echo date("m/d/Y", strtotime($package['Package']['start_date']))."<br>"."To"."<br>".date("m/d/Y", strtotime($package['Package']['expiration_date']));
														} ?></td>         		
														<td><?php
																  echo $this->requestAction(array('controller'=>'Passes','action'=>'getPassName',$package['Package']['pass_id']));
																	//echo h($package['Package']['password']); ?>&nbsp;</td>
														<td><?php if($package['Package']['is_fixed_duration']==1){echo h('Yes');}else{echo h('No');} ?>&nbsp;</td>

														<td><?php echo $package['Package']['duration'].' '.$package['Package']['duration_type']; ?>&nbsp;</td>
													

														<td><?php if($package['Package']['is_recurring']==1){echo h('Yes');}else{echo h('No');} ?>&nbsp;</td>

														<td><?php if($package['Package']['is_guest']==1){echo h('Yes');}else{echo h('No');} ?>&nbsp;</td>
														<td><?php echo h($package['Package']['free_guest_credits']); ?>&nbsp;</td>

														<td><?php if($package['Package']['is_required']==1){echo h('Yes');}else{echo h('No');} ?>&nbsp;</td>
														<td><?php echo h($package['Package']['total_spaces_each_package']); ?>&nbsp;</td>
														<td class="actions">

															<?php echo $this->Html->link(__('Edit'), array('controller'=>'Packages','action' => 'edit', $package['Package']['id'],$property['Property']['name'],$property['Property']['id']), array('class' => 'btn green btn-xs green-stripe')); ?>
															<?php
															echo $this->Form->postLink(__('Delete'), array('controller'=>'Packages','action' => 'delete', $package['Package']['id'],$property['Property']['name'],$property['Property']['id']), array('class' => 'btn red btn-xs red-stripe'), __('Are you sure to delete this package ?', $package['Package']['id']));
															//echo $this->Html->link(__('Delete'), array('action' => 'delete', $package['Package']['id'],$propertyName,$propertyId));
															?>
														</td>
													</tr>
											   <?php endforeach; ?>                                                                     
											</tbody>
										</table>
									</div>
								<?php } else {?>
									<h2>No Packages defined yet, Select <b>Actions->Manage Permits</b> to manage permits</h2>
								<?php }?>
							</div>
							<!---TAB 2 END--->
							<!---TAB 3 START--->
							<div class="tab-pane fade" id="tab_2">
								<?php if(!empty($passes)){ ?>
								<h2><?php echo __('Existing Passes'); ?></h2>
								  <div class="table-responsive">

										<table class="table table-striped table-bordered table-hover">
										
										<tr>
												<th><?php echo h('Pass Order'); ?></th>
												<th><?php echo h('Name'); ?></th>
												<th><?php echo h('Property'); ?></th>
												<th><?php echo h('Deposit'); ?></th>
												<th><?php echo h('Cost 1st Year'); ?></th>
												<th><?php echo h('Cost After 1st Year'); ?></th>
												<th><?php echo h('Pass Duration'); ?></th>
												<th><?php echo h('Duration Type'); ?></th>
												<th><?php echo h('Fixed Duration ?'); ?></th>
												<th><?php echo h('Expiration Date'); ?></th>

												<th class="actions"><?php echo __('Actions'); ?></th>
										</tr>
										<?php foreach ($passes as $pass): ?>
										<tr>
											<td><?php //echo h($pass['Pass']['id']);
													   echo h($pass['Pass']['sort_order']); ?>&nbsp;
											</td>
											<td><?php //echo h($pass['Pass']['id']);
												  echo h($pass['Pass']['name']); ?>&nbsp;
											</td>
											<td>
												<?php echo $this->Html->link($property['Property']['name'], array('controller' => 'properties', 'action' => 'view', $property['Property']['id'])); ?>
											</td>
											<td><?php echo h($pass['Pass']['deposit']); ?>&nbsp;</td>
											<td><?php echo h($pass['Pass']['cost_1st_year']); ?>&nbsp;</td>
											<td><?php echo h($pass['Pass']['cost_after_1st_year']); ?>&nbsp;</td>
											<td><?php echo h($pass['Pass']['duration']); ?>&nbsp;</td>
											<td><?php echo h($pass['Pass']['duration_type']); ?>&nbsp;</td>
											 <td><?php if($pass['Pass']['is_fixed_duration']==1){echo h('Yes');}else{echo h('No');} ?>&nbsp;</td>

											<td><?php  if($pass['Pass']['expiration_date']==null){ echo "NA";}else{
											$pass['Pass']['expiration_date']= date("m/d/Y", strtotime($pass['Pass']['expiration_date']));
											echo h($pass['Pass']['expiration_date']);
											}
											?>

											<td class="actions">

												<?php echo $this->Html->link(__('Edit'), array('controller'=>'Passes','action' => 'edit', $pass['Pass']['id'],$property['Property']['id'],$property['Property']['name']), array('class' => 'btn green btn-xs green-stripe')); ?>
												<?php //echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $pass['Pass']['id']), array(), __('Are you sure you want to delete # %s?', $pass['Pass']['id'])); ?>
											</td>
										</tr>
									 <?php endforeach; ?>
										</table>
								  </div>
								<?php }
								else
								{?>
								<h2>No passes defined yet, Select <b>Actions->Manage Passes</b> to manage passes</h2>
								<?php }?>
							</div>
							<!---TAB 3 END--->
							<!---TAB 4 START--->
							<div class="tab-pane fade" id="tab_3">
								 <?php $srNo=0;  ?>
								<div class="table-responsive">
								  <table class="table table-striped table-bordered table-hover">
									 <thead>
										<tr>
										   <th><?php echo "Sr.No."; ?></th>
										   <th><?php echo $this->Paginator->sort('first_name'); ?></th>
										   <th><?php echo $this->Paginator->sort('last_name'); ?></th>
										   <th><?php echo $this->Paginator->sort('username'); ?></th>
										   <th><?php echo $this->Paginator->sort('email'); ?></th>
										   <th><?php echo $this->Paginator->sort('phone'); ?></th>
										</tr>
									  </thead>
									  <tbody>
										   <?php foreach ($propertyAdmin as $customers): $srNo++;?>
										   <tr>
												<td><?php echo h($srNo); ?>&nbsp;</td>

												<td><?php echo h($customers['User']['first_name']); ?>&nbsp;</td>
												<td><?php echo h($customers['User']['last_name']); ?>&nbsp;</td>
												 <td><?php echo $this->Html->link(__($customers['User']['username']), array('controller'=>'Users','action' => 'view_user_details', $customers['User']['id'])); ?></td>

												 <td><?php echo h($customers['User']['email']); ?>&nbsp;</td>
												 <td><?php echo h($customers['User']['phone']); ?>&nbsp;</td>
											</tr>
											 <?php endforeach; ?>

									   </tbody>
									</table>
								</div>
							</div>
							<!---TAB 4 END--->
							<!---TAB 5 START--->
							<div class="tab-pane fade" id="tab_4">
								<div class="clearfix">
									<button class="btn btn-primary" type="button" data-toggle="modal" data-target="#managerModal">Add Manager</button>
								</div>
								<br>
								<?php $srNo=0;  ?>
								<div class="table-responsive">
								  <table class="table table-striped table-bordered table-hover">
									 <thead>
										<tr>
										   <th><?php echo "Sr.No."; ?></th>
										   <th><?php echo ('first_name'); ?></th>
										   <th><?php echo ('last_name'); ?></th>
										   <th><?php echo ('username'); ?></th>
										   <th><?php echo ('email'); ?></th>
										   <th><?php echo ('phone'); ?></th>
										   <th><?php echo ('Assign Property'); ?></th>
										   
										</tr>
									  </thead>
									  <tbody>
										   <?php foreach ($propertyManager as $customers): $srNo++;?>
										   <tr>
												<td><?php echo h($srNo); ?>&nbsp;</td>

												 <td><?php echo h($customers['User']['first_name']); ?>&nbsp;</td>
												 <td><?php echo h($customers['User']['last_name']); ?>&nbsp;</td>
												 <td><?php echo $this->Html->link(__($customers['User']['username']), array('controller'=>'Users','action' => 'view_user_details', $customers['User']['id'])); ?></td>

												 <td><?php echo h($customers['User']['email']); ?>&nbsp;</td>
												 <td><?php echo h($customers['User']['phone']); ?>&nbsp;</td>
												 <td>
													 <button class="btn btn-danger" onclick="setPropertyFunction(<?php echo $customers['User']['id']?>)">Change Property</button>
													 <?php echo $this->Form->postLink('Remove Manager',array('controller'=>'PropertyUsers','action'=>'delete',$customers['User']['id']),array('class'=>'btn btn-warning'),"Do you want to remove manager from this site ? ")?>
												 </td>
											</tr>
											 <?php endforeach; ?>

									   </tbody>
									</table>
								</div>
							</div>
							<!---TAB 5 END--->
							<!---TAB 6 START--->
							<div class="tab-pane fade" id="tab_5">
								 <div class="clear-fix" style="display:none;" id="contactBtn" >	
										<input type="checkbox"  id="bulkDelete"/> <button id="deleteTriger" class="btn btn-danger" >Send Email To Selected Customers</button>
										<span class="help-block">Mark to send mail to all customers present on table </span>
								</div>
								<br>
								<div class="table-responsive">
								  <table id="customers_listing" class="table table-striped table-bordered table-hover" width="100%">
									 <thead>
										<tr>
											<th>
												First Name
											</th>
											<th>
												Last Name
											</th>
											<th>
												Username
											</th>
											<th>
												 Email
											</th>
											<th>
												 Phone
											</th>
											<th>
												 Address Line 1
											</th>
											<th>
												 Apartment # 
											</th>
											<th>
												Property
											</th>
											<th>
												Actions
											</th>
											<!--<th>
												Action
											</th>-->
										</tr>
									  </thead>
									  <tbody>
										   <tr>
												<td colspan="9" class="dataTables_empty">Loading customers...</td>
											</tr>
									   </tbody>
									</table>
								</div>
							</div>
							<!---TAB 6 END--->
							<!---TAB 7 START--->
							<div class="tab-pane fade" id="tab_6">
								<div class="table-responsive">
									 <table id="vehicles_listing" class="table table-striped table-bordered table-hover" width="100%">
									 <thead>
										<tr>
										   <th>User Name</th>
										   <th>Make</th>
										   <th>Model</th>
										   <th>Color</th>
										   <th>Number</th>
										   <th>State</th>
										   <th>Vin</th>
										   <th>Details</th>
										</tr>
									  </thead>
									  <tbody>
										   <tr>
												<td colspan="5" class="dataTables_empty">Loading Vehicles...</td>
											</tr>
									   </tbody>
									</table>
								</div>
							</div>
							<!---TAB 7 END--->
							<!---TAB 8 START--->
							<div class="tab-pane fade" id="tab_7">
								<!--Document Start-->
								<div class="row">
									<div class="col-md-6">
										<h3 align="center">Add Document</h3>
										<?php echo $this->Form->create('Document',['url'=>array('action'=>'add'),'type'=>'file','class'=>'form-horizontal','id'=>'documentForm']); ?>
										<?php
												echo $this->Form->hidden('property_id',['value'=>$property['Property']['id']]);
												echo $this->Form->input('name',array('error'=>array('attributes'=>array('class'=>'model-error')),'required'=>true,'div'=>array('class'=>'form-group'),'label'=>array('class'=>'col-md-3 control-label','text'=>'Document Type'),'between'=>'<div class="col-md-4">','after'=>'</div>','class'=>'form-control','placeholder'=>'Type'));
												echo $this->Form->input('file_name',array('type'=>'file','error'=>array('attributes'=>array('class'=>'model-error')),'required'=>true,'div'=>array('class'=>'form-group'),'label'=>array('class'=>'col-md-3 control-label','text'=>'Document File'),'between'=>'<div class="col-md-4">','after'=>'</div>','class'=>'','placeholder'=>'File'));
										
										?>
										<div class="form-actions fluid">
											<div class="col-md-offset-3 col-md-9">
												<button type="submit" class="btn blue">Submit</button>
											</div>
										</div>
										<?php echo $this->Form->end(); ?>
									</div>
									<div class="col-md-6">
										<h3 align="center">All Documents</h3>
										<!-- BEGIN FORM-->
										<div class="table-container">       
											<div class="table-responsive">
												<table class="table table-striped table-bordered table-hover" id="documents" width="100%">
													<thead>
														<tr>
															<th>Type</th>
															<th>File</th>
															<th>Action</th>
														</tr>
													</thead>
													<tbody>
													   <tr>
															<td colspan="3" align="center">No Data Found</td>
														</tr>
													</tbody>
												</table>
											</div>
										</div>
										<!-- END FORM-->
									</div>
								</div>
								<!---Document END--->
                            </div>
                            <!--Document End-->
							</div>
							<!---TAB 8 END--->
						</div>
					</div>
				</div>
			</div>
		</div>
<!------------------------------------------------- BEGIN PAGE CONTENT Ending Tags------------------------------------------------------------------------------->
	</div>
</div>            
<!----------------------Two Outer Divs Ending Tags----------------------->
 <!-- Term and Condition Modal Start-->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Terms And Conditions</h4>
        </div>
        <div class="modal-body">
          <?php echo $property['Property']['terms_and_conditions']; ?>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
  <!-- Term and Condition Modal End-->
<!--MODAL START-->
<div id="managerModal" class="modal fade" aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" >
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
				<h4>Add/Update Manager for <?php echo $property['Property']['name']; ?></h4>
                <button class="close" aria-hidden="true" data-dismiss="modal" type="button"><i class="fa  fa-times "></i></button>
            </div>
            <div class="modal-body">
                <div id="student_details">
					<?php echo $this->Form->create('PropertyUser',array('url'=>array('contoller'=>'PropertyUsers','action'=>'change_manager')));?>
					<?php echo $this->Form->hidden('property_id',array('value'=>$property['Property']['id'])); ?>
					<?php echo $this->Form->input('user_id',array('type'=>'select','options'=>$managerNotAssign,'required'=>true,'empty'=>'Select One','label'=>false)); ?>
					<?php echo '<br>'.$this->Form->end('Update');?>
                </div>
            </div>
        </div>
    </div>
</div>
<!--MODAL END-->
<!--MODAL START-->
<div id="propertyModal" class="modal fade" aria-hidden="true" role="dialog" tabindex="-1" >
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
				<h4>Change Property</h4>
                <button class="close" aria-hidden="true" data-dismiss="modal" type="button"><i class="fa  fa-times "></i></button>
            </div>
            <div class="modal-body">
                <div id="student_details">
					<?php echo $this->Form->create('PropertyUser',array('url'=>array('contoller'=>'PropertyUsers','action'=>'change_property')));?>
					<?php echo $this->Form->hidden('new_user_id',array('value'=>'')); ?>
					<?php echo $this->Form->hidden('oldproperty',array('value'=>$property['Property']['id'])); ?>
					<?php echo $this->Form->input('property_id',array('type'=>'select','options'=>$allProperties,'required'=>true,'empty'=>'Select One','label'=>false)); ?>
					<?php echo '<br>'.$this->Form->end('Update');?>
                </div>
            </div>
        </div>
    </div>
</div>
<!--MODAL END-->
<div id="myModalForError" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h3 class="modal-title">Send Email</h3>
         <h4 id='returnMessage' style=''></h3>
      </div>
      <div class="modal-body">
		  <?php echo $this->Form->create('Ticket',array('class'=>'form-horizontal','id'=>'reportErrorForm'));?>
			<?php 
					echo $this->Form->hidden('uri',array('value'=>$_SERVER['REQUEST_URI']));
					echo $this->Form->hidden('to_users');
			?>
			<div class="form-group" >   
				<label class="col-md-3 control-label">Subject<font>*</font></label>
				<div class="col-md-9">
					<?php echo $this->Form->input('subject', array('required' => true,'type'=>'text','label' => false, 'div' => false, 'class' => 'form-control')); ?>    
				</div> 
			</div>
			<div class="form-group" >   
				<label class="col-md-3 control-label">Message<font>*</font></label>
				<div class="col-md-9">
					<?php echo $this->Form->input('message', array('required' => true,'type'=>'textarea','label' => false, 'div' => false, 'class' => 'form-control')); ?>    
				</div> 
			</div>
			<div class="form-group">
					<label class="col-md-4 control-label"></label>
					<div class="col-md-8">
						<div class="checkbox-list">
							<label class="checkbox-inline">
								<div class="checker" id="uniform-inlineCheckbox21">
									<span>
										<?php echo $this->Form->input('generate_ticket', array('type'=>'checkbox','label' => false, 'div' => false)); ?>    
									</span>
								</div>Send Message Too
							</label>
						</div>
					</div>
			</div>
			<div class="form-actions">
				<div class="row">
					<div class="col-md-offset-3 col-md-9">
						<button type="submit" class="btn green">Submit</button>
					</div>
				</div>
			</div>
		<?php echo $this->Form->end(); ?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

 <?php echo $this->Html->script('block.js'); 
	   echo $this->Html->script('/assets/global/plugins/bootstrap-tabdrop/js/bootstrap-tabdrop.js');
?>
<?php echo $this->Html->script('jquery.validate.min.js'); ?>
<script type="text/javascript">
	$('#contactBtn').show();
	/*$.blockUI({ css: { 
						border: 'none', 
						padding: '15px', 
						backgroundColor: '#000', 
						'-webkit-border-radius': '10px', 
						'-moz-border-radius': '10px', 
						opacity: .5, 
						color: '#fff' 
			} }); */
		$('#customers_listing').dataTable({
			"bProcessing": false,
			"bServerSide": true,
			 "dom": 'lBfrtip',
			 "bDestroy": true,
			 "initComplete": function(settings, json) {
												$.unblockUI();
											 },
			
			 buttons: [
				{
					extend: 'print',
					exportOptions: {
						columns: [ 0, ':visible' ]
					},
					customize: function ( win ) {
						$(win.document.body)
							.css( 'font-size', '10pt' )
							.prepend(
								'<img src="<?php echo "https://internetparkingpass.com/img/onlineparking-logo.png" ?>" alt="" style="position:absolute; opacity:0.3; top:40%; left:30%;" />'
							);
	 
						$(win.document.body).find( 'table' )
							.addClass( 'compact' )
							.css( 'font-size', 'inherit' );
					}
				},
				{
					extend: 'csv',
					 exportOptions: {
						columns: [ 0, ':visible' ]
					}
				},
				{
					extend: 'excel',
					 exportOptions: {
						columns: [ 0, ':visible' ]
					}
				},
				{
					extend: 'colvis',
					 text: "Select Columns"
				}
				
			],
		   "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
		   oLanguage: {
				sInfoFiltered: "",
				sLengthMenu: "Show _MENU_ entries.&nbsp;&nbsp;",
			},
			
			"columnDefs": [
				{ "searchable": false, "targets": 4 },
				{ "searchable": false, "targets": 5},
				{ "searchable": false, "targets": 6 },
				{ "searchable": false, "targets": 7 },
				{
					
					"render": function ( data, type, row ) {
								str='<a class="btn green btn-xs green-stripe" href="/admin/users/edit/'+row.id+'">Edit</a>';
								str+='<a class="btn default btn-xs " href="/admin/users/view_user_details/'+row.id+'">View</a>';
								str+='<a class="btn yellow btn-xs yellow-stripe" target="_blank" href="/admin/users/print_details/'+row.id+'">Print</a>';
								str+='<a class="btn red btn-xs red-stripe" href="/admin/CustomerPasses/customer_view/'+row.id+'">Manage</a>';
								str+="<input type='checkbox' class='deleteRow' value='"+row.id+"*"+row.name+"*"+row.email+"*"+row.phone+"'><span class='label label-sm label-success'>Send Email</span></input>" ;
								//var token = Math.floor((Math.random() * 100000000111111) + 1);
								//str+= '&nbsp;<form name="post_'+token+'" style="display:none;" method="post" action="/admin/Users/archive_user/'+row.id+'/1"><input  type="hidden" name="_method" value="POST"></form><a href="#"  data-toggle="tooltip" title="Mark Archived" onclick="if (confirm(&quot;Are you sure you want to mark this user archived ?&quot;)) { document.post_'+token+'.submit(); } event.returnValue = false; return false;" class="label label-sm label-danger"><i class="fa fa-archive" aria-hidden="true"></i> ARCHIVE</a>';			
								return str;
					},
					"searchable": false,
					"targets": 8
				}
			],
			"sAjaxSource":"<?php echo $this->Html->Url(array('controller' => 'users', 'action' => 'get_customers',$property['Property']['id'])); ?>",
		});


$(function(){
	$('#vehicles_listing').dataTable({
		"bProcessing": false,
        "bServerSide": true,
        "sAjaxSource": "<?php echo $this->Html->Url(array('controller' => 'vehicles', 'action' => 'get_property_vehicle_details',$property['Property']['id'])); ?>"
	});
	
});
function setPropertyFunction(manager){
	$('#PropertyUserNewUserId').attr('value',manager);
	$("#propertyModal").modal();
	console.log(manager);
}
$("#bulkDelete").on('click', function() { // bulk checked
        var status = this.checked;

        $(".deleteRow").each(function() {
            $(this).prop("checked", status);
        });
    });
$('#deleteTriger').on("click", function(event) {// triggering delete one by one
       
		
        if ($('.deleteRow:checked').length > 0) {  // at-least one checkbox checked
            var ids = [];
            $('.deleteRow').each(function() {
                if ($(this).is(':checked')) {
                    ids.push($(this).val());
                }
            });
			ids_string = ids.toString(); 
			$("#returnMessage").html('');
			$("#TicketSubject").val('');
			$("#TicketMessage").val('');
			$('#TicketToUsers').val(ids_string);
			$('#myModalForError').modal();

        } else {
			alert('Select Customers');
        }
    });
$("#reportErrorForm").submit(function(e)
	{
		e.preventDefault(); //STOP default action
		$.blockUI({
            css: {
                border: 'none',
                padding: '15px',
                margin: 0,
                width: '100%',
                top: '200px',
                left: '0px',
                textAlign: 'center',
                color: '#B35900',
                backgroundColor: 'transparent',
                fontWeight: 'bold',
            },
            overlayCSS: {
                backgroundColor: '#2F2F2F',
                opacity: '0.5',
            },
            message: '<h2><img src="/../../app/webroot/img/busy2.gif", />Loading</h2>'});
		var postData = $(this).serializeArray();
		//console.log(postData);
		$.ajax(
		{
			url : '/admin/Tickets/send_email_bulk',
			type: "POST",
			data : postData,
			success:function(data, textStatus, jqXHR) 
			{
				if(data=='true'){
					$("#returnMessage").html('Message Sent Successfully');
					$("#returnMessage").attr('style','color:green;');
				}else{
					$("#returnMessage").html('Message Cannot Be Sent, Please Try Later');
					$("#returnMessage").attr('style','color:red;');
				}
				  $.unblockUI();
			},
			error: function(jqXHR, textStatus, errorThrown) 
			{
				//if fails      
			}
		});
		
	});
	$("#documentForm").validate({
	rules: {
		documentName: {
			required: true
		},
		 documentFileName: {
			required: true
		}
	},
	messages: {
		documentName: {required: "Please enter document type name."

		},
		documentFileName: {required: "Please add the document."

		}
	},
	
});
$('#documents').DataTable({
	"bProcessing": false,
	"bServerSide": true,
	"bDestroy": true,
	"sAjaxSource": "/admin/Documents/get_documents/<?php echo $property['Property']['id']; ?>",
	"initComplete": function(settings, json) {
						if(json.iTotalDisplayRecords==0){
							$( "#documents" ).find( ".dataTables_empty" ).attr('colspan',$('#documents thead th').length);
						}
	},
	"columnDefs": [
		{
			"targets": 1,
			"data": function (row, type, val, meta) {
				str='<a class= "label label-sm label-danger" target="_blank;" href="/files/docs/'+row.file_name+'">View</a>';
				return str;
			}
		},
		{
			"targets": 2,
			"data": function (row, type, val, meta) {
				var token = Math.floor((Math.random() * 100000000111111) + 1);
				str= '<form name="post_'+token+'" style="display:none;" method="post" action="/admin/Documents/delete/'+row.id+'"><input  type="hidden" name="_method" value="POST"></form><a href="#" class="label label-sm label-success" onclick="if (confirm(&quot;Are you sure you want to delete this document ?&quot;)) { document.post_'+token+'.submit(); } event.returnValue = false; return false;">Delete</a>';
				return str;
			}
		}
	]
});
</script>

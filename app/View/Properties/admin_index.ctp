
<div class="page-content-wrapper">
		<div class="page-content">
<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">Properties <small> List of properties</small>
					</h3>
					<ul class="page-breadcrumb breadcrumb">
						
						<li>
							<i class="fa fa-home"></i>
							<?php echo $this->Html->link('Properties',array('controller'=>'properties','action'=>'index')); ?>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<?php echo $this->Html->link('List Properties',array('controller'=>'properties','action'=>'index')); ?>
							
						</li>
						
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
			<div id="flashMessages">
                	<h2><?php  echo $this->Session->flash();?></h2>
                </div>
            <div class="row">
				<div class="col-md-12">
					<!-- BEGIN EXAMPLE TABLE PORTLET-->
					<div class="portlet box grey-cascade">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-users"></i>Properties List
							</div>
							<div class="tools">
								<a href="javascript:;" class="collapse">
								</a>
								
							</div>
						</div>
						<div class="portlet-body">
							<div class="table-toolbar">
								<div class="btn-group">
                                <?php echo $this->Html->link('Add New <i class="fa fa-plus"></i>',array('controller'=>'properties','action'=>'add'),array('escape'=>false,'class'=>'btn green')); ?>
									
								</div>
								
							</div>
								<div class="table-scrollable">
								<table id="property_list"class="table table-striped table-bordered table-hover"> 
									<thead>
									<tr>
										<th>
											Name
										</th>
										<th>
											Password
										</th>
										<th>
											Sub Domain
										</th>
										<th>
											 Passes Per User
										</th>
										<th>
											 Total Space
										</th>
										<th>
											 Guest Credits
										</th>
										<th>
											Archived
										</th>
										<th>
											Actions
										</th>
										<!--<th>
											Action
										</th>-->
									</tr>
									</thead>
									<tbody>
										<tr>
											<td colspan="10" class="dataTables_empty">Loading Data...</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
					<!-- END EXAMPLE TABLE PORTLET-->
				</div>
			</div>

</div>
</div>
<?php //echo $this->Html->script('//code.jquery.com/jquery-1.12.0.min.js'); ?>
<?php echo $this->Html->script('//cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js'); ?>
<?php echo $this->Html->css('//cdn.datatables.net/1.10.11/css/jquery.dataTables.min.css'); ?>

<script type="text/javascript">
 $('#property_list').dataTable({ 
			"bProcessing": false,
			"bServerSide": true,
			 "bDestroy": true,
			 "columnDefs": [{"className": "dt-center", "targets": "_all"},
							{
								"targets": 0,
								"data": function ( row, type, val, meta ) {
												    str = "&nbsp;<a  href='/admin/Properties/view/"+row.id+"'>"+row.name+"</a>";			
												    return str;
								}
						   },
						   {
								"targets": 6,
								"data": function ( row, type, val, meta ) {
												    if(row.archived==1){		
														return "<span class='label label-sm label-danger'>YES</span>";
													}else{
														return "<span class='label label-sm label-success'>NO</span>";
													}	
								}
						   },
						    {
								"targets": 7,
								"data": function ( row, type, val, meta ) {
												    str = "&nbsp;<a class='label label-sm label-success' href='/admin/Properties/edit/"+row.id+"/"+row.name+"'>Edit</a>";	
												    str += "&nbsp;<a class='label label-sm label-warning' href='/admin/Properties/view/"+row.id+"'>View</a>";					
												    str += "&nbsp;<a class='label label-sm label-danger' target='_blank;' href='/admin/PropertyUsers/create_csv/"+row.id+"'>CSV</a>";													
												    return str;
								}
						   }
						],
			"sAjaxSource":"/admin/Properties/getProperties"
		});
	

</script>

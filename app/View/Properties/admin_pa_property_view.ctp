<div class="page-content-wrapper">
		<div class="page-content" style="min-height:1089px">
			
			<!-- BEGIN PAGE HEADER-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
					<?php echo CakeSession::read('PropertyName');?> <small> Details</small>
					</h3>
					<ul class="page-breadcrumb breadcrumb">
						<li>
							<i class="fa fa-home"></i>
							<?php echo $this->Html->link(
                                              'Home',
                                              array(
                                                 'controller' => 'Users',
                                                 'action' => 'pa_home_page',
                                                 'full_base' => true
                                                                  )
                                              );?>
							<i class="fa fa-angle-right"></i>
						</li>

						<li>
							<a href="#">Property Details</a>
						</li>
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<div class="tabbable tabbable-custom boxless tabbable-reversed">
						<ul class="nav nav-tabs">
							<li class="active">
								<a data-toggle="tab" href="#tab_0">
								Property Details </a>
							</li>
							<li class="">
								<a data-toggle="tab" href="#tab_1">
								Parking Packages </a>
							</li>
							<li class="">
								<a data-toggle="tab" href="#tab_2">
								Passes </a>
							</li>
						</ul>
						<div class="tab-content">
						  <!----------------------------------------TAB Start------------------------->
							<div id="tab_0" class="tab-pane active">
								<div class="portlet box blue">
									<div class="portlet-title">
										<div class="caption">
											<i class="fa fa-gift"></i><?php echo CakeSession::read('PropertyName');?> Details
										</div>
										<div class="tools">
											<a class="collapse" href="javascript:;">
											</a>
										</div>
									</div>
									<div class="portlet-body form">
										<form class="form-horizontal" role="form">		  
											  <div class="row"> 
												  <div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Name:</label>
															<div class="col-md-9">
																<p class="form-control-static">
																	 <?php echo $property['Property']['name'];?>
																</p>
															</div>
														</div>
													</div>
													<!--/span-->
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Password:</label>
															<div class="col-md-9">
																<p class="form-control-static">
																	 <?php echo $property['Property']['password'];?>
																</p>
															</div>
														</div>
													</div>
													<!--/span-->
												</div>
												<!--/row-->
												<div class="row">
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Sub Domain:</label>
															<div class="col-md-9">
																<p class="form-control-static">
																	 <?php echo $property['Property']['sub_domain'];?>
																</p>
															</div>
														</div>
													</div>
													<!--/span-->
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Passes Per User:</label>
															<div class="col-md-9">
																<p class="form-control-static">
																	 <?php echo $property['Property']['total_passes_per_user'];?>
																</p>
															</div>
														</div>
													</div>
													<!--/span-->
												</div>
												<!--/row-->
												<div class="row">
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Free Guest Credits:</label>
															<div class="col-md-9">
																<p class="form-control-static">
																	 <?php echo $property['Property']['free_guest_credits'];?>
																</p>
															</div>
														</div>
													</div>
													<!--/span-->
													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label col-md-3">Total Space:</label>
															<div class="col-md-9">
																<p class="form-control-static">
																	 <?php echo $property['Property']['total_space'];?>
																</p>
															</div>
														</div>
													</div>
												</div>
										 </form>
									</div>
								</div>
							</div>
							<div id="tab_1" class="tab-pane">
								<div class="portlet box blue">
									<div class="portlet-title">
										<div class="caption">
											<i class="fa fa-gift"></i>Parking Packages Details
										</div>
										<div class="tools">
											<a class="collapse" href="javascript:;">
											</a>
										</div>
									</div>
									<div class="portlet-body">
										<!-- BEGIN TABLE-->
											<?php if(!empty($packages)){
											  $srNo=0;  
											?>
														<div class="table-responsive">
																<table id="packages_listing" class="table table-striped table-bordered table-hover">
																		<thead>
                                                							<tr>
                                                                                        <th><?php echo "Sr.No."; ?></th>

                                                                               			<th><?php echo h('Name'); ?></th>
                                                                               			<th><?php echo h('Cost'); ?></th>
                                                                               			<th>From - To</th>
                                                                               			<th><?php echo h('Pass Assigned'); ?></th>
                                                                               			<th><?php echo h('Fixed Duration'); ?></th>
                                                                               			<th><?php echo h('Duration'); ?></th>
                                                                               			<th><?php echo h('Recurring'); ?></th>
                                                                                        <th><?php echo h('Guest'); ?></th>
                                                                                        <th><?php echo h('Free Guest Credits'); ?></th>
                                                                                        <th><?php echo h('Required'); ?></th>
                                                                               		                                                                            			
                                                                            </tr>
																		</thead>
																		<tbody>                                                                          
                                                                            <?php //debug($packages);
                                                                                        foreach ($packages as $package): ?>
                                                                           	<tr>
                                                                           		<td><?php echo h(++$srNo); ?>&nbsp;</td>

                                                                                <td><?php echo h($package['Package']['name']); ?>&nbsp;</td>
                                                                           		<td><?php echo h($package['Package']['cost']); ?>&nbsp;</td>
                                                                           		<td><?php     
                                                                           		if($package['Package']['start_date']==null){ echo "NA";}else{
                                                                           		echo date("m/d/Y", strtotime($package['Package']['start_date']))."<br>"."To"."<br>".date("m/d/Y", strtotime($package['Package']['expiration_date']));
                                                                           		} ?></td>         		
                                                                                <td><?php
                                                                                          echo $this->requestAction(array('controller'=>'Passes','action'=>'getPassName',$package['Package']['pass_id']));
                                                                                            //echo h($package['Package']['password']); ?>&nbsp;</td>
                                                                           		<td><?php if($package['Package']['is_fixed_duration']==1){echo h('Yes');}else{echo h('No');} ?>&nbsp;</td>

                                                                           		<td><?php echo $package['Package']['duration'].' '.$package['Package']['duration_type']; ?>&nbsp;</td>
                                                                           	

                                                                           		<td><?php if($package['Package']['is_recurring']==1){echo h('Yes');}else{echo h('No');} ?>&nbsp;</td>

                                                                           		<td><?php if($package['Package']['is_guest']==1){echo h('Yes');}else{echo h('No');} ?>&nbsp;</td>
                                                                           		<td><?php echo h($package['Package']['free_guest_credits']); ?>&nbsp;</td>

                                                                           		<td><?php if($package['Package']['is_required']==1){echo h('Yes');}else{echo h('No');} ?>&nbsp;</td>
                                                                           		
                                                                           	</tr>
                                                                           <?php endforeach; ?>                                                                     
																		</tbody>
                                               
																	</table>
															</div>	
                                            <?php }?>									
										<!-- END TABLE-->
									</div>
								</div>
							</div>
							<div id="tab_2" class="tab-pane">
								<div class="portlet box green">
									<div class="portlet-title">
										<div class="caption">
											<i class="fa fa-gift"></i>Pass Details
										</div>
										<div class="tools">
											<a class="collapse" href="javascript:;">
											</a>
										</div>
									</div>
									<div class="portlet-body">
										<!-- BEGIN FORM-->
										
											 <?php if(!empty($passes)){?>
												<div class="table-responsive">
													<table class="table table-striped table-bordered table-hover">
														<thead>
															<th><?php echo h('Name'); ?></th>
															<th><?php echo h('Deposit'); ?></th>
															<th><?php echo h('Cost 1st Year'); ?></th>
															<th><?php echo h('Cost After 1st Year'); ?></th>
															<th><?php echo h('Pass Duration'); ?></th>
															<th><?php echo h('Duration Type'); ?></th>
															<th><?php echo h('Fixed Duration ?'); ?></th>
															<th><?php echo h('Expiration Date'); ?></th>
														</thead>
														<tbody> 
															<?php foreach ($passes as $pass): ?>
																<tr>																
																	<td>
																		<?php echo h($pass['Pass']['name']); ?>
																	</td>
																	<td>
																		<?php echo h($pass['Pass']['deposit']); ?>&nbsp;
																	</td>
																	<td>
																		<?php echo h($pass['Pass']['cost_1st_year']); ?>&nbsp;
																	</td>
																	<td>
																		<?php echo h($pass['Pass']['cost_after_1st_year']); ?>&nbsp;
																	</td>
																	<td>
																		<?php echo h($pass['Pass']['duration']); ?>&nbsp;
																	</td>
																	<td>
																		<?php echo h($pass['Pass']['duration_type']); ?>&nbsp;
																	</td>
																	<td>
																		<?php if($pass['Pass']['is_fixed_duration']==1){echo h('Yes');}else{echo h('No');} ?>&nbsp;
																	</td>
																	<td>
																		<?php  if($pass['Pass']['expiration_date']==null){ echo "NA";}else{
																				$pass['Pass']['expiration_date']= date("m/d/Y", strtotime($pass['Pass']['expiration_date']));
																				echo h($pass['Pass']['expiration_date']);
																			   }
																		?>
																</tr>
															  <?php endforeach; ?>
														</tbody>
												    </table>
												</div>
											 <?php }?>
										
										<!-- END FORM-->
									</div>
								</div>
							</div>
			             <!----------------------------------------TAB END------------------------->
						</div>
					</div>
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>

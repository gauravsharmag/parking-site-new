<div class="page-content-wrapper">
		<div class="page-content" style="min-height:1027px">
			<!-- BEGIN PAGE HEADER-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
					Passes <small>Property Wide Guest Passes Costs</small>
					</h3>
					<ul class="page-breadcrumb breadcrumb">
						<li>
							<i class="fa fa-home"></i>
							<a href="/admin/Users/superAdminHome">Home</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="#">Property Wide Guest Pass Cost (valid from 5th December, 2015)</a>
						</li>
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
			<!-- END PAGE HEADER-->
			<div id="flashMessages">
									<h2><?php  echo $this->Session->flash();?></h2>
			</div>
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					
					<!-- Begin: life time stats -->
					<div class="portlet">
						<div class="portlet-title">
							<div class="row">
									<div class="col-md-3">
											<div data-date-format="mm/dd/yyyy" data-date="12/05/2015" class="input-group input-medium date-picker input-daterange col-md-12">
												<input type="text" name="from" id="from" class="form-control">
												<span class="input-group-addon">
												to </span>
												<input type="text" name="to" id="to" class="form-control">
											</div>
											<!-- /input-group -->
											<span class="help-block" id="helpBlock">
											Select Date </span>
									</div>
									<div class="col-md-1"> 
											<input type="submit" class="btn yellow" value="Go" name="submit" id="gobtn">
									</div>
							</div>
						</div>
						<div class="portlet-body" id='portletBody'>
							
						</div>
					</div>
					<!-- End: life time stats -->
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
<?php echo $this->Html->script('block.js'); ?>
<script type="text/javascript">
jQuery(document).ready(function ($) {
	
	$("#gobtn").click(function () {
				if($('#from').val()){
					$.blockUI({ css: { 
						border: 'none', 
						padding: '15px', 
						backgroundColor: '#000', 
						'-webkit-border-radius': '10px', 
						'-moz-border-radius': '10px', 
						opacity: .5, 
						color: '#fff' 
					} });
					$('#helpBlock').html("Select Date");
					$('#helpBlock').css('color','grey');
					var toDate="";
					if($('#to').val()){
						toDate=$('#to').val();
					}else{
						var dt = new Date();
						$('#to').val((dt.getMonth()+1) + '/' + (dt.getDate()-1) + '/' +  dt.getFullYear());
						toDate=(dt.getMonth()+1) + '/' + (dt.getDate()-1) + '/' +  dt.getFullYear();
					}
					var data = {};
					data[0]=$('#from').val();
					data[1]=toDate;
					var dataSend = JSON.stringify(data);
					$.ajax({
							url: "/admin/Transactions/new_guest_pass_cost_listing?toDate="+toDate+"&fromDate="+$('#from').val(),
							type: "get",
							//data: dataSend, 
							dataType: "json",
							success: function (response) {
								document.getElementById("portletBody").innerHTML='';
								var htmlString='<div class="note note-warning"><p>Guest Pass Amount (valid from 5th December, 2015) </p></div><div class="row static-info"><div class="col-md-4 value">PROPERTY</div><div class="col-md-8 value"><div class="col-md-2 name" align="right"><b>TOTAL DAYS</b></div><div class="col-md-2 name" align="right"><b>FREE</b></div><div class="col-md-2 name" align="right"><b>PAID(DAYS)</b></div><div class="col-md-2 name" align="right"><b>PAID(AMOUNT)</b><br>($)</div></div></div>';
								for(var i=0;i<(response.length)-4;i++){
									if(response[i].Property.freeDays==null){
										response[i].Property.freeDays=0;
									}
									if(response[i].Property.paidDays==null){
										response[i].Property.paidDays=0;
									}
									if(response[i].Property.paidAmount==null){
										response[i].Property.paidAmount=0;
									}
									if(response[i].Property.days==null){
										response[i].Property.days=0;
									}
									htmlString=htmlString+'<div class="row static-info"><div class="col-md-4 name">'+response[i].Property.name+'</div><div class="col-md-8 value"><div class="col-md-2 value" align="right">'+response[i].Property.days+'</div><div class="col-md-2 value" align="right">'+response[i].Property.freeDays+'</div><div class="col-md-2 value" align="right">'+response[i].Property.paidDays+'</div> <div class="col-md-2 value" align="right">  '+response[i].Property.paidAmount+'</div></div></div>';
								//	console.log(response[i]);
								}
								if(response[(response.length)-1].totalDays==null){
									response[(response.length)-1].totalDays=0;
								}
								if(response[(response.length)-2].totalPaidDays==null){
									response[(response.length)-2].totalPaidDays=0;
								}
								if(response[(response.length)-3].totalFreeDays==null){
									response[(response.length)-3].totalFreeDays=0;
								}
								if(response[(response.length)-4].TotalAmount==null){
									response[(response.length)-4].TotalAmount=0;
								}
						
								htmlString=htmlString+'<div class="row static-info"><div class="col-md-4 name"><b>TOTAL</b></div><div class="col-md-8 value"><div class="col-md-2 value" align="right">'+response[(response.length)-1].totalDays+'</div><div class="col-md-2 value" align="right">'+response[(response.length)-3].totalFreeDays+'</div><div class="col-md-2 value" align="right">'+response[(response.length)-2].totalPaidDays+'</div><div class="col-md-2 value" align="right">  '+response[(response.length)-4].TotalAmount+'</div></div></div>';

								document.getElementById("portletBody").innerHTML=htmlString;
								//console.log(response[0]);
								$.unblockUI();
							}
					});
				}else{
					$('#helpBlock').html("Select From date");
					$('#helpBlock').css('color','red')
				}
	});
	  
});
</script>

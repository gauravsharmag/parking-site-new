<div class="transactions form">
<?php echo $this->Form->create('Transaction'); ?>
	<fieldset>
		<legend><?php echo __('Admin Edit Transaction'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('user_id');
		echo $this->Form->input('date_time');
		echo $this->Form->input('amount');
		echo $this->Form->input('result');
		echo $this->Form->input('pass_id');
		echo $this->Form->input('message');
		echo $this->Form->input('credits_used');
		echo $this->Form->input('payment_method_used');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Transaction.id')), array(), __('Are you sure you want to delete # %s?', $this->Form->value('Transaction.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Transactions'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Passes'), array('controller' => 'passes', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Pass'), array('controller' => 'passes', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Customer Passes'), array('controller' => 'customer_passes', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Customer Pass'), array('controller' => 'customer_passes', 'action' => 'add')); ?> </li>
	</ul>
</div>

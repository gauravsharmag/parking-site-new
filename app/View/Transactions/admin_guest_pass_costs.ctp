<div class="page-content-wrapper">
		<div class="page-content" style="min-height:1027px">
			<!-- BEGIN PAGE HEADER-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
					Passes <small>Property Wide Guest Passes Costs</small>
					</h3>
					<ul class="page-breadcrumb breadcrumb">
						<li>
							<i class="fa fa-home"></i>
							<a href="/admin/Users/superAdminHome">Home</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="#">Property Wide Guest Pass Cost</a>
						</li>
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
			<!-- END PAGE HEADER-->
			<div id="flashMessages">
									<h2><?php  echo $this->Session->flash();?></h2>
			</div>
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					
					<!-- Begin: life time stats -->
					<div class="portlet">
						<div class="portlet-title">
							
						</div>
						<div class="portlet-body" >
							<div class="row">
								 <div class="col-md-12">
									<?php echo $this->Form->create('',array('class'=>'form-inline'));?>
										<div class="form-group">
											 <div data-date-format="mm/dd/yyyy" data-date="08/15/2014" class="input-group input-medium date-picker input-daterange col-md-12">
												<input type="text" name="from" id="from" class="form-control">
												<span class="input-group-addon">
												to </span>
												<input type="text" name="to" id="to" class="form-control">
												
											</div>
										</div>
											<input type="button" class="btn yellow" value="Go" name="submit" id="gobtn">
									<?php echo $this->Form->end();?>
								</div>
							</div>
							<hr>
							<div id='portletBody'></div>
						</div>
					</div>
					<!-- End: life time stats -->
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
<?php echo $this->Html->script('block.js'); ?>
<?php echo $this->Html->script('//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.min.js'); ?> 
<?php echo $this->Html->script('/assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js'); ?>
<?php echo $this->Html->script('/assets/admin/pages/scripts/components-pickers.js');   ?>
<?php echo $this->Html->css('/assets/global/plugins/bootstrap-datepicker/css/datepicker3.css'); ?>
<script>
    jQuery(document).ready(function() {     
      ComponentsPickers.init(); 
    });
</script>


<script type="text/javascript">
jQuery(document).ready(function ($) {
	
	$("#gobtn").click(function () {
				if($('#from').val()){
					$.blockUI({    
							message: '<h4>Loading..</h4>',  
							css: { 
									border: 'none', 
									padding: '8px', 
									backgroundColor: '#000', 
									'-webkit-border-radius': '10px', 
									'-moz-border-radius': '10px', 
									opacity: .5, 
                                    width:'35%',
									color: '#fff' 
						}
				  });
					$('#helpBlock').html("Select Date");
					$('#helpBlock').css('color','grey');
					var toDate="";
					if($('#to').val()){
						toDate=$('#to').val();
					}else{
						var dt = new Date();
						$('#to').val((dt.getMonth()+1) + '/' + (dt.getDate()-1) + '/' +  dt.getFullYear());
						toDate=(dt.getMonth()+1) + '/' + (dt.getDate()-1) + '/' +  dt.getFullYear();
					}
					var data = {};
					data[0]=$('#from').val();
					data[1]=toDate;
					var dataSend = JSON.stringify(data);
					$.ajax({
							url: "/admin/Transactions/guest_pass_cost_listing?toDate="+toDate+"&fromDate="+$('#from').val(),
							type: "get",
							//data: dataSend, 
							dataType: "json",
							success: function (response) {
								document.getElementById("portletBody").innerHTML='';
								var htmlString='<div class="note note-warning"><p>Guest Pass Amount</p></div><div class="table-scrollable"><table class="table table-striped table-bordered table-hover"><thead><tr><th>PROPERTY</th><th>FREE</th><th>PAID(DAYS)</th><th>PAID AMOUNT ($)</th></tr></thead><tbody>';
								for(var i=0;i<(response.length)-3;i++){
									if(response[i].Property.freeDays==null){
										response[i].Property.freeDays=0;
									}
									if(response[i].Property.paidDays==null){
										response[i].Property.paidDays=0;
									}
									if(response[i].Property.paidAmount==null){
										response[i].Property.paidAmount=0;
									}
									htmlString=htmlString+'<tr><td>'+response[i].Property.name+'</td><td>  '+response[i].Property.freeDays+'</td><td>'+response[i].Property.paidDays+'</td><td>'+response[i].Property.paidAmount+'</td></tr>';
								}
								if(response[(response.length)-1].totalPaidDays==null){
									response[(response.length)-1].totalPaidDays=0;
								}
								if(response[(response.length)-2].totalFreeDays==null){
									response[(response.length)-2].totalFreeDays=0;
								}
								if(response[(response.length)-3].TotalAmount==null){
									response[(response.length)-3].TotalAmount=0;
								}
								htmlString=htmlString+'<tr><td><b>TOTAL</b></td><td> '+response[(response.length)-2].totalFreeDays+'</td><td> '+response[(response.length)-1].totalPaidDays+'</td><td> '+response[(response.length)-3].TotalAmount+'</td></tr></tbody></table></div>';

								document.getElementById("portletBody").innerHTML=htmlString;
								//console.log(response[0]);
								$.unblockUI();
							}
					});
				}else{
					alert('Select dates');
				}
	});
	  
});
</script>

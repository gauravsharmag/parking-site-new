
<div class="page-content-wrapper">
		<div class="page-content" style="min-height:1027px">
			<!-- BEGIN PAGE HEADER-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
					Search <small>Search Results</small>
					</h3>
					<ul class="page-breadcrumb breadcrumb">
						<li>
							<i class="fa fa-home"></i>
							<?php 
								if(AuthComponent::user('role_id')==1){
										echo $this->Html->link('Home',array(
                                          'controller' => 'Users',
                                          'action' => 'superAdminHome',
                                          'full_base' => true
                                      ));
                                    }elseif(AuthComponent::user('role_id')==2){
										echo $this->Html->link('Home',array(
                                          'controller' => 'Users',
                                          'action' => 'pa_home_page',
                                          'full_base' => true
                                      ));
                                    }
                                  
                               ?>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="#">Search Results</a>
						</li>
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
			<!-- END PAGE HEADER-->
			<div id="flashMessages">
									<h2><?php  echo $this->Session->flash();?></h2>
			</div>
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					
					<!-- Begin: life time stats -->
					<div class="portlet">
						<div class="portlet-title">
							<div class="caption">
								Search
							</div>
						</div>
						<div class="portlet-body">
							<div class="nav-pills">
								<?php echo $this->Form->create('Transaction',array('class'=>'form-horizontal'))?>
									  <div class="input-group col-md-3">
										<?php echo $this->Form->input('search_key',array('placeholder'=>'Search...','div'=>false,'class'=>'form-control','label'=>false,'required'=>true));?>
											<span class="input-group-btn">
												<button class="btn submit yellow">GO</button>
											</span>
									</div>
								<?php echo $this->Form->end();?>
							</div>
							<hr>
							<div class="table-container">		
								<div class="table-responsive">
                                              <table id="search" class="table table-striped table-bordered table-hover">
                                            	 <thead>
                                            	   	<tr>
													   <th>Transaction ID</th>
													   <th>Billing Name</th>
                                                       <th>User</th>
                                                       <th>Date</th>
                                                       <th>Amount</th>
                                                       <th>Result</th> 
                                                       <th>Pass</th>
                                                    </tr>
                        						  </thead>
                                            	  <tbody>
													   <?php if(!empty($results)){
																foreach($results as $result){
														   ?>
															<tr>
																<td><?php echo $result['Transaction']['paypal_tranc_id']; ?></td>
																<td><?php echo $result['Transaction']['first_name'].' '.$result['Transaction']['last_name']; ?></td>
																<td><?php echo $result['User']['first_name'].' '.$result['User']['last_name']; ?><br>
																	<a href="/admin/users/view_user_details/<?php echo $result['User']['id'];?>"><?php echo $result['User']['username'];?></a><br>
																	<?php echo $result['User']['phone']; ?>
																</td>
																<td><?php echo date('m/d/Y H:i:s',strtotime($result['Transaction']['date_time'])); ?></td>
																<td><?php echo "$ ".$result['Transaction']['amount']; ?></td>
																<td><?php echo $result['Transaction']['result']; ?></td>
																<td><?php echo $result['Transaction']['pass_id']; ?></td>
															</tr>
														<?php }}else{?> 
															 <tr>
																	<td colspan="7"> No Results Found</td>
															</tr>
															
														<?php	}?>
                                            	   </tbody>
                                            	</table>
												
                                        </div>
							</div>
						</div>
					</div>
					<!-- End: life time stats -->
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
 <?php if(!empty($results)){?>
<?php echo $this->Html->script('//cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js'); ?>
<?php echo $this->Html->css('//cdn.datatables.net/1.10.11/css/jquery.dataTables.min.css'); ?>
<script type="text/javascript">
$(document).ready(function() {
	$('#search').dataTable();
	//$('#search').show();
});
</script>
<?php }?>


<div class="page-content-wrapper">
		<div class="page-content" style="min-height:996px">
			<!-- BEGIN PAGE HEADER-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
					<?php //debug($this->Session->params);
					//debug($this->Session->read('Auth'));
					//debug($this->Session->read('PropertyId')['Property']['id']);
					//debug($this->Session->read('PropertyName'));
					?>
					<?php echo $this->Session->read('PropertyName')." ";?>Passes <small></small>
					</h3>
					<ul class="page-breadcrumb breadcrumb">

						<li>
							<i class="fa fa-home"></i>
							<?php echo $this->Html->link(
                             'Home',
                              array(
                                    'controller' => 'Users',
                                    'action' => 'myAccount',
                                    'full_base' => true
                                                                                              )
                              );?>
							<i class="fa fa-angle-right"></i>
						</li>

						<li>
							<a href="#">Select Passes</a>
						</li>
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>

			<!-- END PAGE HEADER-->
			<div id="flashMessages">
									<h2><?php  echo $this->Session->flash();?></h2>
			</div>
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					<!-- Begin: life time stats -->
					<div class="portlet">
						<div class="portlet-title">
							<div class="row">
									<?php  echo $this->Form->create();
											echo "<div class='col-md-4'>";
												echo $this->Form->input('other_passes',array( 'type'=>'select',
																										'options' => $otherPassSelectOptions,
																										//'empty'=>$empty,
																										'div'=>false,
																										'label'=>array('class'=>'col-md-4 control-label',
																										'text'=>'How many Vehicles?'),
																										'between'=>'<div class="col-md-8">',
																										'after'=>'</div>',
																										'class'=>'form-control' 
																									));
									    	echo "</div>";
											echo "<div class='col-md-4'>";
													echo $this->Form->input('guest_pass',array( 'type'=>'select',
																										'options' => $guestSelectOptions,
																										//'selected'=>$selectedPassId,
																										//'empty'=>'Select Pass',
																										'div'=>false,
																										'label'=>array('class'=>'col-md-4 control-label',
																										'text'=>'Guest Pass'),
																										'placeholder'=>"",
																										'between'=>'<div class="col-md-8">',
																										'after'=>'</div>',
																										'class'=>'form-control' 
																									));
											echo "</div>";
											
									?>
									<div class="col-md-4"> 
											<?php echo $this->Form->button('Proceed',array('class'=>'btn yellow','type'=>'submit')); ?>
											<!--<button class="btn yellow" 'onClick'='return false'><i class="m-icon-swapright m-icon-white"> </i> Proceed</button>-->
											<!--<input type="button" class="btn yellow" value="Proceed" name="submit" id="gobtn">-->
									</div>
									<?php echo $this->Form->end();?>
							</div>
						</div>
						<div class="portlet-body">
							<?php if($guestPass){ $pass=$guestPass;?>
                              <div class="table-responsive" id="guest_div" style="display: none">
									<div class="note note-warning">
                                 						<p> Guest Pass Details</p>
                                     </div>
                                     <table class="table table-striped table-bordered table-hover">
                                           <thead>
                                                 <tr>
                                                     <th>Pass Name</th>
                                                     <th>Deposit</th>
                                                     <th>Cost</th>
                                                     <th>Expiry Date</th>
                                                     <th>Parking Location Name</th>
                                                     <th>Parking Location Cost</th>
                                                     <th>Total Amount</th>
                                                   </tr>
                                            </thead>
                                            <tbody>
												 <tr>
                                                     <td><?php echo $guestPass['Pass']['name'];?></td>
                                                     <td>
                                                          <?php echo '$ '.$pass['Pass']['deposit'];?>
                                                     </td>
                                                     <td>
                                                          <?php echo '$ '.$pass['Pass']['cost_1st_year'];?>
                                                     </td>
                                                     <td>
                                                     <?php if($pass['Pass']['is_fixed_duration']==1){
                                                                     echo date("m/d/Y",strtotime($pass['Pass']['expiration_date']));
                                                             }else{
                                                     
                                                                    switch ($pass['Pass']['duration_type']) {
																					case "Hour":
																						$date = new DateTime();
																						$k=(int)$pass['Pass']['duration'];
																						$date->add(new DateInterval('PT'.$k.'H'));
																						$pass['Pass']['expiration_date']=date_format($date, 'Y-m-d H:i:s');
																						break;
																					case "Day":
																						$date = new DateTime();
																						$k=$pass['Pass']['duration'];
																						$date->add(new DateInterval('P'.$k.'D'));
																						$pass['Pass']['expiration_date']=date_format($date, 'Y-m-d H:i:s');
																						break;
																					case "Week":
																						$date = new DateTime();
																						$k=(int)$pass['Pass']['duration']*7;
																						$date->add(new DateInterval('P'.$k.'D'));
																						$pass['Pass']['expiration_date']=date_format($date, 'Y-m-d H:i:s');
																						break;
																					case "Month":
																						$date = new DateTime();
																						$k=(int)$pass['Pass']['duration'];
																						$date->add(new DateInterval('P'.$k.'M'));
																						$pass['Pass']['expiration_date']=date_format($date, 'Y-m-d H:i:s');
																						break;
																					case "Year":
																						$date = new DateTime();
																						$k=(int)$pass['Pass']['duration'];
																						$date->add(new DateInterval('P'.$k.'Y'));
																						$pass['Pass']['expiration_date']=date_format($date, 'Y-m-d H:i:s');
																						break;
																		}
																	echo date("m/d/Y",strtotime($pass['Pass']['expiration_date']));
                                                          }
                                                       ?>
                                                      </td>
                                                      <td>
                                                           <?php if($pass['Package']['is_guest']==0){
																		echo $pass['Package']['name'];
																}else{
																		echo "NA";
																}
                                                           ?>
                                                       </td>
                                                        <td>
                                                             <?php 
																	if($pass['Package']['is_guest']==0){
																				echo '$ '.$pass['Package']['cost'];
																	}else{
																			echo "NA";
																	}
                                                            ?>
                                                        </td>
                                                         <td>
                                                             <?php 
																if($pass['Package']['is_guest']==0){
																		echo '$ <label id="guestPassCost">'.($pass['Pass']['deposit']+$pass['Pass']['cost_1st_year']+ $pass['Package']['cost'].'<label>');
																}else{
																		echo '$ <label id="guestPassCost">'.($pass['Pass']['deposit']+$pass['Pass']['cost_1st_year'].'<label>');
																}
                                                            ?>
                                                          </td>
                                                     </tr>
											</tbody>
									</table>
							</div>
							<?php }?>
							<div class="table-responsive" id="other_pass_div" style="display: none">
									<div class="note note-success">
                                 						<p> Other Pass Details</p>
                                     </div>
                                     <table class="table table-striped table-bordered table-hover">
                                           <thead>
                                                 <tr>
                                                     <th>Pass Name</th>
                                                     <th>Deposit</th>
                                                     <th>Cost</th>
                                                     <th>Expiry Date</th>
                                                     <th>Parking Location Name</th>
                                                     <th>Parking Location Cost</th>
                                                     <th>Required</th>
                                                     <th>Total Amount</th>
                                                 </tr>
                                            </thead>
                                            <tbody id="table_body">
											</tbody>
									</table>
							</div>
							<div class="note note-warning">
                                 						Net Amount Payable : $ <label id="net_amount"></label>
                            </div>
					</div>
					<!-- End: life time stats -->
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
<?php echo $this->Html->script('block.js'); ?>
<script type="text/javascript">
jQuery(document).ready(function ($) {
	
	if($('#TransactionOtherPasses').val()!='select'){
		/*$.blockUI({ css: { 
						border: 'none', 
						padding: '15px', 
						backgroundColor: '#000', 
						'-webkit-border-radius': '10px', 
						'-moz-border-radius': '10px', 
						opacity: .5, 
						color: '#fff' 
			} }); */
		$.ajax({
                type: "GET",
                url: "/Passes/give_selected_passes_details/"+jQuery('#TransactionOtherPasses').val()+"",
                dataType: "HTML",
                success: function (response) {
					//console.log(response);
                    var arr=$.parseJSON(response);
                   // console.log(arr);
                    if(arr.length>1){
					for(var i=0;i<(arr.length-1);i++){
					  $('#table_body').append('<tr><td>'+arr[i].Pass[0]+'</td>'+
											  '<td>'+arr[i].Pass[1]+'</td>'+
											  '<td>'+arr[i].Pass[2]+'</td>'+
											  '<td>'+arr[i].Pass[3]+'</td>'+
											  '<td>'+arr[i].Pass[5]+'</td>'+
											  '<td>'+arr[i].Pass[6]+'</td>'+
											  '<td>'+arr[i].Pass[4]+'</td>'+
											  '<td>'+arr[i].Pass[7]+'</td></tr>'
					  );
				   }
                  $('#other_pass_div').show();
                   if($('#guest_div').is(":visible") ){
					var amount1=(parseInt($('#guestPassCost').text())+parseInt(arr[arr.length-1]));
				   }else{
					var amount1=(parseInt(arr[arr.length-1]));
				   }
                    $('#net_amount').text(amount1);
				  }else{
					$('#TransactionOtherPasses').val("No Pass");
				  }
				  //$.unblockUI();
				}
            }); 
	
	}else{
		if($('#TransactionGuestPass').val()=='required' || $('#TransactionGuestPass').val()=='yes'){
			$('#net_amount').text($('#guestPassCost').text());
		}else{
			$('#net_amount').text('0');
		}
	}
	if($('#TransactionGuestPass').val()=='required' || $('#TransactionGuestPass').val()=='yes'){
			$('#guest_div').show();
	}
	$('#TransactionGuestPass').change(function () {
			if($('#TransactionGuestPass').val()=='required' || $('#TransactionGuestPass').val()=='yes'){
				   var amount2=(parseInt($('#net_amount').text())+parseInt($('#guestPassCost').text()));
                    $('#net_amount').text(amount2);
					$('#guest_div').hide();
				    $('#guest_div').show();
			}else{
					if($('#TransactionOtherPasses').val()=='select'){
						$('#net_amount').text('0');
					}else{
						 var amount3=(parseInt($('#net_amount').text())-parseInt($('#guestPassCost').text()));
						$('#net_amount').text(amount3);
					}
					$('#guest_div').hide();
			}
		
     });
     $('#TransactionOtherPasses').change(function () {
		 if($('#TransactionOtherPasses').val()!='select'){
		/* $.blockUI({ css: { 
						border: 'none', 
						padding: '15px', 
						backgroundColor: '#000', 
						'-webkit-border-radius': '10px', 
						'-moz-border-radius': '10px', 
						opacity: .5, 
						color: '#fff' 
			} });*/
		  $('#table_body').empty();
			$.ajax({
                type: "GET",
                url: "/Passes/give_selected_passes_details/"+jQuery('#TransactionOtherPasses').val()+"",
                dataType: "HTML",
                success: function (response) {
					var arr=$.parseJSON(response);
					//console.log(arr);
					for(var i=0;i<(arr.length-1);i++){
					  $('#table_body').append('<tr><td>'+arr[i].Pass[0]+'</td>'+
											  '<td>'+arr[i].Pass[1]+'</td>'+
											  '<td>'+arr[i].Pass[2]+'</td>'+
											  '<td>'+arr[i].Pass[3]+'</td>'+
											  '<td>'+arr[i].Pass[5]+'</td>'+
											  '<td>'+arr[i].Pass[6]+'</td>'+
											  '<td>'+arr[i].Pass[4]+'</td>'+
											  '<td>'+arr[i].Pass[7]+'</td></tr>'
					  );
				   }
                  $('#other_pass_div').show();
                  var amount4=0;
                  if($('#guest_div').is(":visible") ){
					var amount4=(parseInt($('#guestPassCost').text())+parseInt(arr[arr.length-1]));
				  }else{
					var amount4=parseInt(arr[arr.length-1]);
			      }	
                  $('#net_amount').text(amount4);
                  // $.unblockUI();
                }
            }); 
       }else{
		   if($('#guest_div').is(':visible')){
			$('#net_amount').text($('#guestPassCost').text());
		   }else{
			$('#net_amount').text('0');
		   }
			$('#other_pass_div').hide();
	   }
     });
	
	  
});
</script>

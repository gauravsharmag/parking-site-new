<div class="page-content-wrapper">
		<div class="page-content" style="min-height:1027px">
			
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
					
					<?php echo CakeSession::read('UserFullNameAdmin');?> <small>Passes Details</small>
					</h3>
					<ul class="page-breadcrumb breadcrumb">
						<li>
							<i class="fa fa-home"></i>
							<?php 
										echo $this->Html->link('Home',array(
                                          'controller' => 'Users',
                                          'action' => 'superAdminHome',
                                          'full_base' => true
                                      ));
                                 
                               ?>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="#">Passes</a>
						</li>
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
			<!-- END PAGE HEADER-->
			<div id="flashMessages">
                	<h2><?php  echo $this->Session->flash();?></h2>
                </div>
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					
					<!-- BEGIN SAMPLE TABLE PORTLET-->
					<div class="portlet box green">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-cogs"></i>My Passes
							</div>
							<div class="tools">
								<a class="collapse" href="javascript:;">
								</a>
							</div>
						</div>
						<div class="portlet-body flip-scroll">
							<div class="form-body">
									  <h3><?php echo $this->Session->flash();?></h3>
									  <div class="note note-success">
																<p>
																	 User Details
																</p>
									 </div>
									  <div class="row static-info">
										<div class="col-md-5 name">
												Property :
										</div>
										<div class="col-md-7 value">
											<?php 
													echo $propertyName['Property']['name']?$propertyName['Property']['name']:"No Property";
											?>
										</div>
									</div>
									<div class="row static-info">
										<div class="col-md-5 name">
												Customer Name :
										</div>
										<div class="col-md-7 value">
											<?php echo $userDetail['User']['first_name'].' '.$userDetail['User']['last_name'];?>
										</div>
									</div>
									<div class="row static-info">
										<div class="col-md-5 name">
												Username :
										</div>
										<div class="col-md-7 value">
											<?php echo $userDetail['User']['username'];?>
										</div>
									</div>
									<div class="row static-info">
										<div class="col-md-5 name">
												Address :
										</div>
										<div class="col-md-7 value">
											<?php echo $userDetail['User']['address_line_1'].' '.$userDetail['User']['address_line_2'].'<br>'.
													   $userDetail['User']['city'].' '.$userDetail['User']['state'].'<br>'.
													   $userDetail['User']['zip'].'<br>'.
													   'Email : '.$userDetail['User']['email'].'<br>'.
													   'Mobile : '.$userDetail['User']['phone'].'';?>
										</div>
									</div>
								</div>
							 <div class="note note-warning">
                                    <p>Customer Pass Details</p>
                             </div>
							<div class="table-scrollable">
							<table class="table table-bordered table-striped">
							<thead >
							<tr>
								<th width="5%">
									 #
								</th>
								<th>
									Parking Location Name
								</th>
								<th>
									Pass Name
								</th>
								<th>
									 Vehicle Plate Number
								</th>
								<th class="numeric">
									 Status
								</th>
								<th class="numeric">
									Expiration Date
								</th>
								<th class="numeric">
									Recurring Profile
								</th>
							</tr>
							</thead>
							<tbody>
							<!------Active Passes Start------>
							<?php $srNo=1;foreach($customerActivePass as $activePass){?>
							<tr>
								<td>
									<?php echo $srNo++; ?>
								</td>
								<td>
									<?php echo $this->requestAction(array('controller'=>'Packages','action'=>'getPackageNamePassBought',$activePass['CustomerPass']['id']));?>
								</td>
								<td>
									<?php echo $this->requestAction(array('controller'=>'Passes','action'=>'getPName',$activePass['CustomerPass']['id']));?>
								</td>
								<td>
									 <?php
									    if($activePass['CustomerPass']['vehicle_id']==null){
                                            echo "No Vehicle Registered"."<br>";
                                            echo $this->Html->link('Add Vehicle', array('controller' => 'Vehicles', 'action' => 'add',$activePass['CustomerPass']['id'],$userDetail['User']['id'],$propertyIdAdmin));
                                        }else{
                                           // echo $activePass['CustomerPass']['vehicle_id'];
                                             $vehiclePlateNumber=$this->requestAction(array('controller'=>'vehicles','action'=>'getVehiclePlateNumber',$activePass['CustomerPass']['vehicle_id']));
                                             echo $vehiclePlateNumber['Vehicle']['license_plate_number'];
                                        }
									 ?>
								</td>
								<td class="numeric">
                                	 <span class="label label-sm label-danger">Active Pass</span>
                                	<?php //if($activePass['CustomerPass']['is_guest_pass']==1){
											if(!is_null($activePass['CustomerPass']['membership_vaild_upto'])){
												echo "Valid Upto: ".date("m/d/Y H:i:s", strtotime($activePass['CustomerPass']['membership_vaild_upto']));
											}
											
										//}
										//echo "Valid Upto: ".date("m/d/Y H:i:s", strtotime($activePass['CustomerPass']['membership_vaild_upto']));
									?>
                                </td>
								<!--<td class="numeric">
									<?php 
										//echo date("m/d/Y H:i:s", strtotime($activePass['CustomerPass']['membership_vaild_upto']));
									?>
								</td>-->
								<td class="numeric">
									 <?php echo date("m/d/Y H:i:s", strtotime($activePass['CustomerPass']['pass_valid_upto']));
											$currentDateTime= time();
										    $pass_date = strtotime($activePass['CustomerPass']['pass_valid_upto']);
										    $datediff =$pass_date-$currentDateTime;
										    $daysLeft=floor($datediff/(60*60*24));
						
										    if($daysLeft<=7){
												echo "<br>DAYS LEFT FOR EXPIRATION: ".$daysLeft;
												echo '<br>'.$this->Html->link(__('RENEW NOW'), array('controller' => 'CustomerPasses', 'action' => 'renewPass', $activePass['CustomerPass']['id'], $userDetail['User']['id'], $propertyIdAdmin,true));
											}
									 
									 ?>
								</td>
								<td>
									<?php if($activePass['CustomerPass']['is_guest_pass']==0){
												if($activePass['CustomerPass']['recurring_profile_id']){
													//echo '<button id="'.$activePass['CustomerPass']['id'].'" class="btn yellow btn-xs" type="button" onclick="showModal(this);">View Profiles</button><br> 
													echo  'Current Profile : '.$activePass['CustomerPass']['recurring_profile_id'].'<br>
														 Current Status : '.$activePass['CustomerPass']['recurring_profile_status'];
												}
										  }else{
												echo "NA";
										  }
									?>
								</td>
							</tr>
							<?php }?>
							<!------Active Passes End------>
							<?php foreach($remainingPasses as $remainingPass){?>
                                                								<tr>
                                                									<td>
                                                										<?php echo $srNo++;?>
                                                									</td>
																					<td>
																						<?php echo $this->requestAction(array('controller'=>'Packages','action'=>'remainingPassPackageName',$remainingPass['Pass']['id']));?>
																					</td>																					
																					<td>
																					<?php echo $remainingPass['Pass']['name'];?>
																					</td>
                                                									<td>
                                                                                         <?php echo "No Vehicle Registered"?>
                                                									</td>
                                                									<td><?php if($noCurrentPass){ ?>
																								<span class="label label-sm label-info">Remaining</span>
                                                										<?php }else{
																								echo "<span class='label label-sm label-danger'>New Pass</span>";
																							}
																						 ?><br>
																						 <?php echo $this->Html->link('Buy Pass', array('controller' => 'Transactions', 'action' => 'buyRemainingPass',$remainingPass['Pass']['id'],$userDetail['User']['id'],$propertyIdAdmin));
																							  echo "<br>";
																							  $totalCost=$remainingPass['Pass']['deposit']+$remainingPass['Pass']['cost_1st_year'];
																							  if(isset($remainingPass['Package'])){
																								 $totalCost= $totalCost+$remainingPass['Package']['deposit'];
																							  }
																							 // echo  "Total Cost : $ ".$totalCost." (Including depost and pass cost)";
																							
																						?>
																						 
                                                									</td>
                                                									<td>
																					    <?php	echo date("m/d/Y H:i:s", strtotime($remainingPass['Pass']['expiration_date']));?>
                                                                                    </td>
                                                                                    <td>NA</td>
                                                								</tr>
                                                				<?php }?>
							<!------Valid Passes Start------>
                            <?php foreach($customerValidPass as $validPass){?>
							<tr>
								<td>
									<?php echo $srNo++; ?>
								</td>
								<td>
									<?php echo $this->requestAction(array('controller'=>'Packages','action'=>'getPackageNamePassBought',$validPass['CustomerPass']['id']));?>
								</td>
								<td>
									<?php echo $this->requestAction(array('controller'=>'Passes','action'=>'getPName',$validPass['CustomerPass']['id']));?>
								</td>
								<td>
									 <?php
									    if($validPass['CustomerPass']['vehicle_id']==null){
                                            echo "No Vehicle Registered"."<br>";
                                           // echo $this->Html->link('Add Vehicle', array('controller' => 'Vehicles', 'action' => 'add',$validPass['CustomerPass']['id']));
                                        }else{
                                            $vehiclePlateNumber=$this->requestAction(array('controller'=>'vehicles','action'=>'getVehiclePlateNumber',$validPass['CustomerPass']['vehicle_id']));
                                            echo $vehiclePlateNumber['Vehicle']['license_plate_number'];
                                           // debug($vehiclePlateNumber);
                                        }
									 ?>
								</td>
								<td class="numeric">
                                	 <span class="label label-sm label-success">Valid Pass</span><br>
                                	 <?php if($validPass['CustomerPass']['membership_vaild_upto']==null){
											echo $this->Html->link('Activate Pass', array('controller' => 'CustomerPasses', 'action' => 'getCustomerPassDetails',$validPass['CustomerPass']['id'],$userDetail['User']['id'],$propertyIdAdmin));
									}else{ 
										if($validPass['CustomerPass']['is_guest_pass']==1){
											if(!is_null($validPass['CustomerPass']['membership_vaild_upto'])){
												echo "Last Used Upto: ".date("m/d/Y H:i:s", strtotime($validPass['CustomerPass']['membership_vaild_upto']));
												echo "<br>";
											}
										}
										echo $this->Html->link('Activate Pass', array('controller' => 'CustomerPasses', 'action' => 'getCustomerPassDetails',$validPass['CustomerPass']['id'],$userDetail['User']['id'],$propertyIdAdmin));
									}
									?>
                                </td>
								<td class="numeric">
									 <?php echo date("m/d/Y H:i:s", strtotime($validPass['CustomerPass']['pass_valid_upto']));?>
								</td>
								<td>
									<?php if($validPass['CustomerPass']['is_guest_pass']==0){
												if($validPass['CustomerPass']['recurring_profile_id']){
													//echo '<button id="'.$validPass['CustomerPass']['id'].'" class="btn yellow btn-xs" type="button" onclick="showModal(this);">View Profiles</button><br> 
													echo  'Current Profile : '.$validPass['CustomerPass']['recurring_profile_id'].'<br>
														 Current Status : '.$validPass['CustomerPass']['recurring_profile_status'];
												}else{
													echo "NA";
												}
										  }else{
												echo "NA";
										  }
									?>
								</td>
							</tr>
							<?php }?>
							<!------Valid Passes End------>
                            <!------Expired Passes Renewable Start------>
                            <?php foreach($customerRenewableExpiredPass as $expiredPass){?>
							<tr>
								<td>
									<?php echo $srNo++; ?>
								</td>
								<td>
									<?php echo $this->requestAction(array('controller'=>'Packages','action'=>'getPackageNamePassBought',$expiredPass['CustomerPass']['id']));?>
								</td>
								<td>
									<?php echo $this->requestAction(array('controller'=>'Passes','action'=>'getPName',$expiredPass['CustomerPass']['id']));?>
								</td>
								<td>
									 <?php
									    if($expiredPass['CustomerPass']['vehicle_id']==null){
                                            echo "No Vehicle Registered"."<br>";
                                            //echo $this->Html->link('Register Now', array('controller' => '', 'action' => ''));
                                        }else{
                                           //echo $expiredPass['CustomerPass']['vehicle_id'];
                                           $vehiclePlateNumber=$this->requestAction(array('controller'=>'vehicles','action'=>'getVehiclePlateNumber',$expiredPass['CustomerPass']['vehicle_id']));
                                           echo $vehiclePlateNumber['Vehicle']['license_plate_number'];
                                        }
									 ?>
								</td>
								<td class="numeric">
                                	<span class="label label-sm label-warning">Expired Renewable Pass</span><br>
                                	<?php  if(!is_null($expiredPass['CustomerPass']['membership_vaild_upto'])){
												//echo date("m/d/Y H:i:s", strtotime($expiredPass['CustomerPass']['membership_vaild_upto'])); 
												//echo "<br>";
											}else{
												echo "Permit Expired";
											}
											
										   echo $this->Html->link(__('Renew'), array('controller' => 'CustomerPasses', 'action' => 'renewPass', $expiredPass['CustomerPass']['id'], $userDetail['User']['id'], $propertyIdAdmin));
									?>
                                </td>
								<td class="numeric">
									 <?php 
										echo date("m/d/Y H:i:s", strtotime($expiredPass['CustomerPass']['pass_valid_upto'])); 
										
									 ?>
								</td>
								<td>
									<?php if($expiredPass['CustomerPass']['is_guest_pass']==0){
												if($expiredPass['CustomerPass']['recurring_profile_id']){
													//echo '<button id="'.$expiredPass['CustomerPass']['id'].'" class="btn yellow btn-xs" type="button" onclick="showModal(this);">View Profiles</button><br> 
													echo 'Current Profile : '.$expiredPass['CustomerPass']['recurring_profile_id'].'<br>
														 Current Status : '.$expiredPass['CustomerPass']['recurring_profile_status'];
												}
										  }else{
												echo "NA";
										  }
									?>
								</td>
							</tr>
							<?php }?>
							<!------Expired Passes Renewable End------>
                            <!------Expired Passes Non Renewable Start------>
                            <?php foreach($customerNonRenewableExpiredPass as $expiredPass1){?>
							<tr>
								<td>
									<?php echo $srNo++; ?>
								</td>
								<td>
									<?php echo $this->requestAction(array('controller'=>'Packages','action'=>'getPackageNamePassBought',$expiredPass1['CustomerPass']['id']));?>
								</td>
								<td>
									<?php echo $this->requestAction(array('controller'=>'Passes','action'=>'getPName',$expiredPass1['CustomerPass']['id']));?>
								</td>
								<td>
									 <?php
									    if($expiredPass1['CustomerPass']['vehicle_id']==null){
                                            echo "No Vehicle Registered"."<br>";
                                            //echo $this->Html->link('Register Now', array('controller' => '', 'action' => ''));
                                        }else{
                                           $vehiclePlateNumber=$this->requestAction(array('controller'=>'vehicles','action'=>'getVehiclePlateNumber',$expiredPass1['CustomerPass']['vehicle_id']));
                                           echo $vehiclePlateNumber['Vehicle']['license_plate_number'];
                                        }
									 ?>
								</td>
								<td class="numeric">
                                	 <span class="label label-sm label-success">Expired Non-Renewable Pass</span><br>
                                	 <?php if(!is_null($expiredPass1['CustomerPass']['membership_vaild_upto'])){
											echo date("m/d/Y H:i:s", strtotime($expiredPass1['CustomerPass']['membership_vaild_upto']));
										}else{
											echo "Permit Expired";
										}
									?>        
                                </td>
								<td class="numeric">
									 <?php echo date("m/d/Y H:i:s", strtotime($expiredPass1['CustomerPass']['pass_valid_upto']));?>
								</td>
								<td>
									<?php if($expiredPass1['CustomerPass']['is_guest_pass']==0){
												if($expiredPass1['CustomerPass']['recurring_profile_id']){
													//echo '<button id="'.$expiredPass1['CustomerPass']['id'].'" class="btn yellow btn-xs" type="button" onclick="showModal(this);">View Profiles</button><br> 
													echo 'Current Profile : '.$expiredPass1['CustomerPass']['recurring_profile_id'].'<br>
														 Current Status : '.$expiredPass1['CustomerPass']['recurring_profile_status'];
												}
										  }else{
												echo "NA";
										  }
									?>
								</td>
							</tr>
							<?php }?>
							<!------Expired Passes Non Renewable End------>
							</tbody>
							</table>
							</div>
						</div>
					</div>
					<!-- END SAMPLE TABLE PORTLET-->

				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>


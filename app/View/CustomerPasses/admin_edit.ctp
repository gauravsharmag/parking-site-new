<script>
$(document).ready(function(){
	function setRequireTrue(){
		    $("#vehiclePanel").show("slow");
            $("#CustomerPassOwner").attr("required",true);
            $("#CustomerPassMake").attr("required",true);
            $("#CustomerPassModel").attr("required",true);
            $("#CustomerPassColor").attr("required",true);
            $("#CustomerPassLicensePlateNumber").attr("required",true);
            $("#CustomerPassLicensePlateState").attr("required",true);
            $("#CustomerPassLast4DigitalOfVin").attr("required",true);
	}
	function setRequireFalse(){
			$("#vehiclePanel").hide("slow");
			$("#CustomerPassOwner").attr("required",false);
            $("#CustomerPassMake").attr("required",false);
            $("#CustomerPassModel").attr("required",false);
            $("#CustomerPassColor").attr("required",false);
            $("#CustomerPassLicensePlateNumber").attr("required",false);
            $("#CustomerPassLicensePlateState").attr("required",false);
            $("#CustomerPassLast4DigitalOfVin").attr("required",false);
	}
	if ($(CustomerPassAddVehicle).is(':checked')){
		   //$(CustomerPassSelectedVehicle).(function()
           setRequireTrue();

     }else{
			setRequireFalse();
	}
	$(CustomerPassAddVehicle).change(function()
    {
        if ($(this).is(':checked'))
        {
			$("#vehiclePanel").show("slow");
           if($(CustomerPassSelectedVehicle).val()==null || $(CustomerPassSelectedVehicle).val()==''){
				$("#CustomerPassOwner").attr("required",true);
				$("#CustomerPassMake").attr("required",true);
				$("#CustomerPassModel").attr("required",true);
				$("#CustomerPassColor").attr("required",true);
				$("#CustomerPassLicensePlateNumber").attr("required",true);
				$("#CustomerPassLicensePlateState").attr("required",true);
				$("#CustomerPassLast4DigitalOfVin").attr("required",true);
		   }else{
				$("#CustomerPassOwner").attr("required",false);
				$("#CustomerPassMake").attr("required",false);
				$("#CustomerPassModel").attr("required",false);
				$("#CustomerPassColor").attr("required",false);
				$("#CustomerPassLicensePlateNumber").attr("required",false);
				$("#CustomerPassLicensePlateState").attr("required",false);
				$("#CustomerPassLast4DigitalOfVin").attr("required",false);
		   }
           
        }
        else
        {
			setRequireFalse();
        }
    });

		$(CustomerPassSelectedVehicle).change(function()
		{
			if ($(this).val()=='')
			{
				$("#CustomerPassOwner").attr("required",true);
				$("#CustomerPassMake").attr("required",true);
				$("#CustomerPassModel").attr("required",true);
				$("#CustomerPassColor").attr("required",true);
				$("#CustomerPassLicensePlateNumber").attr("required",true);
				$("#CustomerPassLicensePlateState").attr("required",true);
				$("#CustomerPassLast4DigitalOfVin").attr("required",true);
			}
			else
			{
				$("#CustomerPassOwner").attr("required",false);
				$("#CustomerPassMake").attr("required",false);
				$("#CustomerPassModel").attr("required",false);
				$("#CustomerPassColor").attr("required",false);
				$("#CustomerPassLicensePlateNumber").attr("required",false);
				$("#CustomerPassLicensePlateState").attr("required",false);
				$("#CustomerPassLast4DigitalOfVin").attr("required",false);
			}
    });
 
});
</script>
<div class="page-content-wrapper">
		<div class="page-content">
			<div id="flashMessages">
											<h2><?php  echo $this->Session->flash();?></h2>
			</div>
<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
				<h3 class="page-title">RFID Tags <small>Add RFID Tag Number</small></h3>
					<div class="tabbable tabbable-custom boxless tabbable-reversed">


							<div class="tab-pane active" id="tab_0">
								<div class="portlet box green">
									<div class="portlet-title">
										<div class="caption">
											<i class="fa fa-gift"></i><?php  echo 'RFID Tags';?>
										</div>

									</div>
									
										<!-- BEGIN FORM-->

									<div class="portlet-body form">

									<?php echo $this->Form->create('CustomerPass',array('class'=>'form-horizontal'));
                                         //echo $this->Html->link(__('List Existing Packages'), array('action' => 'index',$propertyId,$propertyName));
									?>
                                    <div class="form-body">
                                              <h3><?php echo $this->Session->flash();?></h3>
                                              <div class="note note-success">
                                                						<p>
                                                							 Pass Details
                                                						</p>
                                             </div>
                                              <div class="row static-info">
												<div class="col-md-5 name">
														Approved :
												</div>
												<div class="col-md-7 value">
													<?php 
														if($this->request->data['CustomerPass']['approved']){
															echo '<span class="label label-sm label-info">YES</span>';
														}else{
															echo '<span class="label label-sm label-danger">NO</span>';
														}
													?>
												</div>
											</div>
											  <div class="row static-info">
												<div class="col-md-5 name">
														Property :
												</div>
												<div class="col-md-7 value">
													<?php echo $this->requestAction(array('controller'=>'Properties','action'=>'getPropertyNameRFIF',$this->request->data['CustomerPass']['property_id']));?>
												</div>
											</div>
											 <div class="row static-info">
												<div class="col-md-5 name">
														Pass :
												</div>
												<div class="col-md-7 value">
													<?php 
															echo $this->request->data['CustomerPass']['pass_name'];
													?>
												</div>
											</div>
											<div class="row static-info">
												<div class="col-md-5 name">
														Parking Location :
												</div>
												<div class="col-md-7 value">
													<?php if(!is_null($this->request->data['CustomerPass']['package_name'])){
															echo $this->request->data['CustomerPass']['package_name'];
														}else{ echo "Permit Not Bought";}
													?>
												</div>
											</div>
											<div class="row static-info">
												<div class="col-md-5 name">
														Customer :
												</div>
												<div class="col-md-7 value">
													<?php echo $this->request->data['CustomerPass']['customer_name'];?>
												</div>
											</div>
											<div class="row static-info">
												<div class="col-md-5 name">
														Pass Valid Upto :
												</div>
												<div class="col-md-2 value">
													<?php //echo $this->request->data['CustomerPass']['pass_expiry'];
													
														 echo $this->Form->input('pass_valid_upto',array('required'=>true,'type'=>'text','div'=>array('class'=>'form-group'),
                                                                                'label'=>false,
                                                                                'required'=>true,
																				'error'=>array('attributes'=>array('class'=>'model-error')),
                                                                                'between'=>'<div class="input-group">',
                                                                                'after'=>'<span class="input-group-btn"></span></div>',
                                                                                'class'=>'form-control'));
													
													
													?>
												</div>
											</div>
											<div class="row static-info">
												<div class="col-md-5 name">
														Parking Location Valid Upto :
												</div>
												<div class="col-md-2 value">	
													<?php if(!is_null($this->request->data['CustomerPass']['membership_vaild_upto'])){
																echo $this->Form->input('membership_vaild_upto',array('required'=>true,'type'=>'text','div'=>array('class'=>'form-group'),
                                                                                'label'=>false,
                                                                                'required'=>true,
																				'error'=>array('attributes'=>array('class'=>'model-error')),
                                                                                'between'=>'<div class="input-group">',
                                                                                'after'=>'<span class="input-group-btn"></div>',
                                                                                'class'=>'form-control'));
															}else{
																echo "Permit Not Bought";
															}
													?>
												</div>
											</div>
											   <div class="row static-info">
                                        <div class="col-md-5 name">
                                            <?php echo (__('Assigned_location :')); ?> 
                                        </div>
                                        <div class="col-md-2 value">
                                            <?php
                                            //if (!is_null($this->request->data['CustomerPass']['assigned_location'])) {
                                            echo $this->Form->input('assigned_location', array('required' => true, 'type' => 'text', 'div' => array('class' => 'form-group'),
                                                'label' => false,
                                                'required' => false,
                                                'error' => array('attributes' => array('class' => 'model-error')),
                                                'between' => '<div class="input-group">',
                                                'after' => '<span class="input-group-btn"></div>',
                                                'class' => 'form-control'));
                                            // } else {
                                            // echo "No Location Assigned";
                                            //}
                                            ?>
                                        </div>
                                    </div>     
                                             <div class="note note-success">
                                                						<p>
                                                							 Vehicle Details
                                                						</p>
                                             </div>
                                            <?php        
													echo $this->Form->input('id'); 
													echo $this->Form->hidden('vehicle_id');
													
                                                  	echo $this->Form->input('RFID_tag_number',array('div'=>array('class'=>'form-group'),
                                                                                         'label'=>array('class'=>'col-md-3 control-label','text'=>'RFID Tag Number'),
                                                                                         'between'=>'<div class="col-md-4">',
                                                                                         'error'=>array('attributes'=>array('class'=>'model-error')),
                                                                                         'after'=>'</div>',
                                                                                         'class'=>'form-control'));
                                                     echo $this->Form->input('add_vehicle',array('type'=>'checkbox','div'=>array('class'=>'form-group'),
																					'label'=>false,
																					'before'=>'<label class="col-md-3 control-label">Add/Edit Vehicle ?</label><div class="col-md-4">',
																					'after'=>'</div>'
																					));
                                               echo "<div id='vehiclePanel' style='display: none'>";
                                                if(empty($this->request->data['CustomerPass']['vehicle_id'])){
													if(!empty($vehicle_info)){
														echo  "<div class='note note-warning'><p>Select Vehicle</p></div>";
														echo $this->Form->input('selected_vehicle',array( 'type'=>'select',
																										'options' => $vehicle_info,
																										'empty'=>'Select Vehicle',
																										'div'=>array('class'=>'form-group'),
																										'error'=>array('attributes'=>array('class'=>'model-error')),
																										'label'=>array('class'=>'col-md-3 control-label',
																										'text'=>'Select Vehicle'),'between'=>'<div class="col-md-4">',
																										'after'=>'</div>',
																										'class'=>'form-control' ));
													}else{
														echo $this->Form->hidden('selected_vehicle',array('default'=>NULL));
													}
												}else{
													echo $this->Form->hidden('selected_vehicle',array('default'=>NULL));
												}
													echo  "<div class='note note-success'><p>Add/Edit Vehicle</p></div>";
													echo $this->Form->input('owner',array('div'=>array('class'=>'form-group'),
                                                                                         'label'=>array('class'=>'col-md-3 control-label','text'=>'Name For Vehicle'),
                                                                                         'between'=>'<div class="col-md-4">',
                                                                                         //'value'=>$this->request->data['Vehicle']['owner'],
                                                                                         'error'=>array('attributes'=>array('class'=>'model-error')),
                                                                                         'after'=>'</div>',
                                                                                         'class'=>'form-control'));
                                                    echo $this->Form->input('make',array('div'=>array('class'=>'form-group'),
                                                                                         'label'=>array('class'=>'col-md-3 control-label','text'=>'Make'),
                                                                                         'between'=>'<div class="col-md-4">',
                                                                                        // 'value'=>$this->request->data['Vehicle']['make'],
                                                                                         'error'=>array('attributes'=>array('class'=>'model-error')),
                                                                                         'after'=>'</div>',
                                                                                         'class'=>'form-control'));
                                                    echo $this->Form->input('model',array('div'=>array('class'=>'form-group'),
                                                                                         'label'=>array('class'=>'col-md-3 control-label','text'=>'Model'),
                                                                                         'between'=>'<div class="col-md-4">',
                                                                                         //'value'=>$this->request->data['Vehicle']['model'],
                                                                                         'error'=>array('attributes'=>array('class'=>'model-error')),
                                                                                         'after'=>'</div>',
                                                                                         'class'=>'form-control'));
                                                     echo $this->Form->input('color',array('div'=>array('class'=>'form-group'),
                                                                                         'label'=>array('class'=>'col-md-3 control-label','text'=>'Color'),
                                                                                         'between'=>'<div class="col-md-4">',
                                                                                        // 'value'=>$this->request->data['Vehicle']['color'],
                                                                                         'error'=>array('attributes'=>array('class'=>'model-error')),
                                                                                         'after'=>'</div>',
                                                                                         'class'=>'form-control'));
                                                     echo $this->Form->input('license_plate_number',array('div'=>array('class'=>'form-group'),
                                                                                         'label'=>array('class'=>'col-md-3 control-label','text'=>'License Plate Number'),
                                                                                         'between'=>'<div class="col-md-4">',
                                                                                         //'value'=>$this->request->data['Vehicle']['license_plate_number'],
                                                                                         'error'=>array('attributes'=>array('class'=>'model-error')),
                                                                                         'after'=>'</div>',
                                                                                         'class'=>'form-control'));
                                                     echo $this->Form->input('license_plate_state',array('type'=>'select','div'=>array('class'=>'form-group'),
                                                                                         'label'=>array('class'=>'col-md-3 control-label','text'=>'License Plate State'),
                                                                                         'between'=>'<div class="col-md-4">',
                                                                                         'options'=>$states,
                                                                                         'empty'=>'Select One',
                                                                                         //'value'=>$this->request->data['Vehicle']['license_plate_state'],
                                                                                         'error'=>array('attributes'=>array('class'=>'model-error')),
                                                                                         'after'=>'</div>',
                                                                                         'class'=>'form-control'));
                                                     echo $this->Form->input('last_4_digital_of_vin',array('div'=>array('class'=>'form-group'),
                                                                                         'label'=>array('class'=>'col-md-3 control-label','text'=>'Vin Last 4 Digits'),
                                                                                         'between'=>'<div class="col-md-4">',
                                                                                         //'value'=>$this->request->data['Vehicle']['last_4_digital_of_vin'],
                                                                                         'error'=>array('attributes'=>array('class'=>'model-error')),
                                                                                         'after'=>'</div>',
                                                                                         'class'=>'form-control'));
													
												echo "</div>";
                                          ?>
                                           <div class="form-actions fluid">
												<div class="col-md-offset-3 col-md-9">
													<button type="submit" class="btn blue">Submit</button>
													<!--<button type="button" class="btn default">Cancel</button>-->
												</div>
											</div>
                                         <?php echo $this->Form->end(); ?>
                                     </div>

										<!-- END FORM-->
									</div>
								</div>

							</div>



					</div>
				</div>
			</div>
			<!-- END PAGE CONTENT-->
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.3.1/jquery.maskedinput.min.js" type="text/javascript"></script>
<script type="text/javascript">
  $("#CustomerPassPassValidUpto").mask("99/99/9999 99:99:99");
   $("#CustomerPassMembershipVaildUpto").mask("99/99/9999 99:99:99");
    </script>

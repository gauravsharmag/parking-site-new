<?php
echo $this->Html->script('chosen.jquery.js');
echo $this->Html->css('chosen.css');
//echo $this->Html->css('bootstrap.css');
?>
<style>
	.chosen-container{
		width:305px !important;}
</style>
<div class="page-content-wrapper">
    <div class="page-content" style="min-height:1027px">
        <!-- BEGIN PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                <h3 class="page-title">
                    <?php echo (__('Last Seen')); ?>
                    <small> <?php echo (__('rfid tags')); ?></small>
                </h3>
                <ul class="page-breadcrumb breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <a href="#"> <?php echo (__('Home')); ?></a>
                        <i class="fa fa-angle-right"></i>
                    </li>
                    <li>
                        <a href="#"> <?php echo (__('RFID Tags scanning  history')); ?></a>
                    </li>
                </ul>
                <!-- END PAGE TITLE & BREADCRUMB-->
            </div>
        </div>
        <!-- END PAGE HEADER-->
        <div id="flashMessages">
            <h2><?php echo $this->Session->flash(); ?></h2>
        </div>
        <!-- BEGIN PAGE CONTENT-->
        <div class="row">
            <div class="col-md-12">

                <!-- Begin: life time stats -->
                <div class="portlet">
                    <div class="portlet-title">
    
                    </div>
                    <div class="portlet-body">
						<div class="row">
							 <div class="col-md-12"> 
								<h3>Search RFID</h3>
								<?php
							   // echo "<div class='col-md-4'>";
								echo $this->Form->input('rfids', array('type' => 'select',
									'options' => $rfids,
									'selected' =>'',
									'empty'=>'Select RFID Tag',   
									'div' => false,
									'label' => false,
									'class' => 'form-control'
								));
								?>
								</div>
							</div>
                           <!-- <div class="col-md-1"> 
                                <input type="submit" class="btn yellow" value="Search Tag" name="submit" id="gobtn">
                            </div>-->
                        </div>
                       
                        <div class="row">
                             <div class="col-md-12">
								 <h3>Search By Property</h3>
                                <?php echo $this->Form->create('',array('class'=>'form-inline'));?>
                                    <div class="form-group">
                                         <?php
                                            echo $this->Form->input('current_property', array('type' => 'select',
                                                'options' => $allProperties,
                                                'empty' => 'Select Property',
                                                'div' => false,
                                                'label' => "",
                                                'id'=>'current_property',
                                                'placeholder' => "",
                                                'class' => 'form-control'
                                            ));
                                         ?>
                                    </div>
                                    <div class="form-group">
                                        <div data-date-format="mm/dd/yyyy" data-date="08/15/2014" class="input-group input-large date-picker input-daterange col-md-12">
                                            <input type="text" name="from" id="from" class="form-control">
                                            <span class="input-group-addon">
                                                <?php echo (__('To')); ?> </span>
                                            <input type="text" name="to" id="to" class="form-control">
                                        </div>
                                    </div>
                                    <input type="button" class="btn red" value="Go" name="submit" id="gobtn1">
                                <?php echo $this->Form->end();?>
                            </div>
                        </div>
                        <hr>
                        <div class="note note-warning" id="pass_intro" style="display: none">
                            <p id="pass_name"><?php echo (__('Select Pass To View Details')); ?></p><br>
                            <?php echo (__('Total Passes Purchased In Given Interval:')); ?> <label id="totalPasses"></label>
                        </div>
                        <div class="table-container" id="table_list" >		
                            <div class="table-scrollable">
                                <table id="view_rfid_list" class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th><?php echo (__('RFID Tag')); ?></th>
                                            <th><?php echo (__('Username')); ?></th>
                                            <th><?php echo (__('Property')); ?></th>
                                            <th><?php echo (__('Vehicle')); ?></th>
                                            <th><?php echo (__('Last Seen On')); ?></th>
                                            <th><?php echo (__('Record Added On')); ?></th>
                                            <th><?php echo (__('Pass')); ?></th>
                                            <th><?php echo (__('Valid Upto')); ?></th>
                                            <th><?php 	if(AuthComponent::user('role_id')==1){
														echo (__('Edit')); 
													}
                                            ?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td colspan="9" class="dataTables_empty"><?php echo (__('Select options to load data')); ?>......</td>
                                        </tr>
                                    </tbody>
                                </table>
                                <?php //echo $this->Js->writeBuffer(); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End: life time stats -->
            </div>
        </div>
        <!-- END PAGE CONTENT-->
    </div>
</div>
<?php echo $this->Html->script('//cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js'); ?>
<?php echo $this->Html->css('//cdn.datatables.net/1.10.11/css/jquery.dataTables.min.css'); ?>
<?php echo $this->Html->script('block.js'); ?>
<?php echo $this->Html->script('//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.min.js'); ?> 
<?php echo $this->Html->script('/assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js'); ?>
<?php echo $this->Html->script('/assets/admin/pages/scripts/components-pickers.js');   ?>
<?php echo $this->Html->css('/assets/global/plugins/bootstrap-datepicker/css/datepicker3.css'); ?>
<script>
    jQuery(document).ready(function() {     
      ComponentsPickers.init(); 
    });
</script>

<script type="text/javascript">
	jQuery('#rfids').chosen();
     $("#rfids").change(function() {
			var rfid=jQuery('#rfids').val();
			if(rfid){
				  $('#view_rfid_list').dataTable({
                                    "bProcessing": false,
                                    "bServerSide": true,
                                    "bDestroy": true,
                                    "sAjaxSource": "/admin/CustomerPasses/get_last_seen/" + rfid + ""
                                });
                   $('#table_list').show();
			}else{
				alert('Please Select RFID Tag');
			}
            console.log(rfid);

    });
       $("#gobtn1").click(function() {
            var currentProperty = $('#current_property').val();
            if (currentProperty) {
                if ($('#from').val()) {
                   /* $.blockUI({css: {
                            border: 'none',
                            padding: '15px',
                            backgroundColor: '#000',
                            '-webkit-border-radius': '10px',
                            '-moz-border-radius': '10px',
                            opacity: .5,
                            color: '#fff'
                        }});*/
                    $('#helpBlock').html("Select Date");
                    $('#helpBlock').css('color', 'grey');
                    var toDate = "";
                    if ($('#to').val()) {
                        toDate = $('#to').val();
                    } else {
                        var dt = new Date();
                        $('#to').val((dt.getMonth() + 1) + '/' + (dt.getDate() - 1) + '/' + dt.getFullYear());
                        toDate = (dt.getMonth() + 1) + '/' + (dt.getDate() - 1) + '/' + dt.getFullYear();
                    }
                    var data = {};
                    data[0] = $('#from').val();
                    data[1] = toDate;
                    data[2] = $('#current_property').val();
                    var dataSend = JSON.stringify(data);                                                                           
                    $('#view_rfid_list').dataTable({
                          "bProcessing": false,
                          "bServerSide": true,
                          "bDestroy": true,
                          "sAjaxSource": "/admin/CustomerPasses/get_last_seen_propertywise?fromDate="+$('#from').val()+"&toDate="+toDate+"&propertyId="+currentProperty+"" 
                    });
                    $('#table_list').show();
                          //  $.unblockUI();
                        
                   
                } else {
                    $('#helpBlock').html("Select From date");
                    $('#helpBlock').css('color', 'red')
                }
            } else {
                alert('Select Property');
            }

        });
</script>

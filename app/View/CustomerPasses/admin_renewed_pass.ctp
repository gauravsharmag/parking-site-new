<div class="page-content-wrapper">
		<div class="page-content" style="min-height:1027px">
			<!-- BEGIN PAGE HEADER-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
					Passes <small>Property Wide Renwed Passes</small>
					</h3>
					<ul class="page-breadcrumb breadcrumb">
						<li>
							<i class="fa fa-home"></i>
							<a href="#">Home</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="#">Property Wide Renwed Passes</a>
						</li>
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
			<!-- END PAGE HEADER-->
			<div id="flashMessages">
				<h2><?php  echo $this->Session->flash();?></h2>
			</div>
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					
					<!-- Begin: life time stats -->
					<div class="portlet">
						<div class="portlet-title">
							
						</div>
						<div class="portlet-body">
							<div class="row">
								 <div class="col-md-12">
									<?php echo $this->Form->create('',array('class'=>'form-inline'));?>
										<div class="form-group">
											 <?php
												echo $this->Form->input('current_property',array( 'type'=>'select',
																											'options' => $property_list,
																											'selected'=>$selectedId,
																											'div'=>false,
																											'label'=>false,
																											'class'=>'form-control',
																											'id'=>'current_property' 
																										));
											 ?>
										</div>
										
										<div class="form-group">
											<div data-date-format="mm/dd/yyyy" data-date="08/15/2014" class="input-group input-large date-picker input-daterange col-md-12">
												<input type="text" name="from" id="from" class="form-control">
												<span class="input-group-addon">
													<?php echo (__('To')); ?> </span>
												<input type="text" name="to" id="to" class="form-control">
											</div>
										</div>
										<input type="button" class="btn red" value="Go" name="submit" id="gobtn">
									<?php echo $this->Form->end();?>
								</div>
							</div>
							<hr>
							<div class="note note-warning" id="pass_intro" style="display:none">
									<p id="pass_name">Select Pass To View Details</p><br>
									Total Passes Expiring In Given Interval: <label id="totalPasses"></label>
							</div>
							<div class="clear-fix" style="display:none;" id="contactBtn" >	
									<input type="checkbox"  id="bulkDelete"/> <button id="deleteTriger" class="btn btn-danger" >Send Email To Selected Customers</button>
									<span class="help-block">Mark to send mail to all customers present on table </span>
							</div>
							
								<div class="table-scrollable">
								  <table id="view_rfid_list" class="table table-striped table-bordered table-hover">
									 <thead>
										<tr>
										   <th>User</th>
										   <th>Pass</th>
										   <th>Vehicle</th>
										   <th>Vehicle Number</th>
										   <th>Pass Validity</th>
										   <th>Permit Validity</th>
										   <th>Assigned RFID</th> 
										   <th>Edit RFID</th> 
										</tr>
									  </thead>
									  <tbody>
										   <tr>
												<td colspan="8" class="dataTables_empty">...Select Options....</td> 
											</tr>
									   </tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
					<!-- End: life time stats -->
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>

<?php //echo $this->Html->script('//code.jquery.com/jquery-1.12.0.min.js'); ?>
<?php echo $this->Html->script('//cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js'); ?>
<?php echo $this->Html->script('//cdn.datatables.net/buttons/1.1.2/js/dataTables.buttons.min.js'); ?> 
<?php echo $this->Html->script('//cdn.datatables.net/buttons/1.1.2/js/buttons.print.min.js'); ?>
<?php echo $this->Html->script('//cdn.datatables.net/buttons/1.1.2/js/buttons.flash.min.js'); ?>
<?php echo $this->Html->script('//cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js'); ?>
<?php echo $this->Html->script('//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js'); ?>
<?php echo $this->Html->script('//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js'); ?>
<?php echo $this->Html->script('//cdn.datatables.net/buttons/1.1.2/js/buttons.html5.min.js'); ?>
<?php echo $this->Html->script('//cdn.datatables.net/buttons/1.1.2/js/buttons.colVis.min.js'); ?>
<?php echo $this->Html->css('//cdn.datatables.net/1.10.11/css/jquery.dataTables.min.css'); ?>
 <?php echo $this->Html->css('//cdn.datatables.net/buttons/1.1.2/css/buttons.dataTables.min.css'); ?> 
<?php echo $this->Html->script('block.js'); ?>
<?php echo $this->Html->script('moment.min.js'); ?>

<?php echo $this->Html->script('//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.min.js'); ?> 
<?php echo $this->Html->script('/assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js'); ?>
<?php echo $this->Html->script('/assets/admin/pages/scripts/components-pickers.js');   ?>
<?php echo $this->Html->css('/assets/global/plugins/bootstrap-datepicker/css/datepicker3.css'); ?>
<script>
    jQuery(document).ready(function() {     
      ComponentsPickers.init(); 
    });
</script>
<script type="text/javascript">
jQuery(document).ready(function ($) {
	/*$('#current_property').change(function () {
			$('#current_pass').empty();
			$.blockUI({ css: { 
						border: 'none', 
						padding: '15px', 
						backgroundColor: '#000', 
						'-webkit-border-radius': '10px', 
						'-moz-border-radius': '10px', 
						opacity: .5, 
						color: '#fff' 
			} }); 
			$.ajax({
                type: "GET",
                url: "/admin/Passes/get_passes_list/"+jQuery(this).val()+"",
                dataType: "HTML",
                success: function (response) {
					//jQuery('#current_pass').append("<option value=''>Select Pass</option>");
                    var arr=$.parseJSON(response);
                    jQuery.each(arr, function (i, text) {
                        jQuery('#current_pass').append(jQuery('<option></option>').val(i).html(text));
                    });
                    $.unblockUI();
                }
            }); 
        });*/
	$("#gobtn").click(function () {
		//var currentPass = $('#current_pass').val();
		if($('#from').val()){
			var toDate="";
			if($('#to').val()){
				toDate=$('#to').val();
			}else{
				var dt = new Date();
				$('#to').val((dt.getMonth()+1) + '/' + (dt.getDate()-1) + '/' +  dt.getFullYear());
				toDate=(dt.getMonth()+1) + '/' + (dt.getDate()-1) + '/' +  dt.getFullYear();
			}
			$('#view_rfid_list').dataTable({
						"bProcessing": false,
						"bServerSide": true,
						"bDestroy": true,
						"dom": 'lBfrtip',
						"initComplete": function(settings, json) {
							$.unblockUI();
						 },
						"buttons": [
							{
								extend: 'print',
								exportOptions: {
									columns: [ 0, ':visible' ]
								},
								customize: function ( win ) {
									$(win.document.body)
										.css( 'font-size', '10pt' )
										.prepend(
											'<img src="<?php echo "https://netparkingpass.com/img/onlineparking-logo.png" ?>" alt="" style="position:absolute; opacity:0.3; top:40%; left:30%;" />'
										);
				 
									$(win.document.body).find( 'table' )
										.addClass( 'compact' )
										.css( 'font-size', 'inherit' );
								}
							},
							{
								extend: 'csv',
								 exportOptions: {
									columns: [ 0, ':visible' ]
								}
							},
							{
								extend: 'pdf',
								 exportOptions: {
									columns: [ 0, ':visible' ]
								}
							},
							{
								extend: 'excel',
								 exportOptions: {
									columns: [ 0, ':visible' ]
								}
							},
							{
								extend: 'colvis',
								 text: "Select Columns"
							}
							
						],
					   "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
					   oLanguage: {
							sInfoFiltered: "",
							sLengthMenu: "Show _MENU_ entries.&nbsp;&nbsp;",
						},
						"sAjaxSource": "/admin/Transactions/renewed?from="+$('#from').val()+"&to="+toDate+"&propertyId="+$('#current_property').val(), 
						"columnDefs": [
							{
								"render": function ( data, type, row ) {
									return "<b>Username: </b> <a href='/admin/users/view_user_details/"+row.user_id+"'>"+row.username+"</a><br> <b>Name: </b>"+row.first_name+" "+row.last_name+"<br> <b>Address: </b>"+row.address_line_1+" "+row.address_line_2+"<br> "+row.city+" "+row.state+"<br> "+row.zip+"<br> <b>Mobile: </b>"+row.phone+"<br> <b>Email: </b>"+row.email;
								},
								"targets": 0
							},
							{
								"render": function ( data, type, row ) {
									return "<a class='btn green btn-xs green-stripe' href='/admin/CustomerPasses/edit/"+row.id+"'>View Pass</a>";
								},
								"targets": 7
							},
							{
								"targets": 4,
								"data": function ( row, type, val, meta ) {
											var date = moment(row.pass_valid_upto);
											var newDate = date.format("DD MMM YYYY HH:mm:ss"); 
											return newDate;	
								}
							},
							{
								"targets": 5,
								"data": function ( row, type, val, meta ) {
											if (row.membership_vaild_upto) {
												var date = moment(row.membership_vaild_upto);
												var newDate = date.format("DD MMM YYYY HH:mm:ss"); 
												return newDate;
											}else{
												return "NA";
											}
								}
							},
						]
				});
			
		}else{
			$('#helpBlock').html("Select From date");
			$('#helpBlock').css('color','red');     
		}
	});
	  
});

</script>


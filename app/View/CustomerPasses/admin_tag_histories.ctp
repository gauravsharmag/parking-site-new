<h4>CURRENT RFID: <b><?php echo $currentRFID?$currentRFID:"NO RFID"; ?></b></h4>
<table class="table table-hover">
		<thead>
			<tr>
				<th> # </th>
				<th> RFID TAG NUMBER </th>
				<th> VALID UPTO </th>
			</tr>
		</thead>
		<tbody>
			<?php if($history){ ?>
			
				<?php  $counter=1;
					   foreach($history as $pass){ ?>
						   <tr>
							<td> <?php echo $counter++; ?> </td>
							<td> <?php echo $pass['CustomerPassBackup']['RFID_tag_number'] ?> </td>
							<td> <?php echo date('m/d/Y H:i:s',strtotime($pass['CustomerPassBackup']['backup_date'])); ?> </td>
						 </tr>
					 <?php } ?>
			<?php }else{ ?>
				<tr><td colspan='3' align="center">No Record Found</td></tr>
			<?php } ?>
		</tbody>
	</table>

<div class="customerPasses view">
<h2><?php echo __('Customer Pass'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($customerPass['CustomerPass']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('User'); ?></dt>
		<dd>
			<?php echo $this->Html->link($customerPass['User']['id'], array('controller' => 'users', 'action' => 'view', $customerPass['User']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Vehicle'); ?></dt>
		<dd>
			<?php echo $this->Html->link($customerPass['Vehicle']['id'], array('controller' => 'vehicles', 'action' => 'view', $customerPass['Vehicle']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Property'); ?></dt>
		<dd>
			<?php echo $this->Html->link($customerPass['Property']['name'], array('controller' => 'properties', 'action' => 'view', $customerPass['Property']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Transaction'); ?></dt>
		<dd>
			<?php echo $this->Html->link($customerPass['Transaction']['id'], array('controller' => 'transactions', 'action' => 'view', $customerPass['Transaction']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Status'); ?></dt>
		<dd>
			<?php echo h($customerPass['CustomerPass']['status']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Membership Vaild Upto'); ?></dt>
		<dd>
			<?php echo h($customerPass['CustomerPass']['membership_vaild_upto']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Pass Valid Upto'); ?></dt>
		<dd>
			<?php echo h($customerPass['CustomerPass']['pass_valid_upto']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('RFID Tag Number'); ?></dt>
		<dd>
			<?php echo h($customerPass['CustomerPass']['RFID_tag_number']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Assigned Location'); ?></dt>
		<dd>
			<?php echo h($customerPass['CustomerPass']['assigned_location']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Package'); ?></dt>
		<dd>
			<?php echo $this->Html->link($customerPass['Package']['name'], array('controller' => 'packages', 'action' => 'view', $customerPass['Package']['id'])); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Customer Pass'), array('action' => 'edit', $customerPass['CustomerPass']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Customer Pass'), array('action' => 'delete', $customerPass['CustomerPass']['id']), array(), __('Are you sure you want to delete # %s?', $customerPass['CustomerPass']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Customer Passes'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Customer Pass'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Vehicles'), array('controller' => 'vehicles', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Vehicle'), array('controller' => 'vehicles', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Properties'), array('controller' => 'properties', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Property'), array('controller' => 'properties', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Transactions'), array('controller' => 'transactions', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Transaction'), array('controller' => 'transactions', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Packages'), array('controller' => 'packages', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Package'), array('controller' => 'packages', 'action' => 'add')); ?> </li>
	</ul>
</div>

<div class="page-content-wrapper">
		<div class="page-content" style="min-height:1027px">
			<!-- BEGIN PAGE HEADER-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
					Passes <small>Property Wide Passes Expiring Status</small>
					</h3>
					<ul class="page-breadcrumb breadcrumb">
						<li>
							<i class="fa fa-home"></i>
							<a href="#">Home</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="#">Property Wide Pass Expiring Status</a>
						</li>
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
			<!-- END PAGE HEADER-->
			<div id="flashMessages">
									<h2><?php  echo $this->Session->flash();?></h2>
			</div>
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">
				<div class="col-md-12">
					
					<!-- Begin: life time stats -->
					<div class="portlet">
						<div class="portlet-title">
							
						</div>
						<div class="portlet-body">
							<div class="row">
                             <div class="col-md-12">
                                <?php echo $this->Form->create('',array('class'=>'form-inline'));?>
                                    <div class="form-group">
                                         <?php
                                            echo $this->Form->input('current_property', array('type' => 'select',
                                                'options' => $property_list,
												'selected'=>$selectedId,
                                                'empty' => 'Select Property',
                                                'div' => false,
                                                'label' => false,
                                                'id'=>'current_property',
                                                'placeholder' => "",
                                                'class' => 'form-control'
                                            ));
                                         ?>
                                    </div>
                                    <div class="form-group">
                                         <?php
                                           echo $this->Form->input('current_pass',array( 'type'=>'select',
																						'options' => $passes_list,
																						'empty'=>'Select Pass',
																						'div'=>false,
																						'label'=>false,
																						'placeholder'=>"",
																						'class'=>'form-control',
																						'id'=>'current_pass'
																					));
                                         ?>
                                    </div>
                                    <div class="form-group">
                                        <div data-date-format="mm/dd/yyyy" data-date="08/15/2014" class="input-group input-large date-picker input-daterange col-md-12">
                                            <input type="text" name="from" id="from" class="form-control">
                                            <span class="input-group-addon">
                                                <?php echo (__('To')); ?> </span>
                                            <input type="text" name="to" id="to" class="form-control">
                                        </div>
                                    </div>
                                    <input type="button" class="btn red" value="Go" name="submit" id="gobtn">
                                <?php echo $this->Form->end();?>
                            </div>
                        </div>
						<hr>
							<div class="note note-warning" id="pass_intro" style="display:none">
									<p id="pass_name">Select Pass To View Details</p><br>
									Total Passes Expiring In Given Interval: <label id="totalPasses"></label>
							</div>
							<div class="clear-fix" style="display:none;" id="contactBtn" >	
									<input type="checkbox"  id="bulkDelete"/> <button id="deleteTriger" class="btn btn-danger" >Send Email To Selected Customers</button>
									<span class="help-block">Mark to send mail to all customers present on table </span>
							</div>
								<br>
							  <table id="view_rfid_list" class="table table-striped table-bordered table-hover">
								 <thead>
									<tr>
									   <th>User</th>
									   <th>Property</th>
									   <th>Vehicle</th>
									   <th>Vehicle Number</th>
									   <th>Pass Validity</th>
									   <th>Permit Validity</th>
									   <th>Assigned RFID</th> 
									   <th>Edit RFID</th> 
									</tr>
								  </thead>
								  <tbody>
									   <tr>
											<td colspan="8" class="dataTables_empty">...Select Options....</td> 
										</tr>
								   </tbody>
								</table>
							</div>
						</div>
					</div>
					<!-- End: life time stats -->
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>
<div id="myModalForError" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h3 class="modal-title">Send Email</h3>
         <h4 id='returnMessage' style=''></h3>
      </div>
      <div class="modal-body">
		  <?php echo $this->Form->create('Ticket',array('class'=>'form-horizontal','id'=>'reportErrorForm'));?>
			<?php 
					echo $this->Form->hidden('uri',array('value'=>$_SERVER['REQUEST_URI']));
					echo $this->Form->hidden('to_users');
			?>
			<div class="form-group" >   
				<label class="col-md-3 control-label">Subject<font>*</font></label>
				<div class="col-md-9">
					<?php echo $this->Form->input('subject', array('required' => true,'type'=>'text','label' => false, 'div' => false, 'class' => 'form-control')); ?>    
				</div> 
			</div>
			<div class="form-group" >   
				<label class="col-md-3 control-label">Message<font>*</font></label>
				<div class="col-md-9">
					<?php echo $this->Form->input('message', array('required' => true,'type'=>'textarea','label' => false, 'div' => false, 'class' => 'form-control')); ?>    
				</div> 
			</div>
			<div class="form-group">
					<label class="col-md-4 control-label"></label>
					<div class="col-md-8">
						<div class="checkbox-list">
							<label class="checkbox-inline">
								<div class="checker" id="uniform-inlineCheckbox21">
									<span>
										<?php echo $this->Form->input('generate_ticket', array('type'=>'checkbox','label' => false, 'div' => false)); ?>    
									</span>
								</div>Send Message Too
							</label>
						</div>
					</div>
			</div>
			<div class="form-actions">
				<div class="row">
					<div class="col-md-offset-3 col-md-9">
						<button type="submit" class="btn green">Submit</button>
					</div>
				</div>
			</div>
		<?php echo $this->Form->end(); ?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
<?php //echo $this->Html->script('//code.jquery.com/jquery-1.12.0.min.js'); ?>
<?php echo $this->Html->script('//cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js'); ?>
<?php echo $this->Html->script('//cdn.datatables.net/buttons/1.1.2/js/dataTables.buttons.min.js'); ?> 
<?php echo $this->Html->script('//cdn.datatables.net/buttons/1.1.2/js/buttons.print.min.js'); ?>
<?php echo $this->Html->script('//cdn.datatables.net/buttons/1.1.2/js/buttons.flash.min.js'); ?>
<?php echo $this->Html->script('//cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js'); ?>
<?php echo $this->Html->script('//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js'); ?>
<?php echo $this->Html->script('//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js'); ?>
<?php echo $this->Html->script('//cdn.datatables.net/buttons/1.1.2/js/buttons.html5.min.js'); ?>
<?php echo $this->Html->script('//cdn.datatables.net/buttons/1.1.2/js/buttons.colVis.min.js'); ?>
<?php echo $this->Html->css('//cdn.datatables.net/1.10.11/css/jquery.dataTables.min.css'); ?>
 <?php echo $this->Html->css('//cdn.datatables.net/buttons/1.1.2/css/buttons.dataTables.min.css'); ?> 
<?php echo $this->Html->script('block.js'); ?>

<?php echo $this->Html->script('//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.7.1/js/bootstrap-datepicker.min.js'); ?> 
<?php echo $this->Html->script('/assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js'); ?>
<?php echo $this->Html->script('/assets/admin/pages/scripts/components-pickers.js');   ?>
<?php echo $this->Html->css('/assets/global/plugins/bootstrap-datepicker/css/datepicker3.css'); ?>
<script>
    jQuery(document).ready(function() {     
      ComponentsPickers.init(); 
    });
</script>

<script type="text/javascript">
jQuery(document).ready(function ($) {
	$('#current_property').change(function () {
			$('#current_pass').empty();
			$.blockUI({    
							message: '<h4>Loading..</h4>',  
							css: { 
									border: 'none', 
									padding: '8px', 
									backgroundColor: '#000', 
									'-webkit-border-radius': '10px', 
									'-moz-border-radius': '10px', 
									opacity: .5, 
                                    width:'35%',
									color: '#fff' 
						}
				  });
			$.ajax({
                type: "GET",
                url: "/admin/Passes/get_passes_list/"+jQuery(this).val()+"",
                dataType: "HTML",
                success: function (response) {
					jQuery('#current_pass').append("<option value=''>Select Pass</option>");
                    var arr=$.parseJSON(response);
                    jQuery.each(arr, function (i, text) {
                        jQuery('#current_pass').append(jQuery('<option></option>').val(i).html(text));
                    });
                    $.unblockUI();
                }
            }); 
        });
	$("#gobtn").click(function () {
		var currentPass = $('#current_pass').val();
				if($('#from').val()){
					$.blockUI({    
							message: '<h4>Loading..</h4>',  
							css: { 
									border: 'none', 
									padding: '8px', 
									backgroundColor: '#000', 
									'-webkit-border-radius': '10px', 
									'-moz-border-radius': '10px', 
									opacity: .5, 
                                    width:'35%',
									color: '#fff' 
						}
				  });
					$('#helpBlock').html("Select Date");
					$('#helpBlock').css('color','grey');
					var toDate="";
					if($('#to').val()){
						toDate=$('#to').val();
					}else{
						var dt = new Date();
						$('#to').val((dt.getMonth()+1) + '/' + (dt.getDate()-1) + '/' +  dt.getFullYear());
						toDate=(dt.getMonth()+1) + '/' + (dt.getDate()-1) + '/' +  dt.getFullYear();
					}
					var data = {};
					data['fromDate']=$('#from').val();
					data['todate']=toDate;
					data['passId']=$('#current_pass').val();
					data['propertyId']=$('#current_property').val();
					var dataSend = JSON.stringify(data);
					$.ajax({
							url: "/admin/CustomerPasses/get_passes_expiring_interval",
							type: "GET",
							data: dataSend, 
							dataType: "json",
							success: function (response) {
								$('#totalPasses').html(response.totalPass);
								if(response.passName!='No Pass Found'){
									$('#pass_name').html(response.passName);
								}else{
									$('#pass_name').html('All Passes');
								}
								$('#contactBtn').show();
								dataToSend= JSON.stringify(response.condition);
								$(function(){
									$.blockUI({ css: { 
												border: 'none', 
												padding: '15px', 
												backgroundColor: '#000', 
												'-webkit-border-radius': '10px', 
												'-moz-border-radius': '10px', 
												opacity: .5, 
												color: '#fff' 
									} }); 
									$('#view_rfid_list').dataTable({
											"bProcessing": false,
											"bServerSide": true,
											"bDestroy": true,
											"dom": 'lBfrtip',
											"initComplete": function(settings, json) {
												$.unblockUI();
											 },
											"buttons": [
												{
													extend: 'print',
													exportOptions: {
														columns: [ 0, ':visible' ]
													},
													customize: function ( win ) {
														$(win.document.body)
															.css( 'font-size', '10pt' )
															.prepend(
																'<img src="<?php echo "https://netparkingpass.com/img/onlineparking-logo.png" ?>" alt="" style="position:absolute; opacity:0.3; top:40%; left:30%;" />'
															);
									 
														$(win.document.body).find( 'table' )
															.addClass( 'compact' )
															.css( 'font-size', 'inherit' );
													}
												},
												{
													extend: 'csv',
													 exportOptions: {
														columns: [ 0, ':visible' ]
													}
												},
												{
													extend: 'pdf',
													 exportOptions: {
														columns: [ 0, ':visible' ]
													}
												},
												{
													extend: 'excel',
													 exportOptions: {
														columns: [ 0, ':visible' ]
													}
												},
												{
													extend: 'colvis',
													 text: "Select Columns"
												}
												
											],  
										   "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
										   oLanguage: {
												sInfoFiltered: "",
												sLengthMenu: "Show_MENU_entries.&nbsp;&nbsp;<b>Download Current Table As :</b>&nbsp;",
											},
											"sAjaxSource": "/admin/CustomerPasses/passes_interval/"+dataToSend+"",
											"columnDefs": [
												{
													// The `data` parameter refers to the data for the cell (defined by the
													// `data` option, which defaults to the column being worked with, in
													// this case `data: 0`.
													"render": function ( data, type, row ) {
														return "<b>Username: </b>"+data+"<br> <b>Name: </b>"+row[9]+" "+row[10]+"<br> <b>Address: </b>"+row[11]+" "+row[12]+"<br> "+row[13]+" "+row[14]+"<br> "+row[15]+"<br> <b>Mobile: </b>"+row[16]+"<br> <b>Email: </b>"+row[17];
													},
													"targets": 0
												},
												{
													"render": function ( data, type, row ) {
														newStr='<b>Owner: </b>'+row[2]+'<br>';
														newStr+='<b>Make: </b>'+row[20]+'<br>';
														newStr+='<b>Model: </b>'+row[21]+'<br>';
														newStr+='<b>Color: </b>'+row[22]+'<br>';
														newStr+='<b>VIN: </b>'+row[18];
														// return row[2]+' '+row[3];
														return newStr;
													},
													"targets": 2
												}, 
												{
													"render": function ( data, type, row ) {
														str='<b>Plate Number: </b>'+row[3]+'<br>';
														str+='<b>State: </b> '+row[14];
														return str; 
													},
													"targets": 3
												},
												{
													"render": function ( data, type, row ) {
														return "<input type='checkbox' class='deleteRow' value='"+row[18]+"*"+$("#current_property option:selected").text()+"*"+row[17]+"*"+row[16]+"'><span class='label label-sm label-success'>Send Email</span></input>" ;
													},   
													"targets": 7
												}
											]
									});
								});
								$('#pass_intro').show();
								$('#table_list').show();
								$.unblockUI();
							}
					});
				}else{
					$('#helpBlock').html("Select From date");
					$('#helpBlock').css('color','red');     
				}
	});
	  
});

$("#bulkDelete").on('click', function() { // bulk checked
        var status = this.checked;

        $(".deleteRow").each(function() {
            $(this).prop("checked", status);
        });
    });
  $('#deleteTriger').on("click", function(event) {// triggering delete one by one
       
		
        if ($('.deleteRow:checked').length > 0) {  // at-least one checkbox checked
            var ids = [];
            $('.deleteRow').each(function() {
                if ($(this).is(':checked')) {
                    ids.push($(this).val());
                }
            });
			ids_string = ids.toString(); 
			$("#returnMessage").html('');
			$("#TicketSubject").val('');
			$("#TicketMessage").val('');
			$('#TicketToUsers').val(ids_string);
			$('#myModalForError').modal();

        } else {
			alert('Select Customers');
        }
    });
$("#reportErrorForm").submit(function(e)
	{
		e.preventDefault(); //STOP default action
		$.blockUI({
            css: {
                border: 'none',
                padding: '15px',
                margin: 0,
                width: '100%',
                top: '200px',
                left: '0px',
                textAlign: 'center',
                color: '#B35900',
                backgroundColor: 'transparent',
                fontWeight: 'bold',
            },
            overlayCSS: {
                backgroundColor: '#2F2F2F',
                opacity: '0.5',
            },
            message: '<h2><img src="/../../app/webroot/img/busy2.gif", />Loading</h2>'});
		var postData = $(this).serializeArray();
		//console.log(postData);
		$.ajax(
		{
			url : '/admin/Tickets/send_email_bulk',
			type: "POST",
			data : postData,
			success:function(data, textStatus, jqXHR) 
			{
				if(data=='true'){
					$("#returnMessage").html('Message Sent Successfully');
					$("#returnMessage").attr('style','color:green;');
				}else{
					$("#returnMessage").html('Message Cannot Be Sent, Please Try Later');
					$("#returnMessage").attr('style','color:red;');
				}
				  $.unblockUI();
			},
			error: function(jqXHR, textStatus, errorThrown) 
			{
				//if fails      
			}
		});
		
	});
</script>



<div class="page-content-wrapper">
		<div class="page-content">
		   <div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
					Passes<small>Deleted Passes</small>
					</h3>
					<ul class="page-breadcrumb breadcrumb">
					
						<li>
							<i class="fa fa-home"></i>
							<?php echo $this->Html->link('Home',array('controller'=>'users','action'=>'superAdminHome')); ?>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="javascript:;">Deleted Passes</a>
						</li>
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
			<div id="flashMessages">
                	<h2><?php  echo $this->Session->flash();?></h2>
            </div>
            <div class="row">
				<div class="col-md-12">
					<!-- BEGIN EXAMPLE TABLE PORTLET-->
					<div class="portlet box grey-cascade">
						<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-users"></i>Deleted Passes
							</div>
							<div class="tools">
								<a href="javascript:;" class="collapse">
								</a>
								
							</div>
						</div>
						<div class="portlet-body">
							<div class="row">
                             <div class="col-md-12">
                                <?php echo $this->Form->create('',array('class'=>'form-inline'));?>
									<div class="form-group">
                                         <?php
                                            echo $this->Form->input('property', array('type' => 'select',
                                                'options' => $properties,
                                                'div' => false,
                                                'label' => false,
                                                'empty'=>'All Property',
                                                'id'=>'property',
                                                'placeholder' => "",
                                                'class' => 'form-control'
                                            ));
                                         ?>
                                    </div>
                                    <div class="form-group">
                                         <?php
                                            echo $this->Form->input('status', array('type' => 'select',
                                                'options' => ['Pending','Refunded','Denied','All'],
                                                'div' => false,
                                                'label' => false,
                                                'id'=>'status',
                                                'placeholder' => "",
                                                'class' => 'form-control'
                                            ));
                                         ?>
                                    </div>
                                    <input type="button" class="btn red" value="Go" name="submit" id="gobtn1">
                                <?php echo $this->Form->end();?>
                            </div>
                        </div>
						<hr>
			
							<div class="table-scrollable">
								<table id="all_customer_list"class="table table-striped table-bordered table-hover"> 
									<thead>
									<tr>
										<th>
											User Details
										</th>
										<th>
											Property
										</th>
										<th>
											Pass
										</th>
										<th>
											Permit
										</th>
										<th>
											 Pass Cost
										</th>
										<th>
											 Pass Deposit
										</th>
										<th>
											 Permit Cost
										</th>
										<th>
											 Total Cost
										</th>
										<th>
											Deleted On
										</th>
										<th>
											Refund Done
										</th>
										<th>
											Actions
										</th>
									</tr>
									</thead>
									<tbody>
										<tr>
											<td colspan="11" class="dataTables_empty">Loading Data...</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
					<!-- END EXAMPLE TABLE PORTLET-->
				</div>
			</div>
</div>

<?php echo $this->Html->script('//cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js'); ?>
<?php echo $this->Html->script('//cdn.datatables.net/buttons/1.1.2/js/dataTables.buttons.min.js'); ?>
<?php echo $this->Html->script('//cdn.datatables.net/buttons/1.1.2/js/buttons.print.min.js'); ?>
<?php echo $this->Html->script('//cdn.datatables.net/buttons/1.1.2/js/buttons.flash.min.js'); ?>
<?php echo $this->Html->script('//cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js'); ?>
<?php echo $this->Html->script('//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js'); ?>
<?php echo $this->Html->script('//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js'); ?>
<?php echo $this->Html->script('//cdn.datatables.net/buttons/1.1.2/js/buttons.html5.min.js'); ?>
<?php echo $this->Html->script('//cdn.datatables.net/buttons/1.1.2/js/buttons.colVis.min.js'); ?>
<?php echo $this->Html->css('//cdn.datatables.net/1.10.11/css/jquery.dataTables.min.css'); ?>
 <?php echo $this->Html->css('//cdn.datatables.net/buttons/1.1.2/css/buttons.dataTables.min.css'); ?> 
 <?php echo $this->Html->script(['moment-with-locales.js']); ?>
<script type="text/javascript">
createTable();
function createTable(){	
	$('#all_customer_list').dataTable({ 
			"bProcessing": false,
			"bServerSide": true,
			"bDestroy": true,
			"dom": 'lBrftip',
			"sAjaxSource": "/admin/Refunds/getData/"+$('#status').val()+"/"+$('#property').val(),
			"columnDefs": [
						    {	"targets": 0,
								"render": function ( data, type, row ) {
									return "<b>Username: </b> <a href='/admin/users/view_user_details/"+row.user_id+"'>"+row.username+"</a><br> <b>Name: </b>"+row.first_name+" "+row.last_name+"<br> <b>Address: </b>"+row.address_line_1+" "+row.address_line_2+"<br> "+row.city+" "+row.state+"<br> "+row.zip+"<br> <b>Mobile: </b>"+row.phone+"<br> <b>Email: </b>"+row.email;
								},
								"className": "dt-left"
							},
							{
								"targets": 8,
								"data": function ( row, type, val, meta ) {
												var date = moment(row.created);
												var newDate = date.format("DD MMM YYYY HH:mm:ss"); 
												return newDate;
											
								}
							},
							{
								"targets": 9,
								"data": function ( row, type, val, meta ) {
											if(row.refunded==1){
												return '<span class="label label-sm label-info">YES</span>';
											}else if(row.refunded==0){
												return '<span class="label label-sm label-success">NO</span>';
											}else{
												return '<span class="label label-sm label-danger">DENIED</span>';
											}
										}
							},
							{   "targets":10,
								"render": function ( data, type, row ) {
										 str='';
										 if(row.refunded==1){
											var token = Math.floor((Math.random() * 100000000111111) + 1);
											str='<form name="post_'+token+'" style="display:none;" method="post" action="Refunds/refund/'+row.id+'/0"><input  type="hidden" name="_method" value="POST"></form><a href="#" class="label label-success" onclick="if (confirm(&quot;Are you sure ?&quot; )) { document.post_'+token+'.submit(); } event.returnValue = false; return false;">Not-Refunded</a>';
											var token = Math.floor((Math.random() * 100000000111111) + 1);
											str+='&nbsp;<form name="post_'+token+'" style="display:none;" method="post" action="Refunds/refund/'+row.id+'/2"><input  type="hidden" name="_method" value="POST"></form><a href="#" class="label  label-danger" onclick="if (confirm(&quot;Are you sure you want to deny this refund ? &quot;)) { document.post_'+token+'.submit(); } event.returnValue = false; return false;">Deny</a>';
										}else if(row.refunded==0){
											var token = Math.floor((Math.random() * 100000000111111) + 1);
											str= '<form name="post_'+token+'" style="display:none;" method="post" action="Refunds/refund/'+row.id+'/1"><input  type="hidden" name="_method" value="POST"></form><a href="#" class="label  label-info" onclick="if (confirm(&quot;Are you sure you have refunded the amount ? &quot;)) { document.post_'+token+'.submit(); } event.returnValue = false; return false;">Refunded</a>';
											var token = Math.floor((Math.random() * 100000000111111) + 1);
											str+='&nbsp;<form name="post_'+token+'" style="display:none;" method="post" action="Refunds/refund/'+row.id+'/2"><input  type="hidden" name="_method" value="POST"></form><a href="#" class="label  label-danger" onclick="if (confirm(&quot;Are you sure you want to deny this refund ? &quot;)) { document.post_'+token+'.submit(); } event.returnValue = false; return false;">Deny</a>';
										}else{
											var token = Math.floor((Math.random() * 100000000111111) + 1);
											str= '<form name="post_'+token+'" style="display:none;" method="post" action="Refunds/refund/'+row.id+'/1"><input  type="hidden" name="_method" value="POST"></form><a href="#" class="label  label-info" onclick="if (confirm(&quot;Are you sure you have refunded the amount ? &quot;)) { document.post_'+token+'.submit(); } event.returnValue = false; return false;">Refunded</a>';
											var token = Math.floor((Math.random() * 100000000111111) + 1);
											str+='&nbsp;<form name="post_'+token+'" style="display:none;" method="post" action="Refunds/refund/'+row.id+'/0"><input  type="hidden" name="_method" value="POST"></form><a href="#" class="label label-success" onclick="if (confirm(&quot;Are you sure ?&quot; )) { document.post_'+token+'.submit(); } event.returnValue = false; return false;">Not-Refunded</a>';
										}
										return str;
								}
							},
						],
			buttons: [
              {
                extend: 'print',
                exportOptions: {
                    columns: [ 0, ':visible' ]
                },
                customize: function ( win ) {
                    $(win.document.body)
                        .css( 'font-size', '10pt' )
                        .prepend(
                            '<img src="<?php echo "https://internetparkingpass.com/img/onlineparking-logo.png" ?>" alt="" style="position:absolute; opacity:0.3; top:40%; left:30%;" />'
                        );
 
                    $(win.document.body).find( 'table' )
                        .addClass( 'compact' )
                        .css( 'font-size', 'inherit' );
                }
            },
            {
				extend: 'csv',
				 exportOptions: {
                    columns: [ 0, ':visible' ]
                }
			},
			{
				extend: 'pdf',
				 exportOptions: {
                    columns: [ 0, ':visible' ]
                }
			},
			{
				extend: 'excel',
				 exportOptions: {
                    columns: [ 0, ':visible' ]
                }
			},
			{
				extend: 'colvis',
				 text: "Select Columns"
			}
			
        ],
       "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "All"]],
       oLanguage: {
			sInfoFiltered: "",
			sLengthMenu: "Show _MENU_ entries.&nbsp;&nbsp;",
		},
	});
}
 $("#gobtn1").click(function() {
	 createTable();
});
</script>

<div class="vehicleTypes form">
<?php echo $this->Form->create('VehicleType'); ?>
	<fieldset>
		<legend><?php echo __('Add Vehicle Type'); ?></legend>
	<?php
		echo $this->Form->input('vehicle_type_name',array('required'=>true));
		echo $this->Form->input('property_id',array('required'=>true));
		echo $this->Form->input('deposit',array('required'=>true));
		echo $this->Form->input('pass_cost',array('required'=>true));
		echo $this->Form->input('pass_cost_after_1_year',array('required'=>true));
		echo $this->Form->input('recurring_cost_duration',array('required'=>true));
		echo $this->Form->input('recurring_cost',array('required'=>true));
		echo $this->Form->input('vehicle_password',array('required'=>true));
		echo $this->Form->input('is_recurring',array('required'=>true));
		echo $this->Form->input('total_number_of_spaces',array('required'=>true));
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div>
    <?php echo $this->element('homePage');?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Vehicle Types'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Properties'), array('controller' => 'properties', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Property'), array('controller' => 'properties', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Vehicles'), array('controller' => 'vehicles', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Vehicle'), array('controller' => 'vehicles', 'action' => 'add')); ?> </li>
	</ul>
</div>

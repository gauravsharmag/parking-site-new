<div class="page-content-wrapper">
        <div class="page-content">
<div class="row">
<div class="col-md-12">

<div class="portlet box green ">
<div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-gift"></i> Admin Add Vehicle Type
                            </div>
                            <div class="tools">
                                <a href="" class="collapse">
                                </a>
                            </div>
                        </div>
                        <div class="portlet-body form">
<?php echo $this->Form->create('VehicleType'); //debug($property_id); ?>
	<fieldset>
		<legend></legend>
	<?php
		echo $this->Form->input('vehicle_type_name');
		echo $this->Form->hidden('VehicleType.property_id',array('default'=>$property_id));
		echo $this->Form->input('deposit');
		echo $this->Form->input('pass_cost');
		echo $this->Form->input('pass_cost_after_1_year');
		echo $this->Form->input('recurring_cost_duration').'   '.$this->Form->input('duration_type', array('options' => array('hr' => 'Hour', 'day' => 'Days'),'empty'=>'Select'));
		echo $this->Form->input('recurring_cost');
		echo $this->Form->input('vehicle_password');
		echo $this->Form->input('is_recurring');
		echo $this->Form->input('total_number_of_spaces');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>


		<li><?php echo $this->Html->link(__('List Properties'), array('controller' => 'properties', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Property'), array('controller' => 'properties', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Vehicles'), array('controller' => 'vehicles', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Vehicle'), array('controller' => 'vehicles', 'action' => 'add')); ?> </li>
	</ul>
</div>
</div></div></div></div>
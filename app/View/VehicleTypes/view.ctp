<div class="vehicleTypes view">
<h2><?php echo __('Vehicle Type'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($vehicleType['VehicleType']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Vehicle Type Name'); ?></dt>
		<dd>
			<?php echo h($vehicleType['VehicleType']['vehicle_type_name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Property'); ?></dt>
		<dd>
			<?php echo $this->Html->link($vehicleType['Property']['name'], array('controller' => 'properties', 'action' => 'view', $vehicleType['Property']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Deposit'); ?></dt>
		<dd>
			<?php echo h($vehicleType['VehicleType']['deposit']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Pass Cost'); ?></dt>
		<dd>
			<?php echo h($vehicleType['VehicleType']['pass_cost']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Pass Cost After 1 Year'); ?></dt>
		<dd>
			<?php echo h($vehicleType['VehicleType']['pass_cost_after_1_year']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Recurring Cost Duration'); ?></dt>
		<dd>
			<?php echo h($vehicleType['VehicleType']['recurring_cost_duration']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Recurring Cost'); ?></dt>
		<dd>
			<?php echo h($vehicleType['VehicleType']['recurring_cost']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Vehicle Password'); ?></dt>
		<dd>
			<?php echo h($vehicleType['VehicleType']['vehicle_password']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Is Recurring'); ?></dt>
		<dd>
			<?php echo h($vehicleType['VehicleType']['is_recurring']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Total Number Of Spaces'); ?></dt>
		<dd>
			<?php echo h($vehicleType['VehicleType']['total_number_of_spaces']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Vehicle Type'), array('action' => 'edit', $vehicleType['VehicleType']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Vehicle Type'), array('action' => 'delete', $vehicleType['VehicleType']['id']), array(), __('Are you sure you want to delete # %s?', $vehicleType['VehicleType']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Vehicle Types'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Vehicle Type'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Properties'), array('controller' => 'properties', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Property'), array('controller' => 'properties', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Vehicles'), array('controller' => 'vehicles', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Vehicle'), array('controller' => 'vehicles', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Vehicles'); ?></h3>
	<?php if (!empty($vehicleType['Vehicle'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Property Id'); ?></th>
		<th><?php echo __('User Id'); ?></th>
		<th><?php echo __('Vehicle Type Id'); ?></th>
		<th><?php echo __('Make'); ?></th>
		<th><?php echo __('Model'); ?></th>
		<th><?php echo __('Color'); ?></th>
		<th><?php echo __('License Plate Number'); ?></th>
		<th><?php echo __('License Plate State'); ?></th>
		<th><?php echo __('Reserved Space'); ?></th>
		<th><?php echo __('Last 4 Digital Of Vin'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($vehicleType['Vehicle'] as $vehicle): ?>
		<tr>
			<td><?php echo $vehicle['id']; ?></td>
			<td><?php echo $vehicle['property_id']; ?></td>
			<td><?php echo $vehicle['user_id']; ?></td>
			<td><?php echo $vehicle['vehicle_type_id']; ?></td>
			<td><?php echo $vehicle['make']; ?></td>
			<td><?php echo $vehicle['model']; ?></td>
			<td><?php echo $vehicle['color']; ?></td>
			<td><?php echo $vehicle['license_plate_number']; ?></td>
			<td><?php echo $vehicle['license_plate_state']; ?></td>
			<td><?php echo $vehicle['reserved_space']; ?></td>
			<td><?php echo $vehicle['last_4_digital_of_vin']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'vehicles', 'action' => 'view', $vehicle['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'vehicles', 'action' => 'edit', $vehicle['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'vehicles', 'action' => 'delete', $vehicle['id']), array(), __('Are you sure you want to delete # %s?', $vehicle['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Vehicle'), array('controller' => 'vehicles', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>

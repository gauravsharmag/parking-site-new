<div class="vehicleTypes form">
<?php echo $this->Form->create('VehicleType'); ?>
	<fieldset>
		<legend><?php echo __('Admin Edit Vehicle Type'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('vehicle_type_name');
		echo $this->Form->hidden('property_id',array('default'=>$propertyId));
		echo $this->Form->input('deposit');
		echo $this->Form->input('pass_cost');
		echo $this->Form->input('pass_cost_after_1_year');
		echo $this->Form->input('recurring_cost_duration');
		echo $this->Form->input('recurring_cost');
		echo $this->Form->input('vehicle_password');
		echo $this->Form->input('is_recurring');
		echo $this->Form->input('total_number_of_spaces');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('VehicleType.id')), array(), __('Are you sure you want to delete # %s?', $this->Form->value('VehicleType.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Vehicle Types'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Properties'), array('controller' => 'properties', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Property'), array('controller' => 'properties', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Vehicles'), array('controller' => 'vehicles', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Vehicle'), array('controller' => 'vehicles', 'action' => 'add')); ?> </li>
	</ul>
</div>


	<div class="page-content-wrapper">
		<div class="page-content">
        
        	<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
					Welcome To <?php echo CakeSession::read('PropertyName');?><small>Dashboard</small>
					</h3>
					<ul class="page-breadcrumb breadcrumb">
						<li>
							<i class="fa fa-home"></i>
							<a href="javascript;">Home</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="">Contact Us</a>
						</li>
						<li class="pull-right">
							<div id="dashboard-report-range" class="dashboard-date-range tooltips" data-placement="top" data-original-title="Change dashboard date range">
								<i class="icon-calendar"></i>
								<span></span>
								<i class="fa fa-angle-down"></i>
							</div>
						</li>
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
			<div id="flashMessages">
					<h2><?php  echo $this->Session->flash();?></h2>
				</div>
      	<div class="row">
				<div class="col-md-12">
				<div class="portlet box blue">
									<div class="portlet-title">
										<div class="caption">
											<i class="fa fa-gift"></i>Contact Us Form
										</div>
										<div class="tools">
											<a href="javascript:;" class="collapse">
											</a>
											
											
										</div>
									</div>
									<div class="portlet-body form">
										<!-- BEGIN FORM-->
										<?php echo $this->Form->create('Chat',array('url'=>array('action'=>'add'),'class'=>'form-horizontal'))?>
								
										<div class="form-body">
											<?php
                                            					  echo $this->Form->input('user_id',array('label'=>false,
																														'errorMessage' => false,
                                            												                   'value'=>$user_id, 'type'=>'hidden',
                                            												                  'class'=>'form-control'));
                                                            
                                                             ?>
												<div class="form-group">
													<label class="col-md-3 control-label">Subject<span class="required">
																										* </span></label>
													 <div class="col-md-4">
														 <?php
                                            					  echo $this->Form->input('subject',array('label'=>false,
																														'errorMessage' => false,
                                            												                   'value'=>'',
                                            												                  'class'=>'form-control'));
                                                                if ($this->Form->isFieldError('subject')){echo $this->Form->error('subject',array('div'=>false,'attributes'=>array('wrap' => 'span', 'class' => 'model-error')));

                                                             }?>
													</div>
											
													</div>
												<div class="form-group">
													<label class="col-md-3 control-label">Email<span class="required">
																										* </span></label>
													<div class="col-md-4">
														<div class="input-group">
															<span class="input-group-addon">
															<i class="fa fa-envelope"></i>
															</span>
															<?php
                                            					  echo $this->Form->input('user_email',array('label'=>false,
                                            												          'errorMessage' => false,
                                            												          'value'=>$email,
                                            												          'class'=>'form-control'));
                                                                if ($this->Form->isFieldError('user_email')){echo $this->Form->error('user_email',array('div'=>false,'attributes'=>array('wrap' => 'span', 'class' => 'model-error')));

                                                             }?>
														</div>
													</div>
												</div>
												<div class="form-group">
													<label class="col-md-3 control-label">Name<span class="required">
																										* </span></label>
													 <div class="col-md-4">
														<?php
                                            					  echo $this->Form->input('username',array('label'=>false,
                                            												                  'errorMessage' => false,
                                            												                   'value'=>$name,
                                            												                  'class'=>'form-control'));
                                                                if ($this->Form->isFieldError('username')){echo $this->Form->error('username',array('div'=>false,'attributes'=>array('wrap' => 'span', 'class' => 'model-error')));

                                                             }?>
													</div>
											
													</div>
												<div class="form-group">
													<label class="col-md-3 control-label">Mobile<span class="required">
																										* </span></label>
													 <div class="col-md-4">
														 <?php
                                            					  echo $this->Form->input('phone',array('label'=>false,
                                            												                  'errorMessage' => false,
                                            												                  'value'=>$mobile,
                                            												                  'class'=>'form-control'));
                                                                if ($this->Form->isFieldError('phone')){echo $this->Form->error('phone',array('div'=>false,'attributes'=>array('wrap' => 'span', 'class' => 'model-error')));

                                                             }?>
													</div>
											
												</div>
												<div class="form-group">
													<label class="col-md-3 control-label">Division</label>
													 <div class="col-md-4">
														 <?php
                                            					  echo $this->Form->input('division',array('type'=>'select',
                                            					                                              'options'=>array('Sales'=>'Sales','Binding'=>'Binding','Support'=>'Support'),
                                            					                                              'empty'=>'Select',
                                            					                                              'value'=>'',
                                            					                                              'label'=>false,
                                            												                  'errorMessage' => false,
                                            												                  'class'=>'form-control'));
                                                                if ($this->Form->isFieldError('division')){echo $this->Form->error('division',array('div'=>false,'attributes'=>array('wrap' => 'span', 'class' => 'model-error')));

                                                             }?>
													</div>
											
													</div>
												<div class="form-group">
													<label class="col-md-3 control-label">Priority</label>
													<div class="col-md-4">
														                                                             <?php
                                            					  echo $this->Form->input('priority',array('type'=>'select',
                                            					                                              'options'=>array('Medium'=>'Medium','High'=>'High'),
                                            					                                              'empty'=>array('Low'=>'Low'),
                                            					                                              
                                            					                                              'label'=>false,
                                            												                  'errorMessage' => false,
                                            												                  'class'=>'form-control'));
                                                                if ($this->Form->isFieldError('priority')){echo $this->Form->error('priority',array('div'=>false,'attributes'=>array('wrap' => 'span', 'class' => 'model-error')));

                                                             }?>
													</div>
												</div>
												<div class="form-group">
													<label class="col-md-3 control-label">Product</label>
													<div class="col-md-4">
<?php
                                            					  echo $this->Form->input('product',array('type'=>'select',
                                            					                                              'options'=>$res, 'optgroup'=>array('display'=>'none', 'label'=>false),
                                            					                                             
																											  'empty'=>'Select',
                                            					                                              'value'=>'',
                                            					                                              'label'=>false,
                                            												                  'errorMessage' => false,
                                            												                  'class'=>'form-control'));
                                                                if ($this->Form->isFieldError('product')){echo $this->Form->error('product',array('div'=>false,'attributes'=>array('wrap' => 'span', 'class' => 'model-error')));

                                                             }?>
													</div>
												</div>
												<div class="form-group">
													<label class="col-md-3 control-label">Message<span class="required">
																										* </span> </label>
													<div class="col-md-4">
														<?php
                                            					  echo $this->Form->textarea('query',array('label'=>false,
                                            												          'errorMessage' => false,
                                            												          
                                            												          'class'=>'form-control'));
                                                                if ($this->Form->isFieldError('query')){echo $this->Form->error('query',array('div'=>false,'attributes'=>array('wrap' => 'span', 'class' => 'model-error')));

                                                             }?>
													</div>
												</div>
										
										
										
										
										
										</div>
										<div class="form-actions fluid">
												<div class="col-md-offset-3 col-md-9">
													<button type="submit" class="btn green">Send Message</button>
													</div>
											</div>
										<?php echo $this->Form->end(); ?>
										<!-- END FORM-->
									</div>
								</div>
                </div>
		</div>
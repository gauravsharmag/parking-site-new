<div class="page-content-wrapper">
        <div class="page-content">
<div class="row">
<div class="col-md-12">
<?php echo $this->element('useractions'); ?>
<div class="portlet box green ">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-gift"></i> Edit User Details
                            </div>
                            <div class="tools">
                                <a href="" class="collapse">
                                </a>
                            </div>
                        </div>
                        <div class="portlet-body form">
<?php echo $this->Form->create('Vehicle',array('url'=>array('controller'=>'vehicles','action'=>'add')));  //$userdata = $this->session->read('Auth.User'); 
                                                                                               // debug($userdata);
    //debug($propertyId);
   // debug($this->session->read('Auth.User.id'));

      ?>
	<fieldset>
		<legend><?php echo __('Register New Vehicle'); ?></legend>
	<?php
        echo $this->Form->hidden('Vehicle.user_id',array('default'=>$this->session->read('Auth.User.id')));
        echo $this->Form->hidden('property_id',array('default'=>$propertyId['PropertyUser']['property_id']));
        echo $this->Form->input('vehicle_type_name',array( 'label'=>'Select Vehicle Type',
                                                                                      'type'    => 'select',
                                                                                      'options' => $VehicleTypes,
                                                                                      'empty'=>'Choose One'
                                                                                         ));
		echo $this->Form->input('make',array('required'=>true,'placeholder'=>'Brand name'));
		echo $this->Form->input('model',array('required'=>true,'placeholder'=>'Model Name'));
		echo $this->Form->input('color',array('required'=>true,'placeholder'=>'Color'));
		echo $this->Form->input('license_plate_number',array('required'=>true,'placeholder'=>'Licence Number'));
		echo $this->Form->input('license_plate_state',array('required'=>true,'placeholder'=>'???'));
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
</div></div></div></div></div>
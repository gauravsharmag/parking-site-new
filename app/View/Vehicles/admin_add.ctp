<?php echo $this->Html->script('jquery.min'); ?>
<script>
$(document).ready(function(){
 //$("#AddNew").show();
 $("#AllVehicle").hide();

  $("#ListAll").click(function(){
    $("#AllVehicle").slideToggle();
  });
});
</script>
<div class="page-content-wrapper">
		<div class="page-content" style="min-height:1089px">
			<!-- BEGIN PAGE HEADER-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
					Add Vehicle <small></small>
					</h3>
					<ul class="page-breadcrumb breadcrumb">
						<li class="btn-group">
							<button class="btn btn-primary" id="ListAll">List Vehicles
							</button>
						</li>
						<li>
							<i class="fa fa-home"></i>
							<?php 
										echo $this->Html->link('Home',array(
                                          'controller' => 'Users',
                                          'action' => 'superAdminHome',
                                          'full_base' => true
                                      ));
                                 
                               ?>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="#">Add Vehicle</a>
						</li>
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
			<!-- END PAGE HEADER-->
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">

				<div class="col-md-12">
				     <div id="flashMessages">
						<h2><?php  echo $this->Session->flash();?></h2>
					</div>
					<div class="tabbable tabbable-custom boxless tabbable-reversed">

						<div class="tab-content">
							<div id="tab_0" class="tab-pane active">
								 <div class="form-body">
                                              <div class="note note-success">
                                                						<p>
                                                							 User Details
                                                						</p>
                                             </div>
                                             <div class="row static-info">
												<div class="col-md-5 name">
														Property :
												</div>
												<div class="col-md-7 value">
													<?php echo $propertyName['Property']['name'];?>
												</div>
											 </div>
											  <div class="row static-info">
												<div class="col-md-5 name">
														Customer Name :
												</div>
												<div class="col-md-7 value">
													<?php echo $userDetails['User']['first_name'].' '.$userDetails['User']['last_name'];?>
												</div>
											</div>
											<div class="row static-info">
												<div class="col-md-5 name">
														Username :
												</div>
												<div class="col-md-7 value">
													<?php echo $userDetails['User']['username'];?>
												</div>
											</div>
								</div>
								<div id="AllVehicle" class="portlet box yellow" style="display:none;">
									<div class="portlet-title">
										<div class="caption">
											<i class="fa fa-gift"></i>Registered Vehicles
										</div>
										<div class="tools">
											<a class="collapse" href="javascript:;">
											</a>
										</div>
									</div>
									<div class="portlet-body form">
										
										<table class="table table-bordered table-striped table-condensed flip-content">
											<thead>
												<tr>
													<th>#</th>
													<th>Name For Vehicle</th>
													<th>Make</th>
													<th>Model</th>
													<th>Color</th>
													<th>Number</th>
													<th>State</th>
													<th>VIN</th>
													<th>Select</th>
												</tr>
											</thead>
											<tbody>
												<?php $i=1;foreach($customerOtherVehicles as $vehicle){?>
													<tr>
														<td><?php echo $i++;?></td>
														<td><?php echo $vehicle['Vehicle']['owner'];?></td>
														<td><?php echo $vehicle['Vehicle']['make'];?></td>
														<td><?php echo $vehicle['Vehicle']['model'];?></td>
														<td><?php echo $vehicle['Vehicle']['color'];?></td>
														<td><?php echo $vehicle['Vehicle']['license_plate_number'];?></td>
														<td><?php echo $vehicle['Vehicle']['license_plate_state'];?></td>
														<td><?php echo $vehicle['Vehicle']['last_4_digital_of_vin'];?></td>
														<td><?php echo $this->Html->link('Select',array('action'=>'addSelectedVehicle',
																		$customerPass,$vehicle['Vehicle']['id'],$userDetails['User']['id'],$propertyIdAdmin));?></td>
													</tr>
												<?php }?>
											</tbody>
										</table>
										</form>
									</div>
								</div>

								<div id="AddNew" class="portlet box green">
									<div class="portlet-title">
										<div class="caption">
											<i class="fa fa-gift"></i>Add Vehicle Details
										</div>
										<div class="tools">
											<a class="collapse" href="javascript:;">
											</a>

										</div>
									</div>
									<div class="portlet-body form">

										<!-- BEGIN FORM-->
										<?php echo $this->Form->create('Vehicle',array('class'=>'form-horizontal'));?>
											<div class="form-body">
                                                <?php
													echo $this->Form->input('owner',array('div'=>array('class'=>'form-group'),'error'=>array('attributes'=>array('class'=>'model-error')),'label'=>array('class'=>'col-md-3 control-label','text'=>'Name For Vehicle'),'between'=>'<div class="col-md-4">','after'=>'</div>','class'=>'form-control','placeholder'=>'Vehicle Name'));
												    echo $this->Form->input('make',array('div'=>array('class'=>'form-group'),'error'=>array('attributes'=>array('class'=>'model-error')),'label'=>array('class'=>'col-md-3 control-label','text'=>'Make'),'between'=>'<div class="col-md-4">','after'=>'</div>','class'=>'form-control','placeholder'=>'Make'));

												    echo $this->Form->input('model',array('div'=>array('class'=>'form-group'),'error'=>array('attributes'=>array('class'=>'model-error')),'label'=>array('class'=>'col-md-3 control-label','text'=>'Model'),'between'=>'<div class="col-md-4">','after'=>'</div>','class'=>'form-control','placeholder'=>'Model'));

												    echo $this->Form->input('color',array('div'=>array('class'=>'form-group'),'error'=>array('attributes'=>array('class'=>'model-error')),'label'=>array('class'=>'col-md-3 control-label','text'=>'Color'),'between'=>'<div class="col-md-4">','after'=>'</div>','class'=>'form-control','placeholder'=>'Color'));

												    echo $this->Form->input('license_plate_number',array('div'=>array('class'=>'form-group'),'error'=>array('attributes'=>array('class'=>'model-error')),'label'=>array('class'=>'col-md-3 control-label','text'=>'License Plate Number'),'between'=>'<div class="col-md-4">','after'=>'</div>','class'=>'form-control','placeholder'=>'License Plate Number'));

												   
                                                    echo $this->Form->input('license_plate_state',array('type' => 'select',
                                                                            'div'=>array('class'=>'form-group'),
                                                                            'label'=>array('class'=>'col-md-3 control-label','text'=>'License Plate State'),
																			'error'=>array('attributes'=>array('class'=>'model-error')),
                                                                            'options' => $states,
                                                                            'empty'=>'State',
                                                                            'between'=>'<div class="col-md-4">',
                                                                            'after'=>'</div>',
                                                                            'class'=>'form-control'));

												    echo $this->Form->input('last_4_digital_of_vin',array('div'=>array('class'=>'form-group'),'error'=>array('attributes'=>array('class'=>'model-error')),'label'=>array('class'=>'col-md-3 control-label','text'=>'Last 4 Digital Of Vin'),'between'=>'<div class="col-md-4">','after'=>'</div>','class'=>'form-control','placeholder'=>'Last 4 Digital Of Vin'));
										        ?>
											</div>
											<div class="form-actions fluid">
												<div class="col-md-offset-3 col-md-9">
													<button class="btn blue" type="submit">Submit</button>
												</div>
												 <?php echo $this->Form->end(); ?>
											</div>
										</form>
										<!-- END FORM-->
									</div>
								</div>

							</div>

						</div>
					</div>
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>

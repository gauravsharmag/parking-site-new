<div class="vehicles view">
<h2><?php echo __('Vehicle'); //debug($vehicle); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($vehicle['Vehicle']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Property'); ?></dt>
		<dd>
			<?php echo $this->Html->link($vehicle['Property']['name'], array('controller' => 'properties', 'action' => 'view', $vehicle['Property']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('User'); ?></dt>
		<dd>
			<?php echo $this->Html->link($vehicle['User']['id'], array('controller' => 'users', 'action' => 'view', $vehicle['User']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Vehicle Type'); ?></dt>
		<dd>
			<?php echo h($vehicle['Vehicle']['vehicle_type_name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Make'); ?></dt>
		<dd>
			<?php echo h($vehicle['Vehicle']['make']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Model'); ?></dt>
		<dd>
			<?php echo h($vehicle['Vehicle']['model']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Color'); ?></dt>
		<dd>
			<?php echo h($vehicle['Vehicle']['color']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('License Plate Number'); ?></dt>
		<dd>
			<?php echo h($vehicle['Vehicle']['license_plate_number']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('License Plate State'); ?></dt>
		<dd>
			<?php echo h($vehicle['Vehicle']['license_plate_state']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Reserved Space'); ?></dt>
		<dd>
			<?php echo h($vehicle['Vehicle']['reserved_space']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Last 4 Digital Of Vin'); ?></dt>
		<dd>
			<?php echo h($vehicle['Vehicle']['last_4_digital_of_vin']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div>
    <?php echo $this->element('homePage');?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Vehicle'), array('action' => 'edit', $vehicle['Vehicle']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Vehicle'), array('action' => 'delete', $vehicle['Vehicle']['id']), array(), __('Are you sure you want to delete # %s?', $vehicle['Vehicle']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Vehicles'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Vehicle'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Properties'), array('controller' => 'properties', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Property'), array('controller' => 'properties', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Vehicle Types'), array('controller' => 'vehicle_types', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Vehicle Type'), array('controller' => 'vehicle_types', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Passes'), array('controller' => 'passes', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Pass'), array('controller' => 'passes', 'action' => 'add')); ?> </li>
	</ul>
</div>
	<div class="related">
		<h3><?php echo __('Related Passes'); ?></h3>
	<?php if (!empty($vehicle['Pass'])): ?>
		<dl>
			<dt><?php echo __('Id'); ?></dt>
		<dd>
	<?php echo $vehicle['Pass']['id']; ?>
&nbsp;</dd>
		<dt><?php echo __('User Id'); ?></dt>
		<dd>
	<?php echo $vehicle['Pass']['user_id']; ?>
&nbsp;</dd>
		<dt><?php echo __('Vehicle Id'); ?></dt>
		<dd>
	<?php echo $vehicle['Pass']['vehicle_id']; ?>
&nbsp;</dd>
		<dt><?php echo __('Property Id'); ?></dt>
		<dd>
	<?php echo $vehicle['Pass']['property_id']; ?>
&nbsp;</dd>
		<dt><?php echo __('Transaction Id'); ?></dt>
		<dd>
	<?php echo $vehicle['Pass']['transaction_id']; ?>
&nbsp;</dd>
		<dt><?php echo __('Status'); ?></dt>
		<dd>
	<?php echo $vehicle['Pass']['status']; ?>
&nbsp;</dd>
		<dt><?php echo __('Membership Vaild Upto'); ?></dt>
		<dd>
	<?php echo $vehicle['Pass']['membership_vaild_upto']; ?>
&nbsp;</dd>
		<dt><?php echo __('Pass Valid Upto'); ?></dt>
		<dd>
	<?php echo $vehicle['Pass']['pass_valid_upto']; ?>
&nbsp;</dd>
		<dt><?php echo __('RFID Tag Number'); ?></dt>
		<dd>
	<?php echo $vehicle['Pass']['RFID_tag_number']; ?>
&nbsp;</dd>
		</dl>
	<?php endif; ?>
		<div class="actions">
			<ul>
				<li><?php //echo $this->Html->link(__('Edit Pass'), array('controller' => 'passes', 'action' => 'edit', $vehicle['Pass']['id'])); ?></li>
			</ul>
		</div>
	</div>

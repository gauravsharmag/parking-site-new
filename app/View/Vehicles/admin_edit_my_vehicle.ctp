<div class="vehicles form">
<?php echo $this->Form->create('Vehicle');
   // debug($vehicle);
?>
	<fieldset>
		<legend><?php echo __('Edit Vehicle'); ?></legend>
	<?php
		//echo $this->Form->hidden($vehicle['Vehicle']['id']);
		echo $this->Form->input('id');
		//echo $this->Form->input('property_id');
		//echo $this->Form->input('user_id');
		echo $this->Form->input('vehicle_type_name',array('options'=>array('General'=>'General','VIP'=>'VIP','Commercial'=>'Commercial'),'empty'=>'Select'));
		echo $this->Form->input('make',array('required'=>true));
		echo $this->Form->input('model',array('required'=>true));
		echo $this->Form->input('color',array('required'=>true));
		echo $this->Form->input('license_plate_number',array('required'=>true));
		echo $this->Form->input('license_plate_state',array('required'=>true));
		//echo $this->Form->input('reserved_space');
		//echo $this->Form->input('last_4_digital_of_vin');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div>
    <?php echo $this->element('homePage');?>
</div>
<div class="actions">
	<h3><?php echo __(''); ?></h3>
	<ul>
         <li><?php echo $this->Html->link(__('My Account'), array('controller'=>'users','action' => 'myAccount')); ?></li>
         <li><?php echo $this->Html->link(__('New Vehicle'), array('controller' => 'Vehicles', 'action' => 'registerNewVehicle')); ?> </li>
       <li><?php echo $this->Html->link(__('Guest Vehicle'), array('controller' => 'Vehicles', 'action' => 'registerGuestVehicle')); ?> </li>
	</ul>
</div>
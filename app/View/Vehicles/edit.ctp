
<div class="page-content-wrapper">
		<div class="page-content" style="min-height:1089px">
			<!-- BEGIN SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			
			<!-- /.modal -->
			<!-- END SAMPLE PORTLET CONFIGURATION MODAL FORM-->
			<!-- BEGIN STYLE CUSTOMIZER -->

			<!-- END STYLE CUSTOMIZER -->
			<!-- BEGIN PAGE HEADER-->
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN PAGE TITLE & BREADCRUMB-->
					<h3 class="page-title">
					Add Vehicle <small></small>
					</h3>
					<ul class="page-breadcrumb breadcrumb">
						<li>
							<i class="fa fa-home"></i>
							<a href="index.html">Home</a>
							<i class="fa fa-angle-right"></i>
						</li>
						<li>
							<a href="#">Edit Vehicle</a>
						</li>
					</ul>
					<!-- END PAGE TITLE & BREADCRUMB-->
				</div>
			</div>
			<!-- END PAGE HEADER-->
			<div id="flashMessages">
						<h2><?php  echo $this->Session->flash();?></h2>
			</div>
			<!-- BEGIN PAGE CONTENT-->
			<div class="row">

				<div class="col-md-12">
				     
					<div class="tabbable tabbable-custom boxless tabbable-reversed">

						<div class="tab-content">
							<div id="tab_0" class="tab-pane active">
								<div class="portlet box green">
									<div class="portlet-title">
										<div class="caption">
											<i class="fa fa-gift"></i>Add Vehicle Details
										</div>
										<div class="tools">
											<a class="collapse" href="javascript:;">
											</a>

										</div>
									</div>
									<div class="portlet-body form">

										<!-- BEGIN FORM-->
										<?php echo $this->Form->create('Vehicle',array('class'=>'form-horizontal'));?>
											<div class="form-body">
                                                <?php
													 echo $this->Form->input('owner',array('div'=>array('class'=>'form-group'),'error'=>array('attributes'=>array('class'=>'model-error')),'label'=>array('class'=>'col-md-3 control-label','text'=>'Owner\'s Name'),'between'=>'<div class="col-md-4">','after'=>'</div>','class'=>'form-control','placeholder'=>'Owner\'s Name'));
	
												    echo $this->Form->input('make',array('div'=>array('class'=>'form-group'),'error'=>array('attributes'=>array('class'=>'model-error')),'label'=>array('class'=>'col-md-3 control-label','text'=>'Make'),'between'=>'<div class="col-md-4">','after'=>'</div>','class'=>'form-control','placeholder'=>'Make'));

												    echo $this->Form->input('model',array('div'=>array('class'=>'form-group'),'error'=>array('attributes'=>array('class'=>'model-error')),'label'=>array('class'=>'col-md-3 control-label','text'=>'Model'),'between'=>'<div class="col-md-4">','after'=>'</div>','class'=>'form-control','placeholder'=>'Model'));

												    echo $this->Form->input('color',array('div'=>array('class'=>'form-group'),'error'=>array('attributes'=>array('class'=>'model-error')),'label'=>array('class'=>'col-md-3 control-label','text'=>'Color'),'between'=>'<div class="col-md-4">','after'=>'</div>','class'=>'form-control','placeholder'=>'Color'));

												    echo $this->Form->input('license_plate_number',array('div'=>array('class'=>'form-group'),'error'=>array('attributes'=>array('class'=>'model-error')),'label'=>array('class'=>'col-md-3 control-label','text'=>'License Plate Number'),'between'=>'<div class="col-md-4">','after'=>'</div>','class'=>'form-control','placeholder'=>'License Plate Number'));

												    //echo $this->Form->input('license_plate_state',array('div'=>array('class'=>'form-group'),'label'=>array('class'=>'col-md-3 control-label','text'=>'License Plate State'),'between'=>'<div class="col-md-4">','after'=>'</div>','class'=>'form-control','form-control','placeholder'=>'License Plate State'));
                                                    echo $this->Form->input('license_plate_state',array('type' => 'select',
                                                                            'div'=>array('class'=>'form-group'),
                                                                            'label'=>array('class'=>'col-md-3 control-label','text'=>'License Plate State'),
																			'error'=>array('attributes'=>array('class'=>'model-error')),
                                                                            'options' => $states,
                                                                            'empty'=>'State',
                                                                            'between'=>'<div class="col-md-4">',
                                                                            'after'=>'</div>',
                                                                            'class'=>'form-control'));

												    //echo $this->Form->input('reserved_space',array('div'=>array('class'=>'form-group'),'label'=>array('class'=>'col-md-3 control-label','text'=>'Reserved Space'),'between'=>'<div class="col-md-4">','after'=>'</div>','class'=>'form-control','placeholder'=>'Reserved Space'));

												    echo $this->Form->input('last_4_digital_of_vin',array('div'=>array('class'=>'form-group'),'error'=>array('attributes'=>array('class'=>'model-error')),'label'=>array('class'=>'col-md-3 control-label','text'=>'Last 4 Digital Of Vin'),'between'=>'<div class="col-md-4">','after'=>'</div>','class'=>'form-control','placeholder'=>'Last 4 Digital Of Vin'));
										        ?>
											</div>
											<div class="form-actions fluid">
												<div class="col-md-offset-3 col-md-9">
													<button class="btn blue" type="submit">Submit</button>
												</div>
												 <?php echo $this->Form->end(); ?>
											</div>
										</form>
										<!-- END FORM-->
									</div>
								</div>

							</div>

						</div>
					</div>
				</div>
			</div>
			<!-- END PAGE CONTENT-->
		</div>
	</div>

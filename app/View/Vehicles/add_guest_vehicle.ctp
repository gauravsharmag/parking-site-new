<?php echo $this->Html->script('jquery.min'); ?>
<?php echo $this->Html->script('block.js'); ?>
<script>
$(document).ready(function(){
	
	$("#VehicleVehicleId").change(function() {
		if($("#VehicleVehicleId option:selected").val()!=""){
			$("#VehicleOwner").attr("required",false);
			$("#VehicleMake").attr("required",false);
			$("#VehicleModel").attr("required",false);
			$("#VehicleColor").attr("required",false);
			$("#VehicleLicensePlateNumber").attr("required",false);
			$("#VehicleLicensePlateState").attr("required",false);
			$("#VehicleLast4DigitalOfVin").attr("required",false);
		}else{
			console.log($("#VehicleVehicleId option:selected").val());
			$("#VehicleOwner").attr("required",true);
			$("#VehicleMake").attr("required",true);
			$("#VehicleModel").attr("required",true);
			$("#VehicleColor").attr("required",true);
			$("#VehicleLicensePlateNumber").attr("required",true);
			$("#VehicleLicensePlateState").attr("required",true);
			$("#VehicleLast4DigitalOfVin").attr("required",true);
		}
			 
	}); 
	if($("#VehicleVehicleId option:selected").val()!=""){
		$("#VehicleOwner").attr("required",false);
		$("#VehicleMake").attr("required",false);
		$("#VehicleModel").attr("required",false);
		$("#VehicleColor").attr("required",false);
		$("#VehicleLicensePlateNumber").attr("required",false);
		$("#VehicleLicensePlateState").attr("required",false);
		$("#VehicleLast4DigitalOfVin").attr("required",false);
	}else{
		console.log($("#VehicleVehicleId option:selected").val());
		$("#VehicleOwner").attr("required",true);
		$("#VehicleMake").attr("required",true);
		$("#VehicleModel").attr("required",true);
		$("#VehicleColor").attr("required",true);
		$("#VehicleLicensePlateNumber").attr("required",true);
		$("#VehicleLicensePlateState").attr("required",true);
		$("#VehicleLast4DigitalOfVin").attr("required",true);
	}
	 
});
function parseDate(str) {
			var d = str.split('/');
			return new Date(parseInt(d[2]),parseInt(d[0]), parseInt(d[1]));
		}
</script>
<div class="page-content-wrapper">
	<div class="page-content" style="min-height:996px">	
		<!-- BEGIN PAGE HEADER-->
		<div class="row">
			<div class="col-md-12">
				<!-- BEGIN PAGE TITLE & BREADCRUMB-->
				<h3 class="page-title">
				Guest Vehicle <small>add guest vehicle</small>
				</h3>
				<ul class="page-breadcrumb breadcrumb">

					<li>
						<i class="fa fa-home"></i> 
						<?php echo $this->Html->link(
													 'Home',
													  array(
															'controller' => 'Users',
															'action' => 'myAccount',
															'full_base' => true
																													  )
													  );?>
						<i class="fa fa-angle-right"></i>
					</li>
					<li>
						<a href="#">Guest Vehicle</a>
					</li>
				</ul>
				<!-- END PAGE TITLE & BREADCRUMB-->
			</div>
		</div>
		<!-- END PAGE HEADER-->
		<div id="flashMessages">
								<h2><?php  echo $this->Session->flash();?></h2>
		</div>
		<!-- BEGIN PAGE CONTENT-->
		<div class="row">
			<div class="col-md-12">
				<!-- BEGIN PORTLET-->
				<div class="portlet box blue-hoki">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-gift"></i>Add Vehicle To Guest Pass
					    </div>
					</div>
					<div class="portlet-body">
						<div class="note note-danger">
							<h4 class="block">IMPORTANT !</h4>
							<p> 
								Please note that it is important to add the vehicle to the pass. Please select the vehicle from the dropdown or add new vehicle details.
								Without vehicle the pass will not be considered valid. Thanks.  
							</p>
						</div>
						<div class="alert alert-info">
                                        <strong>Select Existing Vehicle  </strong>
                         </div>
                        <?php echo $this->Form->create('Vehicle',array('class'=>'form-horizontal'));?>
							<div class="form-body">
								 <?php
									 echo $this->Form->input('vehicle_id',array('type' => 'select',
															'div'=>array('class'=>'form-group'),
															'label'=>array('class'=>'col-md-3 control-label','text'=>'Select Existing Vehicle'),
															'error'=>array('attributes'=>array('class'=>'model-error')),
															'options' => $customerOtherVehicles,
															'empty'=>'New Vehicle',
															'between'=>'<div class="col-md-4">',
															'after'=>'</div>',
															'onChange'=>'getVehicleDetails()',
															'class'=>'form-control'));
                                 ?>
                                <div id="ExistingVehicleDiv" >
								
								</div>	
								<div id="addNewVehicle" >					
									 <div class="note note-success">
										<p>
											 OR Add New Guest Vehicle
										</p>
								   </div>
								   <?php
										echo $this->Form->input('owner',array('div'=>array('class'=>'form-group'),'error'=>array('attributes'=>array('class'=>'model-error')),'label'=>array('class'=>'col-md-3 control-label','text'=>'Vehicle Name'),'between'=>'<div class="col-md-4">','after'=>'</div>','class'=>'form-control','placeholder'=>'Vehicle Name'));
										
										echo $this->Form->input('make',array('div'=>array('class'=>'form-group'),'error'=>array('attributes'=>array('class'=>'model-error')),'label'=>array('class'=>'col-md-3 control-label','text'=>'Make'),'between'=>'<div class="col-md-4">','after'=>'</div>','class'=>'form-control','placeholder'=>'Make'));

										echo $this->Form->input('model',array('div'=>array('class'=>'form-group'),'error'=>array('attributes'=>array('class'=>'model-error')),'label'=>array('class'=>'col-md-3 control-label','text'=>'Model'),'between'=>'<div class="col-md-4">','after'=>'</div>','class'=>'form-control','placeholder'=>'Model'));

										echo $this->Form->input('color',array('div'=>array('class'=>'form-group'),'error'=>array('attributes'=>array('class'=>'model-error')),'label'=>array('class'=>'col-md-3 control-label','text'=>'Color'),'between'=>'<div class="col-md-4">','after'=>'</div>','class'=>'form-control','placeholder'=>'Color'));

										echo $this->Form->input('license_plate_number',array('div'=>array('class'=>'form-group'),'error'=>array('attributes'=>array('class'=>'model-error')),'label'=>array('class'=>'col-md-3 control-label','text'=>'License Plate Number'),'between'=>'<div class="col-md-4">','after'=>'</div>','class'=>'form-control','placeholder'=>'License Plate Number'));

										echo $this->Form->input('license_plate_state',array('type' => 'select',
																'div'=>array('class'=>'form-group'),
																'label'=>array('class'=>'col-md-3 control-label','text'=>'License Plate State'),
																'error'=>array('attributes'=>array('class'=>'model-error')),
																'options' => $states,
																'empty'=>'State',
																'between'=>'<div class="col-md-4">',
																'after'=>'</div>',
																'class'=>'form-control'));

										echo $this->Form->input('last_4_digital_of_vin',array('div'=>array('class'=>'form-group'),'error'=>array('attributes'=>array('class'=>'model-error')),'label'=>array('class'=>'col-md-3 control-label','text'=>'Last 4 Digital Of Vin'),'between'=>'<div class="col-md-4">','after'=>'</div>','class'=>'form-control','placeholder'=>'Last 4 Digital Of Vin'));
									?>
								   
								</div>
							</div>
							<div class="form-actions">
								<div class="row">
									<div class="col-md-offset-3 col-md-9">
										<button type="submit" class="btn green">Submit</button>
									</div>
								</div>
							</div>
                       
                        <?php echo $this->Form->end(); ?>
					</div>
				</div>
				<!-- END PORTLET-->
			</div>
		</div>
		<!-- END PAGE CONTENT-->
	</div>
</div>
<script>
	function getVehicleDetails(){
		if($('#VehicleVehicleId').val()){
			$('#addNewVehicle').hide('slow');
			$.blockUI({ css: { 
						border: 'none', 
						padding: '15px', 
						backgroundColor: '#000', 
						'-webkit-border-radius': '10px', 
						'-moz-border-radius': '10px', 
						opacity: .5, 
						color: '#fff' 
			} }); 
			
			$.ajax({
					url: "/Vehicles/my_vehicle_details/"+$('#VehicleVehicleId').val(), 
					success: function(result){
						$("#ExistingVehicleDiv").html(result);
						$('#ExistingVehicleDiv').show('slow');
						$.unblockUI();
					}
				});
		}else{
			$('#addNewVehicle').show('slow');
			$('#ExistingVehicleDiv').hide('slow');
		}
		
	}
</script>

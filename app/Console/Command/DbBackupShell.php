<?php
class DbBackupShell extends AppShell {
	public function main() {
		$databaseConfig = $rowsPerQuery = $backupDirectory = null;
		   $args = array();
        //database configuration, default is "default"
        if (is_null($databaseConfig)) {
            $args[0] = 'default';
        } else {
            $args[0] = $databaseConfig;
        }

        //rows per query (less rows = less ram usage but more running time), default is 0 which means all rows
        if (is_null($rowsPerQuery)) {
            $args[1] = 0;
        } else {
            $args[1] = $rowsPerQuery;
        }

        //directory to save your backup, it will be created automatically if not found., default is webroot/db-backups/yyyy-mm-dd
        if (is_null($backupDirectory)) {
            $args[2] = 'db-backups/' . date('Y-m-d', time());
        } else {
            $args[2] = $backupDirectory;
        }

        App::import('Core', 'ConnectionManager');
        $db = ConnectionManager::getDataSource($args[0]);
        $backupdir = $args[2];
        $seleced_tables = '*';
        //$tables = array('orders', 'users', 'profiles');

        if ($seleced_tables == '*') {
            $sources = $db->query("show full tables where Table_Type = 'BASE TABLE'", false);
            foreach ($sources as $table) {
                $table = array_shift($table);
                $tables[] = array_shift($table);
            }
        } else {
            $tables = is_array($tables) ? $tables : explode(',', $tables);
        }

        $filename = 'db-backup-' . date('Y-m-d-H-i-s', time()) . '_' . (md5(implode(',', $tables))) . '.sql';

        $return = '';
        $limit = $args[1];
        $start = 0;

        if (!is_dir($backupdir)) {
            CakeLog::write('BackUpDirectory', 'Will create "' . $backupdir . '" directory!');
            if (mkdir($backupdir, 0755, true)) {
                CakeLog::write('BackUpDirectorySuccess', 'Directory created!');
            } else {
                CakeLog::write('BackUpDirectoryError', 'Failed to create destination directory! Can not proceed with the backup!');
                die;
            }
        }

        if ($this->__isDbConnected($args[0])) {
            CakeLog::write('BackUpStart', 'Starting Backup..');

            foreach ($tables as $table) {

                // $this->out(" ",2);
                //$this->out($table);

                $handle = fopen($backupdir . '/' . $filename, 'a+');
                $return = 'DROP TABLE IF EXISTS `' . $table . '`;';

                $row2 = $db->query('SHOW CREATE TABLE ' . $table . ';');
                //$this->out($row2);
                $return.= "\n\n" . $row2[0][0]['Create Table'] . ";\n\n";
                fwrite($handle, $return); 

                for (;;) {
                    if ($limit == 0) {
                        $limitation = '';
                    } else {
                        $limitation = ' Limit ' . $start . ', ' . $limit;
                    }

                    $result = $db->query('SELECT * FROM ' . $table . $limitation . ';', false);
                    $num_fields = count($result);
                    //$this->ProgressBar->start($num_fields);

                    if ($num_fields == 0) {
                        $start = 0;
                        break;
                    }

                    foreach ($result as $row) {
                        //$this->ProgressBar->next();
                        $return2 = 'INSERT INTO ' . $table . ' VALUES(';
                        $j = 0;
                        foreach ($row[$table] as $key => $inner) {
                            $j++;
                            if (isset($inner)) {
                                if ($inner == NULL) {
                                    $return2 .= 'NULL';
                                } else {
                                    $inner = addslashes($inner);
                                    $inner = ereg_replace("\n", "\\n", $inner);
                                    $return2.= '"' . $inner . '"';
                                }
                            } else {
                                $return2.= '""';
                            }

                            if ($j < (count($row[$table]))) {
                                $return2.= ',';
                            }
                        }
                        $return2.= ");\n";
                        fwrite($handle, $return2);
                    }
                    $start+=$limit;
                    if ($limit == 0) {
                        break;
                    }
                }

                $return.="\n\n\n";
                fclose($handle);
            }

            CakeLog::write('BackUpComplete', 'Backup Completed');
        } else {
            CakeLog::write('BackUpError', 'Error! Can\'t connect to "' . $args[0] . '" database!');
        }
	}
	function __isDbConnected($db = NULL) {
        $datasource = ConnectionManager::getDataSource($db);
        //debug($datasource);die;
        return $datasource->isConnected();
    }
}

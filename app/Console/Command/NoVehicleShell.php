<?php

App::import('Core', 'Helper');
App::uses('ComponentCollection', 'Controller');
App::import('Component', 'Email');

class NoVehicleShell extends AppShell {

    public function main() { 
        $domain = Configure::read('SITE_URL');
        $site=Configure::read('SITE');
        $collection = new ComponentCollection();
        $email = new EmailComponent($collection);
        $this->loadModel('CustomerPass');
        $all_cp = $this->CustomerPass->find('all', array('fields' => 'CustomerPass.*,Pass.name as Pass_name,Property.name as Property_name,Property.sub_domain, Property.logo,User.first_name, User.last_name, User.email',
														   'conditions'=>array( 'CustomerPass.vehicle_id IS NULL AND DATE(CustomerPass.pass_valid_upto)>= "'.date('Y-m-d').'" AND DATE(CustomerPass.membership_vaild_upto)>= "'.date('Y-m-d').'"','CustomerPass.pass_archived'=>0)
														));
        
		//debug($all_cp);die;\
		foreach ($all_cp as $cp) {
			 $Email = new CakeEmail();
			 $Email->config('smtp');
			 $Email->template('no_vehicle', 'default'); 
			 $Email->emailFormat('html');
			 $setArr=array(
						   'domain'=>$domain,
						   'name'=>$cp['User']['first_name'].' '.$cp['User']['last_name'],
						   'site'=>$site,
						   'pass'=>$cp['Pass']['Pass_name'],
						   'property'=>$cp['Property']['Property_name'],
						   'subdomain'=>$cp['Property']['sub_domain']
			 );
			 $Email->viewVars($setArr);
			 $Email->from(array('noreply@' . $domain => ucfirst($site)));
			 $Email->to($cp['User']['email']);
			 $Email->subject('VEHICLE NOT ASSIGNED');
			 if ($Email->send()) {
				  CakeLog::write('NoVehicleAlertSuccess', 'mail sent to: ' . $cp['User']['email']);
			 }else{
				  CakeLog::write('NoVehicleAlertError', 'mail cannot be sent to: ' . $cp['User']['email']);
			 }
			
	   }
   }
}   

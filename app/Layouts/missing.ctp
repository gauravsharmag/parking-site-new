<!DOCTYPE html>
<!-- 
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.2.0
Version: 3.1.1
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" >
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
<meta charset="utf-8"/>
<title>Online Parking Pass | - 404 Page Not Found</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta content="" name="description"/>
<meta content="" name="author"/>
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" type="text/css"/>
<?php 
echo $this->Html->css('/assets/global/plugins/font-awesome/css/font-awesome.min.css');
echo $this->Html->css('/assets/global/plugins/simple-line-icons/simple-line-icons.min.css');
echo $this->Html->css('/assets/global/plugins/bootstrap/css/bootstrap.min.css');
echo $this->Html->css('/assets/global/plugins/uniform/css/uniform.default.css');
echo $this->Html->css('/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css');
echo $this->Html->css('/assets/admin/pages/css/error.css');
echo $this->Html->css('/assets/global/css/components.css');
echo $this->Html->css('/assets/global/css/plugins.css');
echo $this->Html->css('/assets/admin/layout/css/layout.css');
echo $this->Html->css('/assets/admin/layout/css/themes/default.css');
echo $this->Html->css('/assets/admin/layout/css/custom.css'); 
?>
<!-- END THEME STYLES -->
</head>
<body class="page-404-3">
<div class="page-inner">
<?php echo $this->Html->link($this->Html->image('/assets/admin/pages/media/pages/earth.jpg','','',array('escape'=>false)); ?>
</div>
<div class="container error-404">
<?php echo $this->fetch('content'); ?>
	<h1>404</h1>
	<h2>Houston, we have a problem.</h2>
	<p>
		 Actually, the page you are looking for does not exist.
	</p>
	<p>
		<a href="index.html">
		Return home </a>
		<br>
	</p>
</div>
<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
echo $this->Html->script('/assets/global/plugins/respond.min.js"></script>
echo $this->Html->script('/assets/global/plugins/excanvas.min.js"></script> 
<![endif]-->
<?php 
echo $this->Html->script('/assets/global/plugins/jquery-1.11.0.min.js');
echo $this->Html->script('/assets/global/plugins/jquery-migrate-1.2.1.min.js');
echo $this->Html->script('/assets/global/plugins/jquery-ui/jquery-ui-1.10.3.custom.min.js');
echo $this->Html->script('/assets/global/plugins/bootstrap/js/bootstrap.min.js');
echo $this->Html->script('/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js');
echo $this->Html->script('/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js');
echo $this->Html->script('/assets/global/plugins/jquery.blockui.min.js');
echo $this->Html->script('/assets/global/plugins/jquery.cokie.min.js');
echo $this->Html->script('/assets/global/plugins/uniform/jquery.uniform.min.js');
echo $this->Html->script('/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js');
echo $this->Html->script('/assets/global/scripts/metronic.js');
echo $this->Html->script('/assets/admin/layout/scripts/layout.js');
echo $this->Html->script('/assets/admin/layout/scripts/quick-sidebar.js');
	?>
<script>
jQuery(document).ready(function() {    
   Metronic.init(); // init metronic core components
Layout.init(); // init current layout
QuickSidebar.init() // init quick sidebar
});
</script>
<!-- END JAVASCRIPTS -->
<?php echo $this->element('sql_dump'); ?>
</body>
<!-- END BODY -->
</html>

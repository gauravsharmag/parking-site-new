class AppError extends ErrorHandler {
 
 function _outputMessage($template) {
  $this->controller->layout = 'missing'; // /app/views/layouts/error_template.ctp
  parent::_outputMessage($template);
 }

}
